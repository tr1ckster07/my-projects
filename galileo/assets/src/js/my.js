
$(function () {

    $('.banner-slider').slick({
        dots: true,
        infinite: true,
        speed: 500,
        autoplay: true,
        autoplayspeed: 4000,
        cssEase: 'linear'
    });



    $('.news_block--slider').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                }
            },
            {
                breakpoint: 560,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $('.testimonials_block--slider').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                }
            },
            {
                breakpoint: 560,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    var header = $(".notFixed");
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        setTimeout(function () {
            if (scroll >= 84) {
                header.removeClass('notFixed').addClass("fixed");
            } else {
                header.removeClass("fixed").addClass('notFixed');
            }
        }, 250);
    });
    $(document).ready(function () {
        var scroll = $(window).scrollTop();
        setTimeout(function () {
            if (scroll >= 84) {
                header.removeClass('notFixed').addClass("fixed");
            } else {
                header.removeClass("fixed").addClass('notFixed');
            }
        }, 250);


    });

    $('nav#menu').mmenu({
        extensions: ['fx-menu-slide', 'shadow-page', 'shadow-panels', 'listview-large', 'pagedim-white'],

        navbar: {
            title: '<a href="#menu" class="d-inline-block  d-xl-none toggle"><img src="img/cancel.svg" width="24"></a>'
        },
        navbars: [
            {
                position: 'bottom',
                content: [
                    '<a href="#" class="main-btn">' + 'Расписание' + '</a>' +
                    ' <address class="mmenu_contacts">' +
                    '<p><img src="../img/phone-orange.svg" alt=""><span> 8 (3822) 30-30-85</span></p>' +
                    '<p><img src="../img/email_orange.svg" alt=""><span>galileo.tomsk@gmail.com</span></p></address>'
                ]
            }
        ]
    }, {
        searchfield: {
            clear: true
        }
    });



    if ($(window).width() < 992) {
        $(document).ready(function () {
            var name = $('.nav-link.active').text();
            $('.toggle_nav-text').text(name)
        })
        $('.nav-tabs').on('click', '.nav-link', function () {
            var name = $(this).text();
            $(this).parent().parent().find('.nav-tabs').slideToggle().addClass('active')
            $('.toggle_nav-text').text(name)
        })

        $('.toggle_nav').on('click', function () {
            $(this).parent().find('.nav-tabs').slideToggle().addClass('active')
        })
    }
    else {

    }



    $('.header').scrollToFixed();


    $(function () {
        'use strict';
        $('#modal-sign form').on('submit', function (e) {
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: '../form.php',
                type: 'POST',
                contentType: false,
                processData: false,
                data: fd,
                success: function (data) {
                    $.fancybox.close($('.form-main--modal'), {});
                    $.fancybox.open($('#modal-sign_thanks'), {
                    });
                },
            });
        });
    });


    $("[name='phone']").mask("+7 (999) 999-99-99");


    var API = $("nav#menu").data("mmenu");

    $(".toggle").click(function (ev) {
        ev.preventDefault();
        if ($("html").hasClass("mm-opened")) {
            API.close();
        } else {
            API.open();
        }
    });
})