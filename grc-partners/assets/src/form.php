<?php
		
	$to_send = 'pro@grc-sochi.ru'; // email
	$siteTitle = "Анкета клиента - Городской Риэлторский Центр (агентство недвижимости)";
	$header_ = 'MIME-Version: 1.0' . "rn" . 'Content-type: text/html; charset=UTF-8' . "rn";
	
	if ($_POST){
		
		$type = implode(', ', array_map(function($value) {
			return htmlspecialchars(trim($value));
		  }, $_POST['type']));
		$needs = implode(', ', array_map(function($value) {
			return htmlspecialchars(trim($value));
		  }, $_POST['needs']));
		$criteria = implode(', ', array_map(function($value) {
			return htmlspecialchars(trim($value));
		  }, $_POST['criteria']));
		  $priceFrom = htmlspecialchars(trim($_POST['priceFrom']));
		  $priceTo = htmlspecialchars(trim($_POST['priceTo']));
		  $areaFrom = htmlspecialchars(trim($_POST['areaFrom']));
		  $areaTo = htmlspecialchars(trim($_POST['areaTo']));
		$buy = implode(', ', array_map(function($value) {
			return htmlspecialchars(trim($value));
		  }, $_POST['buy']));
		  $application_companyContact = htmlspecialchars(trim($_POST['application_companyContact']));
		  $application_companyPhone = htmlspecialchars(trim($_POST['application_companyPhone']));
		  $application_partnerContact = htmlspecialchars(trim($_POST['application_companyContact2']));
		  $application_partnerPhone = htmlspecialchars(trim($_POST['application_companyPhone2']));
		  $application_partnerName = htmlspecialchars(trim($_POST['application_companyName2']));
		  $application_partnerEmail = htmlspecialchars(trim($_POST['application_companyEmail']));
		$message = 
		"\nТип недвижимости: ".$type. 
	    "\nЦель приобретения: ".$needs.
		"\nВажные критерии: ".$criteria. 
		"\nЦена: от ".$priceFrom." руб."." до ".$priceTo." руб.".
		"\nПлощадь, м2: от ".$areaFrom." до ".$areaTo.
		"\nСпособы покупки: ".$buy. 
		"\n".
		"\n".
		"\nКонтактные данные клиента: ".
		"\nФИО: ".$application_companyContact. 
		"\nНомер телефона: ".$application_companyPhone. 
		"\n".
		"\n".
		"\nКонтактные данные партнера: ".
		"\nФИО: ".$application_partnerContact. 
		"\nНомер телефона партнера: ".$application_partnerPhone. 
		"\nНазвание компании партнера: ".$application_partnerName. 
		"\nE-mail партнера: ".$application_partnerEmail. 
		"\n".
	    "\nДата: ".date("H:i:s d-m-Y");


		mail($to_send, '=?UTF-8?B?'.base64_encode($siteTitle).'?=', $message, $header_ . $header);
	}
