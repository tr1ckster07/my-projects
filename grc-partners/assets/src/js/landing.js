webpackJsonp([6], {
    0: function (t, e, n) {
        t.exports = n(302);
    },
    302: function (t, e, n) {
        "use strict";
        n(2), n(303), n(304), n(305), n(307), n(308), n(309);
    },
    303: function (t, e, n) {
        "use strict";
        function i(t) {
            return t && t.__esModule ? t : { default: t };
        }
        function a(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        var o = (function () {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    (i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
                }
            }
            return function (e, n, i) {
                return n && t(e.prototype, n), i && t(e, i), e;
            };
        })(),
            s = n(3),
            r = i(s),
            u = n(65),
            l = i(u),
            c = (function () {
                function t(e, n) {
                    a(this, t);
                    var i = ((this.options = r["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, r["default"])(e))),
                        o = i.find(".js-notification-toggle"),
                        s = i.find(".js-notification-close");
                    o.on("tap", this.toggle.bind(this)), s.on("tap", this.close.bind(this));
                }
                return (
                    o(t, null, [
                        {
                            key: "Defaults",
                            get: function () {
                                return {};
                            },
                        },
                    ]),
                    o(t, [
                        {
                            key: "toggle",
                            value: function (t) {
                                var e = this.$container;
                                e.toggleClass("notification--collapsed").toggleClass("notification--expanded"), t.preventDefault();
                            },
                        },
                        {
                            key: "close",
                            value: function (t) {
                                var e = (0, r["default"])("html"),
                                    n = this.$container;
                                e.removeClass("with-notification"), n.trigger("resize").remove(), t.preventDefault();
                            },
                        },
                    ]),
                    t
                );
            })();
        r["default"].fn.notification = (0, l["default"])(c);
    },
    304: function (t, e, n) {
        "use strict";
        function i(t) {
            return t && t.__esModule ? t : { default: t };
        }
        function a(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        Object.defineProperty(e, "__esModule", { value: !0 });
        var o = (function () {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    (i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
                }
            }
            return function (e, n, i) {
                return n && t(e.prototype, n), i && t(e, i), e;
            };
        })(),
            s = n(3),
            r = i(s),
            u = n(65),
            l = i(u),
            c = 1,
            f = -1,
            d = (function () {
                function t(e, n) {
                    a(this, t);
                    var i = ((this.options = r["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, r["default"])(e))),
                        o = (this.$items = i.find(".js-process-item"));
                    this.$backgrounds = i.find(".js-process-background");
                    (this.active = null), (this.animations = []), (this.animationQueue = []), i.appear({ callback: this.animateIn.bind(this, 0), margin: "-50vh", effects: "" }), o.on("tap", this.handleItemClick.bind(this));
                }
                return (
                    o(t, null, [
                        {
                            key: "Defaults",
                            get: function () {
                                return { index: 0 };
                            },
                        },
                    ]),
                    o(t, [
                        {
                            key: "handleItemClick",
                            value: function (t) {
                                var e = this.$items,
                                    n = e.index((0, r["default"])(t.target).closest(e));
                                this.animateIn(n);
                            },
                        },
                        {
                            key: "animateIn",
                            value: function (t) {
                                if ("undefined" == typeof bodymovin) return setTimeout(this.animateIn.bind(this, t), 60);
                                var e = this.animations[t];
                                if (this.active !== t) {
                                    var n = this.$items,
                                        i = n.eq(t);
                                    var b = this.$backgrounds;
                                    var a = this.$backgrounds.eq(t);
                                    if ((n.removeClass("is-active"), i.addClass("is-active"), a.appear({ effects: "fade" }), b.addClass("invisible"), a.removeClass("is-hidden"), i.find("p").appear({ effects: "text" }), i.find("h2").appear({ effects: "text" }), e || (e = this.createAnimation(t)), null !== this.active)) return this.animateOut(t);


                                    // (this.active = t), i.find("p").appear({ effects: "text" }), a.addClass("is-hidden"), e.setDirection(c), e.play();

                                }
                            },
                        },
                        {
                            key: "animateOut",
                            value: function (t) {
                                var e = this,
                                    n = this.$backgrounds.eq(this.active),
                                    i = (this.$items.eq(this.active), this.animations[this.active]);
                                this.animationQueue.push(function () {
                                    n.addClass("is-hidden"), e.animateIn(t);
                                }),
                                    (this.active = null),
                                    i.setDirection(f),
                                    i.play();
                            },
                        },
                        {
                            key: "createAnimation",
                            value: function (t) {
                                var e = (this.$container, this.$backgrounds.eq(t)),
                                    n = e.data(),
                                    i = (0, r["default"])('<span style="display: inline-block; position: absolute; left: 0; top: 0; width: 100%; height: 100%"></span>').appendTo(e);
                                (n.bmWidth || n.bmHeight) && e.css({ width: n.bmWidth || "100%", height: n.bmHeight || "100%" });
                                var a = bodymovin.loadAnimation({ wrapper: i.get(0), path: n.bmPath || null, loop: n.bmLoop || !1, animType: n.bmRenderer || "svg", autoplay: !1, assetsPath: n.bmAssetsPath || null });
                                return a.addEventListener("complete", this.animationQueueNext.bind(this)), (this.animations[t] = a), a;
                            },
                        },
                        {
                            key: "animationQueueNext",
                            value: function () {
                                var t = this.animationQueue.shift();
                                t && t();
                            },
                        },
                    ]),
                    t
                );
            })();
        (e["default"] = d), (r["default"].fn.process = (0, l["default"])(d));
    },
    305: function (t, e, n) {
        "use strict";
        function i(t) {
            return t && t.__esModule ? t : { default: t };
        }
        function a(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function o(t, e) {
            if (!t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !e || ("object" != typeof e && "function" != typeof e) ? t : e;
        }
        function s(t, e) {
            if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + typeof e);
            (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, enumerable: !1, writable: !0, configurable: !0 } })), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : (t.__proto__ = e));
        }
        Object.defineProperty(e, "__esModule", { value: !0 });
        var r = (function () {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    (i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
                }
            }
            return function (e, n, i) {
                return n && t(e.prototype, n), i && t(e, i), e;
            };
        })(),
            u = n(3),
            l = i(u),
            c = n(65),
            f = i(c);
        n(306);
        var d = n(304),
            h = i(d);
        l["default"].fn.andSelf = l["default"].fn.addBack;
        var p = (function (t) {
            function e(t, n) {
                a(this, e);
                var i = o(this, (e.__proto__ || Object.getPrototypeOf(e)).call(this, t, n)),
                    s = ((i.options = l["default"].extend({}, i.constructor.Defaults, n)), (i.$container = (0, l["default"])(t)));
                i.$slideshow = s.find(".js-slideshow");
                return (i.$items = (0, l["default"])()), i.createCarousel(), s.appear({ callback: i.animateIn.bind(i, 0), margin: "-50vh", effects: "" }), i;
            }
            return (
                s(e, t),
                r(e, null, [
                    {
                        key: "Defaults",
                        get: function () {
                            return {};
                        },
                    },
                ]),
                r(e, [
                    {
                        key: "createCarousel",
                        value: function () {
                            this.$slideshow.owlCarousel({ loop: !1, margin: 0, nav: !1, dots: !0, lazyLoad: !0, responsive: { 0: { items: 1 } } }), this.$slideshow.on("changed.owl.carousel", this.animateItemIn.bind(this));
                        },
                    },
                    {
                        key: "getCarousel",
                        value: function () {
                            return this.$slideshow.data("owl.carousel");
                        },
                    },
                    {
                        key: "animateItemIn",
                        value: function (t) {
                            var e = this.getCarousel().current();
                            this.animateIn(e);
                        },
                    },
                ]),
                e
            );
        })(h["default"]);
        (e["default"] = p), (l["default"].fn.processSlideshow = (0, f["default"])(p));
    },
    307: function (t, e, n) {
        "use strict";
        function i(t) {
            return t && t.__esModule ? t : { default: t };
        }
        function a(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        var o = (function () {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    (i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
                }
            }
            return function (e, n, i) {
                return n && t(e.prototype, n), i && t(e, i), e;
            };
        })(),
            s = n(3),
            r = i(s),
            u = n(65),
            l = i(u),
            c = (function () {
                function t(e, n) {
                    a(this, t);
                    var i = ((this.options = r["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, r["default"])(e)));
                    i.on("tap", this.handleClick.bind(this));
                }
                return (
                    o(t, null, [
                        {
                            key: "Defaults",
                            get: function () {
                                return { index: 0 };
                            },
                        },
                    ]),
                    o(t, [
                        {
                            key: "handleClick",
                            value: function (t) {
                                var e = (0, r["default"])(".js-landing-questionnaire-tabs"),
                                    n = this.options.index;
                                e.accordion("expand", n, !0), t.preventDefault();
                            },
                        },
                    ]),
                    t
                );
            })();
        r["default"].fn.introQuestionnaire = (0, l["default"])(c);
    },
    308: function (t, e, n) {
        "use strict";
        function i(t) {
            return t && t.__esModule ? t : { default: t };
        }
        function a(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        var o = (function () {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    (i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
                }
            }
            return function (e, n, i) {
                return n && t(e.prototype, n), i && t(e, i), e;
            };
        })(),
            s = n(3),
            r = i(s),
            u = n(65),
            l = i(u),
            c = (function () {
                function t(e, n) {
                    a(this, t);
                    (this.options = r["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, r["default"])(e));
                    this.createHoverIcon();
                }
                return (
                    o(t, null, [
                        {
                            key: "Defaults",
                            get: function () {
                                return { index: 0 };
                            },
                        },
                    ]),
                    o(t, [
                        {
                            key: "createHoverIcon",
                            value: function () {
                                if ("undefined" == typeof bodymovin) return setTimeout(this.createHoverIcon.bind(this), 60);
                                var t = this.$container,
                                    e = t.data();
                                this.$inner = (0, r["default"])('<span style="display: inline-block; position: absolute; left: 0; top: 0; width: 100%; height: 100%"></span>').appendTo(t);
                                t.css({ width: e.bmWidth || "100%", height: e.bmHeight || "100%" });
                                var n = t.closest("a, div, p");
                                n.hover(this.playOverAnimation.bind(this), this.playOutAnimation.bind(this)), this.createHoverAnimation();
                            },
                        },
                        {
                            key: "createHoverAnimation",
                            value: function () {
                                var t = this.$inner,
                                    e = this.$container,
                                    n = e.data(),
                                    i = bodymovin.loadAnimation({ wrapper: t.get(0), path: n.bmPath || null, loop: n.bmLoop || !1, animType: n.bmRenderer || "svg", autoplay: !1, assetsPath: n.bmAssetsPath || null });
                                (this.segments = n.bmHoverSegments), (this.anim = i);
                            },
                        },
                        {
                            key: "playOverAnimation",
                            value: function () {
                                var t = this.anim;
                                t.isLoaded && t.playSegments(this.segments[0], !0);
                            },
                        },
                        {
                            key: "playOutAnimation",
                            value: function () {
                                var t = this.anim;
                                if (t.isLoaded) {
                                    var e = [t.currentFrame, this.segments[1][1]];
                                    t.playSegments(e, !0);
                                }
                            },
                        },
                    ]),
                    t
                );
            })();
        r["default"].fn.iconhover = (0, l["default"])(c);
    },
    309: function (t, e, n) {
        (function (t) {
            "use strict";
            t(function () {
                var e = t(".js-clone-source"),
                    n = t(".js-clone-target");
                e.each(function (e, i) {
                    var a = t(i),
                        o = n.filter('[data-clone-id="' + a.data("cloneId") + '"]');
                    o.append(a.html());
                });
            });
        }.call(e, n(3)));
    },
});
// Get the container element
var btnContainer = document.getElementById("first");

// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("btn");


$('.btn--switch').on('click', function(){
   
    if($(this).hasClass('active')){
        $(this).removeClass('active');

    } else{
        $(this).addClass('active');
       
    }
})
