
// Импортируем jQuery
/*!
 * jQuery JavaScript Library v3.4.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2019-05-01T21:04Z
 */
( function( global, factory ) {

	"use strict";

	if ( typeof module === "object" && typeof module.exports === "object" ) {

		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
// throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
// arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
// enough that all such attempts are guarded in a try block.
"use strict";

var arr = [];

var document = window.document;

var getProto = Object.getPrototypeOf;

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var fnToString = hasOwn.toString;

var ObjectFunctionString = fnToString.call( Object );

var support = {};

var isFunction = function isFunction( obj ) {

      // Support: Chrome <=57, Firefox <=52
      // In some browsers, typeof returns "function" for HTML <object> elements
      // (i.e., `typeof document.createElement( "object" ) === "function"`).
      // We don't want to classify *any* DOM node as a function.
      return typeof obj === "function" && typeof obj.nodeType !== "number";
  };


var isWindow = function isWindow( obj ) {
		return obj != null && obj === obj.window;
	};




	var preservedScriptAttributes = {
		type: true,
		src: true,
		nonce: true,
		noModule: true
	};

	function DOMEval( code, node, doc ) {
		doc = doc || document;

		var i, val,
			script = doc.createElement( "script" );

		script.text = code;
		if ( node ) {
			for ( i in preservedScriptAttributes ) {

				// Support: Firefox 64+, Edge 18+
				// Some browsers don't support the "nonce" property on scripts.
				// On the other hand, just using `getAttribute` is not enough as
				// the `nonce` attribute is reset to an empty string whenever it
				// becomes browsing-context connected.
				// See https://github.com/whatwg/html/issues/2369
				// See https://html.spec.whatwg.org/#nonce-attributes
				// The `node.getAttribute` check was added for the sake of
				// `jQuery.globalEval` so that it can fake a nonce-containing node
				// via an object.
				val = node[ i ] || node.getAttribute && node.getAttribute( i );
				if ( val ) {
					script.setAttribute( i, val );
				}
			}
		}
		doc.head.appendChild( script ).parentNode.removeChild( script );
	}


function toType( obj ) {
	if ( obj == null ) {
		return obj + "";
	}

	// Support: Android <=2.3 only (functionish RegExp)
	return typeof obj === "object" || typeof obj === "function" ?
		class2type[ toString.call( obj ) ] || "object" :
		typeof obj;
}
/* global Symbol */
// Defining this global in .eslintrc.json would create a danger of using the global
// unguarded in another place, it seems safer to define global only for this module



var
	version = "3.4.1",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {

		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android <=4.0 only
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

jQuery.fn = jQuery.prototype = {

	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {

		// Return all the elements in a clean array
		if ( num == null ) {
			return slice.call( this );
		}

		// Return just the one element from the set
		return num < 0 ? this[ num + this.length ] : this[ num ];
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	each: function( callback ) {
		return jQuery.each( this, callback );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map( this, function( elem, i ) {
			return callback.call( elem, i, elem );
		} ) );
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor();
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[ 0 ] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !isFunction( target ) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {

		// Only deal with non-null/undefined values
		if ( ( options = arguments[ i ] ) != null ) {

			// Extend the base object
			for ( name in options ) {
				copy = options[ name ];

				// Prevent Object.prototype pollution
				// Prevent never-ending loop
				if ( name === "__proto__" || target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
					( copyIsArray = Array.isArray( copy ) ) ) ) {
					src = target[ name ];

					// Ensure proper type for the source value
					if ( copyIsArray && !Array.isArray( src ) ) {
						clone = [];
					} else if ( !copyIsArray && !jQuery.isPlainObject( src ) ) {
						clone = {};
					} else {
						clone = src;
					}
					copyIsArray = false;

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend( {

	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isPlainObject: function( obj ) {
		var proto, Ctor;

		// Detect obvious negatives
		// Use toString instead of jQuery.type to catch host objects
		if ( !obj || toString.call( obj ) !== "[object Object]" ) {
			return false;
		}

		proto = getProto( obj );

		// Objects with no prototype (e.g., `Object.create( null )`) are plain
		if ( !proto ) {
			return true;
		}

		// Objects with prototype are plain iff they were constructed by a global Object function
		Ctor = hasOwn.call( proto, "constructor" ) && proto.constructor;
		return typeof Ctor === "function" && fnToString.call( Ctor ) === ObjectFunctionString;
	},

	isEmptyObject: function( obj ) {
		var name;

		for ( name in obj ) {
			return false;
		}
		return true;
	},

	// Evaluates a script in a global context
	globalEval: function( code, options ) {
		DOMEval( code, { nonce: options && options.nonce } );
	},

	each: function( obj, callback ) {
		var length, i = 0;

		if ( isArrayLike( obj ) ) {
			length = obj.length;
			for ( ; i < length; i++ ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else {
			for ( i in obj ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		}

		return obj;
	},

	// Support: Android <=4.0 only
	trim: function( text ) {
		return text == null ?
			"" :
			( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArrayLike( Object( arr ) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	// Support: Android <=4.0 only, PhantomJS 1 only
	// push.apply(_, arraylike) throws on ancient WebKit
	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var length, value,
			i = 0,
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArrayLike( elems ) ) {
			length = elems.length;
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
} );

if ( typeof Symbol === "function" ) {
	jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( i, name ) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

	// Support: real iOS 8.2 only (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = !!obj && "length" in obj && obj.length,
		type = toType( obj );

	if ( isFunction( obj ) || isWindow( obj ) ) {
		return false;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.3.4
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://js.foundation/
 *
 * Date: 2019-04-08
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	nonnativeSelectorCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// https://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",

	// http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
		"*\\]",

	pseudos = ":(" + identifier + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),
	rdescend = new RegExp( whitespace + "|>" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + identifier + ")" ),
		"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
		"TAG": new RegExp( "^(" + identifier + "|[*])" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rhtml = /HTML$/i,
	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,

	// CSS escapes
	// http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// CSS string/identifier serialization
	// https://drafts.csswg.org/cssom/#common-serializing-idioms
	rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
	fcssescape = function( ch, asCodePoint ) {
		if ( asCodePoint ) {

			// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
			if ( ch === "\0" ) {
				return "\uFFFD";
			}

			// Control characters and (dependent upon position) numbers get escaped as code points
			return ch.slice( 0, -1 ) + "\\" + ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
		}

		// Other potentially-special ASCII characters get backslash-escaped
		return "\\" + ch;
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	},

	inDisabledFieldset = addCombinator(
		function( elem ) {
			return elem.disabled === true && elem.nodeName.toLowerCase() === "fieldset";
		},
		{ dir: "parentNode", next: "legend" }
	);

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var m, i, elem, nid, match, groups, newSelector,
		newContext = context && context.ownerDocument,

		// nodeType defaults to 9, since context defaults to document
		nodeType = context ? context.nodeType : 9;

	results = results || [];

	// Return early from calls with invalid selector or context
	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	// Try to shortcut find operations (as opposed to filters) in HTML documents
	if ( !seed ) {

		if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
			setDocument( context );
		}
		context = context || document;

		if ( documentIsHTML ) {

			// If the selector is sufficiently simple, try using a "get*By*" DOM method
			// (excepting DocumentFragment context, where the methods don't exist)
			if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {

				// ID selector
				if ( (m = match[1]) ) {

					// Document context
					if ( nodeType === 9 ) {
						if ( (elem = context.getElementById( m )) ) {

							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}

					// Element context
					} else {

						// Support: IE, Opera, Webkit
						// TODO: identify versions
						// getElementById can match elements by name instead of ID
						if ( newContext && (elem = newContext.getElementById( m )) &&
							contains( context, elem ) &&
							elem.id === m ) {

							results.push( elem );
							return results;
						}
					}

				// Type selector
				} else if ( match[2] ) {
					push.apply( results, context.getElementsByTagName( selector ) );
					return results;

				// Class selector
				} else if ( (m = match[3]) && support.getElementsByClassName &&
					context.getElementsByClassName ) {

					push.apply( results, context.getElementsByClassName( m ) );
					return results;
				}
			}

			// Take advantage of querySelectorAll
			if ( support.qsa &&
				!nonnativeSelectorCache[ selector + " " ] &&
				(!rbuggyQSA || !rbuggyQSA.test( selector )) &&

				// Support: IE 8 only
				// Exclude object elements
				(nodeType !== 1 || context.nodeName.toLowerCase() !== "object") ) {

				newSelector = selector;
				newContext = context;

				// qSA considers elements outside a scoping root when evaluating child or
				// descendant combinators, which is not what we want.
				// In such cases, we work around the behavior by prefixing every selector in the
				// list with an ID selector referencing the scope context.
				// Thanks to Andrew Dupont for this technique.
				if ( nodeType === 1 && rdescend.test( selector ) ) {

					// Capture the context ID, setting it first if necessary
					if ( (nid = context.getAttribute( "id" )) ) {
						nid = nid.replace( rcssescape, fcssescape );
					} else {
						context.setAttribute( "id", (nid = expando) );
					}

					// Prefix every selector in the list
					groups = tokenize( selector );
					i = groups.length;
					while ( i-- ) {
						groups[i] = "#" + nid + " " + toSelector( groups[i] );
					}
					newSelector = groups.join( "," );

					// Expand context for sibling selectors
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
						context;
				}

				try {
					push.apply( results,
						newContext.querySelectorAll( newSelector )
					);
					return results;
				} catch ( qsaError ) {
					nonnativeSelectorCache( selector, true );
				} finally {
					if ( nid === expando ) {
						context.removeAttribute( "id" );
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created element and returns a boolean result
 */
function assert( fn ) {
	var el = document.createElement("fieldset");

	try {
		return !!fn( el );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( el.parentNode ) {
			el.parentNode.removeChild( el );
		}
		// release memory in IE
		el = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = arr.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			a.sourceIndex - b.sourceIndex;

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for :enabled/:disabled
 * @param {Boolean} disabled true for :disabled; false for :enabled
 */
function createDisabledPseudo( disabled ) {

	// Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
	return function( elem ) {

		// Only certain elements can match :enabled or :disabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
		if ( "form" in elem ) {

			// Check for inherited disabledness on relevant non-disabled elements:
			// * listed form-associated elements in a disabled fieldset
			//   https://html.spec.whatwg.org/multipage/forms.html#category-listed
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
			// * option elements in a disabled optgroup
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
			// All such elements have a "form" property.
			if ( elem.parentNode && elem.disabled === false ) {

				// Option elements defer to a parent optgroup if present
				if ( "label" in elem ) {
					if ( "label" in elem.parentNode ) {
						return elem.parentNode.disabled === disabled;
					} else {
						return elem.disabled === disabled;
					}
				}

				// Support: IE 6 - 11
				// Use the isDisabled shortcut property to check for disabled fieldset ancestors
				return elem.isDisabled === disabled ||

					// Where there is no isDisabled, check manually
					/* jshint -W018 */
					elem.isDisabled !== !disabled &&
						inDisabledFieldset( elem ) === disabled;
			}

			return elem.disabled === disabled;

		// Try to winnow out elements that can't be disabled before trusting the disabled property.
		// Some victims get caught in our net (label, legend, menu, track), but it shouldn't
		// even exist on them, let alone have a boolean value.
		} else if ( "label" in elem ) {
			return elem.disabled === disabled;
		}

		// Remaining elements are neither :enabled nor :disabled
		return false;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	var namespace = elem.namespaceURI,
		docElem = (elem.ownerDocument || elem).documentElement;

	// Support: IE <=8
	// Assume HTML when documentElement doesn't yet exist, such as inside loading iframes
	// https://bugs.jquery.com/ticket/4833
	return !rhtml.test( namespace || docElem && docElem.nodeName || "HTML" );
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, subWindow,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// Return early if doc is invalid or already selected
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Update global variables
	document = doc;
	docElem = document.documentElement;
	documentIsHTML = !isXML( document );

	// Support: IE 9-11, Edge
	// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
	if ( preferredDoc !== document &&
		(subWindow = document.defaultView) && subWindow.top !== subWindow ) {

		// Support: IE 11, Edge
		if ( subWindow.addEventListener ) {
			subWindow.addEventListener( "unload", unloadHandler, false );

		// Support: IE 9 - 10 only
		} else if ( subWindow.attachEvent ) {
			subWindow.attachEvent( "onunload", unloadHandler );
		}
	}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( el ) {
		el.className = "i";
		return !el.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( el ) {
		el.appendChild( document.createComment("") );
		return !el.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( document.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programmatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( el ) {
		docElem.appendChild( el ).id = expando;
		return !document.getElementsByName || !document.getElementsByName( expando ).length;
	});

	// ID filter and find
	if ( support.getById ) {
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var elem = context.getElementById( id );
				return elem ? [ elem ] : [];
			}
		};
	} else {
		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" &&
					elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};

		// Support: IE 6 - 7 only
		// getElementById is not reliable as a find shortcut
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var node, i, elems,
					elem = context.getElementById( id );

				if ( elem ) {

					// Verify the id attribute
					node = elem.getAttributeNode("id");
					if ( node && node.value === id ) {
						return [ elem ];
					}

					// Fall back on getElementsByName
					elems = context.getElementsByName( id );
					i = 0;
					while ( (elem = elems[i++]) ) {
						node = elem.getAttributeNode("id");
						if ( node && node.value === id ) {
							return [ elem ];
						}
					}
				}

				return [];
			}
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See https://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( el ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// https://bugs.jquery.com/ticket/12359
			docElem.appendChild( el ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\r\\' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( el.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !el.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
			if ( !el.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !el.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibling-combinator selector` fails
			if ( !el.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

		assert(function( el ) {
			el.innerHTML = "<a href='' disabled='disabled'></a>" +
				"<select disabled='disabled'><option/></select>";

			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = document.createElement("input");
			input.setAttribute( "type", "hidden" );
			el.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( el.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( el.querySelectorAll(":enabled").length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Support: IE9-11+
			// IE's :disabled selector does not pick up the children of disabled fieldsets
			docElem.appendChild( el ).disabled = true;
			if ( el.querySelectorAll(":disabled").length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			el.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( el ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( el, "*" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( el, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully self-exclusive
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === document ? -1 :
				b === document ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return document;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	if ( support.matchesSelector && documentIsHTML &&
		!nonnativeSelectorCache[ expr + " " ] &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch (e) {
			nonnativeSelectorCache( expr, true );
		}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.escape = function( sel ) {
	return (sel + "").replace( rcssescape, fcssescape );
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[6] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, uniqueCache, outerCache, node, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType,
						diff = false;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) {

										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {

							// Seek `elem` from a previously-cached index

							// ...in a gzip-friendly way
							node = parent;
							outerCache = node[ expando ] || (node[ expando ] = {});

							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ node.uniqueID ] ||
								(outerCache[ node.uniqueID ] = {});

							cache = uniqueCache[ type ] || [];
							nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
							diff = nodeIndex && cache[ 2 ];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						} else {
							// Use previously-cached element index if available
							if ( useCache ) {
								// ...in a gzip-friendly way
								node = elem;
								outerCache = node[ expando ] || (node[ expando ] = {});

								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									(outerCache[ node.uniqueID ] = {});

								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex;
							}

							// xml :nth-child(...)
							// or :nth-last-child(...) or :nth(-last)?-of-type(...)
							if ( diff === false ) {
								// Use the same loop as above to seek `elem` from the start
								while ( (node = ++nodeIndex && node && node[ dir ] ||
									(diff = nodeIndex = 0) || start.pop()) ) {

									if ( ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) &&
										++diff ) {

										// Cache the index of each encountered element
										if ( useCache ) {
											outerCache = node[ expando ] || (node[ expando ] = {});

											// Support: IE <9 only
											// Defend against cloned attroperties (jQuery gh-1709)
											uniqueCache = outerCache[ node.uniqueID ] ||
												(outerCache[ node.uniqueID ] = {});

											uniqueCache[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": createDisabledPseudo( false ),
		"disabled": createDisabledPseudo( true ),

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ?
				argument + length :
				argument > length ?
					length :
					argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		skip = combinator.next,
		key = skip || dir,
		checkNonElements = base && key === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
			return false;
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, uniqueCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});

						// Support: IE <9 only
						// Defend against cloned attroperties (jQuery gh-1709)
						uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});

						if ( skip && skip === elem.nodeName.toLowerCase() ) {
							elem = elem[ dir ] || elem;
						} else if ( (oldCache = uniqueCache[ key ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							uniqueCache[ key ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
			return false;
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context === document || context || outermost;
			}

			// Add elements passing elementMatchers directly to results
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					if ( !context && elem.ownerDocument !== document ) {
						setDocument( elem );
						xml = !documentIsHTML;
					}
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context || document, xml) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// `i` is now the count of elements visited above, and adding it to `matchedCount`
			// makes the latter nonnegative.
			matchedCount += i;

			// Apply set filters to unmatched elements
			// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
			// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
			// no element matchers and no seed.
			// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
			// case, which will result in a "00" `matchedCount` that differs from `i` but is also
			// numerically zero.
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( (selector = compiled.selector || selector) );

	results = results || [];

	// Try to minimize operations if there is only one selector in the list and no seed
	// (the latter of which guarantees us context)
	if ( match.length === 1 ) {

		// Reduce context if the leading compound selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
				context.nodeType === 9 && documentIsHTML && Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
				)) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( el ) {
	// Should return 1, but returns 4 (following)
	return el.compareDocumentPosition( document.createElement("fieldset") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( el ) {
	el.innerHTML = "<a href='#'></a>";
	return el.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( el ) {
	el.innerHTML = "<input/>";
	el.firstChild.setAttribute( "value", "" );
	return el.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( el ) {
	return el.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;

// Deprecated
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;
jQuery.escapeSelector = Sizzle.escape;




var dir = function( elem, dir, until ) {
	var matched = [],
		truncate = until !== undefined;

	while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
		if ( elem.nodeType === 1 ) {
			if ( truncate && jQuery( elem ).is( until ) ) {
				break;
			}
			matched.push( elem );
		}
	}
	return matched;
};


var siblings = function( n, elem ) {
	var matched = [];

	for ( ; n; n = n.nextSibling ) {
		if ( n.nodeType === 1 && n !== elem ) {
			matched.push( n );
		}
	}

	return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;



function nodeName( elem, name ) {

  return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();

};
var rsingleTag = ( /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i );



// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			return !!qualifier.call( elem, i, elem ) !== not;
		} );
	}

	// Single element
	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		} );
	}

	// Arraylike of elements (jQuery, arguments, Array)
	if ( typeof qualifier !== "string" ) {
		return jQuery.grep( elements, function( elem ) {
			return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
		} );
	}

	// Filtered directly for both simple and complex selectors
	return jQuery.filter( qualifier, elements, not );
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	if ( elems.length === 1 && elem.nodeType === 1 ) {
		return jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [];
	}

	return jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
		return elem.nodeType === 1;
	} ) );
};

jQuery.fn.extend( {
	find: function( selector ) {
		var i, ret,
			len = this.length,
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter( function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			} ) );
		}

		ret = this.pushStack( [] );

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		return len > 1 ? jQuery.uniqueSort( ret ) : ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow( this, selector || [], false ) );
	},
	not: function( selector ) {
		return this.pushStack( winnow( this, selector || [], true ) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	// Shortcut simple #id case for speed
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,

	init = jQuery.fn.init = function( selector, context, root ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Method init() accepts an alternate rootjQuery
		// so migrate can support jQuery.sub (gh-2101)
		root = root || rootjQuery;

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[ 0 ] === "<" &&
				selector[ selector.length - 1 ] === ">" &&
				selector.length >= 3 ) {

				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && ( match[ 1 ] || !context ) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[ 1 ] ) {
					context = context instanceof jQuery ? context[ 0 ] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[ 1 ],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {

							// Properties of context are called as methods if possible
							if ( isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[ 2 ] );

					if ( elem ) {

						// Inject the element directly into the jQuery object
						this[ 0 ] = elem;
						this.length = 1;
					}
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || root ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this[ 0 ] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( isFunction( selector ) ) {
			return root.ready !== undefined ?
				root.ready( selector ) :

				// Execute immediately if ready is not present
				selector( jQuery );
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.fn.extend( {
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter( function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[ i ] ) ) {
					return true;
				}
			}
		} );
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			targets = typeof selectors !== "string" && jQuery( selectors );

		// Positional selectors never match, since there's no _selection_ context
		if ( !rneedsContext.test( selectors ) ) {
			for ( ; i < l; i++ ) {
				for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

					// Always skip document fragments
					if ( cur.nodeType < 11 && ( targets ?
						targets.index( cur ) > -1 :

						// Don't pass non-elements to Sizzle
						cur.nodeType === 1 &&
							jQuery.find.matchesSelector( cur, selectors ) ) ) {

						matched.push( cur );
						break;
					}
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.uniqueSort(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter( selector )
		);
	}
} );

function sibling( cur, dir ) {
	while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each( {
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return siblings( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return siblings( elem.firstChild );
	},
	contents: function( elem ) {
		if ( typeof elem.contentDocument !== "undefined" ) {
			return elem.contentDocument;
		}

		// Support: IE 9 - 11 only, iOS 7 only, Android Browser <=4.3 only
		// Treat the template element as a regular one in browsers that
		// don't support it.
		if ( nodeName( elem, "template" ) ) {
			elem = elem.content || elem;
		}

		return jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {

			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.uniqueSort( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
} );
var rnothtmlwhite = ( /[^\x20\t\r\n\f]+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
	var object = {};
	jQuery.each( options.match( rnothtmlwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	} );
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		createOptions( options ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,

		// Last fire value for non-forgettable lists
		memory,

		// Flag to know if list was already fired
		fired,

		// Flag to prevent firing
		locked,

		// Actual callback list
		list = [],

		// Queue of execution data for repeatable lists
		queue = [],

		// Index of currently firing callback (modified by add/remove as needed)
		firingIndex = -1,

		// Fire callbacks
		fire = function() {

			// Enforce single-firing
			locked = locked || options.once;

			// Execute callbacks for all pending executions,
			// respecting firingIndex overrides and runtime changes
			fired = firing = true;
			for ( ; queue.length; firingIndex = -1 ) {
				memory = queue.shift();
				while ( ++firingIndex < list.length ) {

					// Run callback and check for early termination
					if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
						options.stopOnFalse ) {

						// Jump to end and forget the data so .add doesn't re-fire
						firingIndex = list.length;
						memory = false;
					}
				}
			}

			// Forget the data if we're done with it
			if ( !options.memory ) {
				memory = false;
			}

			firing = false;

			// Clean up if we're done firing for good
			if ( locked ) {

				// Keep an empty list if we have data for future add calls
				if ( memory ) {
					list = [];

				// Otherwise, this object is spent
				} else {
					list = "";
				}
			}
		},

		// Actual Callbacks object
		self = {

			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {

					// If we have memory from a past run, we should fire after adding
					if ( memory && !firing ) {
						firingIndex = list.length - 1;
						queue.push( memory );
					}

					( function add( args ) {
						jQuery.each( args, function( _, arg ) {
							if ( isFunction( arg ) ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && toType( arg ) !== "string" ) {

								// Inspect recursively
								add( arg );
							}
						} );
					} )( arguments );

					if ( memory && !firing ) {
						fire();
					}
				}
				return this;
			},

			// Remove a callback from the list
			remove: function() {
				jQuery.each( arguments, function( _, arg ) {
					var index;
					while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
						list.splice( index, 1 );

						// Handle firing indexes
						if ( index <= firingIndex ) {
							firingIndex--;
						}
					}
				} );
				return this;
			},

			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ?
					jQuery.inArray( fn, list ) > -1 :
					list.length > 0;
			},

			// Remove all callbacks from the list
			empty: function() {
				if ( list ) {
					list = [];
				}
				return this;
			},

			// Disable .fire and .add
			// Abort any current/pending executions
			// Clear all callbacks and values
			disable: function() {
				locked = queue = [];
				list = memory = "";
				return this;
			},
			disabled: function() {
				return !list;
			},

			// Disable .fire
			// Also disable .add unless we have memory (since it would have no effect)
			// Abort any pending executions
			lock: function() {
				locked = queue = [];
				if ( !memory && !firing ) {
					list = memory = "";
				}
				return this;
			},
			locked: function() {
				return !!locked;
			},

			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( !locked ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					queue.push( args );
					if ( !firing ) {
						fire();
					}
				}
				return this;
			},

			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},

			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


function Identity( v ) {
	return v;
}
function Thrower( ex ) {
	throw ex;
}

function adoptValue( value, resolve, reject, noValue ) {
	var method;

	try {

		// Check for promise aspect first to privilege synchronous behavior
		if ( value && isFunction( ( method = value.promise ) ) ) {
			method.call( value ).done( resolve ).fail( reject );

		// Other thenables
		} else if ( value && isFunction( ( method = value.then ) ) ) {
			method.call( value, resolve, reject );

		// Other non-thenables
		} else {

			// Control `resolve` arguments by letting Array#slice cast boolean `noValue` to integer:
			// * false: [ value ].slice( 0 ) => resolve( value )
			// * true: [ value ].slice( 1 ) => resolve()
			resolve.apply( undefined, [ value ].slice( noValue ) );
		}

	// For Promises/A+, convert exceptions into rejections
	// Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
	// Deferred#then to conditionally suppress rejection.
	} catch ( value ) {

		// Support: Android 4.0 only
		// Strict mode functions invoked without .call/.apply get global-object context
		reject.apply( undefined, [ value ] );
	}
}

jQuery.extend( {

	Deferred: function( func ) {
		var tuples = [

				// action, add listener, callbacks,
				// ... .then handlers, argument index, [final state]
				[ "notify", "progress", jQuery.Callbacks( "memory" ),
					jQuery.Callbacks( "memory" ), 2 ],
				[ "resolve", "done", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 0, "resolved" ],
				[ "reject", "fail", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 1, "rejected" ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				"catch": function( fn ) {
					return promise.then( null, fn );
				},

				// Keep pipe for back-compat
				pipe: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;

					return jQuery.Deferred( function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {

							// Map tuples (progress, done, fail) to arguments (done, fail, progress)
							var fn = isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];

							// deferred.progress(function() { bind to newDefer or newDefer.notify })
							// deferred.done(function() { bind to newDefer or newDefer.resolve })
							// deferred.fail(function() { bind to newDefer or newDefer.reject })
							deferred[ tuple[ 1 ] ]( function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && isFunction( returned.promise ) ) {
									returned.promise()
										.progress( newDefer.notify )
										.done( newDefer.resolve )
										.fail( newDefer.reject );
								} else {
									newDefer[ tuple[ 0 ] + "With" ](
										this,
										fn ? [ returned ] : arguments
									);
								}
							} );
						} );
						fns = null;
					} ).promise();
				},
				then: function( onFulfilled, onRejected, onProgress ) {
					var maxDepth = 0;
					function resolve( depth, deferred, handler, special ) {
						return function() {
							var that = this,
								args = arguments,
								mightThrow = function() {
									var returned, then;

									// Support: Promises/A+ section 2.3.3.3.3
									// https://promisesaplus.com/#point-59
									// Ignore double-resolution attempts
									if ( depth < maxDepth ) {
										return;
									}

									returned = handler.apply( that, args );

									// Support: Promises/A+ section 2.3.1
									// https://promisesaplus.com/#point-48
									if ( returned === deferred.promise() ) {
										throw new TypeError( "Thenable self-resolution" );
									}

									// Support: Promises/A+ sections 2.3.3.1, 3.5
									// https://promisesaplus.com/#point-54
									// https://promisesaplus.com/#point-75
									// Retrieve `then` only once
									then = returned &&

										// Support: Promises/A+ section 2.3.4
										// https://promisesaplus.com/#point-64
										// Only check objects and functions for thenability
										( typeof returned === "object" ||
											typeof returned === "function" ) &&
										returned.then;

									// Handle a returned thenable
									if ( isFunction( then ) ) {

										// Special processors (notify) just wait for resolution
										if ( special ) {
											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special )
											);

										// Normal processors (resolve) also hook into progress
										} else {

											// ...and disregard older resolution values
											maxDepth++;

											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special ),
												resolve( maxDepth, deferred, Identity,
													deferred.notifyWith )
											);
										}

									// Handle all other returned values
									} else {

										// Only substitute handlers pass on context
										// and multiple values (non-spec behavior)
										if ( handler !== Identity ) {
											that = undefined;
											args = [ returned ];
										}

										// Process the value(s)
										// Default process is resolve
										( special || deferred.resolveWith )( that, args );
									}
								},

								// Only normal processors (resolve) catch and reject exceptions
								process = special ?
									mightThrow :
									function() {
										try {
											mightThrow();
										} catch ( e ) {

											if ( jQuery.Deferred.exceptionHook ) {
												jQuery.Deferred.exceptionHook( e,
													process.stackTrace );
											}

											// Support: Promises/A+ section 2.3.3.3.4.1
											// https://promisesaplus.com/#point-61
											// Ignore post-resolution exceptions
											if ( depth + 1 >= maxDepth ) {

												// Only substitute handlers pass on context
												// and multiple values (non-spec behavior)
												if ( handler !== Thrower ) {
													that = undefined;
													args = [ e ];
												}

												deferred.rejectWith( that, args );
											}
										}
									};

							// Support: Promises/A+ section 2.3.3.3.1
							// https://promisesaplus.com/#point-57
							// Re-resolve promises immediately to dodge false rejection from
							// subsequent errors
							if ( depth ) {
								process();
							} else {

								// Call an optional hook to record the stack, in case of exception
								// since it's otherwise lost when execution goes async
								if ( jQuery.Deferred.getStackHook ) {
									process.stackTrace = jQuery.Deferred.getStackHook();
								}
								window.setTimeout( process );
							}
						};
					}

					return jQuery.Deferred( function( newDefer ) {

						// progress_handlers.add( ... )
						tuples[ 0 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onProgress ) ?
									onProgress :
									Identity,
								newDefer.notifyWith
							)
						);

						// fulfilled_handlers.add( ... )
						tuples[ 1 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onFulfilled ) ?
									onFulfilled :
									Identity
							)
						);

						// rejected_handlers.add( ... )
						tuples[ 2 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onRejected ) ?
									onRejected :
									Thrower
							)
						);
					} ).promise();
				},

				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 5 ];

			// promise.progress = list.add
			// promise.done = list.add
			// promise.fail = list.add
			promise[ tuple[ 1 ] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(
					function() {

						// state = "resolved" (i.e., fulfilled)
						// state = "rejected"
						state = stateString;
					},

					// rejected_callbacks.disable
					// fulfilled_callbacks.disable
					tuples[ 3 - i ][ 2 ].disable,

					// rejected_handlers.disable
					// fulfilled_handlers.disable
					tuples[ 3 - i ][ 3 ].disable,

					// progress_callbacks.lock
					tuples[ 0 ][ 2 ].lock,

					// progress_handlers.lock
					tuples[ 0 ][ 3 ].lock
				);
			}

			// progress_handlers.fire
			// fulfilled_handlers.fire
			// rejected_handlers.fire
			list.add( tuple[ 3 ].fire );

			// deferred.notify = function() { deferred.notifyWith(...) }
			// deferred.resolve = function() { deferred.resolveWith(...) }
			// deferred.reject = function() { deferred.rejectWith(...) }
			deferred[ tuple[ 0 ] ] = function() {
				deferred[ tuple[ 0 ] + "With" ]( this === deferred ? undefined : this, arguments );
				return this;
			};

			// deferred.notifyWith = list.fireWith
			// deferred.resolveWith = list.fireWith
			// deferred.rejectWith = list.fireWith
			deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
		} );

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( singleValue ) {
		var

			// count of uncompleted subordinates
			remaining = arguments.length,

			// count of unprocessed arguments
			i = remaining,

			// subordinate fulfillment data
			resolveContexts = Array( i ),
			resolveValues = slice.call( arguments ),

			// the master Deferred
			master = jQuery.Deferred(),

			// subordinate callback factory
			updateFunc = function( i ) {
				return function( value ) {
					resolveContexts[ i ] = this;
					resolveValues[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( !( --remaining ) ) {
						master.resolveWith( resolveContexts, resolveValues );
					}
				};
			};

		// Single- and empty arguments are adopted like Promise.resolve
		if ( remaining <= 1 ) {
			adoptValue( singleValue, master.done( updateFunc( i ) ).resolve, master.reject,
				!remaining );

			// Use .then() to unwrap secondary thenables (cf. gh-3000)
			if ( master.state() === "pending" ||
				isFunction( resolveValues[ i ] && resolveValues[ i ].then ) ) {

				return master.then();
			}
		}

		// Multiple arguments are aggregated like Promise.all array elements
		while ( i-- ) {
			adoptValue( resolveValues[ i ], updateFunc( i ), master.reject );
		}

		return master.promise();
	}
} );


// These usually indicate a programmer mistake during development,
// warn about them ASAP rather than swallowing them by default.
var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;

jQuery.Deferred.exceptionHook = function( error, stack ) {

	// Support: IE 8 - 9 only
	// Console exists when dev tools are open, which can happen at any time
	if ( window.console && window.console.warn && error && rerrorNames.test( error.name ) ) {
		window.console.warn( "jQuery.Deferred exception: " + error.message, error.stack, stack );
	}
};




jQuery.readyException = function( error ) {
	window.setTimeout( function() {
		throw error;
	} );
};




// The deferred used on DOM ready
var readyList = jQuery.Deferred();

jQuery.fn.ready = function( fn ) {

	readyList
		.then( fn )

		// Wrap jQuery.readyException in a function so that the lookup
		// happens at the time of error handling instead of callback
		// registration.
		.catch( function( error ) {
			jQuery.readyException( error );
		} );

	return this;
};

jQuery.extend( {

	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );
	}
} );

jQuery.ready.then = readyList.then;

// The ready event handler and self cleanup method
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed );
	window.removeEventListener( "load", completed );
	jQuery.ready();
}

// Catch cases where $(document).ready() is called
// after the browser event has already occurred.
// Support: IE <=9 - 10 only
// Older IE sometimes signals "interactive" too soon
if ( document.readyState === "complete" ||
	( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

	// Handle it asynchronously to allow scripts the opportunity to delay ready
	window.setTimeout( jQuery.ready );

} else {

	// Use the handy event callback
	document.addEventListener( "DOMContentLoaded", completed );

	// A fallback to window.onload, that will always work
	window.addEventListener( "load", completed );
}




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( toType( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			access( elems, fn, i, key[ i ], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {

			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn(
					elems[ i ], key, raw ?
					value :
					value.call( elems[ i ], i, fn( elems[ i ], key ) )
				);
			}
		}
	}

	if ( chainable ) {
		return elems;
	}

	// Gets
	if ( bulk ) {
		return fn.call( elems );
	}

	return len ? fn( elems[ 0 ], key ) : emptyGet;
};


// Matches dashed string for camelizing
var rmsPrefix = /^-ms-/,
	rdashAlpha = /-([a-z])/g;

// Used by camelCase as callback to replace()
function fcamelCase( all, letter ) {
	return letter.toUpperCase();
}

// Convert dashed to camelCase; used by the css and data modules
// Support: IE <=9 - 11, Edge 12 - 15
// Microsoft forgot to hump their vendor prefix (#9572)
function camelCase( string ) {
	return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
}
var acceptData = function( owner ) {

	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

	cache: function( owner ) {

		// Check if the owner object already has a cache
		var value = owner[ this.expando ];

		// If not, create one
		if ( !value ) {
			value = {};

			// We can accept data for non-element nodes in modern browsers,
			// but we should not, see #8335.
			// Always return an empty object.
			if ( acceptData( owner ) ) {

				// If it is a node unlikely to be stringify-ed or looped over
				// use plain assignment
				if ( owner.nodeType ) {
					owner[ this.expando ] = value;

				// Otherwise secure it in a non-enumerable property
				// configurable must be true to allow the property to be
				// deleted when data is removed
				} else {
					Object.defineProperty( owner, this.expando, {
						value: value,
						configurable: true
					} );
				}
			}
		}

		return value;
	},
	set: function( owner, data, value ) {
		var prop,
			cache = this.cache( owner );

		// Handle: [ owner, key, value ] args
		// Always use camelCase key (gh-2257)
		if ( typeof data === "string" ) {
			cache[ camelCase( data ) ] = value;

		// Handle: [ owner, { properties } ] args
		} else {

			// Copy the properties one-by-one to the cache object
			for ( prop in data ) {
				cache[ camelCase( prop ) ] = data[ prop ];
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		return key === undefined ?
			this.cache( owner ) :

			// Always use camelCase key (gh-2257)
			owner[ this.expando ] && owner[ this.expando ][ camelCase( key ) ];
	},
	access: function( owner, key, value ) {

		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				( ( key && typeof key === "string" ) && value === undefined ) ) {

			return this.get( owner, key );
		}

		// When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i,
			cache = owner[ this.expando ];

		if ( cache === undefined ) {
			return;
		}

		if ( key !== undefined ) {

			// Support array or space separated string of keys
			if ( Array.isArray( key ) ) {

				// If key is an array of keys...
				// We always set camelCase keys, so remove that.
				key = key.map( camelCase );
			} else {
				key = camelCase( key );

				// If a key with the spaces exists, use it.
				// Otherwise, create an array by matching non-whitespace
				key = key in cache ?
					[ key ] :
					( key.match( rnothtmlwhite ) || [] );
			}

			i = key.length;

			while ( i-- ) {
				delete cache[ key[ i ] ];
			}
		}

		// Remove the expando if there's no more data
		if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

			// Support: Chrome <=35 - 45
			// Webkit & Blink performance suffers when deleting properties
			// from DOM nodes, so set to undefined instead
			// https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
			if ( owner.nodeType ) {
				owner[ this.expando ] = undefined;
			} else {
				delete owner[ this.expando ];
			}
		}
	},
	hasData: function( owner ) {
		var cache = owner[ this.expando ];
		return cache !== undefined && !jQuery.isEmptyObject( cache );
	}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /[A-Z]/g;

function getData( data ) {
	if ( data === "true" ) {
		return true;
	}

	if ( data === "false" ) {
		return false;
	}

	if ( data === "null" ) {
		return null;
	}

	// Only convert to a number if it doesn't change the string
	if ( data === +data + "" ) {
		return +data;
	}

	if ( rbrace.test( data ) ) {
		return JSON.parse( data );
	}

	return data;
}

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = getData( data );
			} catch ( e ) {}

			// Make sure we set the data so it isn't changed later
			dataUser.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend( {
	hasData: function( elem ) {
		return dataUser.hasData( elem ) || dataPriv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return dataUser.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		dataUser.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to dataPriv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return dataPriv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		dataPriv.remove( elem, name );
	}
} );

jQuery.fn.extend( {
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = dataUser.get( elem );

				if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE 11 only
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = camelCase( name.slice( 5 ) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					dataPriv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each( function() {
				dataUser.set( this, key );
			} );
		}

		return access( this, function( value ) {
			var data;

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {

				// Attempt to get data from the cache
				// The key will always be camelCased in Data
				data = dataUser.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each( function() {

				// We always store the camelCased key
				dataUser.set( this, key, value );
			} );
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each( function() {
			dataUser.remove( this, key );
		} );
	}
} );


jQuery.extend( {
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = dataPriv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || Array.isArray( data ) ) {
					queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
			empty: jQuery.Callbacks( "once memory" ).add( function() {
				dataPriv.remove( elem, [ type + "queue", key ] );
			} )
		} );
	}
} );

jQuery.fn.extend( {
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[ 0 ], type );
		}

		return data === undefined ?
			this :
			this.each( function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			} );
	},
	dequeue: function( type ) {
		return this.each( function() {
			jQuery.dequeue( this, type );
		} );
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},

	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var documentElement = document.documentElement;



	var isAttached = function( elem ) {
			return jQuery.contains( elem.ownerDocument, elem );
		},
		composed = { composed: true };

	// Support: IE 9 - 11+, Edge 12 - 18+, iOS 10.0 - 10.2 only
	// Check attachment across shadow DOM boundaries when possible (gh-3504)
	// Support: iOS 10.0-10.2 only
	// Early iOS 10 versions support `attachShadow` but not `getRootNode`,
	// leading to errors. We need to check for `getRootNode`.
	if ( documentElement.getRootNode ) {
		isAttached = function( elem ) {
			return jQuery.contains( elem.ownerDocument, elem ) ||
				elem.getRootNode( composed ) === elem.ownerDocument;
		};
	}
var isHiddenWithinTree = function( elem, el ) {

		// isHiddenWithinTree might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;

		// Inline style trumps all
		return elem.style.display === "none" ||
			elem.style.display === "" &&

			// Otherwise, check computed style
			// Support: Firefox <=43 - 45
			// Disconnected elements can have computed display: none, so first confirm that elem is
			// in the document.
			isAttached( elem ) &&

			jQuery.css( elem, "display" ) === "none";
	};

var swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};




function adjustCSS( elem, prop, valueParts, tween ) {
	var adjusted, scale,
		maxIterations = 20,
		currentValue = tween ?
			function() {
				return tween.cur();
			} :
			function() {
				return jQuery.css( elem, prop, "" );
			},
		initial = currentValue(),
		unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

		// Starting value computation is required for potential unit mismatches
		initialInUnit = elem.nodeType &&
			( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
			rcssNum.exec( jQuery.css( elem, prop ) );

	if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

		// Support: Firefox <=54
		// Halve the iteration target value to prevent interference from CSS upper bounds (gh-2144)
		initial = initial / 2;

		// Trust units reported by jQuery.css
		unit = unit || initialInUnit[ 3 ];

		// Iteratively approximate from a nonzero starting point
		initialInUnit = +initial || 1;

		while ( maxIterations-- ) {

			// Evaluate and update our best guess (doubling guesses that zero out).
			// Finish if the scale equals or crosses 1 (making the old*new product non-positive).
			jQuery.style( elem, prop, initialInUnit + unit );
			if ( ( 1 - scale ) * ( 1 - ( scale = currentValue() / initial || 0.5 ) ) <= 0 ) {
				maxIterations = 0;
			}
			initialInUnit = initialInUnit / scale;

		}

		initialInUnit = initialInUnit * 2;
		jQuery.style( elem, prop, initialInUnit + unit );

		// Make sure we update the tween properties later on
		valueParts = valueParts || [];
	}

	if ( valueParts ) {
		initialInUnit = +initialInUnit || +initial || 0;

		// Apply relative offset (+=/-=) if specified
		adjusted = valueParts[ 1 ] ?
			initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
			+valueParts[ 2 ];
		if ( tween ) {
			tween.unit = unit;
			tween.start = initialInUnit;
			tween.end = adjusted;
		}
	}
	return adjusted;
}


var defaultDisplayMap = {};

function getDefaultDisplay( elem ) {
	var temp,
		doc = elem.ownerDocument,
		nodeName = elem.nodeName,
		display = defaultDisplayMap[ nodeName ];

	if ( display ) {
		return display;
	}

	temp = doc.body.appendChild( doc.createElement( nodeName ) );
	display = jQuery.css( temp, "display" );

	temp.parentNode.removeChild( temp );

	if ( display === "none" ) {
		display = "block";
	}
	defaultDisplayMap[ nodeName ] = display;

	return display;
}

function showHide( elements, show ) {
	var display, elem,
		values = [],
		index = 0,
		length = elements.length;

	// Determine new display value for elements that need to change
	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		display = elem.style.display;
		if ( show ) {

			// Since we force visibility upon cascade-hidden elements, an immediate (and slow)
			// check is required in this first loop unless we have a nonempty display value (either
			// inline or about-to-be-restored)
			if ( display === "none" ) {
				values[ index ] = dataPriv.get( elem, "display" ) || null;
				if ( !values[ index ] ) {
					elem.style.display = "";
				}
			}
			if ( elem.style.display === "" && isHiddenWithinTree( elem ) ) {
				values[ index ] = getDefaultDisplay( elem );
			}
		} else {
			if ( display !== "none" ) {
				values[ index ] = "none";

				// Remember what we're overwriting
				dataPriv.set( elem, "display", display );
			}
		}
	}

	// Set the display of the elements in a second loop to avoid constant reflow
	for ( index = 0; index < length; index++ ) {
		if ( values[ index ] != null ) {
			elements[ index ].style.display = values[ index ];
		}
	}

	return elements;
}

jQuery.fn.extend( {
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each( function() {
			if ( isHiddenWithinTree( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		} );
	}
} );
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([a-z][^\/\0>\x20\t\r\n\f]*)/i );

var rscriptType = ( /^$|^module$|\/(?:java|ecma)script/i );



// We have to close these tags to support XHTML (#13200)
var wrapMap = {

	// Support: IE <=9 only
	option: [ 1, "<select multiple='multiple'>", "</select>" ],

	// XHTML parsers do not magically insert elements in the
	// same way that tag soup parsers do. So we cannot shorten
	// this by omitting <tbody> or other required elements.
	thead: [ 1, "<table>", "</table>" ],
	col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
	tr: [ 2, "<table><tbody>", "</tbody></table>" ],
	td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

	_default: [ 0, "", "" ]
};

// Support: IE <=9 only
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;


function getAll( context, tag ) {

	// Support: IE <=9 - 11 only
	// Use typeof to avoid zero-argument method invocation on host objects (#15151)
	var ret;

	if ( typeof context.getElementsByTagName !== "undefined" ) {
		ret = context.getElementsByTagName( tag || "*" );

	} else if ( typeof context.querySelectorAll !== "undefined" ) {
		ret = context.querySelectorAll( tag || "*" );

	} else {
		ret = [];
	}

	if ( tag === undefined || tag && nodeName( context, tag ) ) {
		return jQuery.merge( [ context ], ret );
	}

	return ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		dataPriv.set(
			elems[ i ],
			"globalEval",
			!refElements || dataPriv.get( refElements[ i ], "globalEval" )
		);
	}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
	var elem, tmp, tag, wrap, attached, j,
		fragment = context.createDocumentFragment(),
		nodes = [],
		i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		elem = elems[ i ];

		if ( elem || elem === 0 ) {

			// Add nodes directly
			if ( toType( elem ) === "object" ) {

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

			// Convert non-html into a text node
			} else if ( !rhtml.test( elem ) ) {
				nodes.push( context.createTextNode( elem ) );

			// Convert html into DOM nodes
			} else {
				tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

				// Deserialize a standard representation
				tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
				wrap = wrapMap[ tag ] || wrapMap._default;
				tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

				// Descend through wrappers to the right content
				j = wrap[ 0 ];
				while ( j-- ) {
					tmp = tmp.lastChild;
				}

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, tmp.childNodes );

				// Remember the top-level container
				tmp = fragment.firstChild;

				// Ensure the created nodes are orphaned (#12392)
				tmp.textContent = "";
			}
		}
	}

	// Remove wrapper from fragment
	fragment.textContent = "";

	i = 0;
	while ( ( elem = nodes[ i++ ] ) ) {

		// Skip elements already in the context collection (trac-4087)
		if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
			if ( ignored ) {
				ignored.push( elem );
			}
			continue;
		}

		attached = isAttached( elem );

		// Append to fragment
		tmp = getAll( fragment.appendChild( elem ), "script" );

		// Preserve script evaluation history
		if ( attached ) {
			setGlobalEval( tmp );
		}

		// Capture executables
		if ( scripts ) {
			j = 0;
			while ( ( elem = tmp[ j++ ] ) ) {
				if ( rscriptType.test( elem.type || "" ) ) {
					scripts.push( elem );
				}
			}
		}
	}

	return fragment;
}


( function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Android 4.0 - 4.3 only
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Android <=4.1 only
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE <=11 only
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
} )();


var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

// Support: IE <=9 - 11+
// focus() and blur() are asynchronous, except when they are no-op.
// So expect focus to be synchronous when the element is already active,
// and blur to be synchronous when the element is not already active.
// (focus and blur are always synchronous in other supported browsers,
// this just defines when we can count on it).
function expectSync( elem, type ) {
	return ( elem === safeActiveElement() ) === ( type === "focus" );
}

// Support: IE <=9 only
// Accessing document.activeElement can throw unexpectedly
// https://bugs.jquery.com/ticket/13393
function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
	var origFn, type;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {

		// ( types-Object, selector, data )
		if ( typeof selector !== "string" ) {

			// ( types-Object, data )
			data = data || selector;
			selector = undefined;
		}
		for ( type in types ) {
			on( elem, type, selector, data, types[ type ], one );
		}
		return elem;
	}

	if ( data == null && fn == null ) {

		// ( types, fn )
		fn = selector;
		data = selector = undefined;
	} else if ( fn == null ) {
		if ( typeof selector === "string" ) {

			// ( types, selector, fn )
			fn = data;
			data = undefined;
		} else {

			// ( types, data, fn )
			fn = data;
			data = selector;
			selector = undefined;
		}
	}
	if ( fn === false ) {
		fn = returnFalse;
	} else if ( !fn ) {
		return elem;
	}

	if ( one === 1 ) {
		origFn = fn;
		fn = function( event ) {

			// Can use an empty set, since event contains the info
			jQuery().off( event );
			return origFn.apply( this, arguments );
		};

		// Use same guid so caller can remove using origFn
		fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return elem.each( function() {
		jQuery.event.add( this, types, fn, data, selector );
	} );
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Ensure that invalid selectors throw exceptions at attach time
		// Evaluate against documentElement in case elem is a non-element node (e.g., document)
		if ( selector ) {
			jQuery.find.matchesSelector( documentElement, selector );
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !( events = elemData.events ) ) {
			events = elemData.events = {};
		}
		if ( !( eventHandle = elemData.handle ) ) {
			eventHandle = elemData.handle = function( e ) {

				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend( {
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join( "." )
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !( handlers = events[ type ] ) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup ||
					special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

		if ( !elemData || !( events = elemData.events ) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[ 2 ] &&
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector ||
						selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown ||
					special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove data and the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			dataPriv.remove( elem, "handle events" );
		}
	},

	dispatch: function( nativeEvent ) {

		// Make a writable jQuery.Event from the native event object
		var event = jQuery.event.fix( nativeEvent );

		var i, j, ret, matched, handleObj, handlerQueue,
			args = new Array( arguments.length ),
			handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[ 0 ] = event;

		for ( i = 1; i < arguments.length; i++ ) {
			args[ i ] = arguments[ i ];
		}

		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( ( handleObj = matched.handlers[ j++ ] ) &&
				!event.isImmediatePropagationStopped() ) {

				// If the event is namespaced, then each handler is only invoked if it is
				// specially universal or its namespaces are a superset of the event's.
				if ( !event.rnamespace || handleObj.namespace === false ||
					event.rnamespace.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
						handleObj.handler ).apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( ( event.result = ret ) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, handleObj, sel, matchedHandlers, matchedSelectors,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		if ( delegateCount &&

			// Support: IE <=9
			// Black-hole SVG <use> instance trees (trac-13180)
			cur.nodeType &&

			// Support: Firefox <=42
			// Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
			// https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
			// Support: IE 11 only
			// ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
			!( event.type === "click" && event.button >= 1 ) ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && !( event.type === "click" && cur.disabled === true ) ) {
					matchedHandlers = [];
					matchedSelectors = {};
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matchedSelectors[ sel ] === undefined ) {
							matchedSelectors[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) > -1 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matchedSelectors[ sel ] ) {
							matchedHandlers.push( handleObj );
						}
					}
					if ( matchedHandlers.length ) {
						handlerQueue.push( { elem: cur, handlers: matchedHandlers } );
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		cur = this;
		if ( delegateCount < handlers.length ) {
			handlerQueue.push( { elem: cur, handlers: handlers.slice( delegateCount ) } );
		}

		return handlerQueue;
	},

	addProp: function( name, hook ) {
		Object.defineProperty( jQuery.Event.prototype, name, {
			enumerable: true,
			configurable: true,

			get: isFunction( hook ) ?
				function() {
					if ( this.originalEvent ) {
							return hook( this.originalEvent );
					}
				} :
				function() {
					if ( this.originalEvent ) {
							return this.originalEvent[ name ];
					}
				},

			set: function( value ) {
				Object.defineProperty( this, name, {
					enumerable: true,
					configurable: true,
					writable: true,
					value: value
				} );
			}
		} );
	},

	fix: function( originalEvent ) {
		return originalEvent[ jQuery.expando ] ?
			originalEvent :
			new jQuery.Event( originalEvent );
	},

	special: {
		load: {

			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		click: {

			// Utilize native event to ensure correct state for checkable inputs
			setup: function( data ) {

				// For mutual compressibility with _default, replace `this` access with a local var.
				// `|| data` is dead code meant only to preserve the variable through minification.
				var el = this || data;

				// Claim the first handler
				if ( rcheckableType.test( el.type ) &&
					el.click && nodeName( el, "input" ) ) {

					// dataPriv.set( el, "click", ... )
					leverageNative( el, "click", returnTrue );
				}

				// Return false to allow normal processing in the caller
				return false;
			},
			trigger: function( data ) {

				// For mutual compressibility with _default, replace `this` access with a local var.
				// `|| data` is dead code meant only to preserve the variable through minification.
				var el = this || data;

				// Force setup before triggering a click
				if ( rcheckableType.test( el.type ) &&
					el.click && nodeName( el, "input" ) ) {

					leverageNative( el, "click" );
				}

				// Return non-false to allow normal event-path propagation
				return true;
			},

			// For cross-browser consistency, suppress native .click() on links
			// Also prevent it if we're currently inside a leveraged native-event stack
			_default: function( event ) {
				var target = event.target;
				return rcheckableType.test( target.type ) &&
					target.click && nodeName( target, "input" ) &&
					dataPriv.get( target, "click" ) ||
					nodeName( target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	}
};

// Ensure the presence of an event listener that handles manually-triggered
// synthetic events by interrupting progress until reinvoked in response to
// *native* events that it fires directly, ensuring that state changes have
// already occurred before other listeners are invoked.
function leverageNative( el, type, expectSync ) {

	// Missing expectSync indicates a trigger call, which must force setup through jQuery.event.add
	if ( !expectSync ) {
		if ( dataPriv.get( el, type ) === undefined ) {
			jQuery.event.add( el, type, returnTrue );
		}
		return;
	}

	// Register the controller as a special universal handler for all event namespaces
	dataPriv.set( el, type, false );
	jQuery.event.add( el, type, {
		namespace: false,
		handler: function( event ) {
			var notAsync, result,
				saved = dataPriv.get( this, type );

			if ( ( event.isTrigger & 1 ) && this[ type ] ) {

				// Interrupt processing of the outer synthetic .trigger()ed event
				// Saved data should be false in such cases, but might be a leftover capture object
				// from an async native handler (gh-4350)
				if ( !saved.length ) {

					// Store arguments for use when handling the inner native event
					// There will always be at least one argument (an event object), so this array
					// will not be confused with a leftover capture object.
					saved = slice.call( arguments );
					dataPriv.set( this, type, saved );

					// Trigger the native event and capture its result
					// Support: IE <=9 - 11+
					// focus() and blur() are asynchronous
					notAsync = expectSync( this, type );
					this[ type ]();
					result = dataPriv.get( this, type );
					if ( saved !== result || notAsync ) {
						dataPriv.set( this, type, false );
					} else {
						result = {};
					}
					if ( saved !== result ) {

						// Cancel the outer synthetic event
						event.stopImmediatePropagation();
						event.preventDefault();
						return result.value;
					}

				// If this is an inner synthetic event for an event with a bubbling surrogate
				// (focus or blur), assume that the surrogate already propagated from triggering the
				// native event and prevent that from happening again here.
				// This technically gets the ordering wrong w.r.t. to `.trigger()` (in which the
				// bubbling surrogate propagates *after* the non-bubbling base), but that seems
				// less bad than duplication.
				} else if ( ( jQuery.event.special[ type ] || {} ).delegateType ) {
					event.stopPropagation();
				}

			// If this is a native event triggered above, everything is now in order
			// Fire an inner synthetic event with the original arguments
			} else if ( saved.length ) {

				// ...and capture the result
				dataPriv.set( this, type, {
					value: jQuery.event.trigger(

						// Support: IE <=9 - 11+
						// Extend with the prototype to reset the above stopImmediatePropagation()
						jQuery.extend( saved[ 0 ], jQuery.Event.prototype ),
						saved.slice( 1 ),
						this
					)
				} );

				// Abort handling of the native event
				event.stopImmediatePropagation();
			}
		}
	} );
}

jQuery.removeEvent = function( elem, type, handle ) {

	// This "if" is needed for plain objects
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle );
	}
};

jQuery.Event = function( src, props ) {

	// Allow instantiation without the 'new' keyword
	if ( !( this instanceof jQuery.Event ) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&

				// Support: Android <=2.3 only
				src.returnValue === false ?
			returnTrue :
			returnFalse;

		// Create target properties
		// Support: Safari <=6 - 7 only
		// Target should not be a text node (#504, #13143)
		this.target = ( src.target && src.target.nodeType === 3 ) ?
			src.target.parentNode :
			src.target;

		this.currentTarget = src.currentTarget;
		this.relatedTarget = src.relatedTarget;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || Date.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	constructor: jQuery.Event,
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,
	isSimulated: false,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && !this.isSimulated ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Includes all common event props including KeyEvent and MouseEvent specific props
jQuery.each( {
	altKey: true,
	bubbles: true,
	cancelable: true,
	changedTouches: true,
	ctrlKey: true,
	detail: true,
	eventPhase: true,
	metaKey: true,
	pageX: true,
	pageY: true,
	shiftKey: true,
	view: true,
	"char": true,
	code: true,
	charCode: true,
	key: true,
	keyCode: true,
	button: true,
	buttons: true,
	clientX: true,
	clientY: true,
	offsetX: true,
	offsetY: true,
	pointerId: true,
	pointerType: true,
	screenX: true,
	screenY: true,
	targetTouches: true,
	toElement: true,
	touches: true,

	which: function( event ) {
		var button = event.button;

		// Add which for key events
		if ( event.which == null && rkeyEvent.test( event.type ) ) {
			return event.charCode != null ? event.charCode : event.keyCode;
		}

		// Add which for click: 1 === left; 2 === middle; 3 === right
		if ( !event.which && button !== undefined && rmouseEvent.test( event.type ) ) {
			if ( button & 1 ) {
				return 1;
			}

			if ( button & 2 ) {
				return 3;
			}

			if ( button & 4 ) {
				return 2;
			}

			return 0;
		}

		return event.which;
	}
}, jQuery.event.addProp );

jQuery.each( { focus: "focusin", blur: "focusout" }, function( type, delegateType ) {
	jQuery.event.special[ type ] = {

		// Utilize native event if possible so blur/focus sequence is correct
		setup: function() {

			// Claim the first handler
			// dataPriv.set( this, "focus", ... )
			// dataPriv.set( this, "blur", ... )
			leverageNative( this, type, expectSync );

			// Return false to allow normal processing in the caller
			return false;
		},
		trigger: function() {

			// Force setup before trigger
			leverageNative( this, type );

			// Return non-false to allow normal event-path propagation
			return true;
		},

		delegateType: delegateType
	};
} );

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://bugs.chromium.org/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mouseenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
} );

jQuery.fn.extend( {

	on: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn );
	},
	one: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {

			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ?
					handleObj.origType + "." + handleObj.namespace :
					handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {

			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {

			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each( function() {
			jQuery.event.remove( this, types, fn, selector );
		} );
	}
} );


var

	/* eslint-disable max-len */

	// See https://github.com/eslint/eslint/issues/3229
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,

	/* eslint-enable */

	// Support: IE <=10 - 11, Edge 12 - 13 only
	// In IE/Edge using regex groups here causes severe slowdowns.
	// See https://connect.microsoft.com/IE/feedback/details/1736512/
	rnoInnerhtml = /<script|<style|<link/i,

	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// Prefer a tbody over its parent table for containing new rows
function manipulationTarget( elem, content ) {
	if ( nodeName( elem, "table" ) &&
		nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {

		return jQuery( elem ).children( "tbody" )[ 0 ] || elem;
	}

	return elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	if ( ( elem.type || "" ).slice( 0, 5 ) === "true/" ) {
		elem.type = elem.type.slice( 5 );
	} else {
		elem.removeAttribute( "type" );
	}

	return elem;
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( dataPriv.hasData( src ) ) {
		pdataOld = dataPriv.access( src );
		pdataCur = dataPriv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( dataUser.hasData( src ) ) {
		udataOld = dataUser.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		dataUser.set( dest, udataCur );
	}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

function domManip( collection, args, callback, ignored ) {

	// Flatten any nested arrays
	args = concat.apply( [], args );

	var fragment, first, scripts, hasScripts, node, doc,
		i = 0,
		l = collection.length,
		iNoClone = l - 1,
		value = args[ 0 ],
		valueIsFunction = isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( valueIsFunction ||
			( l > 1 && typeof value === "string" &&
				!support.checkClone && rchecked.test( value ) ) ) {
		return collection.each( function( index ) {
			var self = collection.eq( index );
			if ( valueIsFunction ) {
				args[ 0 ] = value.call( this, index, self.html() );
			}
			domManip( self, args, callback, ignored );
		} );
	}

	if ( l ) {
		fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
		first = fragment.firstChild;

		if ( fragment.childNodes.length === 1 ) {
			fragment = first;
		}

		// Require either new content or an interest in ignored elements to invoke the callback
		if ( first || ignored ) {
			scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
			hasScripts = scripts.length;

			// Use the original fragment for the last item
			// instead of the first because it can end up
			// being emptied incorrectly in certain situations (#8070).
			for ( ; i < l; i++ ) {
				node = fragment;

				if ( i !== iNoClone ) {
					node = jQuery.clone( node, true, true );

					// Keep references to cloned scripts for later restoration
					if ( hasScripts ) {

						// Support: Android <=4.0 only, PhantomJS 1 only
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( scripts, getAll( node, "script" ) );
					}
				}

				callback.call( collection[ i ], node, i );
			}

			if ( hasScripts ) {
				doc = scripts[ scripts.length - 1 ].ownerDocument;

				// Reenable scripts
				jQuery.map( scripts, restoreScript );

				// Evaluate executable scripts on first document insertion
				for ( i = 0; i < hasScripts; i++ ) {
					node = scripts[ i ];
					if ( rscriptType.test( node.type || "" ) &&
						!dataPriv.access( node, "globalEval" ) &&
						jQuery.contains( doc, node ) ) {

						if ( node.src && ( node.type || "" ).toLowerCase()  !== "module" ) {

							// Optional AJAX dependency, but won't run scripts if not present
							if ( jQuery._evalUrl && !node.noModule ) {
								jQuery._evalUrl( node.src, {
									nonce: node.nonce || node.getAttribute( "nonce" )
								} );
							}
						} else {
							DOMEval( node.textContent.replace( rcleanScript, "" ), node, doc );
						}
					}
				}
			}
		}
	}

	return collection;
}

function remove( elem, selector, keepData ) {
	var node,
		nodes = selector ? jQuery.filter( selector, elem ) : elem,
		i = 0;

	for ( ; ( node = nodes[ i ] ) != null; i++ ) {
		if ( !keepData && node.nodeType === 1 ) {
			jQuery.cleanData( getAll( node ) );
		}

		if ( node.parentNode ) {
			if ( keepData && isAttached( node ) ) {
				setGlobalEval( getAll( node, "script" ) );
			}
			node.parentNode.removeChild( node );
		}
	}

	return elem;
}

jQuery.extend( {
	htmlPrefilter: function( html ) {
		return html.replace( rxhtmlTag, "<$1></$2>" );
	},

	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = isAttached( elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	cleanData: function( elems ) {
		var data, elem, type,
			special = jQuery.event.special,
			i = 0;

		for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
			if ( acceptData( elem ) ) {
				if ( ( data = elem[ dataPriv.expando ] ) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataPriv.expando ] = undefined;
				}
				if ( elem[ dataUser.expando ] ) {

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataUser.expando ] = undefined;
				}
			}
		}
	}
} );

jQuery.fn.extend( {
	detach: function( selector ) {
		return remove( this, selector, true );
	},

	remove: function( selector ) {
		return remove( this, selector );
	},

	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each( function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				} );
		}, null, value, arguments.length );
	},

	append: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		} );
	},

	prepend: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		} );
	},

	before: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		} );
	},

	after: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		} );
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; ( elem = this[ i ] ) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map( function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		} );
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = jQuery.htmlPrefilter( value );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch ( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var ignored = [];

		// Make the changes, replacing each non-ignored context element with the new content
		return domManip( this, arguments, function( elem ) {
			var parent = this.parentNode;

			if ( jQuery.inArray( this, ignored ) < 0 ) {
				jQuery.cleanData( getAll( this ) );
				if ( parent ) {
					parent.replaceChild( elem, this );
				}
			}

		// Force callback invocation
		}, ignored );
	}
} );

jQuery.each( {
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: Android <=4.0 only, PhantomJS 1 only
			// .get() because push.apply(_, arraylike) throws on ancient WebKit
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
} );
var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

		// Support: IE <=11 only, Firefox <=30 (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		var view = elem.ownerDocument.defaultView;

		if ( !view || !view.opener ) {
			view = window;
		}

		return view.getComputedStyle( elem );
	};

var rboxStyle = new RegExp( cssExpand.join( "|" ), "i" );



( function() {

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computeStyleTests() {

		// This is a singleton, we need to execute it only once
		if ( !div ) {
			return;
		}

		container.style.cssText = "position:absolute;left:-11111px;width:60px;" +
			"margin-top:1px;padding:0;border:0";
		div.style.cssText =
			"position:relative;display:block;box-sizing:border-box;overflow:scroll;" +
			"margin:auto;border:1px;padding:1px;" +
			"width:60%;top:1%";
		documentElement.appendChild( container ).appendChild( div );

		var divStyle = window.getComputedStyle( div );
		pixelPositionVal = divStyle.top !== "1%";

		// Support: Android 4.0 - 4.3 only, Firefox <=3 - 44
		reliableMarginLeftVal = roundPixelMeasures( divStyle.marginLeft ) === 12;

		// Support: Android 4.0 - 4.3 only, Safari <=9.1 - 10.1, iOS <=7.0 - 9.3
		// Some styles come back with percentage values, even though they shouldn't
		div.style.right = "60%";
		pixelBoxStylesVal = roundPixelMeasures( divStyle.right ) === 36;

		// Support: IE 9 - 11 only
		// Detect misreporting of content dimensions for box-sizing:border-box elements
		boxSizingReliableVal = roundPixelMeasures( divStyle.width ) === 36;

		// Support: IE 9 only
		// Detect overflow:scroll screwiness (gh-3699)
		// Support: Chrome <=64
		// Don't get tricked when zoom affects offsetWidth (gh-4029)
		div.style.position = "absolute";
		scrollboxSizeVal = roundPixelMeasures( div.offsetWidth / 3 ) === 12;

		documentElement.removeChild( container );

		// Nullify the div so it wouldn't be stored in the memory and
		// it will also be a sign that checks already performed
		div = null;
	}

	function roundPixelMeasures( measure ) {
		return Math.round( parseFloat( measure ) );
	}

	var pixelPositionVal, boxSizingReliableVal, scrollboxSizeVal, pixelBoxStylesVal,
		reliableMarginLeftVal,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	// Finish early in limited (non-browser) environments
	if ( !div.style ) {
		return;
	}

	// Support: IE <=9 - 11 only
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	jQuery.extend( support, {
		boxSizingReliable: function() {
			computeStyleTests();
			return boxSizingReliableVal;
		},
		pixelBoxStyles: function() {
			computeStyleTests();
			return pixelBoxStylesVal;
		},
		pixelPosition: function() {
			computeStyleTests();
			return pixelPositionVal;
		},
		reliableMarginLeft: function() {
			computeStyleTests();
			return reliableMarginLeftVal;
		},
		scrollboxSize: function() {
			computeStyleTests();
			return scrollboxSizeVal;
		}
	} );
} )();


function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,

		// Support: Firefox 51+
		// Retrieving style before computed somehow
		// fixes an issue with getting wrong values
		// on detached elements
		style = elem.style;

	computed = computed || getStyles( elem );

	// getPropertyValue is needed for:
	//   .css('filter') (IE 9 only, #12537)
	//   .css('--customProperty) (#3144)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];

		if ( ret === "" && !isAttached( elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// A tribute to the "awesome hack by Dean Edwards"
		// Android Browser returns percentage for some values,
		// but width seems to be reliably pixels.
		// This is against the CSSOM draft spec:
		// https://drafts.csswg.org/cssom/#resolved-values
		if ( !support.pixelBoxStyles() && rnumnonpx.test( ret ) && rboxStyle.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?

		// Support: IE <=9 - 11 only
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {

	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {

				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return ( this.get = hookFn ).apply( this, arguments );
		}
	};
}


var cssPrefixes = [ "Webkit", "Moz", "ms" ],
	emptyStyle = document.createElement( "div" ).style,
	vendorProps = {};

// Return a vendor-prefixed property or undefined
function vendorPropName( name ) {

	// Check for vendor prefixed names
	var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in emptyStyle ) {
			return name;
		}
	}
}

// Return a potentially-mapped jQuery.cssProps or vendor prefixed property
function finalPropName( name ) {
	var final = jQuery.cssProps[ name ] || vendorProps[ name ];

	if ( final ) {
		return final;
	}
	if ( name in emptyStyle ) {
		return name;
	}
	return vendorProps[ name ] = vendorPropName( name ) || name;
}


var

	// Swappable if display is none or starts with table
	// except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rcustomProp = /^--/,
	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	};

function setPositiveNumber( elem, value, subtract ) {

	// Any relative (+/-) values have already been
	// normalized at this point
	var matches = rcssNum.exec( value );
	return matches ?

		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
		value;
}

function boxModelAdjustment( elem, dimension, box, isBorderBox, styles, computedVal ) {
	var i = dimension === "width" ? 1 : 0,
		extra = 0,
		delta = 0;

	// Adjustment may not be necessary
	if ( box === ( isBorderBox ? "border" : "content" ) ) {
		return 0;
	}

	for ( ; i < 4; i += 2 ) {

		// Both box models exclude margin
		if ( box === "margin" ) {
			delta += jQuery.css( elem, box + cssExpand[ i ], true, styles );
		}

		// If we get here with a content-box, we're seeking "padding" or "border" or "margin"
		if ( !isBorderBox ) {

			// Add padding
			delta += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// For "border" or "margin", add border
			if ( box !== "padding" ) {
				delta += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );

			// But still keep track of it otherwise
			} else {
				extra += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}

		// If we get here with a border-box (content + padding + border), we're seeking "content" or
		// "padding" or "margin"
		} else {

			// For "content", subtract padding
			if ( box === "content" ) {
				delta -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// For "content" or "padding", subtract border
			if ( box !== "margin" ) {
				delta -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	// Account for positive content-box scroll gutter when requested by providing computedVal
	if ( !isBorderBox && computedVal >= 0 ) {

		// offsetWidth/offsetHeight is a rounded sum of content, padding, scroll gutter, and border
		// Assuming integer scroll gutter, subtract the rest and round down
		delta += Math.max( 0, Math.ceil(
			elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
			computedVal -
			delta -
			extra -
			0.5

		// If offsetWidth/offsetHeight is unknown, then we can't determine content-box scroll gutter
		// Use an explicit zero to avoid NaN (gh-3964)
		) ) || 0;
	}

	return delta;
}

function getWidthOrHeight( elem, dimension, extra ) {

	// Start with computed style
	var styles = getStyles( elem ),

		// To avoid forcing a reflow, only fetch boxSizing if we need it (gh-4322).
		// Fake content-box until we know it's needed to know the true value.
		boxSizingNeeded = !support.boxSizingReliable() || extra,
		isBorderBox = boxSizingNeeded &&
			jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
		valueIsBorderBox = isBorderBox,

		val = curCSS( elem, dimension, styles ),
		offsetProp = "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 );

	// Support: Firefox <=54
	// Return a confounding non-pixel value or feign ignorance, as appropriate.
	if ( rnumnonpx.test( val ) ) {
		if ( !extra ) {
			return val;
		}
		val = "auto";
	}


	// Fall back to offsetWidth/offsetHeight when value is "auto"
	// This happens for inline elements with no explicit setting (gh-3571)
	// Support: Android <=4.1 - 4.3 only
	// Also use offsetWidth/offsetHeight for misreported inline dimensions (gh-3602)
	// Support: IE 9-11 only
	// Also use offsetWidth/offsetHeight for when box sizing is unreliable
	// We use getClientRects() to check for hidden/disconnected.
	// In those cases, the computed value can be trusted to be border-box
	if ( ( !support.boxSizingReliable() && isBorderBox ||
		val === "auto" ||
		!parseFloat( val ) && jQuery.css( elem, "display", false, styles ) === "inline" ) &&
		elem.getClientRects().length ) {

		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

		// Where available, offsetWidth/offsetHeight approximate border box dimensions.
		// Where not available (e.g., SVG), assume unreliable box-sizing and interpret the
		// retrieved value as a content box dimension.
		valueIsBorderBox = offsetProp in elem;
		if ( valueIsBorderBox ) {
			val = elem[ offsetProp ];
		}
	}

	// Normalize "" and auto
	val = parseFloat( val ) || 0;

	// Adjust for the element's box model
	return ( val +
		boxModelAdjustment(
			elem,
			dimension,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles,

			// Provide the current computed size to request scroll gutter calculation (gh-3589)
			val
		)
	) + "px";
}

jQuery.extend( {

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"animationIterationCount": true,
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"gridArea": true,
		"gridColumn": true,
		"gridColumnEnd": true,
		"gridColumnStart": true,
		"gridRow": true,
		"gridRowEnd": true,
		"gridRowStart": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = camelCase( name ),
			isCustomProp = rcustomProp.test( name ),
			style = elem.style;

		// Make sure that we're working with the right name. We don't
		// want to query the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
				value = adjustCSS( elem, name, ret );

				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add the unit (except for certain CSS properties)
			// The isCustomProp check can be removed in jQuery 4.0 when we only auto-append
			// "px" to a few hardcoded values.
			if ( type === "number" && !isCustomProp ) {
				value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
			}

			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !( "set" in hooks ) ||
				( value = hooks.set( elem, value, extra ) ) !== undefined ) {

				if ( isCustomProp ) {
					style.setProperty( name, value );
				} else {
					style[ name ] = value;
				}
			}

		} else {

			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks &&
				( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = camelCase( name ),
			isCustomProp = rcustomProp.test( name );

		// Make sure that we're working with the right name. We don't
		// want to modify the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || isFinite( num ) ? num || 0 : val;
		}

		return val;
	}
} );

jQuery.each( [ "height", "width" ], function( i, dimension ) {
	jQuery.cssHooks[ dimension ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&

					// Support: Safari 8+
					// Table columns in Safari have non-zero offsetWidth & zero
					// getBoundingClientRect().width unless display is changed.
					// Support: IE <=11 only
					// Running getBoundingClientRect on a disconnected node
					// in IE throws an error.
					( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
						swap( elem, cssShow, function() {
							return getWidthOrHeight( elem, dimension, extra );
						} ) :
						getWidthOrHeight( elem, dimension, extra );
			}
		},

		set: function( elem, value, extra ) {
			var matches,
				styles = getStyles( elem ),

				// Only read styles.position if the test has a chance to fail
				// to avoid forcing a reflow.
				scrollboxSizeBuggy = !support.scrollboxSize() &&
					styles.position === "absolute",

				// To avoid forcing a reflow, only fetch boxSizing if we need it (gh-3991)
				boxSizingNeeded = scrollboxSizeBuggy || extra,
				isBorderBox = boxSizingNeeded &&
					jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
				subtract = extra ?
					boxModelAdjustment(
						elem,
						dimension,
						extra,
						isBorderBox,
						styles
					) :
					0;

			// Account for unreliable border-box dimensions by comparing offset* to computed and
			// faking a content-box to get border and padding (gh-3699)
			if ( isBorderBox && scrollboxSizeBuggy ) {
				subtract -= Math.ceil(
					elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
					parseFloat( styles[ dimension ] ) -
					boxModelAdjustment( elem, dimension, "border", false, styles ) -
					0.5
				);
			}

			// Convert to pixels if value adjustment is needed
			if ( subtract && ( matches = rcssNum.exec( value ) ) &&
				( matches[ 3 ] || "px" ) !== "px" ) {

				elem.style[ dimension ] = value;
				value = jQuery.css( elem, dimension );
			}

			return setPositiveNumber( elem, value, subtract );
		}
	};
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
	function( elem, computed ) {
		if ( computed ) {
			return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
				elem.getBoundingClientRect().left -
					swap( elem, { marginLeft: 0 }, function() {
						return elem.getBoundingClientRect().left;
					} )
				) + "px";
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each( {
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split( " " ) : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( prefix !== "margin" ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
} );

jQuery.fn.extend( {
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( Array.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	}
} );


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || jQuery.easing._default;
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			// Use a property on the element directly when it is not a DOM element,
			// or when there is no matching style property that exists.
			if ( tween.elem.nodeType !== 1 ||
				tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );

			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {

			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.nodeType === 1 && (
					jQuery.cssHooks[ tween.prop ] ||
					tween.elem.style[ finalPropName( tween.prop ) ] != null ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE <=9 only
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	},
	_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, inProgress,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rrun = /queueHooks$/;

function schedule() {
	if ( inProgress ) {
		if ( document.hidden === false && window.requestAnimationFrame ) {
			window.requestAnimationFrame( schedule );
		} else {
			window.setTimeout( schedule, jQuery.fx.interval );
		}

		jQuery.fx.tick();
	}
}

// Animations created synchronously will run synchronously
function createFxNow() {
	window.setTimeout( function() {
		fxNow = undefined;
	} );
	return ( fxNow = Date.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
		isBox = "width" in props || "height" in props,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHiddenWithinTree( elem ),
		dataShow = dataPriv.get( elem, "fxshow" );

	// Queue-skipping animations hijack the fx hooks
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always( function() {

			// Ensure the complete handler is called before this completes
			anim.always( function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			} );
		} );
	}

	// Detect show/hide animations
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.test( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// Pretend to be hidden if this is a "show" and
				// there is still data from a stopped show/hide
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;

				// Ignore all other no-op show/hide data
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
		}
	}

	// Bail out if this is a no-op like .hide().hide()
	propTween = !jQuery.isEmptyObject( props );
	if ( !propTween && jQuery.isEmptyObject( orig ) ) {
		return;
	}

	// Restrict "overflow" and "display" styles during box animations
	if ( isBox && elem.nodeType === 1 ) {

		// Support: IE <=9 - 11, Edge 12 - 15
		// Record all 3 overflow attributes because IE does not infer the shorthand
		// from identically-valued overflowX and overflowY and Edge just mirrors
		// the overflowX value there.
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Identify a display type, preferring old show/hide data over the CSS cascade
		restoreDisplay = dataShow && dataShow.display;
		if ( restoreDisplay == null ) {
			restoreDisplay = dataPriv.get( elem, "display" );
		}
		display = jQuery.css( elem, "display" );
		if ( display === "none" ) {
			if ( restoreDisplay ) {
				display = restoreDisplay;
			} else {

				// Get nonempty value(s) by temporarily forcing visibility
				showHide( [ elem ], true );
				restoreDisplay = elem.style.display || restoreDisplay;
				display = jQuery.css( elem, "display" );
				showHide( [ elem ] );
			}
		}

		// Animate inline elements as inline-block
		if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
			if ( jQuery.css( elem, "float" ) === "none" ) {

				// Restore the original display value at the end of pure show/hide animations
				if ( !propTween ) {
					anim.done( function() {
						style.display = restoreDisplay;
					} );
					if ( restoreDisplay == null ) {
						display = style.display;
						restoreDisplay = display === "none" ? "" : display;
					}
				}
				style.display = "inline-block";
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always( function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		} );
	}

	// Implement show/hide animations
	propTween = false;
	for ( prop in orig ) {

		// General show/hide setup for this element animation
		if ( !propTween ) {
			if ( dataShow ) {
				if ( "hidden" in dataShow ) {
					hidden = dataShow.hidden;
				}
			} else {
				dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
			}

			// Store hidden/visible for toggle so `.stop().toggle()` "reverses"
			if ( toggle ) {
				dataShow.hidden = !hidden;
			}

			// Show elements before animating them
			if ( hidden ) {
				showHide( [ elem ], true );
			}

			/* eslint-disable no-loop-func */

			anim.done( function() {

			/* eslint-enable no-loop-func */

				// The final step of a "hide" animation is actually hiding the element
				if ( !hidden ) {
					showHide( [ elem ] );
				}
				dataPriv.remove( elem, "fxshow" );
				for ( prop in orig ) {
					jQuery.style( elem, prop, orig[ prop ] );
				}
			} );
		}

		// Per-property setup
		propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
		if ( !( prop in dataShow ) ) {
			dataShow[ prop ] = propTween.start;
			if ( hidden ) {
				propTween.end = propTween.start;
				propTween.start = 0;
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( Array.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = Animation.prefilters.length,
		deferred = jQuery.Deferred().always( function() {

			// Don't match elem in the :animated selector
			delete tick.elem;
		} ),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

				// Support: Android 2.3 only
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ] );

			// If there's more to do, yield
			if ( percent < 1 && length ) {
				return remaining;
			}

			// If this was an empty animation, synthesize a final progress notification
			if ( !length ) {
				deferred.notifyWith( elem, [ animation, 1, 0 ] );
			}

			// Resolve the animation and report its conclusion
			deferred.resolveWith( elem, [ animation ] );
			return false;
		},
		animation = deferred.promise( {
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, {
				specialEasing: {},
				easing: jQuery.easing._default
			}, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,

					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.notifyWith( elem, [ animation, 1, 0 ] );
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		} ),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length; index++ ) {
		result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			if ( isFunction( result.stop ) ) {
				jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
					result.stop.bind( result );
			}
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	// Attach callbacks from options
	animation
		.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		} )
	);

	return animation;
}

jQuery.Animation = jQuery.extend( Animation, {

	tweeners: {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value );
			adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
			return tween;
		} ]
	},

	tweener: function( props, callback ) {
		if ( isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.match( rnothtmlwhite );
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length; index++ ) {
			prop = props[ index ];
			Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
			Animation.tweeners[ prop ].unshift( callback );
		}
	},

	prefilters: [ defaultPrefilter ],

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			Animation.prefilters.unshift( callback );
		} else {
			Animation.prefilters.push( callback );
		}
	}
} );

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !isFunction( easing ) && easing
	};

	// Go to the end state if fx are off
	if ( jQuery.fx.off ) {
		opt.duration = 0;

	} else {
		if ( typeof opt.duration !== "number" ) {
			if ( opt.duration in jQuery.fx.speeds ) {
				opt.duration = jQuery.fx.speeds[ opt.duration ];

			} else {
				opt.duration = jQuery.fx.speeds._default;
			}
		}
	}

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend( {
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHiddenWithinTree ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate( { opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {

				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || dataPriv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each( function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = dataPriv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this &&
					( type == null || timers[ index ].queue === type ) ) {

					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		} );
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each( function() {
			var index,
				data = dataPriv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		} );
	}
} );

jQuery.each( [ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
} );

// Generate shortcuts for custom animations
jQuery.each( {
	slideDown: genFx( "show" ),
	slideUp: genFx( "hide" ),
	slideToggle: genFx( "toggle" ),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = Date.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];

		// Run the timer and safely remove it when done (allowing for external removal)
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	jQuery.fx.start();
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
	if ( inProgress ) {
		return;
	}

	inProgress = true;
	schedule();
};

jQuery.fx.stop = function() {
	inProgress = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,

	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// https://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = window.setTimeout( next, time );
		hooks.stop = function() {
			window.clearTimeout( timeout );
		};
	} );
};


( function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: Android <=4.3 only
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE <=11 only
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: IE <=11 only
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
} )();


var boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each( function() {
			jQuery.removeAttr( this, name );
		} );
	}
} );

jQuery.extend( {
	attr: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set attributes on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		// Attribute hooks are determined by the lowercase version
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			hooks = jQuery.attrHooks[ name.toLowerCase() ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
		}

		if ( value !== undefined ) {
			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;
			}

			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			elem.setAttribute( name, value + "" );
			return value;
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		ret = jQuery.find.attr( elem, name );

		// Non-existent attributes return null, we normalize to undefined
		return ret == null ? undefined : ret;
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	},

	removeAttr: function( elem, value ) {
		var name,
			i = 0,

			// Attribute names can contain non-HTML whitespace characters
			// https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
			attrNames = value && value.match( rnothtmlwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( ( name = attrNames[ i++ ] ) ) {
				elem.removeAttribute( name );
			}
		}
	}
} );

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {

			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};

jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle,
			lowercaseName = name.toLowerCase();

		if ( !isXML ) {

			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ lowercaseName ];
			attrHandle[ lowercaseName ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				lowercaseName :
				null;
			attrHandle[ lowercaseName ] = handle;
		}
		return ret;
	};
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each( function() {
			delete this[ jQuery.propFix[ name ] || name ];
		} );
	}
} );

jQuery.extend( {
	prop: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			return ( elem[ name ] = value );
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		return elem[ name ];
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {

				// Support: IE <=9 - 11 only
				// elem.tabIndex doesn't always return the
				// correct value when it hasn't been explicitly set
				// https://web.archive.org/web/20141116233347/http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				if ( tabindex ) {
					return parseInt( tabindex, 10 );
				}

				if (
					rfocusable.test( elem.nodeName ) ||
					rclickable.test( elem.nodeName ) &&
					elem.href
				) {
					return 0;
				}

				return -1;
			}
		}
	},

	propFix: {
		"for": "htmlFor",
		"class": "className"
	}
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
// eslint rule "no-unused-expressions" is disabled for this code
// since it considers such accessions noop
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		},
		set: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent ) {
				parent.selectedIndex;

				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
		}
	};
}

jQuery.each( [
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
} );




	// Strip and collapse whitespace according to HTML spec
	// https://infra.spec.whatwg.org/#strip-and-collapse-ascii-whitespace
	function stripAndCollapse( value ) {
		var tokens = value.match( rnothtmlwhite ) || [];
		return tokens.join( " " );
	}


function getClass( elem ) {
	return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

function classesToArray( value ) {
	if ( Array.isArray( value ) ) {
		return value;
	}
	if ( typeof value === "string" ) {
		return value.match( rnothtmlwhite ) || [];
	}
	return [];
}

jQuery.fn.extend( {
	addClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		classes = classesToArray( value );

		if ( classes.length ) {
			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( !arguments.length ) {
			return this.attr( "class", "" );
		}

		classes = classesToArray( value );

		if ( classes.length ) {
			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );

				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {

						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value,
			isValidValue = type === "string" || Array.isArray( value );

		if ( typeof stateVal === "boolean" && isValidValue ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( isFunction( value ) ) {
			return this.each( function( i ) {
				jQuery( this ).toggleClass(
					value.call( this, i, getClass( this ), stateVal ),
					stateVal
				);
			} );
		}

		return this.each( function() {
			var className, i, self, classNames;

			if ( isValidValue ) {

				// Toggle individual class names
				i = 0;
				self = jQuery( this );
				classNames = classesToArray( value );

				while ( ( className = classNames[ i++ ] ) ) {

					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( value === undefined || type === "boolean" ) {
				className = getClass( this );
				if ( className ) {

					// Store className if set
					dataPriv.set( this, "__className__", className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				if ( this.setAttribute ) {
					this.setAttribute( "class",
						className || value === false ?
						"" :
						dataPriv.get( this, "__className__" ) || ""
					);
				}
			}
		} );
	},

	hasClass: function( selector ) {
		var className, elem,
			i = 0;

		className = " " + selector + " ";
		while ( ( elem = this[ i++ ] ) ) {
			if ( elem.nodeType === 1 &&
				( " " + stripAndCollapse( getClass( elem ) ) + " " ).indexOf( className ) > -1 ) {
					return true;
			}
		}

		return false;
	}
} );




var rreturn = /\r/g;

jQuery.fn.extend( {
	val: function( value ) {
		var hooks, ret, valueIsFunction,
			elem = this[ 0 ];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] ||
					jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks &&
					"get" in hooks &&
					( ret = hooks.get( elem, "value" ) ) !== undefined
				) {
					return ret;
				}

				ret = elem.value;

				// Handle most common string cases
				if ( typeof ret === "string" ) {
					return ret.replace( rreturn, "" );
				}

				// Handle cases where value is null/undef or number
				return ret == null ? "" : ret;
			}

			return;
		}

		valueIsFunction = isFunction( value );

		return this.each( function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( valueIsFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( Array.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				} );
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		} );
	}
} );

jQuery.extend( {
	valHooks: {
		option: {
			get: function( elem ) {

				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :

					// Support: IE <=10 - 11 only
					// option.text throws exceptions (#14686, #14858)
					// Strip and collapse whitespace
					// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
					stripAndCollapse( jQuery.text( elem ) );
			}
		},
		select: {
			get: function( elem ) {
				var value, option, i,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one",
					values = one ? null : [],
					max = one ? index + 1 : options.length;

				if ( index < 0 ) {
					i = max;

				} else {
					i = one ? index : 0;
				}

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// Support: IE <=9 only
					// IE8-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&

							// Don't return options that are disabled or in a disabled optgroup
							!option.disabled &&
							( !option.parentNode.disabled ||
								!nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];

					/* eslint-disable no-cond-assign */

					if ( option.selected =
						jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
					) {
						optionSet = true;
					}

					/* eslint-enable no-cond-assign */
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( Array.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute( "value" ) === null ? "on" : elem.value;
		};
	}
} );




// Return jQuery for attributes-only inclusion


support.focusin = "onfocusin" in window;


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	stopPropagationCallback = function( e ) {
		e.stopPropagation();
	};

jQuery.extend( jQuery.event, {

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special, lastElement,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

		cur = lastElement = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf( "." ) > -1 ) {

			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split( "." );
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf( ":" ) < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join( "." );
		event.rnamespace = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === ( elem.ownerDocument || document ) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {
			lastElement = cur;
			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
				dataPriv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( ( !special._default ||
				special._default.apply( eventPath.pop(), data ) === false ) &&
				acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && isFunction( elem[ type ] ) && !isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;

					if ( event.isPropagationStopped() ) {
						lastElement.addEventListener( type, stopPropagationCallback );
					}

					elem[ type ]();

					if ( event.isPropagationStopped() ) {
						lastElement.removeEventListener( type, stopPropagationCallback );
					}

					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	// Piggyback on a donor event to simulate a different one
	// Used only for `focus(in | out)` events
	simulate: function( type, elem, event ) {
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true
			}
		);

		jQuery.event.trigger( e, null, elem );
	}

} );

jQuery.fn.extend( {

	trigger: function( type, data ) {
		return this.each( function() {
			jQuery.event.trigger( type, data, this );
		} );
	},
	triggerHandler: function( type, data ) {
		var elem = this[ 0 ];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
} );


// Support: Firefox <=44
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
	jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
			jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
		};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					dataPriv.remove( doc, fix );

				} else {
					dataPriv.access( doc, fix, attaches );
				}
			}
		};
	} );
}
var location = window.location;

var nonce = Date.now();

var rquery = ( /\?/ );



// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE 9 - 11 only
	// IE throws on parseFromString with invalid input.
	try {
		xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( Array.isArray( obj ) ) {

		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {

				// Treat each array item as a scalar.
				add( prefix, v );

			} else {

				// Item is non-scalar (array or object), encode its numeric index.
				buildParams(
					prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
					v,
					traditional,
					add
				);
			}
		} );

	} else if ( !traditional && toType( obj ) === "object" ) {

		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {

		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, valueOrFunction ) {

			// If value is a function, invoke it and use its return value
			var value = isFunction( valueOrFunction ) ?
				valueOrFunction() :
				valueOrFunction;

			s[ s.length ] = encodeURIComponent( key ) + "=" +
				encodeURIComponent( value == null ? "" : value );
		};

	if ( a == null ) {
		return "";
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( Array.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		} );

	} else {

		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" );
};

jQuery.fn.extend( {
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map( function() {

			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		} )
		.filter( function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		} )
		.map( function( i, elem ) {
			var val = jQuery( this ).val();

			if ( val == null ) {
				return null;
			}

			if ( Array.isArray( val ) ) {
				return jQuery.map( val, function( val ) {
					return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
				} );
			}

			return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		} ).get();
	}
} );


var
	r20 = /%20/g,
	rhash = /#.*$/,
	rantiCache = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Anchor tag for parsing the document origin
	originAnchor = document.createElement( "a" );
	originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnothtmlwhite ) || [];

		if ( isFunction( func ) ) {

			// For each dataType in the dataTypeExpression
			while ( ( dataType = dataTypes[ i++ ] ) ) {

				// Prepend if requested
				if ( dataType[ 0 ] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

				// Otherwise append
				} else {
					( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" &&
				!seekingTransport && !inspected[ dataTypeOrTransport ] ) {

				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		} );
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {

		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}

		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},

		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

			// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {

								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s.throws ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return {
								state: "parsererror",
								error: conv ? e : "No conversion from " + prev + " to " + current
							};
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend( {

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: location.href,
		type: "GET",
		isLocal: rlocalProtocol.test( location.protocol ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",

		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /\bxml\b/,
			html: /\bhtml/,
			json: /\bjson\b/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": JSON.parse,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,

			// URL without anti-cache param
			cacheURL,

			// Response headers
			responseHeadersString,
			responseHeaders,

			// timeout handle
			timeoutTimer,

			// Url cleanup var
			urlAnchor,

			// Request state (becomes false upon send and true upon completion)
			completed,

			// To know if global events are to be dispatched
			fireGlobals,

			// Loop variable
			i,

			// uncached part of the url
			uncached,

			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),

			// Callbacks context
			callbackContext = s.context || s,

			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context &&
				( callbackContext.nodeType || callbackContext.jquery ) ?
					jQuery( callbackContext ) :
					jQuery.event,

			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks( "once memory" ),

			// Status-dependent callbacks
			statusCode = s.statusCode || {},

			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},

			// Default abort message
			strAbort = "canceled",

			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( completed ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
								responseHeaders[ match[ 1 ].toLowerCase() + " " ] =
									( responseHeaders[ match[ 1 ].toLowerCase() + " " ] || [] )
										.concat( match[ 2 ] );
							}
						}
						match = responseHeaders[ key.toLowerCase() + " " ];
					}
					return match == null ? null : match.join( ", " );
				},

				// Raw string
				getAllResponseHeaders: function() {
					return completed ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					if ( completed == null ) {
						name = requestHeadersNames[ name.toLowerCase() ] =
							requestHeadersNames[ name.toLowerCase() ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( completed == null ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( completed ) {

							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						} else {

							// Lazy-add the new callbacks in a way that preserves old ones
							for ( code in map ) {
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR );

		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || location.href ) + "" )
			.replace( rprotocol, location.protocol + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = ( s.dataType || "*" ).toLowerCase().match( rnothtmlwhite ) || [ "" ];

		// A cross-domain request is in order when the origin doesn't match the current origin.
		if ( s.crossDomain == null ) {
			urlAnchor = document.createElement( "a" );

			// Support: IE <=8 - 11, Edge 12 - 15
			// IE throws exception on accessing the href property if url is malformed,
			// e.g. http://example.com:80x/
			try {
				urlAnchor.href = s.url;

				// Support: IE <=8 - 11 only
				// Anchor's host property isn't correctly set when s.url is relative
				urlAnchor.href = urlAnchor.href;
				s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
					urlAnchor.protocol + "//" + urlAnchor.host;
			} catch ( e ) {

				// If there is an error parsing the URL, assume it is crossDomain,
				// it can be rejected by the transport if it is invalid
				s.crossDomain = true;
			}
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( completed ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger( "ajaxStart" );
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		// Remove hash to simplify url manipulation
		cacheURL = s.url.replace( rhash, "" );

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// Remember the hash so we can put it back
			uncached = s.url.slice( cacheURL.length );

			// If data is available and should be processed, append data to url
			if ( s.data && ( s.processData || typeof s.data === "string" ) ) {
				cacheURL += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data;

				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add or update anti-cache param if needed
			if ( s.cache === false ) {
				cacheURL = cacheURL.replace( rantiCache, "$1" );
				uncached = ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ( nonce++ ) + uncached;
			}

			// Put hash and anti-cache on the URL that will be requested (gh-1732)
			s.url = cacheURL + uncached;

		// Change '%20' to '+' if this is encoded form body content (gh-2658)
		} else if ( s.data && s.processData &&
			( s.contentType || "" ).indexOf( "application/x-www-form-urlencoded" ) === 0 ) {
			s.data = s.data.replace( r20, "+" );
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
				s.accepts[ s.dataTypes[ 0 ] ] +
					( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend &&
			( s.beforeSend.call( callbackContext, jqXHR, s ) === false || completed ) ) {

			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		completeDeferred.add( s.complete );
		jqXHR.done( s.success );
		jqXHR.fail( s.error );

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}

			// If request was aborted inside ajaxSend, stop there
			if ( completed ) {
				return jqXHR;
			}

			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = window.setTimeout( function() {
					jqXHR.abort( "timeout" );
				}, s.timeout );
			}

			try {
				completed = false;
				transport.send( requestHeaders, done );
			} catch ( e ) {

				// Rethrow post-completion exceptions
				if ( completed ) {
					throw e;
				}

				// Propagate others as results
				done( -1, e );
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Ignore repeat invocations
			if ( completed ) {
				return;
			}

			completed = true;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				window.clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader( "Last-Modified" );
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader( "etag" );
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {

				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger( "ajaxStop" );
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
} );

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {

		// Shift arguments if data argument was omitted
		if ( isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		// The url can be an options object (which then must have .url)
		return jQuery.ajax( jQuery.extend( {
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		}, jQuery.isPlainObject( url ) && url ) );
	};
} );


jQuery._evalUrl = function( url, options ) {
	return jQuery.ajax( {
		url: url,

		// Make this explicit, since user can override this through ajaxSetup (#11264)
		type: "GET",
		dataType: "script",
		cache: true,
		async: false,
		global: false,

		// Only evaluate the response if it is successful (gh-4126)
		// dataFilter is not invoked for failure responses, so using it instead
		// of the default converter is kludgy but it works.
		converters: {
			"text script": function() {}
		},
		dataFilter: function( response ) {
			jQuery.globalEval( response, options );
		}
	} );
};


jQuery.fn.extend( {
	wrapAll: function( html ) {
		var wrap;

		if ( this[ 0 ] ) {
			if ( isFunction( html ) ) {
				html = html.call( this[ 0 ] );
			}

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map( function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			} ).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapInner( html.call( this, i ) );
			} );
		}

		return this.each( function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		} );
	},

	wrap: function( html ) {
		var htmlIsFunction = isFunction( html );

		return this.each( function( i ) {
			jQuery( this ).wrapAll( htmlIsFunction ? html.call( this, i ) : html );
		} );
	},

	unwrap: function( selector ) {
		this.parent( selector ).not( "body" ).each( function() {
			jQuery( this ).replaceWith( this.childNodes );
		} );
		return this;
	}
} );


jQuery.expr.pseudos.hidden = function( elem ) {
	return !jQuery.expr.pseudos.visible( elem );
};
jQuery.expr.pseudos.visible = function( elem ) {
	return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
};




jQuery.ajaxSettings.xhr = function() {
	try {
		return new window.XMLHttpRequest();
	} catch ( e ) {}
};

var xhrSuccessStatus = {

		// File protocol always yields status code 0, assume 200
		0: 200,

		// Support: IE <=9 only
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
	var callback, errorCallback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr();

				xhr.open(
					options.type,
					options.url,
					options.async,
					options.username,
					options.password
				);

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
					headers[ "X-Requested-With" ] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							callback = errorCallback = xhr.onload =
								xhr.onerror = xhr.onabort = xhr.ontimeout =
									xhr.onreadystatechange = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {

								// Support: IE <=9 only
								// On a manual native abort, IE9 throws
								// errors on any property access that is not readyState
								if ( typeof xhr.status !== "number" ) {
									complete( 0, "error" );
								} else {
									complete(

										// File: protocol always yields status 0; see #8605, #14207
										xhr.status,
										xhr.statusText
									);
								}
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,

									// Support: IE <=9 only
									// IE9 has no XHR2 but throws on binary (trac-11426)
									// For XHR2 non-text, let the caller handle it (gh-2498)
									( xhr.responseType || "text" ) !== "text"  ||
									typeof xhr.responseText !== "string" ?
										{ binary: xhr.response } :
										{ text: xhr.responseText },
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				errorCallback = xhr.onerror = xhr.ontimeout = callback( "error" );

				// Support: IE 9 only
				// Use onreadystatechange to replace onabort
				// to handle uncaught aborts
				if ( xhr.onabort !== undefined ) {
					xhr.onabort = errorCallback;
				} else {
					xhr.onreadystatechange = function() {

						// Check readyState before timeout as it changes
						if ( xhr.readyState === 4 ) {

							// Allow onerror to be called first,
							// but that will not handle a native abort
							// Also, save errorCallback to a variable
							// as xhr.onerror cannot be accessed
							window.setTimeout( function() {
								if ( callback ) {
									errorCallback();
								}
							} );
						}
					};
				}

				// Create the abort callback
				callback = callback( "abort" );

				try {

					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {

					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




// Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)
jQuery.ajaxPrefilter( function( s ) {
	if ( s.crossDomain ) {
		s.contents.script = false;
	}
} );

// Install script dataType
jQuery.ajaxSetup( {
	accepts: {
		script: "text/javascript, application/javascript, " +
			"application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /\b(?:java|ecma)script\b/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

	// This transport only deals with cross domain or forced-by-attrs requests
	if ( s.crossDomain || s.scriptAttrs ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery( "<script>" )
					.attr( s.scriptAttrs || {} )
					.prop( { charset: s.scriptCharset, src: s.url } )
					.on( "load error", callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					} );

				// Use native DOM manipulation to avoid our domManip AJAX trickery
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" &&
				( s.contentType || "" )
					.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
				rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters[ "script json" ] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// Force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always( function() {

			// If previous value didn't exist - remove it
			if ( overwritten === undefined ) {
				jQuery( window ).removeProp( callbackName );

			// Otherwise restore preexisting value
			} else {
				window[ callbackName ] = overwritten;
			}

			// Save back as free
			if ( s[ callbackName ] ) {

				// Make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// Save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		} );

		// Delegate to script
		return "script";
	}
} );




// Support: Safari 8 only
// In Safari 8 documents created via document.implementation.createHTMLDocument
// collapse sibling forms: the second one becomes a child of the first one.
// Because of that, this security measure has to be disabled in Safari 8.
// https://bugs.webkit.org/show_bug.cgi?id=137337
support.createHTMLDocument = ( function() {
	var body = document.implementation.createHTMLDocument( "" ).body;
	body.innerHTML = "<form></form><form></form>";
	return body.childNodes.length === 2;
} )();


// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( typeof data !== "string" ) {
		return [];
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}

	var base, parsed, scripts;

	if ( !context ) {

		// Stop scripts or inline event handlers from being executed immediately
		// by using document.implementation
		if ( support.createHTMLDocument ) {
			context = document.implementation.createHTMLDocument( "" );

			// Set the base href for the created document
			// so any parsed elements with URLs
			// are based on the document's URL (gh-2965)
			base = context.createElement( "base" );
			base.href = document.location.href;
			context.head.appendChild( base );
		} else {
			context = document;
		}
	}

	parsed = rsingleTag.exec( data );
	scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[ 1 ] ) ];
	}

	parsed = buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	var selector, type, response,
		self = this,
		off = url.indexOf( " " );

	if ( off > -1 ) {
		selector = stripAndCollapse( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax( {
			url: url,

			// If "type" variable is undefined, then "GET" method will be used.
			// Make value of this field explicit since
			// user can override it through ajaxSetup method
			type: type || "GET",
			dataType: "html",
			data: params
		} ).done( function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		// If the request succeeds, this function gets "data", "status", "jqXHR"
		// but they are ignored because response was set above.
		// If it fails, this function gets "jqXHR", "status", "error"
		} ).always( callback && function( jqXHR, status ) {
			self.each( function() {
				callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
			} );
		} );
	}

	return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [
	"ajaxStart",
	"ajaxStop",
	"ajaxComplete",
	"ajaxError",
	"ajaxSuccess",
	"ajaxSend"
], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
} );




jQuery.expr.pseudos.animated = function( elem ) {
	return jQuery.grep( jQuery.timers, function( fn ) {
		return elem === fn.elem;
	} ).length;
};




jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( isFunction( options ) ) {

			// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
			options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend( {

	// offset() relates an element's border box to the document origin
	offset: function( options ) {

		// Preserve chaining for setter
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each( function( i ) {
					jQuery.offset.setOffset( this, options, i );
				} );
		}

		var rect, win,
			elem = this[ 0 ];

		if ( !elem ) {
			return;
		}

		// Return zeros for disconnected and hidden (display: none) elements (gh-2310)
		// Support: IE <=11 only
		// Running getBoundingClientRect on a
		// disconnected node in IE throws an error
		if ( !elem.getClientRects().length ) {
			return { top: 0, left: 0 };
		}

		// Get document-relative position by adding viewport scroll to viewport-relative gBCR
		rect = elem.getBoundingClientRect();
		win = elem.ownerDocument.defaultView;
		return {
			top: rect.top + win.pageYOffset,
			left: rect.left + win.pageXOffset
		};
	},

	// position() relates an element's margin box to its offset parent's padding box
	// This corresponds to the behavior of CSS absolute positioning
	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset, doc,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// position:fixed elements are offset from the viewport, which itself always has zero offset
		if ( jQuery.css( elem, "position" ) === "fixed" ) {

			// Assume position:fixed implies availability of getBoundingClientRect
			offset = elem.getBoundingClientRect();

		} else {
			offset = this.offset();

			// Account for the *real* offset parent, which can be the document or its root element
			// when a statically positioned element is identified
			doc = elem.ownerDocument;
			offsetParent = elem.offsetParent || doc.documentElement;
			while ( offsetParent &&
				( offsetParent === doc.body || offsetParent === doc.documentElement ) &&
				jQuery.css( offsetParent, "position" ) === "static" ) {

				offsetParent = offsetParent.parentNode;
			}
			if ( offsetParent && offsetParent !== elem && offsetParent.nodeType === 1 ) {

				// Incorporate borders into its offset, since they are outside its content origin
				parentOffset = jQuery( offsetParent ).offset();
				parentOffset.top += jQuery.css( offsetParent, "borderTopWidth", true );
				parentOffset.left += jQuery.css( offsetParent, "borderLeftWidth", true );
			}
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	// This method will return documentElement in the following cases:
	// 1) For the element inside the iframe without offsetParent, this method will return
	//    documentElement of the parent window
	// 2) For the hidden or detached element
	// 3) For body or html element, i.e. in case of the html node - it will return itself
	//
	// but those exceptions were never presented as a real life use-cases
	// and might be considered as more preferable results.
	//
	// This logic, however, is not guaranteed and can change at any point in the future
	offsetParent: function() {
		return this.map( function() {
			var offsetParent = this.offsetParent;

			while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || documentElement;
		} );
	}
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {

			// Coalesce documents and windows
			var win;
			if ( isWindow( elem ) ) {
				win = elem;
			} else if ( elem.nodeType === 9 ) {
				win = elem.defaultView;
			}

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : win.pageXOffset,
					top ? val : win.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length );
	};
} );

// Support: Safari <=7 - 9.1, Chrome <=37 - 49
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );

				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
		function( defaultExtra, funcName ) {

		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( isWindow( elem ) ) {

					// $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
					return funcName.indexOf( "outer" ) === 0 ?
						elem[ "inner" + name ] :
						elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?

					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable );
		};
	} );
} );


jQuery.each( ( "blur focus focusin focusout resize scroll click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup contextmenu" ).split( " " ),
	function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
} );

jQuery.fn.extend( {
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	}
} );




jQuery.fn.extend( {

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {

		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ?
			this.off( selector, "**" ) :
			this.off( types, selector || "**", fn );
	}
} );

// Bind a function to a context, optionally partially applying any
// arguments.
// jQuery.proxy is deprecated to promote standards (specifically Function#bind)
// However, it is not slated for removal any time soon
jQuery.proxy = function( fn, context ) {
	var tmp, args, proxy;

	if ( typeof context === "string" ) {
		tmp = fn[ context ];
		context = fn;
		fn = tmp;
	}

	// Quick check to determine if target is callable, in the spec
	// this throws a TypeError, but we will just return undefined.
	if ( !isFunction( fn ) ) {
		return undefined;
	}

	// Simulated bind
	args = slice.call( arguments, 2 );
	proxy = function() {
		return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
	};

	// Set the guid of unique handler to the same of original handler, so it can be removed
	proxy.guid = fn.guid = fn.guid || jQuery.guid++;

	return proxy;
};

jQuery.holdReady = function( hold ) {
	if ( hold ) {
		jQuery.readyWait++;
	} else {
		jQuery.ready( true );
	}
};
jQuery.isArray = Array.isArray;
jQuery.parseJSON = JSON.parse;
jQuery.nodeName = nodeName;
jQuery.isFunction = isFunction;
jQuery.isWindow = isWindow;
jQuery.camelCase = camelCase;
jQuery.type = toType;

jQuery.now = Date.now;

jQuery.isNumeric = function( obj ) {

	// As of jQuery 3.0, isNumeric is limited to
	// strings and numbers (primitives or objects)
	// that can be coerced to finite numbers (gh-2662)
	var type = jQuery.type( obj );
	return ( type === "number" || type === "string" ) &&

		// parseFloat NaNs numeric-cast false positives ("")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		!isNaN( obj - parseFloat( obj ) );
};




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	} );
}




var

	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( !noGlobal ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;
} );
// Импортируем другие js-файлы

/* @preserve
    _____ __ _     __                _
   / ___// /(_)___/ /___  ____      (_)___
  / (_ // // // _  // -_)/ __/_    / /(_-<
  \___//_//_/ \_,_/ \__//_/  (_)__/ //___/
                              |___/

  Version: 1.7.3
  Author: Nick Piscitelli (pickykneee)
  Website: https://nickpiscitelli.com
  Documentation: http://nickpiscitelli.github.io/Glider.js
  License: MIT License
  Release Date: October 25th, 2018

*/

/* global define */

(function (factory) {
  typeof define === 'function' && define.amd
    ? define(factory)
    : typeof exports === 'object'
      ? (module.exports = factory())
      : factory()
})(function () {
  ('use strict') // eslint-disable-line no-unused-expressions

  /* globals window:true */
  var _window = typeof window !== 'undefined' ? window : this

  var Glider = (_window.Glider = function (element, settings) {
    var _ = this

    if (element._glider) return element._glider

    _.ele = element
    _.ele.classList.add('glider')

    // expose glider object to its DOM element
    _.ele._glider = _

    // merge user setting with defaults
    _.opt = Object.assign(
      {},
      {
        slidesToScroll: 1,
        slidesToShow: 1,
        resizeLock: true,
        duration: 0.5,
        // easeInQuad
        easing: function (x, t, b, c, d) {
          return c * (t /= d) * t + b
        }
      },
      settings
    )

    // set defaults
    _.animate_id = _.page = _.slide = 0
    _.arrows = {}

    // preserve original options to
    // extend breakpoint settings
    _._opt = _.opt

    if (_.opt.skipTrack) {
      // first and only child is the track
      _.track = _.ele.children[0]
    } else {
      // create track and wrap slides
      _.track = document.createElement('div')
      _.ele.appendChild(_.track)
      while (_.ele.children.length !== 1) {
        _.track.appendChild(_.ele.children[0])
      }
    }

    _.track.classList.add('glider-track')

    // start glider
    _.init()

    // set events
    _.resize = _.init.bind(_, true)
    _.event(_.ele, 'add', {
      scroll: _.updateControls.bind(_)
    })
    _.event(_window, 'add', {
      resize: _.resize
    })
  })

  var gliderPrototype = Glider.prototype
  gliderPrototype.init = function (refresh, paging) {
    var _ = this

    var width = 0

    var height = 0

    _.slides = _.track.children;

    [].forEach.call(_.slides, function (_) {
      _.classList.add('glider-slide')
    })

    _.containerWidth = _.ele.clientWidth

    var breakpointChanged = _.settingsBreakpoint()
    if (!paging) paging = breakpointChanged

    if (
      _.opt.slidesToShow === 'auto' ||
      typeof _.opt._autoSlide !== 'undefined'
    ) {
      var slideCount = _.containerWidth / _.opt.itemWidth

      _.opt._autoSlide = _.opt.slidesToShow = _.opt.exactWidth
        ? slideCount
        : Math.floor(slideCount)
    }
    if (_.opt.slidesToScroll === 'auto') {
      _.opt.slidesToScroll = Math.floor(_.opt.slidesToShow)
    }

    _.itemWidth = _.opt.exactWidth
      ? _.opt.itemWidth
      : _.containerWidth / _.opt.slidesToShow;

    // set slide dimensions
    [].forEach.call(_.slides, function (__) {
      __.style.height = 'auto'
      __.style.width = _.itemWidth + 'px'
      width += _.itemWidth
      height = Math.max(__.offsetHeight, height)
    })

    _.track.style.width = width + 'px'
    _.trackWidth = width
    _.isDrag = false
    _.preventClick = false

    _.opt.resizeLock && _.scrollTo(_.slide * _.itemWidth, 0)

    if (breakpointChanged || paging) {
      _.bindArrows()
      _.buildDots()
      _.bindDrag()
    }

    _.updateControls()

    _.emit(refresh ? 'refresh' : 'loaded')
  }

  gliderPrototype.bindDrag = function () {
    var _ = this
    _.mouse = _.mouse || _.handleMouse.bind(_)

    var mouseup = function () {
      _.mouseDown = undefined
      _.ele.classList.remove('drag')
      if (_.isDrag) {
        _.preventClick = true
      }
      _.isDrag = false
    }

    var events = {
      mouseup: mouseup,
      mouseleave: mouseup,
      mousedown: function (e) {
        e.preventDefault()
        e.stopPropagation()
        _.mouseDown = e.clientX
        _.ele.classList.add('drag')
      },
      mousemove: _.mouse,
      click: function (e) {
        if (_.preventClick) {
          e.preventDefault()
          e.stopPropagation()
        }
        _.preventClick = false
      }
    }

    _.ele.classList.toggle('draggable', _.opt.draggable === true)
    _.event(_.ele, 'remove', events)
    if (_.opt.draggable) _.event(_.ele, 'add', events)
  }

  gliderPrototype.buildDots = function () {
    var _ = this

    if (!_.opt.dots) {
      if (_.dots) _.dots.innerHTML = ''
      return
    }

    if (typeof _.opt.dots === 'string') {
      _.dots = document.querySelector(_.opt.dots)
    } else _.dots = _.opt.dots
    if (!_.dots) return

    _.dots.innerHTML = ''
    _.dots.classList.add('glider-dots')

    for (var i = 0; i < Math.ceil(_.slides.length / _.opt.slidesToShow); ++i) {
      var dot = document.createElement('button')
      dot.dataset.index = i
      dot.setAttribute('aria-label', 'Page ' + (i + 1))
      dot.className = 'glider-dot ' + (i ? '' : 'active')
      _.event(dot, 'add', {
        click: _.scrollItem.bind(_, i, true)
      })
      _.dots.appendChild(dot)
    }
  }

  gliderPrototype.bindArrows = function () {
    var _ = this
    if (!_.opt.arrows) {
      Object.keys(_.arrows).forEach(function (direction) {
        var element = _.arrows[direction]
        _.event(element, 'remove', { click: element._func })
      })
      return
    }
    ['prev', 'next'].forEach(function (direction) {
      var arrow = _.opt.arrows[direction]
      if (arrow) {
        if (typeof arrow === 'string') arrow = document.querySelector(arrow)
        arrow._func = arrow._func || _.scrollItem.bind(_, direction)
        _.event(arrow, 'remove', {
          click: arrow._func
        })
        _.event(arrow, 'add', {
          click: arrow._func
        })
        _.arrows[direction] = arrow
      }
    })
  }

  gliderPrototype.updateControls = function (event) {
    var _ = this

    if (event && !_.opt.scrollPropagate) {
      event.stopPropagation()
    }

    var disableArrows = _.containerWidth >= _.trackWidth

    if (!_.opt.rewind) {
      if (_.arrows.prev) {
        _.arrows.prev.classList.toggle(
          'disabled',
          _.ele.scrollLeft <= 0 || disableArrows
        )
      }
      if (_.arrows.next) {
        _.arrows.next.classList.toggle(
          'disabled',
          Math.ceil(_.ele.scrollLeft + _.containerWidth) >=
            Math.floor(_.trackWidth) || disableArrows
        )
      }
    }

    _.slide = Math.round(_.ele.scrollLeft / _.itemWidth)
    _.page = Math.round(_.ele.scrollLeft / _.containerWidth)

    var middle = _.slide + Math.floor(Math.floor(_.opt.slidesToShow) / 2)

    var extraMiddle = Math.floor(_.opt.slidesToShow) % 2 ? 0 : middle + 1
    if (Math.floor(_.opt.slidesToShow) === 1) {
      extraMiddle = 0
    }

    // the last page may be less than one half of a normal page width so
    // the page is rounded down. when at the end, force the page to turn
    if (_.ele.scrollLeft + _.containerWidth >= Math.floor(_.trackWidth)) {
      _.page = _.dots ? _.dots.children.length - 1 : 0
    }

    [].forEach.call(_.slides, function (slide, index) {
      var slideClasses = slide.classList

      var wasVisible = slideClasses.contains('visible')

      var start = _.ele.scrollLeft

      var end = _.ele.scrollLeft + _.containerWidth

      var itemStart = _.itemWidth * index

      var itemEnd = itemStart + _.itemWidth;

      [].forEach.call(slideClasses, function (className) {
        /^left|right/.test(className) && slideClasses.remove(className)
      })
      slideClasses.toggle('active', _.slide === index)
      if (middle === index || (extraMiddle && extraMiddle === index)) {
        slideClasses.add('center')
      } else {
        slideClasses.remove('center')
        slideClasses.add(
          [
            index < middle ? 'left' : 'right',
            Math.abs(index - (index < middle ? middle : extraMiddle || middle))
          ].join('-')
        )
      }

      var isVisible =
        Math.ceil(itemStart) >= start && Math.floor(itemEnd) <= end
      slideClasses.toggle('visible', isVisible)
      if (isVisible !== wasVisible) {
        _.emit('slide-' + (isVisible ? 'visible' : 'hidden'), {
          slide: index
        })
      }
    })
    if (_.dots) {
      [].forEach.call(_.dots.children, function (dot, index) {
        dot.classList.toggle('active', _.page === index)
      })
    }

    if (event && _.opt.scrollLock) {
      clearTimeout(_.scrollLock)
      _.scrollLock = setTimeout(function () {
        clearTimeout(_.scrollLock)
        // dont attempt to scroll less than a pixel fraction - causes looping
        if (Math.abs(_.ele.scrollLeft / _.itemWidth - _.slide) > 0.02) {
          if (!_.mouseDown) {
            _.scrollItem(_.round(_.ele.scrollLeft / _.itemWidth))
          }
        }
      }, _.opt.scrollLockDelay || 250)
    }
  }

  gliderPrototype.scrollItem = function (slide, dot, e) {
    if (e) e.preventDefault()

    var _ = this

    var originalSlide = slide
    ++_.animate_id

    if (dot === true) {
      slide = slide * _.containerWidth
      slide = Math.round(slide / _.itemWidth) * _.itemWidth
    } else {
      if (typeof slide === 'string') {
        var backwards = slide === 'prev'

        // use precise location if fractional slides are on
        if (_.opt.slidesToScroll % 1 || _.opt.slidesToShow % 1) {
          slide = _.round(_.ele.scrollLeft / _.itemWidth)
        } else {
          slide = _.slide
        }

        if (backwards) slide -= _.opt.slidesToScroll
        else slide += _.opt.slidesToScroll

        if (_.opt.rewind) {
          var scrollLeft = _.ele.scrollLeft
          slide =
            backwards && !scrollLeft
              ? _.slides.length
              : !backwards &&
                scrollLeft + _.containerWidth >= Math.floor(_.trackWidth)
                ? 0
                : slide
        }
      }

      slide = Math.max(Math.min(slide, _.slides.length), 0)

      _.slide = slide
      slide = _.itemWidth * slide
    }

    _.scrollTo(
      slide,
      _.opt.duration * Math.abs(_.ele.scrollLeft - slide),
      function () {
        _.updateControls()
        _.emit('animated', {
          value: originalSlide,
          type:
            typeof originalSlide === 'string' ? 'arrow' : dot ? 'dot' : 'slide'
        })
      }
    )

    return false
  }

  gliderPrototype.settingsBreakpoint = function () {
    var _ = this

    var resp = _._opt.responsive

    if (resp) {
      // Sort the breakpoints in mobile first order
      resp.sort(function (a, b) {
        return b.breakpoint - a.breakpoint
      })

      for (var i = 0; i < resp.length; ++i) {
        var size = resp[i]
        if (_window.innerWidth >= size.breakpoint) {
          if (_.breakpoint !== size.breakpoint) {
            _.opt = Object.assign({}, _._opt, size.settings)
            _.breakpoint = size.breakpoint
            return true
          }
          return false
        }
      }
    }
    // set back to defaults in case they were overriden
    var breakpointChanged = _.breakpoint !== 0
    _.opt = Object.assign({}, _._opt)
    _.breakpoint = 0
    return breakpointChanged
  }

  gliderPrototype.scrollTo = function (scrollTarget, scrollDuration, callback) {
    var _ = this

    var start = new Date().getTime()

    var animateIndex = _.animate_id

    var animate = function () {
      var now = new Date().getTime() - start
      _.ele.scrollLeft =
        _.ele.scrollLeft +
        (scrollTarget - _.ele.scrollLeft) *
          _.opt.easing(0, now, 0, 1, scrollDuration)
      if (now < scrollDuration && animateIndex === _.animate_id) {
        _window.requestAnimationFrame(animate)
      } else {
        _.ele.scrollLeft = scrollTarget
        callback && callback.call(_)
      }
    }

    _window.requestAnimationFrame(animate)
  }

  gliderPrototype.removeItem = function (index) {
    var _ = this

    if (_.slides.length) {
      _.track.removeChild(_.slides[index])
      _.refresh(true)
      _.emit('remove')
    }
  }

  gliderPrototype.addItem = function (ele) {
    var _ = this

    _.track.appendChild(ele)
    _.refresh(true)
    _.emit('add')
  }

  gliderPrototype.handleMouse = function (e) {
    var _ = this
    if (_.mouseDown) {
      _.isDrag = true
      _.ele.scrollLeft +=
        (_.mouseDown - e.clientX) * (_.opt.dragVelocity || 3.3)
      _.mouseDown = e.clientX
    }
  }

  // used to round to the nearest 0.XX fraction
  gliderPrototype.round = function (double) {
    var _ = this
    var step = _.opt.slidesToScroll % 1 || 1
    var inv = 1.0 / step
    return Math.round(double * inv) / inv
  }

  gliderPrototype.refresh = function (paging) {
    var _ = this
    _.init(true, paging)
  }

  gliderPrototype.setOption = function (opt, global) {
    var _ = this

    if (_.breakpoint && !global) {
      _._opt.responsive.forEach(function (v) {
        if (v.breakpoint === _.breakpoint) {
          v.settings = Object.assign({}, v.settings, opt)
        }
      })
    } else {
      _._opt = Object.assign({}, _._opt, opt)
    }

    _.breakpoint = 0
    _.settingsBreakpoint()
  }

  gliderPrototype.destroy = function () {
    var _ = this

    var replace = _.ele.cloneNode(true)

    var clear = function (ele) {
      ele.removeAttribute('style');
      [].forEach.call(ele.classList, function (className) {
        /^glider/.test(className) && ele.classList.remove(className)
      })
    }
    // remove track
    replace.children[0].outerHTML = replace.children[0].innerHTML
    clear(replace);
    [].forEach.call(replace.getElementsByTagName('*'), clear)
    _.ele.parentNode.replaceChild(replace, _.ele)
    _.event(_window, 'remove', {
      resize: _.resize
    })
    _.emit('destroy')
  }

  gliderPrototype.emit = function (name, arg) {
    var _ = this

    var e = new _window.CustomEvent('glider-' + name, {
      bubbles: !_.opt.eventPropagate,
      detail: arg
    })
    _.ele.dispatchEvent(e)
  }

  gliderPrototype.event = function (ele, type, args) {
    var eventHandler = ele[type + 'EventListener'].bind(ele)
    Object.keys(args).forEach(function (k) {
      eventHandler(k, args[k])
    })
  }

  return Glider
})


// $("#anketa-form").on("submit", function (e) {
//   e.preventDefault();
//   var fd = new FormData(this);
//   let input = $(this).find('button[type="submit"]');
//   input.attr("disabled", true);
//   $.ajax({
//       url: "/Partnering/form.php",
//       type: "POST",
//       contentType: false,
//       processData: false,
//       data: fd,
//       success: function (data) {
//           window.location = "/Partnering/thanks/";
//       },
//       error: function (data) {
//           input.attr("disabled", false);
//       },
//   });
// });

// $("#form-callback").on("submit", function (e) {
//   e.preventDefault();
//   var fd = new FormData(this);
//   let input = $(this).find('button[type="submit"]');
//   input.attr("disabled", true);
//   $.ajax({
//       url: "/Partnering/form-callback.php",
//       type: "POST",
//       contentType: false,
//       processData: false,
//       data: fd,
//       success: function (data) {
//           window.location = "/Partnering/thanks/";
//       },
//       error: function (data) {
//           input.attr("disabled", false);
//       },
//   });
// });

// $("#form-partnering3").on("submit", function (e) {
//   e.preventDefault();
//   var fd = new FormData(this);
//   let input = $(this).find('button[type="submit"]');
//   input.attr("disabled", true);
//   $.ajax({
//       url: "/Partnering/form-partnering.php",
//       type: "POST",
//       contentType: false,
//       processData: false,
//       data: fd,
//       success: function (data) {
//           window.location = "/Partnering/thanks/";
//       },
//       error: function (data) {
//           input.attr("disabled", false);
//       },
//   });
// });

// $("#form-partnering2").on("submit", function (e) {
//   e.preventDefault();
//   var fd = new FormData(this);
//   let input = $(this).find('button[type="submit"]');
//   input.attr("disabled", true);
//   $.ajax({
//       url: "/Partnering/form-partnering.php",
//       type: "POST",
//       contentType: false,
//       processData: false,
//       data: fd,
//       success: function (data) {
//           window.location = "/Partnering/thanks/";
//       },
//       error: function (data) {
//           input.attr("disabled", false);
//       },
//   });
// });
!(function (t) {
	function e(n) {
		if (i[n]) return i[n].exports;
		var r = (i[n] = { exports: {}, id: n, loaded: !1 });
		return t[n].call(r.exports, r, r.exports, e), (r.loaded = !0), r.exports;
	}
	var n = window.webpackJsonp;
	window.webpackJsonp = function (s, o) {
		for (var a, l, u = 0, c = []; u < s.length; u++) (l = s[u]), r[l] && c.push.apply(c, r[l]), (r[l] = 0);
		for (a in o) t[a] = o[a];
		for (n && n(s, o); c.length;) c.shift().call(null, e);
		if (o[0]) return (i[0] = 0), e(0);
	};
	var i = {},
		r = { 14: 0 };
	(e.e = function (t, n) {
		if (0 === r[t]) return n.call(null, e);
		if (void 0 !== r[t]) r[t].push(n);
		else {
			r[t] = [n];
			var i = document.getElementsByTagName("head")[0],
				s = document.createElement("script");
			(s.type = "text/javascript"),
				(s.charset = "utf-8"),
				(s.async = !0),
				(s.src =
					e.p +
					"" +
					t +
					"." +
					({
						0: "about-us",
						1: "animations",
						2: "blank",
						3: "career",
						4: "documents-fees",
						5: "ie11-polyfill",
						6: "landing",
						7: "news-events",
						8: "payment-processing",
						9: "services",
						10: "slideshow",
						11: "social-responsibility",
						12: "solutions-subpages",
						13: "style-guide",
					}[t] || t) +
					".js"),
				i.appendChild(s);
		}
	}),
		(e.m = t),
		(e.c = i),
		(e.p = "javascripts/");
})([
	,
	,
	function (t, e, n) {
		(function (t) {
			"use strict";
			function e(t) {
				return t && t.__esModule ? t : { default: t };
			}
			n(4);
			var i = n(3),
				r = e(i);
			n(5);
			var s = n(6),
				o = e(s),
				a = n(7),
				l = e(a);
			n(8), n(9), n(10);
			var u = n(62),
				c = e(u),
				h = n(63),
				d = e(h);
			n(64),
				n(66),
				n(69),
				n(70),
				n(177),
				n(182),
				n(199),
				n(201),
				n(202),
				n(203),
				n(242),
				n(243),
				n(244),
				n(247),
				n(248),
				n(249),
				n(250),
				(window.$ = t = r["default"]),
				r["default"].extend(r["default"].easing, o["default"]),
				(r["default"].durationFast = 200),
				(r["default"].durationNormal = 400),
				(r["default"].durationSlow = 600),
				(r["default"].easeIn = "easeInExpo"),
				(r["default"].easeOut = "easeOutExpo"),
				(r["default"].easeInOut = "easeInOutExpo"),
				(0, c["default"])(),
				l["default"].hasHoverSupport() || (0, r["default"])("html").removeClass("has-hover").addClass("no-hover"),
				l["default"].isOldIE()
					? ((0, r["default"])("html").addClass("is-ie"), r["default"].getScript("/assets/javascripts/ie11-polyfill.js"))
					: l["default"].isIE()
						? (0, r["default"])("html").addClass("is-edge")
						: (0, r["default"])("html").addClass("is-not-ie-edge"),
				(0, r["default"])(function () {
					(0, r["default"])("body").app(), l["default"].isOldIE() || (0, d["default"])();
				});
		}.call(e, n(3)));
	},
	function (t, e, n) {
		var i, r;
        /*!
         * jQuery JavaScript Library v3.4.1
         * https://jquery.com/
         *
         * Includes Sizzle.js
         * https://sizzlejs.com/
         *
         * Copyright JS Foundation and other contributors
         * Released under the MIT license
         * https://jquery.org/license
         *
         * Date: 2019-05-01T21:04Z
         */
		!(function (e, n) {
			"use strict";
			"object" == typeof t && "object" == typeof t.exports
				? (t.exports = e.document
					? n(e, !0)
					: function (t) {
						if (!t.document) throw new Error("jQuery requires a window with a document");
						return n(t);
					})
				: n(e);
		})("undefined" != typeof window ? window : this, function (n, s) {
			"use strict";
			function o(t, e, n) {
				n = n || dt;
				var i,
					r,
					s = n.createElement("script");
				if (((s.text = t), e)) for (i in St) (r = e[i] || (e.getAttribute && e.getAttribute(i))), r && s.setAttribute(i, r);
				n.head.appendChild(s).parentNode.removeChild(s);
			}
			function a(t) {
				return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? yt[bt.call(t)] || "object" : typeof t;
			}
			function l(t) {
				var e = !!t && "length" in t && t.length,
					n = a(t);
				return !kt(t) && !Tt(t) && ("array" === n || 0 === e || ("number" == typeof e && e > 0 && e - 1 in t));
			}
			function u(t, e) {
				return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase();
			}
			function c(t, e, n) {
				return kt(e)
					? jt.grep(t, function (t, i) {
						return !!e.call(t, i, t) !== n;
					})
					: e.nodeType
						? jt.grep(t, function (t) {
							return (t === e) !== n;
						})
						: "string" != typeof e
							? jt.grep(t, function (t) {
								return mt.call(e, t) > -1 !== n;
							})
							: jt.filter(e, t, n);
			}
			function h(t, e) {
				for (; (t = t[e]) && 1 !== t.nodeType;);
				return t;
			}
			function d(t) {
				var e = {};
				return (
					jt.each(t.match(Ht) || [], function (t, n) {
						e[n] = !0;
					}),
					e
				);
			}
			function f(t) {
				return t;
			}
			function p(t) {
				throw t;
			}
			function v(t, e, n, i) {
				var r;
				try {
					t && kt((r = t.promise)) ? r.call(t).done(e).fail(n) : t && kt((r = t.then)) ? r.call(t, e, n) : e.apply(void 0, [t].slice(i));
				} catch (t) {
					n.apply(void 0, [t]);
				}
			}
			function g() {
				dt.removeEventListener("DOMContentLoaded", g), n.removeEventListener("load", g), jt.ready();
			}
			function m(t, e) {
				return e.toUpperCase();
			}
			function y(t) {
				return t.replace(Wt, "ms-").replace(Vt, m);
			}
			function b() {
				this.expando = jt.expando + b.uid++;
			}
			function x(t) {
				return "true" === t || ("false" !== t && ("null" === t ? null : t === +t + "" ? +t : Gt.test(t) ? JSON.parse(t) : t));
			}
			function w(t, e, n) {
				var i;
				if (void 0 === n && 1 === t.nodeType)
					if (((i = "data-" + e.replace(Jt, "-$&").toLowerCase()), (n = t.getAttribute(i)), "string" == typeof n)) {
						try {
							n = x(n);
						} catch (r) { }
						Xt.set(t, e, n);
					} else n = void 0;
				return n;
			}
			function _(t, e, n, i) {
				var r,
					s,
					o = 20,
					a = i
						? function () {
							return i.cur();
						}
						: function () {
							return jt.css(t, e, "");
						},
					l = a(),
					u = (n && n[3]) || (jt.cssNumber[e] ? "" : "px"),
					c = t.nodeType && (jt.cssNumber[e] || ("px" !== u && +l)) && Kt.exec(jt.css(t, e));
				if (c && c[3] !== u) {
					for (l /= 2, u = u || c[3], c = +l || 1; o--;) jt.style(t, e, c + u), (1 - s) * (1 - (s = a() / l || 0.5)) <= 0 && (o = 0), (c /= s);
					(c = 2 * c), jt.style(t, e, c + u), (n = n || []);
				}
				return n && ((c = +c || +l || 0), (r = n[1] ? c + (n[1] + 1) * n[2] : +n[2]), i && ((i.unit = u), (i.start = c), (i.end = r))), r;
			}
			function C(t) {
				var e,
					n = t.ownerDocument,
					i = t.nodeName,
					r = se[i];
				return r ? r : ((e = n.body.appendChild(n.createElement(i))), (r = jt.css(e, "display")), e.parentNode.removeChild(e), "none" === r && (r = "block"), (se[i] = r), r);
			}
			function k(t, e) {
				for (var n, i, r = [], s = 0, o = t.length; s < o; s++)
					(i = t[s]),
						i.style &&
						((n = i.style.display),
							e ? ("none" === n && ((r[s] = Yt.get(i, "display") || null), r[s] || (i.style.display = "")), "" === i.style.display && ie(i) && (r[s] = C(i))) : "none" !== n && ((r[s] = "none"), Yt.set(i, "display", n)));
				for (s = 0; s < o; s++) null != r[s] && (t[s].style.display = r[s]);
				return t;
			}
			function T(t, e) {
				var n;
				return (
					(n = "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e || "*") : "undefined" != typeof t.querySelectorAll ? t.querySelectorAll(e || "*") : []), void 0 === e || (e && u(t, e)) ? jt.merge([t], n) : n
				);
			}
			function S(t, e) {
				for (var n = 0, i = t.length; n < i; n++) Yt.set(t[n], "globalEval", !e || Yt.get(e[n], "globalEval"));
			}
			function E(t, e, n, i, r) {
				for (var s, o, l, u, c, h, d = e.createDocumentFragment(), f = [], p = 0, v = t.length; p < v; p++)
					if (((s = t[p]), s || 0 === s))
						if ("object" === a(s)) jt.merge(f, s.nodeType ? [s] : s);
						else if (ce.test(s)) {
							for (o = o || d.appendChild(e.createElement("div")), l = (ae.exec(s) || ["", ""])[1].toLowerCase(), u = ue[l] || ue._default, o.innerHTML = u[1] + jt.htmlPrefilter(s) + u[2], h = u[0]; h--;) o = o.lastChild;
							jt.merge(f, o.childNodes), (o = d.firstChild), (o.textContent = "");
						} else f.push(e.createTextNode(s));
				for (d.textContent = "", p = 0; (s = f[p++]);)
					if (i && jt.inArray(s, i) > -1) r && r.push(s);
					else if (((c = ee(s)), (o = T(d.appendChild(s), "script")), c && S(o), n)) for (h = 0; (s = o[h++]);) le.test(s.type || "") && n.push(s);
				return d;
			}
			function j() {
				return !0;
			}
			function O() {
				return !1;
			}
			function $(t, e) {
				return (t === D()) == ("focus" === e);
			}
			function D() {
				try {
					return dt.activeElement;
				} catch (t) { }
			}
			function I(t, e, n, i, r, s) {
				var o, a;
				if ("object" == typeof e) {
					"string" != typeof n && ((i = i || n), (n = void 0));
					for (a in e) I(t, a, n, i, e[a], s);
					return t;
				}
				if ((null == i && null == r ? ((r = n), (i = n = void 0)) : null == r && ("string" == typeof n ? ((r = i), (i = void 0)) : ((r = i), (i = n), (n = void 0))), r === !1)) r = O;
				else if (!r) return t;
				return (
					1 === s &&
					((o = r),
						(r = function (t) {
							return jt().off(t), o.apply(this, arguments);
						}),
						(r.guid = o.guid || (o.guid = jt.guid++))),
					t.each(function () {
						jt.event.add(this, e, r, i, n);
					})
				);
			}
			function A(t, e, n) {
				return n
					? (Yt.set(t, e, !1),
						void jt.event.add(t, e, {
							namespace: !1,
							handler: function (t) {
								var i,
									r,
									s = Yt.get(this, e);
								if (1 & t.isTrigger && this[e]) {
									if (s.length) (jt.event.special[e] || {}).delegateType && t.stopPropagation();
									else if (((s = pt.call(arguments)), Yt.set(this, e, s), (i = n(this, e)), this[e](), (r = Yt.get(this, e)), s !== r || i ? Yt.set(this, e, !1) : (r = {}), s !== r))
										return t.stopImmediatePropagation(), t.preventDefault(), r.value;
								} else s.length && (Yt.set(this, e, { value: jt.event.trigger(jt.extend(s[0], jt.Event.prototype), s.slice(1), this) }), t.stopImmediatePropagation());
							},
						}))
					: void (void 0 === Yt.get(t, e) && jt.event.add(t, e, j));
			}
			function M(t, e) {
				return u(t, "table") && u(11 !== e.nodeType ? e : e.firstChild, "tr") ? jt(t).children("tbody")[0] || t : t;
			}
			function N(t) {
				return (t.type = (null !== t.getAttribute("type")) + "/" + t.type), t;
			}
			function L(t) {
				return "true/" === (t.type || "").slice(0, 5) ? (t.type = t.type.slice(5)) : t.removeAttribute("type"), t;
			}
			function R(t, e) {
				var n, i, r, s, o, a, l, u;
				if (1 === e.nodeType) {
					if (Yt.hasData(t) && ((s = Yt.access(t)), (o = Yt.set(e, s)), (u = s.events))) {
						delete o.handle, (o.events = {});
						for (r in u) for (n = 0, i = u[r].length; n < i; n++) jt.event.add(e, r, u[r][n]);
					}
					Xt.hasData(t) && ((a = Xt.access(t)), (l = jt.extend({}, a)), Xt.set(e, l));
				}
			}
			function P(t, e) {
				var n = e.nodeName.toLowerCase();
				"input" === n && oe.test(t.type) ? (e.checked = t.checked) : ("input" !== n && "textarea" !== n) || (e.defaultValue = t.defaultValue);
			}
			function z(t, e, n, i) {
				e = vt.apply([], e);
				var r,
					s,
					a,
					l,
					u,
					c,
					h = 0,
					d = t.length,
					f = d - 1,
					p = e[0],
					v = kt(p);
				if (v || (d > 1 && "string" == typeof p && !Ct.checkClone && ge.test(p)))
					return t.each(function (r) {
						var s = t.eq(r);
						v && (e[0] = p.call(this, r, s.html())), z(s, e, n, i);
					});
				if (d && ((r = E(e, t[0].ownerDocument, !1, t, i)), (s = r.firstChild), 1 === r.childNodes.length && (r = s), s || i)) {
					for (a = jt.map(T(r, "script"), N), l = a.length; h < d; h++) (u = r), h !== f && ((u = jt.clone(u, !0, !0)), l && jt.merge(a, T(u, "script"))), n.call(t[h], u, h);
					if (l)
						for (c = a[a.length - 1].ownerDocument, jt.map(a, L), h = 0; h < l; h++)
							(u = a[h]),
								le.test(u.type || "") &&
								!Yt.access(u, "globalEval") &&
								jt.contains(c, u) &&
								(u.src && "module" !== (u.type || "").toLowerCase() ? jt._evalUrl && !u.noModule && jt._evalUrl(u.src, { nonce: u.nonce || u.getAttribute("nonce") }) : o(u.textContent.replace(me, ""), u, c));
				}
				return t;
			}
			function H(t, e, n) {
				for (var i, r = e ? jt.filter(e, t) : t, s = 0; null != (i = r[s]); s++) n || 1 !== i.nodeType || jt.cleanData(T(i)), i.parentNode && (n && ee(i) && S(T(i, "script")), i.parentNode.removeChild(i));
				return t;
			}
			function q(t, e, n) {
				var i,
					r,
					s,
					o,
					a = t.style;
				return (
					(n = n || be(t)),
					n &&
					((o = n.getPropertyValue(e) || n[e]),
						"" !== o || ee(t) || (o = jt.style(t, e)),
						!Ct.pixelBoxStyles() && ye.test(o) && xe.test(e) && ((i = a.width), (r = a.minWidth), (s = a.maxWidth), (a.minWidth = a.maxWidth = a.width = o), (o = n.width), (a.width = i), (a.minWidth = r), (a.maxWidth = s))),
					void 0 !== o ? o + "" : o
				);
			}
			function F(t, e) {
				return {
					get: function () {
						return t() ? void delete this.get : (this.get = e).apply(this, arguments);
					},
				};
			}
			function B(t) {
				for (var e = t[0].toUpperCase() + t.slice(1), n = we.length; n--;) if (((t = we[n] + e), t in _e)) return t;
			}
			function W(t) {
				var e = jt.cssProps[t] || Ce[t];
				return e ? e : t in _e ? t : (Ce[t] = B(t) || t);
			}
			function V(t, e, n) {
				var i = Kt.exec(e);
				return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : e;
			}
			function U(t, e, n, i, r, s) {
				var o = "width" === e ? 1 : 0,
					a = 0,
					l = 0;
				if (n === (i ? "border" : "content")) return 0;
				for (; o < 4; o += 2)
					"margin" === n && (l += jt.css(t, n + Qt[o], !0, r)),
						i
							? ("content" === n && (l -= jt.css(t, "padding" + Qt[o], !0, r)), "margin" !== n && (l -= jt.css(t, "border" + Qt[o] + "Width", !0, r)))
							: ((l += jt.css(t, "padding" + Qt[o], !0, r)), "padding" !== n ? (l += jt.css(t, "border" + Qt[o] + "Width", !0, r)) : (a += jt.css(t, "border" + Qt[o] + "Width", !0, r)));
				return !i && s >= 0 && (l += Math.max(0, Math.ceil(t["offset" + e[0].toUpperCase() + e.slice(1)] - s - l - a - 0.5)) || 0), l;
			}
			function Y(t, e, n) {
				var i = be(t),
					r = !Ct.boxSizingReliable() || n,
					s = r && "border-box" === jt.css(t, "boxSizing", !1, i),
					o = s,
					a = q(t, e, i),
					l = "offset" + e[0].toUpperCase() + e.slice(1);
				if (ye.test(a)) {
					if (!n) return a;
					a = "auto";
				}
				return (
					((!Ct.boxSizingReliable() && s) || "auto" === a || (!parseFloat(a) && "inline" === jt.css(t, "display", !1, i))) &&
					t.getClientRects().length &&
					((s = "border-box" === jt.css(t, "boxSizing", !1, i)), (o = l in t), o && (a = t[l])),
					(a = parseFloat(a) || 0),
					a + U(t, e, n || (s ? "border" : "content"), o, i, a) + "px"
				);
			}
			function X(t, e, n, i, r) {
				return new X.prototype.init(t, e, n, i, r);
			}
			function G() {
				Oe && (dt.hidden === !1 && n.requestAnimationFrame ? n.requestAnimationFrame(G) : n.setTimeout(G, jt.fx.interval), jt.fx.tick());
			}
			function J() {
				return (
					n.setTimeout(function () {
						je = void 0;
					}),
					(je = Date.now())
				);
			}
			function Z(t, e) {
				var n,
					i = 0,
					r = { height: t };
				for (e = e ? 1 : 0; i < 4; i += 2 - e) (n = Qt[i]), (r["margin" + n] = r["padding" + n] = t);
				return e && (r.opacity = r.width = t), r;
			}
			function K(t, e, n) {
				for (var i, r = (et.tweeners[e] || []).concat(et.tweeners["*"]), s = 0, o = r.length; s < o; s++) if ((i = r[s].call(n, e, t))) return i;
			}
			function Q(t, e, n) {
				var i,
					r,
					s,
					o,
					a,
					l,
					u,
					c,
					h = "width" in e || "height" in e,
					d = this,
					f = {},
					p = t.style,
					v = t.nodeType && ie(t),
					g = Yt.get(t, "fxshow");
				n.queue ||
					((o = jt._queueHooks(t, "fx")),
						null == o.unqueued &&
						((o.unqueued = 0),
							(a = o.empty.fire),
							(o.empty.fire = function () {
								o.unqueued || a();
							})),
						o.unqueued++,
						d.always(function () {
							d.always(function () {
								o.unqueued--, jt.queue(t, "fx").length || o.empty.fire();
							});
						}));
				for (i in e)
					if (((r = e[i]), $e.test(r))) {
						if ((delete e[i], (s = s || "toggle" === r), r === (v ? "hide" : "show"))) {
							if ("show" !== r || !g || void 0 === g[i]) continue;
							v = !0;
						}
						f[i] = (g && g[i]) || jt.style(t, i);
					}
				if (((l = !jt.isEmptyObject(e)), l || !jt.isEmptyObject(f))) {
					h &&
						1 === t.nodeType &&
						((n.overflow = [p.overflow, p.overflowX, p.overflowY]),
							(u = g && g.display),
							null == u && (u = Yt.get(t, "display")),
							(c = jt.css(t, "display")),
							"none" === c && (u ? (c = u) : (k([t], !0), (u = t.style.display || u), (c = jt.css(t, "display")), k([t]))),
							("inline" === c || ("inline-block" === c && null != u)) &&
							"none" === jt.css(t, "float") &&
							(l ||
								(d.done(function () {
									p.display = u;
								}),
									null == u && ((c = p.display), (u = "none" === c ? "" : c))),
								(p.display = "inline-block"))),
						n.overflow &&
						((p.overflow = "hidden"),
							d.always(function () {
								(p.overflow = n.overflow[0]), (p.overflowX = n.overflow[1]), (p.overflowY = n.overflow[2]);
							})),
						(l = !1);
					for (i in f)
						l ||
							(g ? "hidden" in g && (v = g.hidden) : (g = Yt.access(t, "fxshow", { display: u })),
								s && (g.hidden = !v),
								v && k([t], !0),
								d.done(function () {
									v || k([t]), Yt.remove(t, "fxshow");
									for (i in f) jt.style(t, i, f[i]);
								})),
							(l = K(v ? g[i] : 0, i, d)),
							i in g || ((g[i] = l.start), v && ((l.end = l.start), (l.start = 0)));
				}
			}
			function tt(t, e) {
				var n, i, r, s, o;
				for (n in t)
					if (((i = y(n)), (r = e[i]), (s = t[n]), Array.isArray(s) && ((r = s[1]), (s = t[n] = s[0])), n !== i && ((t[i] = s), delete t[n]), (o = jt.cssHooks[i]), o && "expand" in o)) {
						(s = o.expand(s)), delete t[i];
						for (n in s) n in t || ((t[n] = s[n]), (e[n] = r));
					} else e[i] = r;
			}
			function et(t, e, n) {
				var i,
					r,
					s = 0,
					o = et.prefilters.length,
					a = jt.Deferred().always(function () {
						delete l.elem;
					}),
					l = function () {
						if (r) return !1;
						for (var e = je || J(), n = Math.max(0, u.startTime + u.duration - e), i = n / u.duration || 0, s = 1 - i, o = 0, l = u.tweens.length; o < l; o++) u.tweens[o].run(s);
						return a.notifyWith(t, [u, s, n]), s < 1 && l ? n : (l || a.notifyWith(t, [u, 1, 0]), a.resolveWith(t, [u]), !1);
					},
					u = a.promise({
						elem: t,
						props: jt.extend({}, e),
						opts: jt.extend(!0, { specialEasing: {}, easing: jt.easing._default }, n),
						originalProperties: e,
						originalOptions: n,
						startTime: je || J(),
						duration: n.duration,
						tweens: [],
						createTween: function (e, n) {
							var i = jt.Tween(t, u.opts, e, n, u.opts.specialEasing[e] || u.opts.easing);
							return u.tweens.push(i), i;
						},
						stop: function (e) {
							var n = 0,
								i = e ? u.tweens.length : 0;
							if (r) return this;
							for (r = !0; n < i; n++) u.tweens[n].run(1);
							return e ? (a.notifyWith(t, [u, 1, 0]), a.resolveWith(t, [u, e])) : a.rejectWith(t, [u, e]), this;
						},
					}),
					c = u.props;
				for (tt(c, u.opts.specialEasing); s < o; s++) if ((i = et.prefilters[s].call(u, t, c, u.opts))) return kt(i.stop) && (jt._queueHooks(u.elem, u.opts.queue).stop = i.stop.bind(i)), i;
				return (
					jt.map(c, K, u),
					kt(u.opts.start) && u.opts.start.call(t, u),
					u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always),
					jt.fx.timer(jt.extend(l, { elem: t, anim: u, queue: u.opts.queue })),
					u
				);
			}
			function nt(t) {
				var e = t.match(Ht) || [];
				return e.join(" ");
			}
			function it(t) {
				return (t.getAttribute && t.getAttribute("class")) || "";
			}
			function rt(t) {
				return Array.isArray(t) ? t : "string" == typeof t ? t.match(Ht) || [] : [];
			}
			function st(t, e, n, i) {
				var r;
				if (Array.isArray(e))
					jt.each(e, function (e, r) {
						n || Fe.test(t) ? i(t, r) : st(t + "[" + ("object" == typeof r && null != r ? e : "") + "]", r, n, i);
					});
				else if (n || "object" !== a(e)) i(t, e);
				else for (r in e) st(t + "[" + r + "]", e[r], n, i);
			}
			function ot(t) {
				return function (e, n) {
					"string" != typeof e && ((n = e), (e = "*"));
					var i,
						r = 0,
						s = e.toLowerCase().match(Ht) || [];
					if (kt(n)) for (; (i = s[r++]);) "+" === i[0] ? ((i = i.slice(1) || "*"), (t[i] = t[i] || []).unshift(n)) : (t[i] = t[i] || []).push(n);
				};
			}
			function at(t, e, n, i) {
				function r(a) {
					var l;
					return (
						(s[a] = !0),
						jt.each(t[a] || [], function (t, a) {
							var u = a(e, n, i);
							return "string" != typeof u || o || s[u] ? (o ? !(l = u) : void 0) : (e.dataTypes.unshift(u), r(u), !1);
						}),
						l
					);
				}
				var s = {},
					o = t === tn;
				return r(e.dataTypes[0]) || (!s["*"] && r("*"));
			}
			function lt(t, e) {
				var n,
					i,
					r = jt.ajaxSettings.flatOptions || {};
				for (n in e) void 0 !== e[n] && ((r[n] ? t : i || (i = {}))[n] = e[n]);
				return i && jt.extend(!0, t, i), t;
			}
			function ut(t, e, n) {
				for (var i, r, s, o, a = t.contents, l = t.dataTypes; "*" === l[0];) l.shift(), void 0 === i && (i = t.mimeType || e.getResponseHeader("Content-Type"));
				if (i)
					for (r in a)
						if (a[r] && a[r].test(i)) {
							l.unshift(r);
							break;
						}
				if (l[0] in n) s = l[0];
				else {
					for (r in n) {
						if (!l[0] || t.converters[r + " " + l[0]]) {
							s = r;
							break;
						}
						o || (o = r);
					}
					s = s || o;
				}
				if (s) return s !== l[0] && l.unshift(s), n[s];
			}
			function ct(t, e, n, i) {
				var r,
					s,
					o,
					a,
					l,
					u = {},
					c = t.dataTypes.slice();
				if (c[1]) for (o in t.converters) u[o.toLowerCase()] = t.converters[o];
				for (s = c.shift(); s;)
					if ((t.responseFields[s] && (n[t.responseFields[s]] = e), !l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)), (l = s), (s = c.shift())))
						if ("*" === s) s = l;
						else if ("*" !== l && l !== s) {
							if (((o = u[l + " " + s] || u["* " + s]), !o))
								for (r in u)
									if (((a = r.split(" ")), a[1] === s && (o = u[l + " " + a[0]] || u["* " + a[0]]))) {
										o === !0 ? (o = u[r]) : u[r] !== !0 && ((s = a[0]), c.unshift(a[1]));
										break;
									}
							if (o !== !0)
								if (o && t["throws"]) e = o(e);
								else
									try {
										e = o(e);
									} catch (h) {
										return { state: "parsererror", error: o ? h : "No conversion from " + l + " to " + s };
									}
						}
				return { state: "success", data: e };
			}
			var ht = [],
				dt = n.document,
				ft = Object.getPrototypeOf,
				pt = ht.slice,
				vt = ht.concat,
				gt = ht.push,
				mt = ht.indexOf,
				yt = {},
				bt = yt.toString,
				xt = yt.hasOwnProperty,
				wt = xt.toString,
				_t = wt.call(Object),
				Ct = {},
				kt = function (t) {
					return "function" == typeof t && "number" != typeof t.nodeType;
				},
				Tt = function (t) {
					return null != t && t === t.window;
				},
				St = { type: !0, src: !0, nonce: !0, noModule: !0 },
				Et = "3.4.1",
				jt = function (t, e) {
					return new jt.fn.init(t, e);
				},
				Ot = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
			(jt.fn = jt.prototype = {
				jquery: Et,
				constructor: jt,
				length: 0,
				toArray: function () {
					return pt.call(this);
				},
				get: function (t) {
					return null == t ? pt.call(this) : t < 0 ? this[t + this.length] : this[t];
				},
				pushStack: function (t) {
					var e = jt.merge(this.constructor(), t);
					return (e.prevObject = this), e;
				},
				each: function (t) {
					return jt.each(this, t);
				},
				map: function (t) {
					return this.pushStack(
						jt.map(this, function (e, n) {
							return t.call(e, n, e);
						})
					);
				},
				slice: function () {
					return this.pushStack(pt.apply(this, arguments));
				},
				first: function () {
					return this.eq(0);
				},
				last: function () {
					return this.eq(-1);
				},
				eq: function (t) {
					var e = this.length,
						n = +t + (t < 0 ? e : 0);
					return this.pushStack(n >= 0 && n < e ? [this[n]] : []);
				},
				end: function () {
					return this.prevObject || this.constructor();
				},
				push: gt,
				sort: ht.sort,
				splice: ht.splice,
			}),
				(jt.extend = jt.fn.extend = function () {
					var t,
						e,
						n,
						i,
						r,
						s,
						o = arguments[0] || {},
						a = 1,
						l = arguments.length,
						u = !1;
					for ("boolean" == typeof o && ((u = o), (o = arguments[a] || {}), a++), "object" == typeof o || kt(o) || (o = {}), a === l && ((o = this), a--); a < l; a++)
						if (null != (t = arguments[a]))
							for (e in t)
								(i = t[e]),
									"__proto__" !== e &&
									o !== i &&
									(u && i && (jt.isPlainObject(i) || (r = Array.isArray(i)))
										? ((n = o[e]), (s = r && !Array.isArray(n) ? [] : r || jt.isPlainObject(n) ? n : {}), (r = !1), (o[e] = jt.extend(u, s, i)))
										: void 0 !== i && (o[e] = i));
					return o;
				}),
				jt.extend({
					expando: "jQuery" + (Et + Math.random()).replace(/\D/g, ""),
					isReady: !0,
					error: function (t) {
						throw new Error(t);
					},
					noop: function () { },
					isPlainObject: function (t) {
						var e, n;
						return !(!t || "[object Object]" !== bt.call(t)) && (!(e = ft(t)) || ((n = xt.call(e, "constructor") && e.constructor), "function" == typeof n && wt.call(n) === _t));
					},
					isEmptyObject: function (t) {
						var e;
						for (e in t) return !1;
						return !0;
					},
					globalEval: function (t, e) {
						o(t, { nonce: e && e.nonce });
					},
					each: function (t, e) {
						var n,
							i = 0;
						if (l(t)) for (n = t.length; i < n && e.call(t[i], i, t[i]) !== !1; i++);
						else for (i in t) if (e.call(t[i], i, t[i]) === !1) break;
						return t;
					},
					trim: function (t) {
						return null == t ? "" : (t + "").replace(Ot, "");
					},
					makeArray: function (t, e) {
						var n = e || [];
						return null != t && (l(Object(t)) ? jt.merge(n, "string" == typeof t ? [t] : t) : gt.call(n, t)), n;
					},
					inArray: function (t, e, n) {
						return null == e ? -1 : mt.call(e, t, n);
					},
					merge: function (t, e) {
						for (var n = +e.length, i = 0, r = t.length; i < n; i++) t[r++] = e[i];
						return (t.length = r), t;
					},
					grep: function (t, e, n) {
						for (var i, r = [], s = 0, o = t.length, a = !n; s < o; s++) (i = !e(t[s], s)), i !== a && r.push(t[s]);
						return r;
					},
					map: function (t, e, n) {
						var i,
							r,
							s = 0,
							o = [];
						if (l(t)) for (i = t.length; s < i; s++) (r = e(t[s], s, n)), null != r && o.push(r);
						else for (s in t) (r = e(t[s], s, n)), null != r && o.push(r);
						return vt.apply([], o);
					},
					guid: 1,
					support: Ct,
				}),
				"function" == typeof Symbol && (jt.fn[Symbol.iterator] = ht[Symbol.iterator]),
				jt.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (t, e) {
					yt["[object " + e + "]"] = e.toLowerCase();
				});
			var $t =
                /*!
                 * Sizzle CSS Selector Engine v2.3.4
                 * https://sizzlejs.com/
                 *
                 * Copyright JS Foundation and other contributors
                 * Released under the MIT license
                 * https://js.foundation/
                 *
                 * Date: 2019-04-08
                 */
				(function (t) {
					function e(t, e, n, i) {
						var r,
							s,
							o,
							a,
							l,
							u,
							c,
							d = e && e.ownerDocument,
							p = e ? e.nodeType : 9;
						if (((n = n || []), "string" != typeof t || !t || (1 !== p && 9 !== p && 11 !== p))) return n;
						if (!i && ((e ? e.ownerDocument || e : q) !== A && I(e), (e = e || A), N)) {
							if (11 !== p && (l = bt.exec(t)))
								if ((r = l[1])) {
									if (9 === p) {
										if (!(o = e.getElementById(r))) return n;
										if (o.id === r) return n.push(o), n;
									} else if (d && (o = d.getElementById(r)) && z(e, o) && o.id === r) return n.push(o), n;
								} else {
									if (l[2]) return Q.apply(n, e.getElementsByTagName(t)), n;
									if ((r = l[3]) && _.getElementsByClassName && e.getElementsByClassName) return Q.apply(n, e.getElementsByClassName(r)), n;
								}
							if (_.qsa && !Y[t + " "] && (!L || !L.test(t)) && (1 !== p || "object" !== e.nodeName.toLowerCase())) {
								if (((c = t), (d = e), 1 === p && ht.test(t))) {
									for ((a = e.getAttribute("id")) ? (a = a.replace(Ct, kt)) : e.setAttribute("id", (a = H)), u = S(t), s = u.length; s--;) u[s] = "#" + a + " " + f(u[s]);
									(c = u.join(",")), (d = (xt.test(t) && h(e.parentNode)) || e);
								}
								try {
									return Q.apply(n, d.querySelectorAll(c)), n;
								} catch (v) {
									Y(t, !0);
								} finally {
									a === H && e.removeAttribute("id");
								}
							}
						}
						return j(t.replace(lt, "$1"), e, n, i);
					}
					function n() {
						function t(n, i) {
							return e.push(n + " ") > C.cacheLength && delete t[e.shift()], (t[n + " "] = i);
						}
						var e = [];
						return t;
					}
					function i(t) {
						return (t[H] = !0), t;
					}
					function r(t) {
						var e = A.createElement("fieldset");
						try {
							return !!t(e);
						} catch (n) {
							return !1;
						} finally {
							e.parentNode && e.parentNode.removeChild(e), (e = null);
						}
					}
					function s(t, e) {
						for (var n = t.split("|"), i = n.length; i--;) C.attrHandle[n[i]] = e;
					}
					function o(t, e) {
						var n = e && t,
							i = n && 1 === t.nodeType && 1 === e.nodeType && t.sourceIndex - e.sourceIndex;
						if (i) return i;
						if (n) for (; (n = n.nextSibling);) if (n === e) return -1;
						return t ? 1 : -1;
					}
					function a(t) {
						return function (e) {
							var n = e.nodeName.toLowerCase();
							return "input" === n && e.type === t;
						};
					}
					function l(t) {
						return function (e) {
							var n = e.nodeName.toLowerCase();
							return ("input" === n || "button" === n) && e.type === t;
						};
					}
					function u(t) {
						return function (e) {
							return "form" in e
								? e.parentNode && e.disabled === !1
									? "label" in e
										? "label" in e.parentNode
											? e.parentNode.disabled === t
											: e.disabled === t
										: e.isDisabled === t || (e.isDisabled !== !t && St(e) === t)
									: e.disabled === t
								: "label" in e && e.disabled === t;
						};
					}
					function c(t) {
						return i(function (e) {
							return (
								(e = +e),
								i(function (n, i) {
									for (var r, s = t([], n.length, e), o = s.length; o--;) n[(r = s[o])] && (n[r] = !(i[r] = n[r]));
								})
							);
						});
					}
					function h(t) {
						return t && "undefined" != typeof t.getElementsByTagName && t;
					}
					function d() { }
					function f(t) {
						for (var e = 0, n = t.length, i = ""; e < n; e++) i += t[e].value;
						return i;
					}
					function p(t, e, n) {
						var i = e.dir,
							r = e.next,
							s = r || i,
							o = n && "parentNode" === s,
							a = B++;
						return e.first
							? function (e, n, r) {
								for (; (e = e[i]);) if (1 === e.nodeType || o) return t(e, n, r);
								return !1;
							}
							: function (e, n, l) {
								var u,
									c,
									h,
									d = [F, a];
								if (l) {
									for (; (e = e[i]);) if ((1 === e.nodeType || o) && t(e, n, l)) return !0;
								} else
									for (; (e = e[i]);)
										if (1 === e.nodeType || o)
											if (((h = e[H] || (e[H] = {})), (c = h[e.uniqueID] || (h[e.uniqueID] = {})), r && r === e.nodeName.toLowerCase())) e = e[i] || e;
											else {
												if ((u = c[s]) && u[0] === F && u[1] === a) return (d[2] = u[2]);
												if (((c[s] = d), (d[2] = t(e, n, l)))) return !0;
											}
								return !1;
							};
					}
					function v(t) {
						return t.length > 1
							? function (e, n, i) {
								for (var r = t.length; r--;) if (!t[r](e, n, i)) return !1;
								return !0;
							}
							: t[0];
					}
					function g(t, n, i) {
						for (var r = 0, s = n.length; r < s; r++) e(t, n[r], i);
						return i;
					}
					function m(t, e, n, i, r) {
						for (var s, o = [], a = 0, l = t.length, u = null != e; a < l; a++) (s = t[a]) && ((n && !n(s, i, r)) || (o.push(s), u && e.push(a)));
						return o;
					}
					function y(t, e, n, r, s, o) {
						return (
							r && !r[H] && (r = y(r)),
							s && !s[H] && (s = y(s, o)),
							i(function (i, o, a, l) {
								var u,
									c,
									h,
									d = [],
									f = [],
									p = o.length,
									v = i || g(e || "*", a.nodeType ? [a] : a, []),
									y = !t || (!i && e) ? v : m(v, d, t, a, l),
									b = n ? (s || (i ? t : p || r) ? [] : o) : y;
								if ((n && n(y, b, a, l), r)) for (u = m(b, f), r(u, [], a, l), c = u.length; c--;) (h = u[c]) && (b[f[c]] = !(y[f[c]] = h));
								if (i) {
									if (s || t) {
										if (s) {
											for (u = [], c = b.length; c--;) (h = b[c]) && u.push((y[c] = h));
											s(null, (b = []), u, l);
										}
										for (c = b.length; c--;) (h = b[c]) && (u = s ? et(i, h) : d[c]) > -1 && (i[u] = !(o[u] = h));
									}
								} else (b = m(b === o ? b.splice(p, b.length) : b)), s ? s(null, o, b, l) : Q.apply(o, b);
							})
						);
					}
					function b(t) {
						for (
							var e,
							n,
							i,
							r = t.length,
							s = C.relative[t[0].type],
							o = s || C.relative[" "],
							a = s ? 1 : 0,
							l = p(
								function (t) {
									return t === e;
								},
								o,
								!0
							),
							u = p(
								function (t) {
									return et(e, t) > -1;
								},
								o,
								!0
							),
							c = [
								function (t, n, i) {
									var r = (!s && (i || n !== O)) || ((e = n).nodeType ? l(t, n, i) : u(t, n, i));
									return (e = null), r;
								},
							];
							a < r;
							a++
						)
							if ((n = C.relative[t[a].type])) c = [p(v(c), n)];
							else {
								if (((n = C.filter[t[a].type].apply(null, t[a].matches)), n[H])) {
									for (i = ++a; i < r && !C.relative[t[i].type]; i++);
									return y(a > 1 && v(c), a > 1 && f(t.slice(0, a - 1).concat({ value: " " === t[a - 2].type ? "*" : "" })).replace(lt, "$1"), n, a < i && b(t.slice(a, i)), i < r && b((t = t.slice(i))), i < r && f(t));
								}
								c.push(n);
							}
						return v(c);
					}
					function x(t, n) {
						var r = n.length > 0,
							s = t.length > 0,
							o = function (i, o, a, l, u) {
								var c,
									h,
									d,
									f = 0,
									p = "0",
									v = i && [],
									g = [],
									y = O,
									b = i || (s && C.find.TAG("*", u)),
									x = (F += null == y ? 1 : Math.random() || 0.1),
									w = b.length;
								for (u && (O = o === A || o || u); p !== w && null != (c = b[p]); p++) {
									if (s && c) {
										for (h = 0, o || c.ownerDocument === A || (I(c), (a = !N)); (d = t[h++]);)
											if (d(c, o || A, a)) {
												l.push(c);
												break;
											}
										u && (F = x);
									}
									r && ((c = !d && c) && f--, i && v.push(c));
								}
								if (((f += p), r && p !== f)) {
									for (h = 0; (d = n[h++]);) d(v, g, o, a);
									if (i) {
										if (f > 0) for (; p--;) v[p] || g[p] || (g[p] = Z.call(l));
										g = m(g);
									}
									Q.apply(l, g), u && !i && g.length > 0 && f + n.length > 1 && e.uniqueSort(l);
								}
								return u && ((F = x), (O = y)), v;
							};
						return r ? i(o) : o;
					}
					var w,
						_,
						C,
						k,
						T,
						S,
						E,
						j,
						O,
						$,
						D,
						I,
						A,
						M,
						N,
						L,
						R,
						P,
						z,
						H = "sizzle" + 1 * new Date(),
						q = t.document,
						F = 0,
						B = 0,
						W = n(),
						V = n(),
						U = n(),
						Y = n(),
						X = function (t, e) {
							return t === e && (D = !0), 0;
						},
						G = {}.hasOwnProperty,
						J = [],
						Z = J.pop,
						K = J.push,
						Q = J.push,
						tt = J.slice,
						et = function (t, e) {
							for (var n = 0, i = t.length; n < i; n++) if (t[n] === e) return n;
							return -1;
						},
						nt = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
						it = "[\\x20\\t\\r\\n\\f]",
						rt = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
						st = "\\[" + it + "*(" + rt + ")(?:" + it + "*([*^$|!~]?=)" + it + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + rt + "))|)" + it + "*\\]",
						ot = ":(" + rt + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + st + ")*)|.*)\\)|)",
						at = new RegExp(it + "+", "g"),
						lt = new RegExp("^" + it + "+|((?:^|[^\\\\])(?:\\\\.)*)" + it + "+$", "g"),
						ut = new RegExp("^" + it + "*," + it + "*"),
						ct = new RegExp("^" + it + "*([>+~]|" + it + ")" + it + "*"),
						ht = new RegExp(it + "|>"),
						dt = new RegExp(ot),
						ft = new RegExp("^" + rt + "$"),
						pt = {
							ID: new RegExp("^#(" + rt + ")"),
							CLASS: new RegExp("^\\.(" + rt + ")"),
							TAG: new RegExp("^(" + rt + "|[*])"),
							ATTR: new RegExp("^" + st),
							PSEUDO: new RegExp("^" + ot),
							CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + it + "*(even|odd|(([+-]|)(\\d*)n|)" + it + "*(?:([+-]|)" + it + "*(\\d+)|))" + it + "*\\)|)", "i"),
							bool: new RegExp("^(?:" + nt + ")$", "i"),
							needsContext: new RegExp("^" + it + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + it + "*((?:-\\d)?\\d*)" + it + "*\\)|)(?=[^-]|$)", "i"),
						},
						vt = /HTML$/i,
						gt = /^(?:input|select|textarea|button)$/i,
						mt = /^h\d$/i,
						yt = /^[^{]+\{\s*\[native \w/,
						bt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
						xt = /[+~]/,
						wt = new RegExp("\\\\([\\da-f]{1,6}" + it + "?|(" + it + ")|.)", "ig"),
						_t = function (t, e, n) {
							var i = "0x" + e - 65536;
							return i !== i || n ? e : i < 0 ? String.fromCharCode(i + 65536) : String.fromCharCode((i >> 10) | 55296, (1023 & i) | 56320);
						},
						Ct = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
						kt = function (t, e) {
							return e ? ("\0" === t ? "�" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " ") : "\\" + t;
						},
						Tt = function () {
							I();
						},
						St = p(
							function (t) {
								return t.disabled === !0 && "fieldset" === t.nodeName.toLowerCase();
							},
							{ dir: "parentNode", next: "legend" }
						);
					try {
						Q.apply((J = tt.call(q.childNodes)), q.childNodes), J[q.childNodes.length].nodeType;
					} catch (Et) {
						Q = {
							apply: J.length
								? function (t, e) {
									K.apply(t, tt.call(e));
								}
								: function (t, e) {
									for (var n = t.length, i = 0; (t[n++] = e[i++]););
									t.length = n - 1;
								},
						};
					}
					(_ = e.support = {}),
						(T = e.isXML = function (t) {
							var e = t.namespaceURI,
								n = (t.ownerDocument || t).documentElement;
							return !vt.test(e || (n && n.nodeName) || "HTML");
						}),
						(I = e.setDocument = function (t) {
							var e,
								n,
								i = t ? t.ownerDocument || t : q;
							return i !== A && 9 === i.nodeType && i.documentElement
								? ((A = i),
									(M = A.documentElement),
									(N = !T(A)),
									q !== A && (n = A.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", Tt, !1) : n.attachEvent && n.attachEvent("onunload", Tt)),
									(_.attributes = r(function (t) {
										return (t.className = "i"), !t.getAttribute("className");
									})),
									(_.getElementsByTagName = r(function (t) {
										return t.appendChild(A.createComment("")), !t.getElementsByTagName("*").length;
									})),
									(_.getElementsByClassName = yt.test(A.getElementsByClassName)),
									(_.getById = r(function (t) {
										return (M.appendChild(t).id = H), !A.getElementsByName || !A.getElementsByName(H).length;
									})),
									_.getById
										? ((C.filter.ID = function (t) {
											var e = t.replace(wt, _t);
											return function (t) {
												return t.getAttribute("id") === e;
											};
										}),
											(C.find.ID = function (t, e) {
												if ("undefined" != typeof e.getElementById && N) {
													var n = e.getElementById(t);
													return n ? [n] : [];
												}
											}))
										: ((C.filter.ID = function (t) {
											var e = t.replace(wt, _t);
											return function (t) {
												var n = "undefined" != typeof t.getAttributeNode && t.getAttributeNode("id");
												return n && n.value === e;
											};
										}),
											(C.find.ID = function (t, e) {
												if ("undefined" != typeof e.getElementById && N) {
													var n,
														i,
														r,
														s = e.getElementById(t);
													if (s) {
														if (((n = s.getAttributeNode("id")), n && n.value === t)) return [s];
														for (r = e.getElementsByName(t), i = 0; (s = r[i++]);) if (((n = s.getAttributeNode("id")), n && n.value === t)) return [s];
													}
													return [];
												}
											})),
									(C.find.TAG = _.getElementsByTagName
										? function (t, e) {
											return "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t) : _.qsa ? e.querySelectorAll(t) : void 0;
										}
										: function (t, e) {
											var n,
												i = [],
												r = 0,
												s = e.getElementsByTagName(t);
											if ("*" === t) {
												for (; (n = s[r++]);) 1 === n.nodeType && i.push(n);
												return i;
											}
											return s;
										}),
									(C.find.CLASS =
										_.getElementsByClassName &&
										function (t, e) {
											if ("undefined" != typeof e.getElementsByClassName && N) return e.getElementsByClassName(t);
										}),
									(R = []),
									(L = []),
									(_.qsa = yt.test(A.querySelectorAll)) &&
									(r(function (t) {
										(M.appendChild(t).innerHTML = "<a id='" + H + "'></a><select id='" + H + "-\r\\' msallowcapture=''><option selected=''></option></select>"),
											t.querySelectorAll("[msallowcapture^='']").length && L.push("[*^$]=" + it + "*(?:''|\"\")"),
											t.querySelectorAll("[selected]").length || L.push("\\[" + it + "*(?:value|" + nt + ")"),
											t.querySelectorAll("[id~=" + H + "-]").length || L.push("~="),
											t.querySelectorAll(":checked").length || L.push(":checked"),
											t.querySelectorAll("a#" + H + "+*").length || L.push(".#.+[+~]");
									}),
										r(function (t) {
											t.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
											var e = A.createElement("input");
											e.setAttribute("type", "hidden"),
												t.appendChild(e).setAttribute("name", "D"),
												t.querySelectorAll("[name=d]").length && L.push("name" + it + "*[*^$|!~]?="),
												2 !== t.querySelectorAll(":enabled").length && L.push(":enabled", ":disabled"),
												(M.appendChild(t).disabled = !0),
												2 !== t.querySelectorAll(":disabled").length && L.push(":enabled", ":disabled"),
												t.querySelectorAll("*,:x"),
												L.push(",.*:");
										})),
									(_.matchesSelector = yt.test((P = M.matches || M.webkitMatchesSelector || M.mozMatchesSelector || M.oMatchesSelector || M.msMatchesSelector))) &&
									r(function (t) {
										(_.disconnectedMatch = P.call(t, "*")), P.call(t, "[s!='']:x"), R.push("!=", ot);
									}),
									(L = L.length && new RegExp(L.join("|"))),
									(R = R.length && new RegExp(R.join("|"))),
									(e = yt.test(M.compareDocumentPosition)),
									(z =
										e || yt.test(M.contains)
											? function (t, e) {
												var n = 9 === t.nodeType ? t.documentElement : t,
													i = e && e.parentNode;
												return t === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i)));
											}
											: function (t, e) {
												if (e) for (; (e = e.parentNode);) if (e === t) return !0;
												return !1;
											}),
									(X = e
										? function (t, e) {
											if (t === e) return (D = !0), 0;
											var n = !t.compareDocumentPosition - !e.compareDocumentPosition;
											return n
												? n
												: ((n = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1),
													1 & n || (!_.sortDetached && e.compareDocumentPosition(t) === n)
														? t === A || (t.ownerDocument === q && z(q, t))
															? -1
															: e === A || (e.ownerDocument === q && z(q, e))
																? 1
																: $
																	? et($, t) - et($, e)
																	: 0
														: 4 & n
															? -1
															: 1);
										}
										: function (t, e) {
											if (t === e) return (D = !0), 0;
											var n,
												i = 0,
												r = t.parentNode,
												s = e.parentNode,
												a = [t],
												l = [e];
											if (!r || !s) return t === A ? -1 : e === A ? 1 : r ? -1 : s ? 1 : $ ? et($, t) - et($, e) : 0;
											if (r === s) return o(t, e);
											for (n = t; (n = n.parentNode);) a.unshift(n);
											for (n = e; (n = n.parentNode);) l.unshift(n);
											for (; a[i] === l[i];) i++;
											return i ? o(a[i], l[i]) : a[i] === q ? -1 : l[i] === q ? 1 : 0;
										}),
									A)
								: A;
						}),
						(e.matches = function (t, n) {
							return e(t, null, null, n);
						}),
						(e.matchesSelector = function (t, n) {
							if (((t.ownerDocument || t) !== A && I(t), _.matchesSelector && N && !Y[n + " "] && (!R || !R.test(n)) && (!L || !L.test(n))))
								try {
									var i = P.call(t, n);
									if (i || _.disconnectedMatch || (t.document && 11 !== t.document.nodeType)) return i;
								} catch (r) {
									Y(n, !0);
								}
							return e(n, A, null, [t]).length > 0;
						}),
						(e.contains = function (t, e) {
							return (t.ownerDocument || t) !== A && I(t), z(t, e);
						}),
						(e.attr = function (t, e) {
							(t.ownerDocument || t) !== A && I(t);
							var n = C.attrHandle[e.toLowerCase()],
								i = n && G.call(C.attrHandle, e.toLowerCase()) ? n(t, e, !N) : void 0;
							return void 0 !== i ? i : _.attributes || !N ? t.getAttribute(e) : (i = t.getAttributeNode(e)) && i.specified ? i.value : null;
						}),
						(e.escape = function (t) {
							return (t + "").replace(Ct, kt);
						}),
						(e.error = function (t) {
							throw new Error("Syntax error, unrecognized expression: " + t);
						}),
						(e.uniqueSort = function (t) {
							var e,
								n = [],
								i = 0,
								r = 0;
							if (((D = !_.detectDuplicates), ($ = !_.sortStable && t.slice(0)), t.sort(X), D)) {
								for (; (e = t[r++]);) e === t[r] && (i = n.push(r));
								for (; i--;) t.splice(n[i], 1);
							}
							return ($ = null), t;
						}),
						(k = e.getText = function (t) {
							var e,
								n = "",
								i = 0,
								r = t.nodeType;
							if (r) {
								if (1 === r || 9 === r || 11 === r) {
									if ("string" == typeof t.textContent) return t.textContent;
									for (t = t.firstChild; t; t = t.nextSibling) n += k(t);
								} else if (3 === r || 4 === r) return t.nodeValue;
							} else for (; (e = t[i++]);) n += k(e);
							return n;
						}),
						(C = e.selectors = {
							cacheLength: 50,
							createPseudo: i,
							match: pt,
							attrHandle: {},
							find: {},
							relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } },
							preFilter: {
								ATTR: function (t) {
									return (t[1] = t[1].replace(wt, _t)), (t[3] = (t[3] || t[4] || t[5] || "").replace(wt, _t)), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4);
								},
								CHILD: function (t) {
									return (
										(t[1] = t[1].toLowerCase()),
										"nth" === t[1].slice(0, 3)
											? (t[3] || e.error(t[0]), (t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3]))), (t[5] = +(t[7] + t[8] || "odd" === t[3])))
											: t[3] && e.error(t[0]),
										t
									);
								},
								PSEUDO: function (t) {
									var e,
										n = !t[6] && t[2];
									return pt.CHILD.test(t[0])
										? null
										: (t[3] ? (t[2] = t[4] || t[5] || "") : n && dt.test(n) && (e = S(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && ((t[0] = t[0].slice(0, e)), (t[2] = n.slice(0, e))), t.slice(0, 3));
								},
							},
							filter: {
								TAG: function (t) {
									var e = t.replace(wt, _t).toLowerCase();
									return "*" === t
										? function () {
											return !0;
										}
										: function (t) {
											return t.nodeName && t.nodeName.toLowerCase() === e;
										};
								},
								CLASS: function (t) {
									var e = W[t + " "];
									return (
										e ||
										((e = new RegExp("(^|" + it + ")" + t + "(" + it + "|$)")) &&
											W(t, function (t) {
												return e.test(("string" == typeof t.className && t.className) || ("undefined" != typeof t.getAttribute && t.getAttribute("class")) || "");
											}))
									);
								},
								ATTR: function (t, n, i) {
									return function (r) {
										var s = e.attr(r, t);
										return null == s
											? "!=" === n
											: !n ||
											((s += ""),
												"=" === n
													? s === i
													: "!=" === n
														? s !== i
														: "^=" === n
															? i && 0 === s.indexOf(i)
															: "*=" === n
																? i && s.indexOf(i) > -1
																: "$=" === n
																	? i && s.slice(-i.length) === i
																	: "~=" === n
																		? (" " + s.replace(at, " ") + " ").indexOf(i) > -1
																		: "|=" === n && (s === i || s.slice(0, i.length + 1) === i + "-"));
									};
								},
								CHILD: function (t, e, n, i, r) {
									var s = "nth" !== t.slice(0, 3),
										o = "last" !== t.slice(-4),
										a = "of-type" === e;
									return 1 === i && 0 === r
										? function (t) {
											return !!t.parentNode;
										}
										: function (e, n, l) {
											var u,
												c,
												h,
												d,
												f,
												p,
												v = s !== o ? "nextSibling" : "previousSibling",
												g = e.parentNode,
												m = a && e.nodeName.toLowerCase(),
												y = !l && !a,
												b = !1;
											if (g) {
												if (s) {
													for (; v;) {
														for (d = e; (d = d[v]);) if (a ? d.nodeName.toLowerCase() === m : 1 === d.nodeType) return !1;
														p = v = "only" === t && !p && "nextSibling";
													}
													return !0;
												}
												if (((p = [o ? g.firstChild : g.lastChild]), o && y)) {
													for (
														d = g, h = d[H] || (d[H] = {}), c = h[d.uniqueID] || (h[d.uniqueID] = {}), u = c[t] || [], f = u[0] === F && u[1], b = f && u[2], d = f && g.childNodes[f];
														(d = (++f && d && d[v]) || (b = f = 0) || p.pop());

													)
														if (1 === d.nodeType && ++b && d === e) {
															c[t] = [F, f, b];
															break;
														}
												} else if ((y && ((d = e), (h = d[H] || (d[H] = {})), (c = h[d.uniqueID] || (h[d.uniqueID] = {})), (u = c[t] || []), (f = u[0] === F && u[1]), (b = f)), b === !1))
													for (
														;
														(d = (++f && d && d[v]) || (b = f = 0) || p.pop()) &&
														((a ? d.nodeName.toLowerCase() !== m : 1 !== d.nodeType) || !++b || (y && ((h = d[H] || (d[H] = {})), (c = h[d.uniqueID] || (h[d.uniqueID] = {})), (c[t] = [F, b])), d !== e));

													);
												return (b -= r), b === i || (b % i === 0 && b / i >= 0);
											}
										};
								},
								PSEUDO: function (t, n) {
									var r,
										s = C.pseudos[t] || C.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
									return s[H]
										? s(n)
										: s.length > 1
											? ((r = [t, t, "", n]),
												C.setFilters.hasOwnProperty(t.toLowerCase())
													? i(function (t, e) {
														for (var i, r = s(t, n), o = r.length; o--;) (i = et(t, r[o])), (t[i] = !(e[i] = r[o]));
													})
													: function (t) {
														return s(t, 0, r);
													})
											: s;
								},
							},
							pseudos: {
								not: i(function (t) {
									var e = [],
										n = [],
										r = E(t.replace(lt, "$1"));
									return r[H]
										? i(function (t, e, n, i) {
											for (var s, o = r(t, null, i, []), a = t.length; a--;) (s = o[a]) && (t[a] = !(e[a] = s));
										})
										: function (t, i, s) {
											return (e[0] = t), r(e, null, s, n), (e[0] = null), !n.pop();
										};
								}),
								has: i(function (t) {
									return function (n) {
										return e(t, n).length > 0;
									};
								}),
								contains: i(function (t) {
									return (
										(t = t.replace(wt, _t)),
										function (e) {
											return (e.textContent || k(e)).indexOf(t) > -1;
										}
									);
								}),
								lang: i(function (t) {
									return (
										ft.test(t || "") || e.error("unsupported lang: " + t),
										(t = t.replace(wt, _t).toLowerCase()),
										function (e) {
											var n;
											do if ((n = N ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang"))) return (n = n.toLowerCase()), n === t || 0 === n.indexOf(t + "-");
											while ((e = e.parentNode) && 1 === e.nodeType);
											return !1;
										}
									);
								}),
								target: function (e) {
									var n = t.location && t.location.hash;
									return n && n.slice(1) === e.id;
								},
								root: function (t) {
									return t === M;
								},
								focus: function (t) {
									return t === A.activeElement && (!A.hasFocus || A.hasFocus()) && !!(t.type || t.href || ~t.tabIndex);
								},
								enabled: u(!1),
								disabled: u(!0),
								checked: function (t) {
									var e = t.nodeName.toLowerCase();
									return ("input" === e && !!t.checked) || ("option" === e && !!t.selected);
								},
								selected: function (t) {
									return t.parentNode && t.parentNode.selectedIndex, t.selected === !0;
								},
								empty: function (t) {
									for (t = t.firstChild; t; t = t.nextSibling) if (t.nodeType < 6) return !1;
									return !0;
								},
								parent: function (t) {
									return !C.pseudos.empty(t);
								},
								header: function (t) {
									return mt.test(t.nodeName);
								},
								input: function (t) {
									return gt.test(t.nodeName);
								},
								button: function (t) {
									var e = t.nodeName.toLowerCase();
									return ("input" === e && "button" === t.type) || "button" === e;
								},
								text: function (t) {
									var e;
									return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase());
								},
								first: c(function () {
									return [0];
								}),
								last: c(function (t, e) {
									return [e - 1];
								}),
								eq: c(function (t, e, n) {
									return [n < 0 ? n + e : n];
								}),
								even: c(function (t, e) {
									for (var n = 0; n < e; n += 2) t.push(n);
									return t;
								}),
								odd: c(function (t, e) {
									for (var n = 1; n < e; n += 2) t.push(n);
									return t;
								}),
								lt: c(function (t, e, n) {
									for (var i = n < 0 ? n + e : n > e ? e : n; --i >= 0;) t.push(i);
									return t;
								}),
								gt: c(function (t, e, n) {
									for (var i = n < 0 ? n + e : n; ++i < e;) t.push(i);
									return t;
								}),
							},
						}),
						(C.pseudos.nth = C.pseudos.eq);
					for (w in { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }) C.pseudos[w] = a(w);
					for (w in { submit: !0, reset: !0 }) C.pseudos[w] = l(w);
					return (
						(d.prototype = C.filters = C.pseudos),
						(C.setFilters = new d()),
						(S = e.tokenize = function (t, n) {
							var i,
								r,
								s,
								o,
								a,
								l,
								u,
								c = V[t + " "];
							if (c) return n ? 0 : c.slice(0);
							for (a = t, l = [], u = C.preFilter; a;) {
								(i && !(r = ut.exec(a))) || (r && (a = a.slice(r[0].length) || a), l.push((s = []))),
									(i = !1),
									(r = ct.exec(a)) && ((i = r.shift()), s.push({ value: i, type: r[0].replace(lt, " ") }), (a = a.slice(i.length)));
								for (o in C.filter) !(r = pt[o].exec(a)) || (u[o] && !(r = u[o](r))) || ((i = r.shift()), s.push({ value: i, type: o, matches: r }), (a = a.slice(i.length)));
								if (!i) break;
							}
							return n ? a.length : a ? e.error(t) : V(t, l).slice(0);
						}),
						(E = e.compile = function (t, e) {
							var n,
								i = [],
								r = [],
								s = U[t + " "];
							if (!s) {
								for (e || (e = S(t)), n = e.length; n--;) (s = b(e[n])), s[H] ? i.push(s) : r.push(s);
								(s = U(t, x(r, i))), (s.selector = t);
							}
							return s;
						}),
						(j = e.select = function (t, e, n, i) {
							var r,
								s,
								o,
								a,
								l,
								u = "function" == typeof t && t,
								c = !i && S((t = u.selector || t));
							if (((n = n || []), 1 === c.length)) {
								if (((s = c[0] = c[0].slice(0)), s.length > 2 && "ID" === (o = s[0]).type && 9 === e.nodeType && N && C.relative[s[1].type])) {
									if (((e = (C.find.ID(o.matches[0].replace(wt, _t), e) || [])[0]), !e)) return n;
									u && (e = e.parentNode), (t = t.slice(s.shift().value.length));
								}
								for (r = pt.needsContext.test(t) ? 0 : s.length; r-- && ((o = s[r]), !C.relative[(a = o.type)]);)
									if ((l = C.find[a]) && (i = l(o.matches[0].replace(wt, _t), (xt.test(s[0].type) && h(e.parentNode)) || e))) {
										if ((s.splice(r, 1), (t = i.length && f(s)), !t)) return Q.apply(n, i), n;
										break;
									}
							}
							return (u || E(t, c))(i, e, !N, n, !e || (xt.test(t) && h(e.parentNode)) || e), n;
						}),
						(_.sortStable = H.split("").sort(X).join("") === H),
						(_.detectDuplicates = !!D),
						I(),
						(_.sortDetached = r(function (t) {
							return 1 & t.compareDocumentPosition(A.createElement("fieldset"));
						})),
						r(function (t) {
							return (t.innerHTML = "<a href='#'></a>"), "#" === t.firstChild.getAttribute("href");
						}) ||
						s("type|href|height|width", function (t, e, n) {
							if (!n) return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2);
						}),
						(_.attributes &&
							r(function (t) {
								return (t.innerHTML = "<input/>"), t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value");
							})) ||
						s("value", function (t, e, n) {
							if (!n && "input" === t.nodeName.toLowerCase()) return t.defaultValue;
						}),
						r(function (t) {
							return null == t.getAttribute("disabled");
						}) ||
						s(nt, function (t, e, n) {
							var i;
							if (!n) return t[e] === !0 ? e.toLowerCase() : (i = t.getAttributeNode(e)) && i.specified ? i.value : null;
						}),
						e
					);
				})(n);
			(jt.find = $t),
				(jt.expr = $t.selectors),
				(jt.expr[":"] = jt.expr.pseudos),
				(jt.uniqueSort = jt.unique = $t.uniqueSort),
				(jt.text = $t.getText),
				(jt.isXMLDoc = $t.isXML),
				(jt.contains = $t.contains),
				(jt.escapeSelector = $t.escape);
			var Dt = function (t, e, n) {
				for (var i = [], r = void 0 !== n; (t = t[e]) && 9 !== t.nodeType;)
					if (1 === t.nodeType) {
						if (r && jt(t).is(n)) break;
						i.push(t);
					}
				return i;
			},
				It = function (t, e) {
					for (var n = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && n.push(t);
					return n;
				},
				At = jt.expr.match.needsContext,
				Mt = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;
			(jt.filter = function (t, e, n) {
				var i = e[0];
				return (
					n && (t = ":not(" + t + ")"),
					1 === e.length && 1 === i.nodeType
						? jt.find.matchesSelector(i, t)
							? [i]
							: []
						: jt.find.matches(
							t,
							jt.grep(e, function (t) {
								return 1 === t.nodeType;
							})
						)
				);
			}),
				jt.fn.extend({
					find: function (t) {
						var e,
							n,
							i = this.length,
							r = this;
						if ("string" != typeof t)
							return this.pushStack(
								jt(t).filter(function () {
									for (e = 0; e < i; e++) if (jt.contains(r[e], this)) return !0;
								})
							);
						for (n = this.pushStack([]), e = 0; e < i; e++) jt.find(t, r[e], n);
						return i > 1 ? jt.uniqueSort(n) : n;
					},
					filter: function (t) {
						return this.pushStack(c(this, t || [], !1));
					},
					not: function (t) {
						return this.pushStack(c(this, t || [], !0));
					},
					is: function (t) {
						return !!c(this, "string" == typeof t && At.test(t) ? jt(t) : t || [], !1).length;
					},
				});
			var Nt,
				Lt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
				Rt = (jt.fn.init = function (t, e, n) {
					var i, r;
					if (!t) return this;
					if (((n = n || Nt), "string" == typeof t)) {
						if (((i = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : Lt.exec(t)), !i || (!i[1] && e))) return !e || e.jquery ? (e || n).find(t) : this.constructor(e).find(t);
						if (i[1]) {
							if (((e = e instanceof jt ? e[0] : e), jt.merge(this, jt.parseHTML(i[1], e && e.nodeType ? e.ownerDocument || e : dt, !0)), Mt.test(i[1]) && jt.isPlainObject(e)))
								for (i in e) kt(this[i]) ? this[i](e[i]) : this.attr(i, e[i]);
							return this;
						}
						return (r = dt.getElementById(i[2])), r && ((this[0] = r), (this.length = 1)), this;
					}
					return t.nodeType ? ((this[0] = t), (this.length = 1), this) : kt(t) ? (void 0 !== n.ready ? n.ready(t) : t(jt)) : jt.makeArray(t, this);
				});
			(Rt.prototype = jt.fn), (Nt = jt(dt));
			var Pt = /^(?:parents|prev(?:Until|All))/,
				zt = { children: !0, contents: !0, next: !0, prev: !0 };
			jt.fn.extend({
				has: function (t) {
					var e = jt(t, this),
						n = e.length;
					return this.filter(function () {
						for (var t = 0; t < n; t++) if (jt.contains(this, e[t])) return !0;
					});
				},
				closest: function (t, e) {
					var n,
						i = 0,
						r = this.length,
						s = [],
						o = "string" != typeof t && jt(t);
					if (!At.test(t))
						for (; i < r; i++)
							for (n = this[i]; n && n !== e; n = n.parentNode)
								if (n.nodeType < 11 && (o ? o.index(n) > -1 : 1 === n.nodeType && jt.find.matchesSelector(n, t))) {
									s.push(n);
									break;
								}
					return this.pushStack(s.length > 1 ? jt.uniqueSort(s) : s);
				},
				index: function (t) {
					return t ? ("string" == typeof t ? mt.call(jt(t), this[0]) : mt.call(this, t.jquery ? t[0] : t)) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
				},
				add: function (t, e) {
					return this.pushStack(jt.uniqueSort(jt.merge(this.get(), jt(t, e))));
				},
				addBack: function (t) {
					return this.add(null == t ? this.prevObject : this.prevObject.filter(t));
				},
			}),
				jt.each(
					{
						parent: function (t) {
							var e = t.parentNode;
							return e && 11 !== e.nodeType ? e : null;
						},
						parents: function (t) {
							return Dt(t, "parentNode");
						},
						parentsUntil: function (t, e, n) {
							return Dt(t, "parentNode", n);
						},
						next: function (t) {
							return h(t, "nextSibling");
						},
						prev: function (t) {
							return h(t, "previousSibling");
						},
						nextAll: function (t) {
							return Dt(t, "nextSibling");
						},
						prevAll: function (t) {
							return Dt(t, "previousSibling");
						},
						nextUntil: function (t, e, n) {
							return Dt(t, "nextSibling", n);
						},
						prevUntil: function (t, e, n) {
							return Dt(t, "previousSibling", n);
						},
						siblings: function (t) {
							return It((t.parentNode || {}).firstChild, t);
						},
						children: function (t) {
							return It(t.firstChild);
						},
						contents: function (t) {
							return "undefined" != typeof t.contentDocument ? t.contentDocument : (u(t, "template") && (t = t.content || t), jt.merge([], t.childNodes));
						},
					},
					function (t, e) {
						jt.fn[t] = function (n, i) {
							var r = jt.map(this, e, n);
							return "Until" !== t.slice(-5) && (i = n), i && "string" == typeof i && (r = jt.filter(i, r)), this.length > 1 && (zt[t] || jt.uniqueSort(r), Pt.test(t) && r.reverse()), this.pushStack(r);
						};
					}
				);
			var Ht = /[^\x20\t\r\n\f]+/g;
			(jt.Callbacks = function (t) {
				t = "string" == typeof t ? d(t) : jt.extend({}, t);
				var e,
					n,
					i,
					r,
					s = [],
					o = [],
					l = -1,
					u = function () {
						for (r = r || t.once, i = e = !0; o.length; l = -1) for (n = o.shift(); ++l < s.length;) s[l].apply(n[0], n[1]) === !1 && t.stopOnFalse && ((l = s.length), (n = !1));
						t.memory || (n = !1), (e = !1), r && (s = n ? [] : "");
					},
					c = {
						add: function () {
							return (
								s &&
								(n && !e && ((l = s.length - 1), o.push(n)),
									(function i(e) {
										jt.each(e, function (e, n) {
											kt(n) ? (t.unique && c.has(n)) || s.push(n) : n && n.length && "string" !== a(n) && i(n);
										});
									})(arguments),
									n && !e && u()),
								this
							);
						},
						remove: function () {
							return (
								jt.each(arguments, function (t, e) {
									for (var n; (n = jt.inArray(e, s, n)) > -1;) s.splice(n, 1), n <= l && l--;
								}),
								this
							);
						},
						has: function (t) {
							return t ? jt.inArray(t, s) > -1 : s.length > 0;
						},
						empty: function () {
							return s && (s = []), this;
						},
						disable: function () {
							return (r = o = []), (s = n = ""), this;
						},
						disabled: function () {
							return !s;
						},
						lock: function () {
							return (r = o = []), n || e || (s = n = ""), this;
						},
						locked: function () {
							return !!r;
						},
						fireWith: function (t, n) {
							return r || ((n = n || []), (n = [t, n.slice ? n.slice() : n]), o.push(n), e || u()), this;
						},
						fire: function () {
							return c.fireWith(this, arguments), this;
						},
						fired: function () {
							return !!i;
						},
					};
				return c;
			}),
				jt.extend({
					Deferred: function (t) {
						var e = [
							["notify", "progress", jt.Callbacks("memory"), jt.Callbacks("memory"), 2],
							["resolve", "done", jt.Callbacks("once memory"), jt.Callbacks("once memory"), 0, "resolved"],
							["reject", "fail", jt.Callbacks("once memory"), jt.Callbacks("once memory"), 1, "rejected"],
						],
							i = "pending",
							r = {
								state: function () {
									return i;
								},
								always: function () {
									return s.done(arguments).fail(arguments), this;
								},
								catch: function (t) {
									return r.then(null, t);
								},
								pipe: function () {
									var t = arguments;
									return jt
										.Deferred(function (n) {
											jt.each(e, function (e, i) {
												var r = kt(t[i[4]]) && t[i[4]];
												s[i[1]](function () {
													var t = r && r.apply(this, arguments);
													t && kt(t.promise) ? t.promise().progress(n.notify).done(n.resolve).fail(n.reject) : n[i[0] + "With"](this, r ? [t] : arguments);
												});
											}),
												(t = null);
										})
										.promise();
								},
								then: function (t, i, r) {
									function s(t, e, i, r) {
										return function () {
											var a = this,
												l = arguments,
												u = function () {
													var n, u;
													if (!(t < o)) {
														if (((n = i.apply(a, l)), n === e.promise())) throw new TypeError("Thenable self-resolution");
														(u = n && ("object" == typeof n || "function" == typeof n) && n.then),
															kt(u)
																? r
																	? u.call(n, s(o, e, f, r), s(o, e, p, r))
																	: (o++, u.call(n, s(o, e, f, r), s(o, e, p, r), s(o, e, f, e.notifyWith)))
																: (i !== f && ((a = void 0), (l = [n])), (r || e.resolveWith)(a, l));
													}
												},
												c = r
													? u
													: function () {
														try {
															u();
														} catch (n) {
															jt.Deferred.exceptionHook && jt.Deferred.exceptionHook(n, c.stackTrace), t + 1 >= o && (i !== p && ((a = void 0), (l = [n])), e.rejectWith(a, l));
														}
													};
											t ? c() : (jt.Deferred.getStackHook && (c.stackTrace = jt.Deferred.getStackHook()), n.setTimeout(c));
										};
									}
									var o = 0;
									return jt
										.Deferred(function (n) {
											e[0][3].add(s(0, n, kt(r) ? r : f, n.notifyWith)), e[1][3].add(s(0, n, kt(t) ? t : f)), e[2][3].add(s(0, n, kt(i) ? i : p));
										})
										.promise();
								},
								promise: function (t) {
									return null != t ? jt.extend(t, r) : r;
								},
							},
							s = {};
						return (
							jt.each(e, function (t, n) {
								var o = n[2],
									a = n[5];
								(r[n[1]] = o.add),
									a &&
									o.add(
										function () {
											i = a;
										},
										e[3 - t][2].disable,
										e[3 - t][3].disable,
										e[0][2].lock,
										e[0][3].lock
									),
									o.add(n[3].fire),
									(s[n[0]] = function () {
										return s[n[0] + "With"](this === s ? void 0 : this, arguments), this;
									}),
									(s[n[0] + "With"] = o.fireWith);
							}),
							r.promise(s),
							t && t.call(s, s),
							s
						);
					},
					when: function (t) {
						var e = arguments.length,
							n = e,
							i = Array(n),
							r = pt.call(arguments),
							s = jt.Deferred(),
							o = function (t) {
								return function (n) {
									(i[t] = this), (r[t] = arguments.length > 1 ? pt.call(arguments) : n), --e || s.resolveWith(i, r);
								};
							};
						if (e <= 1 && (v(t, s.done(o(n)).resolve, s.reject, !e), "pending" === s.state() || kt(r[n] && r[n].then))) return s.then();
						for (; n--;) v(r[n], o(n), s.reject);
						return s.promise();
					},
				});
			var qt = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
			(jt.Deferred.exceptionHook = function (t, e) {
				n.console && n.console.warn && t && qt.test(t.name) && n.console.warn("jQuery.Deferred exception: " + t.message, t.stack, e);
			}),
				(jt.readyException = function (t) {
					n.setTimeout(function () {
						throw t;
					});
				});
			var Ft = jt.Deferred();
			(jt.fn.ready = function (t) {
				return (
					Ft.then(t)["catch"](function (t) {
						jt.readyException(t);
					}),
					this
				);
			}),
				jt.extend({
					isReady: !1,
					readyWait: 1,
					ready: function (t) {
						(t === !0 ? --jt.readyWait : jt.isReady) || ((jt.isReady = !0), (t !== !0 && --jt.readyWait > 0) || Ft.resolveWith(dt, [jt]));
					},
				}),
				(jt.ready.then = Ft.then),
				"complete" === dt.readyState || ("loading" !== dt.readyState && !dt.documentElement.doScroll) ? n.setTimeout(jt.ready) : (dt.addEventListener("DOMContentLoaded", g), n.addEventListener("load", g));
			var Bt = function (t, e, n, i, r, s, o) {
				var l = 0,
					u = t.length,
					c = null == n;
				if ("object" === a(n)) {
					r = !0;
					for (l in n) Bt(t, e, l, n[l], !0, s, o);
				} else if (
					void 0 !== i &&
					((r = !0),
						kt(i) || (o = !0),
						c &&
						(o
							? (e.call(t, i), (e = null))
							: ((c = e),
								(e = function (t, e, n) {
									return c.call(jt(t), n);
								}))),
						e)
				)
					for (; l < u; l++) e(t[l], n, o ? i : i.call(t[l], l, e(t[l], n)));
				return r ? t : c ? e.call(t) : u ? e(t[0], n) : s;
			},
				Wt = /^-ms-/,
				Vt = /-([a-z])/g,
				Ut = function (t) {
					return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType;
				};
			(b.uid = 1),
				(b.prototype = {
					cache: function (t) {
						var e = t[this.expando];
						return e || ((e = {}), Ut(t) && (t.nodeType ? (t[this.expando] = e) : Object.defineProperty(t, this.expando, { value: e, configurable: !0 }))), e;
					},
					set: function (t, e, n) {
						var i,
							r = this.cache(t);
						if ("string" == typeof e) r[y(e)] = n;
						else for (i in e) r[y(i)] = e[i];
						return r;
					},
					get: function (t, e) {
						return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][y(e)];
					},
					access: function (t, e, n) {
						return void 0 === e || (e && "string" == typeof e && void 0 === n) ? this.get(t, e) : (this.set(t, e, n), void 0 !== n ? n : e);
					},
					remove: function (t, e) {
						var n,
							i = t[this.expando];
						if (void 0 !== i) {
							if (void 0 !== e) {
								Array.isArray(e) ? (e = e.map(y)) : ((e = y(e)), (e = e in i ? [e] : e.match(Ht) || [])), (n = e.length);
								for (; n--;) delete i[e[n]];
							}
							(void 0 === e || jt.isEmptyObject(i)) && (t.nodeType ? (t[this.expando] = void 0) : delete t[this.expando]);
						}
					},
					hasData: function (t) {
						var e = t[this.expando];
						return void 0 !== e && !jt.isEmptyObject(e);
					},
				});
			var Yt = new b(),
				Xt = new b(),
				Gt = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
				Jt = /[A-Z]/g;
			jt.extend({
				hasData: function (t) {
					return Xt.hasData(t) || Yt.hasData(t);
				},
				data: function (t, e, n) {
					return Xt.access(t, e, n);
				},
				removeData: function (t, e) {
					Xt.remove(t, e);
				},
				_data: function (t, e, n) {
					return Yt.access(t, e, n);
				},
				_removeData: function (t, e) {
					Yt.remove(t, e);
				},
			}),
				jt.fn.extend({
					data: function (t, e) {
						var n,
							i,
							r,
							s = this[0],
							o = s && s.attributes;
						if (void 0 === t) {
							if (this.length && ((r = Xt.get(s)), 1 === s.nodeType && !Yt.get(s, "hasDataAttrs"))) {
								for (n = o.length; n--;) o[n] && ((i = o[n].name), 0 === i.indexOf("data-") && ((i = y(i.slice(5))), w(s, i, r[i])));
								Yt.set(s, "hasDataAttrs", !0);
							}
							return r;
						}
						return "object" == typeof t
							? this.each(function () {
								Xt.set(this, t);
							})
							: Bt(
								this,
								function (e) {
									var n;
									if (s && void 0 === e) {
										if (((n = Xt.get(s, t)), void 0 !== n)) return n;
										if (((n = w(s, t)), void 0 !== n)) return n;
									} else
										this.each(function () {
											Xt.set(this, t, e);
										});
								},
								null,
								e,
								arguments.length > 1,
								null,
								!0
							);
					},
					removeData: function (t) {
						return this.each(function () {
							Xt.remove(this, t);
						});
					},
				}),
				jt.extend({
					queue: function (t, e, n) {
						var i;
						if (t) return (e = (e || "fx") + "queue"), (i = Yt.get(t, e)), n && (!i || Array.isArray(n) ? (i = Yt.access(t, e, jt.makeArray(n))) : i.push(n)), i || [];
					},
					dequeue: function (t, e) {
						e = e || "fx";
						var n = jt.queue(t, e),
							i = n.length,
							r = n.shift(),
							s = jt._queueHooks(t, e),
							o = function () {
								jt.dequeue(t, e);
							};
						"inprogress" === r && ((r = n.shift()), i--), r && ("fx" === e && n.unshift("inprogress"), delete s.stop, r.call(t, o, s)), !i && s && s.empty.fire();
					},
					_queueHooks: function (t, e) {
						var n = e + "queueHooks";
						return (
							Yt.get(t, n) ||
							Yt.access(t, n, {
								empty: jt.Callbacks("once memory").add(function () {
									Yt.remove(t, [e + "queue", n]);
								}),
							})
						);
					},
				}),
				jt.fn.extend({
					queue: function (t, e) {
						var n = 2;
						return (
							"string" != typeof t && ((e = t), (t = "fx"), n--),
							arguments.length < n
								? jt.queue(this[0], t)
								: void 0 === e
									? this
									: this.each(function () {
										var n = jt.queue(this, t, e);
										jt._queueHooks(this, t), "fx" === t && "inprogress" !== n[0] && jt.dequeue(this, t);
									})
						);
					},
					dequeue: function (t) {
						return this.each(function () {
							jt.dequeue(this, t);
						});
					},
					clearQueue: function (t) {
						return this.queue(t || "fx", []);
					},
					promise: function (t, e) {
						var n,
							i = 1,
							r = jt.Deferred(),
							s = this,
							o = this.length,
							a = function () {
								--i || r.resolveWith(s, [s]);
							};
						for ("string" != typeof t && ((e = t), (t = void 0)), t = t || "fx"; o--;) (n = Yt.get(s[o], t + "queueHooks")), n && n.empty && (i++, n.empty.add(a));
						return a(), r.promise(e);
					},
				});
			var Zt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
				Kt = new RegExp("^(?:([+-])=|)(" + Zt + ")([a-z%]*)$", "i"),
				Qt = ["Top", "Right", "Bottom", "Left"],
				te = dt.documentElement,
				ee = function (t) {
					return jt.contains(t.ownerDocument, t);
				},
				ne = { composed: !0 };
			te.getRootNode &&
				(ee = function (t) {
					return jt.contains(t.ownerDocument, t) || t.getRootNode(ne) === t.ownerDocument;
				});
			var ie = function (t, e) {
				return (t = e || t), "none" === t.style.display || ("" === t.style.display && ee(t) && "none" === jt.css(t, "display"));
			},
				re = function (t, e, n, i) {
					var r,
						s,
						o = {};
					for (s in e) (o[s] = t.style[s]), (t.style[s] = e[s]);
					r = n.apply(t, i || []);
					for (s in e) t.style[s] = o[s];
					return r;
				},
				se = {};
			jt.fn.extend({
				show: function () {
					return k(this, !0);
				},
				hide: function () {
					return k(this);
				},
				toggle: function (t) {
					return "boolean" == typeof t
						? t
							? this.show()
							: this.hide()
						: this.each(function () {
							ie(this) ? jt(this).show() : jt(this).hide();
						});
				},
			});
			var oe = /^(?:checkbox|radio)$/i,
				ae = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i,
				le = /^$|^module$|\/(?:java|ecma)script/i,
				ue = {
					option: [1, "<select multiple='multiple'>", "</select>"],
					thead: [1, "<table>", "</table>"],
					col: [2, "<table><colgroup>", "</colgroup></table>"],
					tr: [2, "<table><tbody>", "</tbody></table>"],
					td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
					_default: [0, "", ""],
				};
			(ue.optgroup = ue.option), (ue.tbody = ue.tfoot = ue.colgroup = ue.caption = ue.thead), (ue.th = ue.td);
			var ce = /<|&#?\w+;/;
			!(function () {
				var t = dt.createDocumentFragment(),
					e = t.appendChild(dt.createElement("div")),
					n = dt.createElement("input");
				n.setAttribute("type", "radio"),
					n.setAttribute("checked", "checked"),
					n.setAttribute("name", "t"),
					e.appendChild(n),
					(Ct.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked),
					(e.innerHTML = "<textarea>x</textarea>"),
					(Ct.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue);
			})();
			var he = /^key/,
				de = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
				fe = /^([^.]*)(?:\.(.+)|)/;
			(jt.event = {
				global: {},
				add: function (t, e, n, i, r) {
					var s,
						o,
						a,
						l,
						u,
						c,
						h,
						d,
						f,
						p,
						v,
						g = Yt.get(t);
					if (g)
						for (
							n.handler && ((s = n), (n = s.handler), (r = s.selector)),
							r && jt.find.matchesSelector(te, r),
							n.guid || (n.guid = jt.guid++),
							(l = g.events) || (l = g.events = {}),
							(o = g.handle) ||
							(o = g.handle = function (e) {
								return "undefined" != typeof jt && jt.event.triggered !== e.type ? jt.event.dispatch.apply(t, arguments) : void 0;
							}),
							e = (e || "").match(Ht) || [""],
							u = e.length;
							u--;

						)
							(a = fe.exec(e[u]) || []),
								(f = v = a[1]),
								(p = (a[2] || "").split(".").sort()),
								f &&
								((h = jt.event.special[f] || {}),
									(f = (r ? h.delegateType : h.bindType) || f),
									(h = jt.event.special[f] || {}),
									(c = jt.extend({ type: f, origType: v, data: i, handler: n, guid: n.guid, selector: r, needsContext: r && jt.expr.match.needsContext.test(r), namespace: p.join(".") }, s)),
									(d = l[f]) || ((d = l[f] = []), (d.delegateCount = 0), (h.setup && h.setup.call(t, i, p, o) !== !1) || (t.addEventListener && t.addEventListener(f, o))),
									h.add && (h.add.call(t, c), c.handler.guid || (c.handler.guid = n.guid)),
									r ? d.splice(d.delegateCount++, 0, c) : d.push(c),
									(jt.event.global[f] = !0));
				},
				remove: function (t, e, n, i, r) {
					var s,
						o,
						a,
						l,
						u,
						c,
						h,
						d,
						f,
						p,
						v,
						g = Yt.hasData(t) && Yt.get(t);
					if (g && (l = g.events)) {
						for (e = (e || "").match(Ht) || [""], u = e.length; u--;)
							if (((a = fe.exec(e[u]) || []), (f = v = a[1]), (p = (a[2] || "").split(".").sort()), f)) {
								for (h = jt.event.special[f] || {}, f = (i ? h.delegateType : h.bindType) || f, d = l[f] || [], a = a[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), o = s = d.length; s--;)
									(c = d[s]),
										(!r && v !== c.origType) ||
										(n && n.guid !== c.guid) ||
										(a && !a.test(c.namespace)) ||
										(i && i !== c.selector && ("**" !== i || !c.selector)) ||
										(d.splice(s, 1), c.selector && d.delegateCount--, h.remove && h.remove.call(t, c));
								o && !d.length && ((h.teardown && h.teardown.call(t, p, g.handle) !== !1) || jt.removeEvent(t, f, g.handle), delete l[f]);
							} else for (f in l) jt.event.remove(t, f + e[u], n, i, !0);
						jt.isEmptyObject(l) && Yt.remove(t, "handle events");
					}
				},
				dispatch: function (t) {
					var e,
						n,
						i,
						r,
						s,
						o,
						a = jt.event.fix(t),
						l = new Array(arguments.length),
						u = (Yt.get(this, "events") || {})[a.type] || [],
						c = jt.event.special[a.type] || {};
					for (l[0] = a, e = 1; e < arguments.length; e++) l[e] = arguments[e];
					if (((a.delegateTarget = this), !c.preDispatch || c.preDispatch.call(this, a) !== !1)) {
						for (o = jt.event.handlers.call(this, a, u), e = 0; (r = o[e++]) && !a.isPropagationStopped();)
							for (a.currentTarget = r.elem, n = 0; (s = r.handlers[n++]) && !a.isImmediatePropagationStopped();)
								(a.rnamespace && s.namespace !== !1 && !a.rnamespace.test(s.namespace)) ||
									((a.handleObj = s),
										(a.data = s.data),
										(i = ((jt.event.special[s.origType] || {}).handle || s.handler).apply(r.elem, l)),
										void 0 !== i && (a.result = i) === !1 && (a.preventDefault(), a.stopPropagation()));
						return c.postDispatch && c.postDispatch.call(this, a), a.result;
					}
				},
				handlers: function (t, e) {
					var n,
						i,
						r,
						s,
						o,
						a = [],
						l = e.delegateCount,
						u = t.target;
					if (l && u.nodeType && !("click" === t.type && t.button >= 1))
						for (; u !== this; u = u.parentNode || this)
							if (1 === u.nodeType && ("click" !== t.type || u.disabled !== !0)) {
								for (s = [], o = {}, n = 0; n < l; n++) (i = e[n]), (r = i.selector + " "), void 0 === o[r] && (o[r] = i.needsContext ? jt(r, this).index(u) > -1 : jt.find(r, this, null, [u]).length), o[r] && s.push(i);
								s.length && a.push({ elem: u, handlers: s });
							}
					return (u = this), l < e.length && a.push({ elem: u, handlers: e.slice(l) }), a;
				},
				addProp: function (t, e) {
					Object.defineProperty(jt.Event.prototype, t, {
						enumerable: !0,
						configurable: !0,
						get: kt(e)
							? function () {
								if (this.originalEvent) return e(this.originalEvent);
							}
							: function () {
								if (this.originalEvent) return this.originalEvent[t];
							},
						set: function (e) {
							Object.defineProperty(this, t, { enumerable: !0, configurable: !0, writable: !0, value: e });
						},
					});
				},
				fix: function (t) {
					return t[jt.expando] ? t : new jt.Event(t);
				},
				special: {
					load: { noBubble: !0 },
					click: {
						setup: function (t) {
							var e = this || t;
							return oe.test(e.type) && e.click && u(e, "input") && A(e, "click", j), !1;
						},
						trigger: function (t) {
							var e = this || t;
							return oe.test(e.type) && e.click && u(e, "input") && A(e, "click"), !0;
						},
						_default: function (t) {
							var e = t.target;
							return (oe.test(e.type) && e.click && u(e, "input") && Yt.get(e, "click")) || u(e, "a");
						},
					},
					beforeunload: {
						postDispatch: function (t) {
							void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result);
						},
					},
				},
			}),
				(jt.removeEvent = function (t, e, n) {
					t.removeEventListener && t.removeEventListener(e, n);
				}),
				(jt.Event = function (t, e) {
					return this instanceof jt.Event
						? (t && t.type
							? ((this.originalEvent = t),
								(this.type = t.type),
								(this.isDefaultPrevented = t.defaultPrevented || (void 0 === t.defaultPrevented && t.returnValue === !1) ? j : O),
								(this.target = t.target && 3 === t.target.nodeType ? t.target.parentNode : t.target),
								(this.currentTarget = t.currentTarget),
								(this.relatedTarget = t.relatedTarget))
							: (this.type = t),
							e && jt.extend(this, e),
							(this.timeStamp = (t && t.timeStamp) || Date.now()),
							void (this[jt.expando] = !0))
						: new jt.Event(t, e);
				}),
				(jt.Event.prototype = {
					constructor: jt.Event,
					isDefaultPrevented: O,
					isPropagationStopped: O,
					isImmediatePropagationStopped: O,
					isSimulated: !1,
					preventDefault: function () {
						var t = this.originalEvent;
						(this.isDefaultPrevented = j), t && !this.isSimulated && t.preventDefault();
					},
					stopPropagation: function () {
						var t = this.originalEvent;
						(this.isPropagationStopped = j), t && !this.isSimulated && t.stopPropagation();
					},
					stopImmediatePropagation: function () {
						var t = this.originalEvent;
						(this.isImmediatePropagationStopped = j), t && !this.isSimulated && t.stopImmediatePropagation(), this.stopPropagation();
					},
				}),
				jt.each(
					{
						altKey: !0,
						bubbles: !0,
						cancelable: !0,
						changedTouches: !0,
						ctrlKey: !0,
						detail: !0,
						eventPhase: !0,
						metaKey: !0,
						pageX: !0,
						pageY: !0,
						shiftKey: !0,
						view: !0,
						char: !0,
						code: !0,
						charCode: !0,
						key: !0,
						keyCode: !0,
						button: !0,
						buttons: !0,
						clientX: !0,
						clientY: !0,
						offsetX: !0,
						offsetY: !0,
						pointerId: !0,
						pointerType: !0,
						screenX: !0,
						screenY: !0,
						targetTouches: !0,
						toElement: !0,
						touches: !0,
						which: function (t) {
							var e = t.button;
							return null == t.which && he.test(t.type) ? (null != t.charCode ? t.charCode : t.keyCode) : !t.which && void 0 !== e && de.test(t.type) ? (1 & e ? 1 : 2 & e ? 3 : 4 & e ? 2 : 0) : t.which;
						},
					},
					jt.event.addProp
				),
				jt.each({ focus: "focusin", blur: "focusout" }, function (t, e) {
					jt.event.special[t] = {
						setup: function () {
							return A(this, t, $), !1;
						},
						trigger: function () {
							return A(this, t), !0;
						},
						delegateType: e,
					};
				}),
				jt.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, function (t, e) {
					jt.event.special[t] = {
						delegateType: e,
						bindType: e,
						handle: function (t) {
							var n,
								i = this,
								r = t.relatedTarget,
								s = t.handleObj;
							return (r && (r === i || jt.contains(i, r))) || ((t.type = s.origType), (n = s.handler.apply(this, arguments)), (t.type = e)), n;
						},
					};
				}),
				jt.fn.extend({
					on: function (t, e, n, i) {
						return I(this, t, e, n, i);
					},
					one: function (t, e, n, i) {
						return I(this, t, e, n, i, 1);
					},
					off: function (t, e, n) {
						var i, r;
						if (t && t.preventDefault && t.handleObj) return (i = t.handleObj), jt(t.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
						if ("object" == typeof t) {
							for (r in t) this.off(r, e, t[r]);
							return this;
						}
						return (
							(e !== !1 && "function" != typeof e) || ((n = e), (e = void 0)),
							n === !1 && (n = O),
							this.each(function () {
								jt.event.remove(this, t, n, e);
							})
						);
					},
				});
			var pe = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
				ve = /<script|<style|<link/i,
				ge = /checked\s*(?:[^=]|=\s*.checked.)/i,
				me = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
			jt.extend({
				htmlPrefilter: function (t) {
					return t.replace(pe, "<$1></$2>");
				},
				clone: function (t, e, n) {
					var i,
						r,
						s,
						o,
						a = t.cloneNode(!0),
						l = ee(t);
					if (!(Ct.noCloneChecked || (1 !== t.nodeType && 11 !== t.nodeType) || jt.isXMLDoc(t))) for (o = T(a), s = T(t), i = 0, r = s.length; i < r; i++) P(s[i], o[i]);
					if (e)
						if (n) for (s = s || T(t), o = o || T(a), i = 0, r = s.length; i < r; i++) R(s[i], o[i]);
						else R(t, a);
					return (o = T(a, "script")), o.length > 0 && S(o, !l && T(t, "script")), a;
				},
				cleanData: function (t) {
					for (var e, n, i, r = jt.event.special, s = 0; void 0 !== (n = t[s]); s++)
						if (Ut(n)) {
							if ((e = n[Yt.expando])) {
								if (e.events) for (i in e.events) r[i] ? jt.event.remove(n, i) : jt.removeEvent(n, i, e.handle);
								n[Yt.expando] = void 0;
							}
							n[Xt.expando] && (n[Xt.expando] = void 0);
						}
				},
			}),
				jt.fn.extend({
					detach: function (t) {
						return H(this, t, !0);
					},
					remove: function (t) {
						return H(this, t);
					},
					text: function (t) {
						return Bt(
							this,
							function (t) {
								return void 0 === t
									? jt.text(this)
									: this.empty().each(function () {
										(1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) || (this.textContent = t);
									});
							},
							null,
							t,
							arguments.length
						);
					},
					append: function () {
						return z(this, arguments, function (t) {
							if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
								var e = M(this, t);
								e.appendChild(t);
							}
						});
					},
					prepend: function () {
						return z(this, arguments, function (t) {
							if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
								var e = M(this, t);
								e.insertBefore(t, e.firstChild);
							}
						});
					},
					before: function () {
						return z(this, arguments, function (t) {
							this.parentNode && this.parentNode.insertBefore(t, this);
						});
					},
					after: function () {
						return z(this, arguments, function (t) {
							this.parentNode && this.parentNode.insertBefore(t, this.nextSibling);
						});
					},
					empty: function () {
						for (var t, e = 0; null != (t = this[e]); e++) 1 === t.nodeType && (jt.cleanData(T(t, !1)), (t.textContent = ""));
						return this;
					},
					clone: function (t, e) {
						return (
							(t = null != t && t),
							(e = null == e ? t : e),
							this.map(function () {
								return jt.clone(this, t, e);
							})
						);
					},
					html: function (t) {
						return Bt(
							this,
							function (t) {
								var e = this[0] || {},
									n = 0,
									i = this.length;
								if (void 0 === t && 1 === e.nodeType) return e.innerHTML;
								if ("string" == typeof t && !ve.test(t) && !ue[(ae.exec(t) || ["", ""])[1].toLowerCase()]) {
									t = jt.htmlPrefilter(t);
									try {
										for (; n < i; n++) (e = this[n] || {}), 1 === e.nodeType && (jt.cleanData(T(e, !1)), (e.innerHTML = t));
										e = 0;
									} catch (r) { }
								}
								e && this.empty().append(t);
							},
							null,
							t,
							arguments.length
						);
					},
					replaceWith: function () {
						var t = [];
						return z(
							this,
							arguments,
							function (e) {
								var n = this.parentNode;
								jt.inArray(this, t) < 0 && (jt.cleanData(T(this)), n && n.replaceChild(e, this));
							},
							t
						);
					},
				}),
				jt.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function (t, e) {
					jt.fn[t] = function (t) {
						for (var n, i = [], r = jt(t), s = r.length - 1, o = 0; o <= s; o++) (n = o === s ? this : this.clone(!0)), jt(r[o])[e](n), gt.apply(i, n.get());
						return this.pushStack(i);
					};
				});
			var ye = new RegExp("^(" + Zt + ")(?!px)[a-z%]+$", "i"),
				be = function (t) {
					var e = t.ownerDocument.defaultView;
					return (e && e.opener) || (e = n), e.getComputedStyle(t);
				},
				xe = new RegExp(Qt.join("|"), "i");
			!(function () {
				function t() {
					if (u) {
						(l.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0"),
							(u.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%"),
							te.appendChild(l).appendChild(u);
						var t = n.getComputedStyle(u);
						(i = "1%" !== t.top),
							(a = 12 === e(t.marginLeft)),
							(u.style.right = "60%"),
							(o = 36 === e(t.right)),
							(r = 36 === e(t.width)),
							(u.style.position = "absolute"),
							(s = 12 === e(u.offsetWidth / 3)),
							te.removeChild(l),
							(u = null);
					}
				}
				function e(t) {
					return Math.round(parseFloat(t));
				}
				var i,
					r,
					s,
					o,
					a,
					l = dt.createElement("div"),
					u = dt.createElement("div");
				u.style &&
					((u.style.backgroundClip = "content-box"),
						(u.cloneNode(!0).style.backgroundClip = ""),
						(Ct.clearCloneStyle = "content-box" === u.style.backgroundClip),
						jt.extend(Ct, {
							boxSizingReliable: function () {
								return t(), r;
							},
							pixelBoxStyles: function () {
								return t(), o;
							},
							pixelPosition: function () {
								return t(), i;
							},
							reliableMarginLeft: function () {
								return t(), a;
							},
							scrollboxSize: function () {
								return t(), s;
							},
						}));
			})();
			var we = ["Webkit", "Moz", "ms"],
				_e = dt.createElement("div").style,
				Ce = {},
				ke = /^(none|table(?!-c[ea]).+)/,
				Te = /^--/,
				Se = { position: "absolute", visibility: "hidden", display: "block" },
				Ee = { letterSpacing: "0", fontWeight: "400" };
			jt.extend({
				cssHooks: {
					opacity: {
						get: function (t, e) {
							if (e) {
								var n = q(t, "opacity");
								return "" === n ? "1" : n;
							}
						},
					},
				},
				cssNumber: {
					animationIterationCount: !0,
					columnCount: !0,
					fillOpacity: !0,
					flexGrow: !0,
					flexShrink: !0,
					fontWeight: !0,
					gridArea: !0,
					gridColumn: !0,
					gridColumnEnd: !0,
					gridColumnStart: !0,
					gridRow: !0,
					gridRowEnd: !0,
					gridRowStart: !0,
					lineHeight: !0,
					opacity: !0,
					order: !0,
					orphans: !0,
					widows: !0,
					zIndex: !0,
					zoom: !0,
				},
				cssProps: {},
				style: function (t, e, n, i) {
					if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
						var r,
							s,
							o,
							a = y(e),
							l = Te.test(e),
							u = t.style;
						return (
							l || (e = W(a)),
							(o = jt.cssHooks[e] || jt.cssHooks[a]),
							void 0 === n
								? o && "get" in o && void 0 !== (r = o.get(t, !1, i))
									? r
									: u[e]
								: ((s = typeof n),
									"string" === s && (r = Kt.exec(n)) && r[1] && ((n = _(t, e, r)), (s = "number")),
									null != n &&
									n === n &&
									("number" !== s || l || (n += (r && r[3]) || (jt.cssNumber[a] ? "" : "px")),
										Ct.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (u[e] = "inherit"),
										(o && "set" in o && void 0 === (n = o.set(t, n, i))) || (l ? u.setProperty(e, n) : (u[e] = n))),
									void 0)
						);
					}
				},
				css: function (t, e, n, i) {
					var r,
						s,
						o,
						a = y(e),
						l = Te.test(e);
					return (
						l || (e = W(a)),
						(o = jt.cssHooks[e] || jt.cssHooks[a]),
						o && "get" in o && (r = o.get(t, !0, n)),
						void 0 === r && (r = q(t, e, i)),
						"normal" === r && e in Ee && (r = Ee[e]),
						"" === n || n ? ((s = parseFloat(r)), n === !0 || isFinite(s) ? s || 0 : r) : r
					);
				},
			}),
				jt.each(["height", "width"], function (t, e) {
					jt.cssHooks[e] = {
						get: function (t, n, i) {
							if (n)
								return !ke.test(jt.css(t, "display")) || (t.getClientRects().length && t.getBoundingClientRect().width)
									? Y(t, e, i)
									: re(t, Se, function () {
										return Y(t, e, i);
									});
						},
						set: function (t, n, i) {
							var r,
								s = be(t),
								o = !Ct.scrollboxSize() && "absolute" === s.position,
								a = o || i,
								l = a && "border-box" === jt.css(t, "boxSizing", !1, s),
								u = i ? U(t, e, i, l, s) : 0;
							return (
								l && o && (u -= Math.ceil(t["offset" + e[0].toUpperCase() + e.slice(1)] - parseFloat(s[e]) - U(t, e, "border", !1, s) - 0.5)),
								u && (r = Kt.exec(n)) && "px" !== (r[3] || "px") && ((t.style[e] = n), (n = jt.css(t, e))),
								V(t, n, u)
							);
						},
					};
				}),
				(jt.cssHooks.marginLeft = F(Ct.reliableMarginLeft, function (t, e) {
					if (e)
						return (
							(parseFloat(q(t, "marginLeft")) ||
								t.getBoundingClientRect().left -
								re(t, { marginLeft: 0 }, function () {
									return t.getBoundingClientRect().left;
								})) + "px"
						);
				})),
				jt.each({ margin: "", padding: "", border: "Width" }, function (t, e) {
					(jt.cssHooks[t + e] = {
						expand: function (n) {
							for (var i = 0, r = {}, s = "string" == typeof n ? n.split(" ") : [n]; i < 4; i++) r[t + Qt[i] + e] = s[i] || s[i - 2] || s[0];
							return r;
						},
					}),
						"margin" !== t && (jt.cssHooks[t + e].set = V);
				}),
				jt.fn.extend({
					css: function (t, e) {
						return Bt(
							this,
							function (t, e, n) {
								var i,
									r,
									s = {},
									o = 0;
								if (Array.isArray(e)) {
									for (i = be(t), r = e.length; o < r; o++) s[e[o]] = jt.css(t, e[o], !1, i);
									return s;
								}
								return void 0 !== n ? jt.style(t, e, n) : jt.css(t, e);
							},
							t,
							e,
							arguments.length > 1
						);
					},
				}),
				(jt.Tween = X),
				(X.prototype = {
					constructor: X,
					init: function (t, e, n, i, r, s) {
						(this.elem = t), (this.prop = n), (this.easing = r || jt.easing._default), (this.options = e), (this.start = this.now = this.cur()), (this.end = i), (this.unit = s || (jt.cssNumber[n] ? "" : "px"));
					},
					cur: function () {
						var t = X.propHooks[this.prop];
						return t && t.get ? t.get(this) : X.propHooks._default.get(this);
					},
					run: function (t) {
						var e,
							n = X.propHooks[this.prop];
						return (
							this.options.duration ? (this.pos = e = jt.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration)) : (this.pos = e = t),
							(this.now = (this.end - this.start) * e + this.start),
							this.options.step && this.options.step.call(this.elem, this.now, this),
							n && n.set ? n.set(this) : X.propHooks._default.set(this),
							this
						);
					},
				}),
				(X.prototype.init.prototype = X.prototype),
				(X.propHooks = {
					_default: {
						get: function (t) {
							var e;
							return 1 !== t.elem.nodeType || (null != t.elem[t.prop] && null == t.elem.style[t.prop]) ? t.elem[t.prop] : ((e = jt.css(t.elem, t.prop, "")), e && "auto" !== e ? e : 0);
						},
						set: function (t) {
							jt.fx.step[t.prop] ? jt.fx.step[t.prop](t) : 1 !== t.elem.nodeType || (!jt.cssHooks[t.prop] && null == t.elem.style[W(t.prop)]) ? (t.elem[t.prop] = t.now) : jt.style(t.elem, t.prop, t.now + t.unit);
						},
					},
				}),
				(X.propHooks.scrollTop = X.propHooks.scrollLeft = {
					set: function (t) {
						t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now);
					},
				}),
				(jt.easing = {
					linear: function (t) {
						return t;
					},
					swing: function (t) {
						return 0.5 - Math.cos(t * Math.PI) / 2;
					},
					_default: "swing",
				}),
				(jt.fx = X.prototype.init),
				(jt.fx.step = {});
			var je,
				Oe,
				$e = /^(?:toggle|show|hide)$/,
				De = /queueHooks$/;
			(jt.Animation = jt.extend(et, {
				tweeners: {
					"*": [
						function (t, e) {
							var n = this.createTween(t, e);
							return _(n.elem, t, Kt.exec(e), n), n;
						},
					],
				},
				tweener: function (t, e) {
					kt(t) ? ((e = t), (t = ["*"])) : (t = t.match(Ht));
					for (var n, i = 0, r = t.length; i < r; i++) (n = t[i]), (et.tweeners[n] = et.tweeners[n] || []), et.tweeners[n].unshift(e);
				},
				prefilters: [Q],
				prefilter: function (t, e) {
					e ? et.prefilters.unshift(t) : et.prefilters.push(t);
				},
			})),
				(jt.speed = function (t, e, n) {
					var i = t && "object" == typeof t ? jt.extend({}, t) : { complete: n || (!n && e) || (kt(t) && t), duration: t, easing: (n && e) || (e && !kt(e) && e) };
					return (
						jt.fx.off ? (i.duration = 0) : "number" != typeof i.duration && (i.duration in jt.fx.speeds ? (i.duration = jt.fx.speeds[i.duration]) : (i.duration = jt.fx.speeds._default)),
						(null != i.queue && i.queue !== !0) || (i.queue = "fx"),
						(i.old = i.complete),
						(i.complete = function () {
							kt(i.old) && i.old.call(this), i.queue && jt.dequeue(this, i.queue);
						}),
						i
					);
				}),
				jt.fn.extend({
					fadeTo: function (t, e, n, i) {
						return this.filter(ie).css("opacity", 0).show().end().animate({ opacity: e }, t, n, i);
					},
					animate: function (t, e, n, i) {
						var r = jt.isEmptyObject(t),
							s = jt.speed(e, n, i),
							o = function () {
								var e = et(this, jt.extend({}, t), s);
								(r || Yt.get(this, "finish")) && e.stop(!0);
							};
						return (o.finish = o), r || s.queue === !1 ? this.each(o) : this.queue(s.queue, o);
					},
					stop: function (t, e, n) {
						var i = function (t) {
							var e = t.stop;
							delete t.stop, e(n);
						};
						return (
							"string" != typeof t && ((n = e), (e = t), (t = void 0)),
							e && t !== !1 && this.queue(t || "fx", []),
							this.each(function () {
								var e = !0,
									r = null != t && t + "queueHooks",
									s = jt.timers,
									o = Yt.get(this);
								if (r) o[r] && o[r].stop && i(o[r]);
								else for (r in o) o[r] && o[r].stop && De.test(r) && i(o[r]);
								for (r = s.length; r--;) s[r].elem !== this || (null != t && s[r].queue !== t) || (s[r].anim.stop(n), (e = !1), s.splice(r, 1));
								(!e && n) || jt.dequeue(this, t);
							})
						);
					},
					finish: function (t) {
						return (
							t !== !1 && (t = t || "fx"),
							this.each(function () {
								var e,
									n = Yt.get(this),
									i = n[t + "queue"],
									r = n[t + "queueHooks"],
									s = jt.timers,
									o = i ? i.length : 0;
								for (n.finish = !0, jt.queue(this, t, []), r && r.stop && r.stop.call(this, !0), e = s.length; e--;) s[e].elem === this && s[e].queue === t && (s[e].anim.stop(!0), s.splice(e, 1));
								for (e = 0; e < o; e++) i[e] && i[e].finish && i[e].finish.call(this);
								delete n.finish;
							})
						);
					},
				}),
				jt.each(["toggle", "show", "hide"], function (t, e) {
					var n = jt.fn[e];
					jt.fn[e] = function (t, i, r) {
						return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(Z(e, !0), t, i, r);
					};
				}),
				jt.each({ slideDown: Z("show"), slideUp: Z("hide"), slideToggle: Z("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, function (t, e) {
					jt.fn[t] = function (t, n, i) {
						return this.animate(e, t, n, i);
					};
				}),
				(jt.timers = []),
				(jt.fx.tick = function () {
					var t,
						e = 0,
						n = jt.timers;
					for (je = Date.now(); e < n.length; e++) (t = n[e]), t() || n[e] !== t || n.splice(e--, 1);
					n.length || jt.fx.stop(), (je = void 0);
				}),
				(jt.fx.timer = function (t) {
					jt.timers.push(t), jt.fx.start();
				}),
				(jt.fx.interval = 13),
				(jt.fx.start = function () {
					Oe || ((Oe = !0), G());
				}),
				(jt.fx.stop = function () {
					Oe = null;
				}),
				(jt.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
				(jt.fn.delay = function (t, e) {
					return (
						(t = jt.fx ? jt.fx.speeds[t] || t : t),
						(e = e || "fx"),
						this.queue(e, function (e, i) {
							var r = n.setTimeout(e, t);
							i.stop = function () {
								n.clearTimeout(r);
							};
						})
					);
				}),
				(function () {
					var t = dt.createElement("input"),
						e = dt.createElement("select"),
						n = e.appendChild(dt.createElement("option"));
					(t.type = "checkbox"), (Ct.checkOn = "" !== t.value), (Ct.optSelected = n.selected), (t = dt.createElement("input")), (t.value = "t"), (t.type = "radio"), (Ct.radioValue = "t" === t.value);
				})();
			var Ie,
				Ae = jt.expr.attrHandle;
			jt.fn.extend({
				attr: function (t, e) {
					return Bt(this, jt.attr, t, e, arguments.length > 1);
				},
				removeAttr: function (t) {
					return this.each(function () {
						jt.removeAttr(this, t);
					});
				},
			}),
				jt.extend({
					attr: function (t, e, n) {
						var i,
							r,
							s = t.nodeType;
						if (3 !== s && 8 !== s && 2 !== s)
							return "undefined" == typeof t.getAttribute
								? jt.prop(t, e, n)
								: ((1 === s && jt.isXMLDoc(t)) || (r = jt.attrHooks[e.toLowerCase()] || (jt.expr.match.bool.test(e) ? Ie : void 0)),
									void 0 !== n
										? null === n
											? void jt.removeAttr(t, e)
											: r && "set" in r && void 0 !== (i = r.set(t, n, e))
												? i
												: (t.setAttribute(e, n + ""), n)
										: r && "get" in r && null !== (i = r.get(t, e))
											? i
											: ((i = jt.find.attr(t, e)), null == i ? void 0 : i));
					},
					attrHooks: {
						type: {
							set: function (t, e) {
								if (!Ct.radioValue && "radio" === e && u(t, "input")) {
									var n = t.value;
									return t.setAttribute("type", e), n && (t.value = n), e;
								}
							},
						},
					},
					removeAttr: function (t, e) {
						var n,
							i = 0,
							r = e && e.match(Ht);
						if (r && 1 === t.nodeType) for (; (n = r[i++]);) t.removeAttribute(n);
					},
				}),
				(Ie = {
					set: function (t, e, n) {
						return e === !1 ? jt.removeAttr(t, n) : t.setAttribute(n, n), n;
					},
				}),
				jt.each(jt.expr.match.bool.source.match(/\w+/g), function (t, e) {
					var n = Ae[e] || jt.find.attr;
					Ae[e] = function (t, e, i) {
						var r,
							s,
							o = e.toLowerCase();
						return i || ((s = Ae[o]), (Ae[o] = r), (r = null != n(t, e, i) ? o : null), (Ae[o] = s)), r;
					};
				});
			var Me = /^(?:input|select|textarea|button)$/i,
				Ne = /^(?:a|area)$/i;
			jt.fn.extend({
				prop: function (t, e) {
					return Bt(this, jt.prop, t, e, arguments.length > 1);
				},
				removeProp: function (t) {
					return this.each(function () {
						delete this[jt.propFix[t] || t];
					});
				},
			}),
				jt.extend({
					prop: function (t, e, n) {
						var i,
							r,
							s = t.nodeType;
						if (3 !== s && 8 !== s && 2 !== s)
							return (
								(1 === s && jt.isXMLDoc(t)) || ((e = jt.propFix[e] || e), (r = jt.propHooks[e])),
								void 0 !== n ? (r && "set" in r && void 0 !== (i = r.set(t, n, e)) ? i : (t[e] = n)) : r && "get" in r && null !== (i = r.get(t, e)) ? i : t[e]
							);
					},
					propHooks: {
						tabIndex: {
							get: function (t) {
								var e = jt.find.attr(t, "tabindex");
								return e ? parseInt(e, 10) : Me.test(t.nodeName) || (Ne.test(t.nodeName) && t.href) ? 0 : -1;
							},
						},
					},
					propFix: { for: "htmlFor", class: "className" },
				}),
				Ct.optSelected ||
				(jt.propHooks.selected = {
					get: function (t) {
						var e = t.parentNode;
						return e && e.parentNode && e.parentNode.selectedIndex, null;
					},
					set: function (t) {
						var e = t.parentNode;
						e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex);
					},
				}),
				jt.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
					jt.propFix[this.toLowerCase()] = this;
				}),
				jt.fn.extend({
					addClass: function (t) {
						var e,
							n,
							i,
							r,
							s,
							o,
							a,
							l = 0;
						if (kt(t))
							return this.each(function (e) {
								jt(this).addClass(t.call(this, e, it(this)));
							});
						if (((e = rt(t)), e.length))
							for (; (n = this[l++]);)
								if (((r = it(n)), (i = 1 === n.nodeType && " " + nt(r) + " "))) {
									for (o = 0; (s = e[o++]);) i.indexOf(" " + s + " ") < 0 && (i += s + " ");
									(a = nt(i)), r !== a && n.setAttribute("class", a);
								}
						return this;
					},
					removeClass: function (t) {
						var e,
							n,
							i,
							r,
							s,
							o,
							a,
							l = 0;
						if (kt(t))
							return this.each(function (e) {
								jt(this).removeClass(t.call(this, e, it(this)));
							});
						if (!arguments.length) return this.attr("class", "");
						if (((e = rt(t)), e.length))
							for (; (n = this[l++]);)
								if (((r = it(n)), (i = 1 === n.nodeType && " " + nt(r) + " "))) {
									for (o = 0; (s = e[o++]);) for (; i.indexOf(" " + s + " ") > -1;) i = i.replace(" " + s + " ", " ");
									(a = nt(i)), r !== a && n.setAttribute("class", a);
								}
						return this;
					},
					toggleClass: function (t, e) {
						var n = typeof t,
							i = "string" === n || Array.isArray(t);
						return "boolean" == typeof e && i
							? e
								? this.addClass(t)
								: this.removeClass(t)
							: kt(t)
								? this.each(function (n) {
									jt(this).toggleClass(t.call(this, n, it(this), e), e);
								})
								: this.each(function () {
									var e, r, s, o;
									if (i) for (r = 0, s = jt(this), o = rt(t); (e = o[r++]);) s.hasClass(e) ? s.removeClass(e) : s.addClass(e);
									else (void 0 !== t && "boolean" !== n) || ((e = it(this)), e && Yt.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || t === !1 ? "" : Yt.get(this, "__className__") || ""));
								});
					},
					hasClass: function (t) {
						var e,
							n,
							i = 0;
						for (e = " " + t + " "; (n = this[i++]);) if (1 === n.nodeType && (" " + nt(it(n)) + " ").indexOf(e) > -1) return !0;
						return !1;
					},
				});
			var Le = /\r/g;
			jt.fn.extend({
				val: function (t) {
					var e,
						n,
						i,
						r = this[0];
					{
						if (arguments.length)
							return (
								(i = kt(t)),
								this.each(function (n) {
									var r;
									1 === this.nodeType &&
										((r = i ? t.call(this, n, jt(this).val()) : t),
											null == r
												? (r = "")
												: "number" == typeof r
													? (r += "")
													: Array.isArray(r) &&
													(r = jt.map(r, function (t) {
														return null == t ? "" : t + "";
													})),
											(e = jt.valHooks[this.type] || jt.valHooks[this.nodeName.toLowerCase()]),
											(e && "set" in e && void 0 !== e.set(this, r, "value")) || (this.value = r));
								})
							);
						if (r)
							return (
								(e = jt.valHooks[r.type] || jt.valHooks[r.nodeName.toLowerCase()]), e && "get" in e && void 0 !== (n = e.get(r, "value")) ? n : ((n = r.value), "string" == typeof n ? n.replace(Le, "") : null == n ? "" : n)
							);
					}
				},
			}),
				jt.extend({
					valHooks: {
						option: {
							get: function (t) {
								var e = jt.find.attr(t, "value");
								return null != e ? e : nt(jt.text(t));
							},
						},
						select: {
							get: function (t) {
								var e,
									n,
									i,
									r = t.options,
									s = t.selectedIndex,
									o = "select-one" === t.type,
									a = o ? null : [],
									l = o ? s + 1 : r.length;
								for (i = s < 0 ? l : o ? s : 0; i < l; i++)
									if (((n = r[i]), (n.selected || i === s) && !n.disabled && (!n.parentNode.disabled || !u(n.parentNode, "optgroup")))) {
										if (((e = jt(n).val()), o)) return e;
										a.push(e);
									}
								return a;
							},
							set: function (t, e) {
								for (var n, i, r = t.options, s = jt.makeArray(e), o = r.length; o--;) (i = r[o]), (i.selected = jt.inArray(jt.valHooks.option.get(i), s) > -1) && (n = !0);
								return n || (t.selectedIndex = -1), s;
							},
						},
					},
				}),
				jt.each(["radio", "checkbox"], function () {
					(jt.valHooks[this] = {
						set: function (t, e) {
							if (Array.isArray(e)) return (t.checked = jt.inArray(jt(t).val(), e) > -1);
						},
					}),
						Ct.checkOn ||
						(jt.valHooks[this].get = function (t) {
							return null === t.getAttribute("value") ? "on" : t.value;
						});
				}),
				(Ct.focusin = "onfocusin" in n);
			var Re = /^(?:focusinfocus|focusoutblur)$/,
				Pe = function (t) {
					t.stopPropagation();
				};
			jt.extend(jt.event, {
				trigger: function (t, e, i, r) {
					var s,
						o,
						a,
						l,
						u,
						c,
						h,
						d,
						f = [i || dt],
						p = xt.call(t, "type") ? t.type : t,
						v = xt.call(t, "namespace") ? t.namespace.split(".") : [];
					if (
						((o = d = a = i = i || dt),
							3 !== i.nodeType &&
							8 !== i.nodeType &&
							!Re.test(p + jt.event.triggered) &&
							(p.indexOf(".") > -1 && ((v = p.split(".")), (p = v.shift()), v.sort()),
								(u = p.indexOf(":") < 0 && "on" + p),
								(t = t[jt.expando] ? t : new jt.Event(p, "object" == typeof t && t)),
								(t.isTrigger = r ? 2 : 3),
								(t.namespace = v.join(".")),
								(t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + v.join("\\.(?:.*\\.|)") + "(\\.|$)") : null),
								(t.result = void 0),
								t.target || (t.target = i),
								(e = null == e ? [t] : jt.makeArray(e, [t])),
								(h = jt.event.special[p] || {}),
								r || !h.trigger || h.trigger.apply(i, e) !== !1))
					) {
						if (!r && !h.noBubble && !Tt(i)) {
							for (l = h.delegateType || p, Re.test(l + p) || (o = o.parentNode); o; o = o.parentNode) f.push(o), (a = o);
							a === (i.ownerDocument || dt) && f.push(a.defaultView || a.parentWindow || n);
						}
						for (s = 0; (o = f[s++]) && !t.isPropagationStopped();)
							(d = o),
								(t.type = s > 1 ? l : h.bindType || p),
								(c = (Yt.get(o, "events") || {})[t.type] && Yt.get(o, "handle")),
								c && c.apply(o, e),
								(c = u && o[u]),
								c && c.apply && Ut(o) && ((t.result = c.apply(o, e)), t.result === !1 && t.preventDefault());
						return (
							(t.type = p),
							r ||
							t.isDefaultPrevented() ||
							(h._default && h._default.apply(f.pop(), e) !== !1) ||
							!Ut(i) ||
							(u &&
								kt(i[p]) &&
								!Tt(i) &&
								((a = i[u]),
									a && (i[u] = null),
									(jt.event.triggered = p),
									t.isPropagationStopped() && d.addEventListener(p, Pe),
									i[p](),
									t.isPropagationStopped() && d.removeEventListener(p, Pe),
									(jt.event.triggered = void 0),
									a && (i[u] = a))),
							t.result
						);
					}
				},
				simulate: function (t, e, n) {
					var i = jt.extend(new jt.Event(), n, { type: t, isSimulated: !0 });
					jt.event.trigger(i, null, e);
				},
			}),
				jt.fn.extend({
					trigger: function (t, e) {
						return this.each(function () {
							jt.event.trigger(t, e, this);
						});
					},
					triggerHandler: function (t, e) {
						var n = this[0];
						if (n) return jt.event.trigger(t, e, n, !0);
					},
				}),
				Ct.focusin ||
				jt.each({ focus: "focusin", blur: "focusout" }, function (t, e) {
					var n = function (t) {
						jt.event.simulate(e, t.target, jt.event.fix(t));
					};
					jt.event.special[e] = {
						setup: function () {
							var i = this.ownerDocument || this,
								r = Yt.access(i, e);
							r || i.addEventListener(t, n, !0), Yt.access(i, e, (r || 0) + 1);
						},
						teardown: function () {
							var i = this.ownerDocument || this,
								r = Yt.access(i, e) - 1;
							r ? Yt.access(i, e, r) : (i.removeEventListener(t, n, !0), Yt.remove(i, e));
						},
					};
				});
			var ze = n.location,
				He = Date.now(),
				qe = /\?/;
			jt.parseXML = function (t) {
				var e;
				if (!t || "string" != typeof t) return null;
				try {
					e = new n.DOMParser().parseFromString(t, "text/xml");
				} catch (i) {
					e = void 0;
				}
				return (e && !e.getElementsByTagName("parsererror").length) || jt.error("Invalid XML: " + t), e;
			};
			var Fe = /\[\]$/,
				Be = /\r?\n/g,
				We = /^(?:submit|button|image|reset|file)$/i,
				Ve = /^(?:input|select|textarea|keygen)/i;
			(jt.param = function (t, e) {
				var n,
					i = [],
					r = function (t, e) {
						var n = kt(e) ? e() : e;
						i[i.length] = encodeURIComponent(t) + "=" + encodeURIComponent(null == n ? "" : n);
					};
				if (null == t) return "";
				if (Array.isArray(t) || (t.jquery && !jt.isPlainObject(t)))
					jt.each(t, function () {
						r(this.name, this.value);
					});
				else for (n in t) st(n, t[n], e, r);
				return i.join("&");
			}),
				jt.fn.extend({
					serialize: function () {
						return jt.param(this.serializeArray());
					},
					serializeArray: function () {
						return this.map(function () {
							var t = jt.prop(this, "elements");
							return t ? jt.makeArray(t) : this;
						})
							.filter(function () {
								var t = this.type;
								return this.name && !jt(this).is(":disabled") && Ve.test(this.nodeName) && !We.test(t) && (this.checked || !oe.test(t));
							})
							.map(function (t, e) {
								var n = jt(this).val();
								return null == n
									? null
									: Array.isArray(n)
										? jt.map(n, function (t) {
											return { name: e.name, value: t.replace(Be, "\r\n") };
										})
										: { name: e.name, value: n.replace(Be, "\r\n") };
							})
							.get();
					},
				});
			var Ue = /%20/g,
				Ye = /#.*$/,
				Xe = /([?&])_=[^&]*/,
				Ge = /^(.*?):[ \t]*([^\r\n]*)$/gm,
				Je = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
				Ze = /^(?:GET|HEAD)$/,
				Ke = /^\/\//,
				Qe = {},
				tn = {},
				en = "*/".concat("*"),
				nn = dt.createElement("a");
			(nn.href = ze.href),
				jt.extend({
					active: 0,
					lastModified: {},
					etag: {},
					ajaxSettings: {
						url: ze.href,
						type: "GET",
						isLocal: Je.test(ze.protocol),
						global: !0,
						processData: !0,
						async: !0,
						contentType: "application/x-www-form-urlencoded; charset=UTF-8",
						accepts: { "*": en, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript" },
						contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
						responseFields: { xml: "responseXML", text: "responseText", json: "responseJSON" },
						converters: { "* text": String, "text html": !0, "text json": JSON.parse, "text xml": jt.parseXML },
						flatOptions: { url: !0, context: !0 },
					},
					ajaxSetup: function (t, e) {
						return e ? lt(lt(t, jt.ajaxSettings), e) : lt(jt.ajaxSettings, t);
					},
					ajaxPrefilter: ot(Qe),
					ajaxTransport: ot(tn),
					ajax: function (t, e) {
						function i(t, e, i, a) {
							var u,
								d,
								f,
								x,
								w,
								_ = e;
							c ||
								((c = !0),
									l && n.clearTimeout(l),
									(r = void 0),
									(o = a || ""),
									(C.readyState = t > 0 ? 4 : 0),
									(u = (t >= 200 && t < 300) || 304 === t),
									i && (x = ut(p, C, i)),
									(x = ct(p, x, C, u)),
									u
										? (p.ifModified && ((w = C.getResponseHeader("Last-Modified")), w && (jt.lastModified[s] = w), (w = C.getResponseHeader("etag")), w && (jt.etag[s] = w)),
											204 === t || "HEAD" === p.type ? (_ = "nocontent") : 304 === t ? (_ = "notmodified") : ((_ = x.state), (d = x.data), (f = x.error), (u = !f)))
										: ((f = _), (!t && _) || ((_ = "error"), t < 0 && (t = 0))),
									(C.status = t),
									(C.statusText = (e || _) + ""),
									u ? m.resolveWith(v, [d, _, C]) : m.rejectWith(v, [C, _, f]),
									C.statusCode(b),
									(b = void 0),
									h && g.trigger(u ? "ajaxSuccess" : "ajaxError", [C, p, u ? d : f]),
									y.fireWith(v, [C, _]),
									h && (g.trigger("ajaxComplete", [C, p]), --jt.active || jt.event.trigger("ajaxStop")));
						}
						"object" == typeof t && ((e = t), (t = void 0)), (e = e || {});
						var r,
							s,
							o,
							a,
							l,
							u,
							c,
							h,
							d,
							f,
							p = jt.ajaxSetup({}, e),
							v = p.context || p,
							g = p.context && (v.nodeType || v.jquery) ? jt(v) : jt.event,
							m = jt.Deferred(),
							y = jt.Callbacks("once memory"),
							b = p.statusCode || {},
							x = {},
							w = {},
							_ = "canceled",
							C = {
								readyState: 0,
								getResponseHeader: function (t) {
									var e;
									if (c) {
										if (!a) for (a = {}; (e = Ge.exec(o));) a[e[1].toLowerCase() + " "] = (a[e[1].toLowerCase() + " "] || []).concat(e[2]);
										e = a[t.toLowerCase() + " "];
									}
									return null == e ? null : e.join(", ");
								},
								getAllResponseHeaders: function () {
									return c ? o : null;
								},
								setRequestHeader: function (t, e) {
									return null == c && ((t = w[t.toLowerCase()] = w[t.toLowerCase()] || t), (x[t] = e)), this;
								},
								overrideMimeType: function (t) {
									return null == c && (p.mimeType = t), this;
								},
								statusCode: function (t) {
									var e;
									if (t)
										if (c) C.always(t[C.status]);
										else for (e in t) b[e] = [b[e], t[e]];
									return this;
								},
								abort: function (t) {
									var e = t || _;
									return r && r.abort(e), i(0, e), this;
								},
							};
						if (
							(m.promise(C),
								(p.url = ((t || p.url || ze.href) + "").replace(Ke, ze.protocol + "//")),
								(p.type = e.method || e.type || p.method || p.type),
								(p.dataTypes = (p.dataType || "*").toLowerCase().match(Ht) || [""]),
								null == p.crossDomain)
						) {
							u = dt.createElement("a");
							try {
								(u.href = p.url), (u.href = u.href), (p.crossDomain = nn.protocol + "//" + nn.host != u.protocol + "//" + u.host);
							} catch (k) {
								p.crossDomain = !0;
							}
						}
						if ((p.data && p.processData && "string" != typeof p.data && (p.data = jt.param(p.data, p.traditional)), at(Qe, p, e, C), c)) return C;
						(h = jt.event && p.global),
							h && 0 === jt.active++ && jt.event.trigger("ajaxStart"),
							(p.type = p.type.toUpperCase()),
							(p.hasContent = !Ze.test(p.type)),
							(s = p.url.replace(Ye, "")),
							p.hasContent
								? p.data && p.processData && 0 === (p.contentType || "").indexOf("application/x-www-form-urlencoded") && (p.data = p.data.replace(Ue, "+"))
								: ((f = p.url.slice(s.length)),
									p.data && (p.processData || "string" == typeof p.data) && ((s += (qe.test(s) ? "&" : "?") + p.data), delete p.data),
									p.cache === !1 && ((s = s.replace(Xe, "$1")), (f = (qe.test(s) ? "&" : "?") + "_=" + He++ + f)),
									(p.url = s + f)),
							p.ifModified && (jt.lastModified[s] && C.setRequestHeader("If-Modified-Since", jt.lastModified[s]), jt.etag[s] && C.setRequestHeader("If-None-Match", jt.etag[s])),
							((p.data && p.hasContent && p.contentType !== !1) || e.contentType) && C.setRequestHeader("Content-Type", p.contentType),
							C.setRequestHeader("Accept", p.dataTypes[0] && p.accepts[p.dataTypes[0]] ? p.accepts[p.dataTypes[0]] + ("*" !== p.dataTypes[0] ? ", " + en + "; q=0.01" : "") : p.accepts["*"]);
						for (d in p.headers) C.setRequestHeader(d, p.headers[d]);
						if (p.beforeSend && (p.beforeSend.call(v, C, p) === !1 || c)) return C.abort();
						if (((_ = "abort"), y.add(p.complete), C.done(p.success), C.fail(p.error), (r = at(tn, p, e, C)))) {
							if (((C.readyState = 1), h && g.trigger("ajaxSend", [C, p]), c)) return C;
							p.async &&
								p.timeout > 0 &&
								(l = n.setTimeout(function () {
									C.abort("timeout");
								}, p.timeout));
							try {
								(c = !1), r.send(x, i);
							} catch (k) {
								if (c) throw k;
								i(-1, k);
							}
						} else i(-1, "No Transport");
						return C;
					},
					getJSON: function (t, e, n) {
						return jt.get(t, e, n, "json");
					},
					getScript: function (t, e) {
						return jt.get(t, void 0, e, "script");
					},
				}),
				jt.each(["get", "post"], function (t, e) {
					jt[e] = function (t, n, i, r) {
						return kt(n) && ((r = r || i), (i = n), (n = void 0)), jt.ajax(jt.extend({ url: t, type: e, dataType: r, data: n, success: i }, jt.isPlainObject(t) && t));
					};
				}),
				(jt._evalUrl = function (t, e) {
					return jt.ajax({
						url: t,
						type: "GET",
						dataType: "script",
						cache: !0,
						async: !1,
						global: !1,
						converters: { "text script": function () { } },
						dataFilter: function (t) {
							jt.globalEval(t, e);
						},
					});
				}),
				jt.fn.extend({
					wrapAll: function (t) {
						var e;
						return (
							this[0] &&
							(kt(t) && (t = t.call(this[0])),
								(e = jt(t, this[0].ownerDocument).eq(0).clone(!0)),
								this[0].parentNode && e.insertBefore(this[0]),
								e
									.map(function () {
										for (var t = this; t.firstElementChild;) t = t.firstElementChild;
										return t;
									})
									.append(this)),
							this
						);
					},
					wrapInner: function (t) {
						return kt(t)
							? this.each(function (e) {
								jt(this).wrapInner(t.call(this, e));
							})
							: this.each(function () {
								var e = jt(this),
									n = e.contents();
								n.length ? n.wrapAll(t) : e.append(t);
							});
					},
					wrap: function (t) {
						var e = kt(t);
						return this.each(function (n) {
							jt(this).wrapAll(e ? t.call(this, n) : t);
						});
					},
					unwrap: function (t) {
						return (
							this.parent(t)
								.not("body")
								.each(function () {
									jt(this).replaceWith(this.childNodes);
								}),
							this
						);
					},
				}),
				(jt.expr.pseudos.hidden = function (t) {
					return !jt.expr.pseudos.visible(t);
				}),
				(jt.expr.pseudos.visible = function (t) {
					return !!(t.offsetWidth || t.offsetHeight || t.getClientRects().length);
				}),
				(jt.ajaxSettings.xhr = function () {
					try {
						return new n.XMLHttpRequest();
					} catch (t) { }
				});
			var rn = { 0: 200, 1223: 204 },
				sn = jt.ajaxSettings.xhr();
			(Ct.cors = !!sn && "withCredentials" in sn),
				(Ct.ajax = sn = !!sn),
				jt.ajaxTransport(function (t) {
					var e, i;
					if (Ct.cors || (sn && !t.crossDomain))
						return {
							send: function (r, s) {
								var o,
									a = t.xhr();
								if ((a.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)) for (o in t.xhrFields) a[o] = t.xhrFields[o];
								t.mimeType && a.overrideMimeType && a.overrideMimeType(t.mimeType), t.crossDomain || r["X-Requested-With"] || (r["X-Requested-With"] = "XMLHttpRequest");
								for (o in r) a.setRequestHeader(o, r[o]);
								(e = function (t) {
									return function () {
										e &&
											((e = i = a.onload = a.onerror = a.onabort = a.ontimeout = a.onreadystatechange = null),
												"abort" === t
													? a.abort()
													: "error" === t
														? "number" != typeof a.status
															? s(0, "error")
															: s(a.status, a.statusText)
														: s(
															rn[a.status] || a.status,
															a.statusText,
															"text" !== (a.responseType || "text") || "string" != typeof a.responseText ? { binary: a.response } : { text: a.responseText },
															a.getAllResponseHeaders()
														));
									};
								}),
									(a.onload = e()),
									(i = a.onerror = a.ontimeout = e("error")),
									void 0 !== a.onabort
										? (a.onabort = i)
										: (a.onreadystatechange = function () {
											4 === a.readyState &&
												n.setTimeout(function () {
													e && i();
												});
										}),
									(e = e("abort"));
								try {
									a.send((t.hasContent && t.data) || null);
									window.location = "/thanks/";
								} catch (l) {
									if (e) throw l;
								}
							},
							abort: function () {
								e && e();
							},
						};
				}),
				jt.ajaxPrefilter(function (t) {
					t.crossDomain && (t.contents.script = !1);
				}),
				jt.ajaxSetup({
					accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" },
					contents: { script: /\b(?:java|ecma)script\b/ },
					converters: {
						"text script": function (t) {
							return jt.globalEval(t), t;
						},
					},
				}),
				jt.ajaxPrefilter("script", function (t) {
					void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET");
				}),
				jt.ajaxTransport("script", function (t) {
					if (t.crossDomain || t.scriptAttrs) {
						var e, n;
						return {
							send: function (i, r) {
								(e = jt("<script>")
									.attr(t.scriptAttrs || {})
									.prop({ charset: t.scriptCharset, src: t.url })
									.on(
										"load error",
										(n = function (t) {
											e.remove(), (n = null), t && r("error" === t.type ? 404 : 200, t.type);
										})
									)),
									dt.head.appendChild(e[0]);
							},
							abort: function () {
								n && n();
							},
						};
					}
				});
			var on = [],
				an = /(=)\?(?=&|$)|\?\?/;
			jt.ajaxSetup({
				jsonp: "callback",
				jsonpCallback: function () {
					var t = on.pop() || jt.expando + "_" + He++;
					return (this[t] = !0), t;
				},
			}),
				jt.ajaxPrefilter("json jsonp", function (t, e, i) {
					var r,
						s,
						o,
						a = t.jsonp !== !1 && (an.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && an.test(t.data) && "data");
					if (a || "jsonp" === t.dataTypes[0])
						return (
							(r = t.jsonpCallback = kt(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback),
							a ? (t[a] = t[a].replace(an, "$1" + r)) : t.jsonp !== !1 && (t.url += (qe.test(t.url) ? "&" : "?") + t.jsonp + "=" + r),
							(t.converters["script json"] = function () {
								return o || jt.error(r + " was not called"), o[0];
							}),
							(t.dataTypes[0] = "json"),
							(s = n[r]),
							(n[r] = function () {
								o = arguments;
							}),
							i.always(function () {
								void 0 === s ? jt(n).removeProp(r) : (n[r] = s), t[r] && ((t.jsonpCallback = e.jsonpCallback), on.push(r)), o && kt(s) && s(o[0]), (o = s = void 0);
							}),
							"script"
						);
				}),
				(Ct.createHTMLDocument = (function () {
					var t = dt.implementation.createHTMLDocument("").body;
					return (t.innerHTML = "<form></form><form></form>"), 2 === t.childNodes.length;
				})()),
				(jt.parseHTML = function (t, e, n) {
					if ("string" != typeof t) return [];
					"boolean" == typeof e && ((n = e), (e = !1));
					var i, r, s;
					return (
						e || (Ct.createHTMLDocument ? ((e = dt.implementation.createHTMLDocument("")), (i = e.createElement("base")), (i.href = dt.location.href), e.head.appendChild(i)) : (e = dt)),
						(r = Mt.exec(t)),
						(s = !n && []),
						r ? [e.createElement(r[1])] : ((r = E([t], e, s)), s && s.length && jt(s).remove(), jt.merge([], r.childNodes))
					);
				}),
				(jt.fn.load = function (t, e, n) {
					var i,
						r,
						s,
						o = this,
						a = t.indexOf(" ");
					return (
						a > -1 && ((i = nt(t.slice(a))), (t = t.slice(0, a))),
						kt(e) ? ((n = e), (e = void 0)) : e && "object" == typeof e && (r = "POST"),
						o.length > 0 &&
						jt
							.ajax({ url: t, type: r || "GET", dataType: "html", data: e })
							.done(function (t) {
								(s = arguments), o.html(i ? jt("<div>").append(jt.parseHTML(t)).find(i) : t);
							})
							.always(
								n &&
								function (t, e) {
									o.each(function () {
										n.apply(this, s || [t.responseText, e, t]);
									});
								}
							),
						this
					);
				}),
				jt.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
					jt.fn[e] = function (t) {
						return this.on(e, t);
					};
				}),
				(jt.expr.pseudos.animated = function (t) {
					return jt.grep(jt.timers, function (e) {
						return t === e.elem;
					}).length;
				}),
				(jt.offset = {
					setOffset: function (t, e, n) {
						var i,
							r,
							s,
							o,
							a,
							l,
							u,
							c = jt.css(t, "position"),
							h = jt(t),
							d = {};
						"static" === c && (t.style.position = "relative"),
							(a = h.offset()),
							(s = jt.css(t, "top")),
							(l = jt.css(t, "left")),
							(u = ("absolute" === c || "fixed" === c) && (s + l).indexOf("auto") > -1),
							u ? ((i = h.position()), (o = i.top), (r = i.left)) : ((o = parseFloat(s) || 0), (r = parseFloat(l) || 0)),
							kt(e) && (e = e.call(t, n, jt.extend({}, a))),
							null != e.top && (d.top = e.top - a.top + o),
							null != e.left && (d.left = e.left - a.left + r),
							"using" in e ? e.using.call(t, d) : h.css(d);
					},
				}),
				jt.fn.extend({
					offset: function (t) {
						if (arguments.length)
							return void 0 === t
								? this
								: this.each(function (e) {
									jt.offset.setOffset(this, t, e);
								});
						var e,
							n,
							i = this[0];
						if (i) return i.getClientRects().length ? ((e = i.getBoundingClientRect()), (n = i.ownerDocument.defaultView), { top: e.top + n.pageYOffset, left: e.left + n.pageXOffset }) : { top: 0, left: 0 };
					},
					position: function () {
						if (this[0]) {
							var t,
								e,
								n,
								i = this[0],
								r = { top: 0, left: 0 };
							if ("fixed" === jt.css(i, "position")) e = i.getBoundingClientRect();
							else {
								for (e = this.offset(), n = i.ownerDocument, t = i.offsetParent || n.documentElement; t && (t === n.body || t === n.documentElement) && "static" === jt.css(t, "position");) t = t.parentNode;
								t && t !== i && 1 === t.nodeType && ((r = jt(t).offset()), (r.top += jt.css(t, "borderTopWidth", !0)), (r.left += jt.css(t, "borderLeftWidth", !0)));
							}
							return { top: e.top - r.top - jt.css(i, "marginTop", !0), left: e.left - r.left - jt.css(i, "marginLeft", !0) };
						}
					},
					offsetParent: function () {
						return this.map(function () {
							for (var t = this.offsetParent; t && "static" === jt.css(t, "position");) t = t.offsetParent;
							return t || te;
						});
					},
				}),
				jt.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function (t, e) {
					var n = "pageYOffset" === e;
					jt.fn[t] = function (i) {
						return Bt(
							this,
							function (t, i, r) {
								var s;
								return Tt(t) ? (s = t) : 9 === t.nodeType && (s = t.defaultView), void 0 === r ? (s ? s[e] : t[i]) : void (s ? s.scrollTo(n ? s.pageXOffset : r, n ? r : s.pageYOffset) : (t[i] = r));
							},
							t,
							i,
							arguments.length
						);
					};
				}),
				jt.each(["top", "left"], function (t, e) {
					jt.cssHooks[e] = F(Ct.pixelPosition, function (t, n) {
						if (n) return (n = q(t, e)), ye.test(n) ? jt(t).position()[e] + "px" : n;
					});
				}),
				jt.each({ Height: "height", Width: "width" }, function (t, e) {
					jt.each({ padding: "inner" + t, content: e, "": "outer" + t }, function (n, i) {
						jt.fn[i] = function (r, s) {
							var o = arguments.length && (n || "boolean" != typeof r),
								a = n || (r === !0 || s === !0 ? "margin" : "border");
							return Bt(
								this,
								function (e, n, r) {
									var s;
									return Tt(e)
										? 0 === i.indexOf("outer")
											? e["inner" + t]
											: e.document.documentElement["client" + t]
										: 9 === e.nodeType
											? ((s = e.documentElement), Math.max(e.body["scroll" + t], s["scroll" + t], e.body["offset" + t], s["offset" + t], s["client" + t]))
											: void 0 === r
												? jt.css(e, n, a)
												: jt.style(e, n, r, a);
								},
								e,
								o ? r : void 0,
								o
							);
						};
					});
				}),
				jt.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (t, e) {
					jt.fn[e] = function (t, n) {
						return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e);
					};
				}),
				jt.fn.extend({
					hover: function (t, e) {
						return this.mouseenter(t).mouseleave(e || t);
					},
				}),
				jt.fn.extend({
					bind: function (t, e, n) {
						return this.on(t, null, e, n);
					},
					unbind: function (t, e) {
						return this.off(t, null, e);
					},
					delegate: function (t, e, n, i) {
						return this.on(e, t, n, i);
					},
					undelegate: function (t, e, n) {
						return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n);
					},
				}),
				(jt.proxy = function (t, e) {
					var n, i, r;
					if (("string" == typeof e && ((n = t[e]), (e = t), (t = n)), kt(t)))
						return (
							(i = pt.call(arguments, 2)),
							(r = function () {
								return t.apply(e || this, i.concat(pt.call(arguments)));
							}),
							(r.guid = t.guid = t.guid || jt.guid++),
							r
						);
				}),
				(jt.holdReady = function (t) {
					t ? jt.readyWait++ : jt.ready(!0);
				}),
				(jt.isArray = Array.isArray),
				(jt.parseJSON = JSON.parse),
				(jt.nodeName = u),
				(jt.isFunction = kt),
				(jt.isWindow = Tt),
				(jt.camelCase = y),
				(jt.type = a),
				(jt.now = Date.now),
				(jt.isNumeric = function (t) {
					var e = jt.type(t);
					return ("number" === e || "string" === e) && !isNaN(t - parseFloat(t));
				}),
				(i = []),
				(r = function () {
					return jt;
				}.apply(e, i)),
				!(void 0 !== r && (t.exports = r));
			var ln = n.jQuery,
				un = n.$;
			return (
				(jt.noConflict = function (t) {
					return n.$ === jt && (n.$ = un), t && n.jQuery === jt && (n.jQuery = ln), jt;
				}),
				s || (n.jQuery = n.$ = jt),
				jt
			);
		});
	},
	function (t, e) {
		"use strict";
	},
	function (t, e, n) {
		var i, r, s;
        /*! jquery.finger - v0.1.6 - 2016-10-05
         * https://github.com/ngryman/jquery.finger
         * Copyright (c) 2016 Nicolas Gryman; Licensed MIT */
		!(function (o) {
			(r = [n(3)]), (i = o), (s = "function" == typeof i ? i.apply(e, r) : i), !(void 0 !== s && (t.exports = s));
		})(function (t) {
			function e(n) {
				n.preventDefault(), t.event.remove(x, "click", e);
			}
			function n(t, e) {
				return (v ? e.originalEvent.touches[0] : e)["page" + t.toUpperCase()];
			}
			function i(n, i, r) {
				var a = t.Event(i, _);
				t.event.trigger(a, { originalEvent: n }, n.target),
					a.isDefaultPrevented() && (~i.indexOf("tap") && !v ? t.event.add(x, "click", e) : n.preventDefault()),
					r && (t.event.remove(x, y + "." + b, s), t.event.remove(x, m + "." + b, o));
			}
			function r(r) {
				if (!(r.which > 1)) {
					var c = r.timeStamp || +new Date();
					l != c &&
						((l = c),
							(w.x = _.x = n("x", r)),
							(w.y = _.y = n("y", r)),
							(w.time = c),
							(w.target = r.target),
							(_.orientation = null),
							(_.end = !1),
							(a = !1),
							(u = setTimeout(function () {
								i(r, "press", !0);
							}, C.pressDuration)),
							t.event.add(x, y + "." + b, s),
							t.event.add(x, m + "." + b, o),
							C.preventDefault && (r.preventDefault(), t.event.add(x, "click", e)));
				}
			}
			function s(e) {
				if (((_.x = n("x", e)), (_.y = n("y", e)), (_.dx = _.x - w.x), (_.dy = _.y - w.y), (_.adx = Math.abs(_.dx)), (_.ady = Math.abs(_.dy)), (a = _.adx > C.motionThreshold || _.ady > C.motionThreshold))) {
					for (
						clearTimeout(u), _.orientation || (_.adx > _.ady ? ((_.orientation = "horizontal"), (_.direction = _.dx > 0 ? 1 : -1)) : ((_.orientation = "vertical"), (_.direction = _.dy > 0 ? 1 : -1)));
						e.target && e.target !== w.target;

					)
						e.target = e.target.parentNode;
					return e.target !== w.target ? ((e.target = w.target), void o.call(this, t.Event(m + "." + b, e))) : void i(e, "drag");
				}
			}
			function o(t) {
				var e,
					n = t.timeStamp || +new Date(),
					r = n - w.time;
				if ((clearTimeout(u), a)) (t.target = w.target), r < C.flickDuration && i(t, "flick"), (_.end = !0), (e = "drag");
				else if (t.target === w.target) {
					var s = c === t.target && n - h < C.doubleTapInterval;
					(e = s ? "doubletap" : "tap"), (c = s ? null : w.target), (h = n);
				}
				e && i(t, e, !0);
			}
			var a,
				l,
				u,
				c,
				h,
				d = navigator.userAgent,
				f = /chrome/i.exec(d),
				p = /android/i.exec(d),
				v = "ontouchstart" in window && !(f && !p),
				g = v ? "touchstart" : "mousedown",
				m = v ? "touchend touchcancel" : "mouseup mouseleave",
				y = v ? "touchmove" : "mousemove",
				b = "finger",
				x = t("html")[0],
				w = {},
				_ = {},
				C = (t.Finger = { pressDuration: 300, doubleTapInterval: 300, flickDuration: 150, motionThreshold: 5 });
			return (
				t.event.add(x, g + "." + b, r),
				t.each("tap doubletap press drag flick".split(" "), function (e, n) {
					t.fn[n] = function (t) {
						return t ? this.on(n, t) : this.trigger(n);
					};
				}),
				C
			);
		});
	},
	function (t, e) {
		"use strict";
		function n(t, e, n, i, r) {
			return 0 === e ? n : i * Math.pow(2, 10 * (e / r - 1)) + n;
		}
		function i(t, e, n, i, r) {
			return e === r ? n + i : i * (-Math.pow(2, (-10 * e) / r) + 1) + n;
		}
		function r(t, e, n, i, r) {
			return 0 === e ? n : e === r ? n + i : (e /= r / 2) < 1 ? (i / 2) * Math.pow(2, 10 * (e - 1)) + n : (i / 2) * (-Math.pow(2, -10 * --e) + 2) + n;
		}
		Object.defineProperty(e, "__esModule", { value: !0 }), (e["default"] = { easeInExpo: n, easeOutExpo: i, easeInOutExpo: r });
	},
	function (t, e) {
		"use strict";
		function n() {
			var t = void 0;
			return (
				(t =
					!(!matchMedia("(any-hover: hover)").matches && !matchMedia("(hover: hover)").matches) ||
					(!matchMedia("(hover: none)").matches && (!(!r() || !i()) || (!o() && "undefined" == typeof document.documentElement.ontouchstart)))),
				function () {
					return t;
				}
			);
		}
		function i() {
			var t = document.createElement("div");
			(t.style.cssText = "width:100px;height:100px;overflow:scroll !important;position:absolute;top:-9999px"), document.body.appendChild(t);
			var e = t.offsetWidth - t.clientWidth;
			return document.body.removeChild(t), e;
		}
		function r() {
			var t = navigator.userAgent;
			return t.indexOf("MSIE ") > 0 || t.indexOf("Trident/") > 0 || t.indexOf("Edge/") > 0;
		}
		function s() {
			var t = navigator.userAgent;
			return t.indexOf("MSIE ") > 0 || t.indexOf("Trident/") > 0;
		}
		function o() {
			return a() || l();
		}
		function a() {
			var t = navigator.userAgent || navigator.vendor || window.opera;
			return u.test(t) || h.test(t.substr(0, 4));
		}
		function l() {
			var t = navigator.userAgent || navigator.vendor || window.opera;
			return c.test(t);
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var u = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i,
			c = /android|ipad|playbook|silk/i,
			h = /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i;
		e["default"] = { hasHoverSupport: n(), isIE: r, isOldIE: s, isMobile: o, isPhone: a, isTablet: l };
	},
	function (t, e, n) {
		var i, r, s;
        /*!
         * jquery-app <https://github.com/kasparsz/jquery-app>
         *
         * Copyright (c) 2016, Kaspars Zuks.
         * Licensed under the MIT License.
         */
		!(function (o) {
			(r = [n(3)]), (i = o), (s = "function" == typeof i ? i.apply(e, r) : i), !(void 0 !== s && (t.exports = s));
		})(function (t) {
			var e = /\s*,\s*/,
				n = /[^a-z]/,
				i = "jQueryAppData";
			(t.app = {
				settings: { namespace: "plugin", namespaceOptions: !0, debug: !1 },
				call: function (e) {
					var n = t(e),
						r = t.app.getPlugins(n),
						s = n.data(i);
					s || n.data(i, (s = {})),
						r.forEach(function (i) {
							if (s[i]) t.app.settings.debug && console.log('$.app skipped plugin "%s" on %o because it already has been called previously', i, e);
							else {
								s[i] = !0;
								var r = t.app.getPluginOptions(n, i);
								n[i](r), t.app.settings.debug && console.log('$.app called plugin "%s" on %o with options %O', i, e, r);
							}
						});
				},
				getPlugins: function (n) {
					var i = n.data(t.app.settings.namespace).split(e);
					return i.filter(function (e) {
						if (e) {
							if ("function" == typeof t.fn[e]) return !0;
							t.app.settings.debug && console.error('$.app coundn\'t find jQuery plugin "%s" declared on element %o', e, n.get(0));
						}
						return !1;
					});
				},
				getPluginOptions: function (e, i) {
					var r = {},
						s = e.data();
					if (t.app.settings.namespaceOptions)
						for (var o in s) {
							var a = s[o];
							if (o === i) t.extend(r, t.isPlainObject(a) ? a : {});
							else if (0 === o.indexOf(i) && o.substr(i.length, 1).match(n)) {
								var l = o.substr(i.length);
								(l = l[0].toLowerCase() + l.substr(1)), (r[l] = a);
							}
						}
					else t.extend(r, s);
					return r;
				},
			}),
				(t.fn.app = function (e) {
					var n = t.extend(t.app.settings, e),
						i = "[data-" + n.namespace + "]",
						r = this.find(i).addBack(i);
					return (
						r.each(function (e, n) {
							return t.app.call(n);
						}),
						this
					);
				});
		});
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			for (var n = 0, i = t.length; n < i; n++) e(t[n], n);
		}
		function s(t) {
			return t.replace(p, function (t, e) {
				return e.toUpperCase();
			});
		}
		function o(t, e) {
			r(e, function (e) {
				if (e.cssText.indexOf("vh") !== -1) {
					var n = [];
					e.style
						? (r(d, function (t) {
							if (e.style[t]) {
								var i = e.style[t],
									r = i.match(f);
								r && n.push({ name: s(t), rule: i.replace(r[0], "%value%"), value: parseFloat(r[1]) });
							}
						}),
							n.length && t.push({ rule: e, properties: n }))
						: e.cssRules && o(t, e.cssRules);
				}
			});
		}
		function a() {
			var t = [];
			return (
				r(document.styleSheets, function (e) {
					e.cssRules && o(t, e.cssRules);
				}),
				t
			);
		}
		function l(t, e) {
			r(t, function (t) {
				r(t.properties, function (n) {
					var i = Math.round((e * n.value) / 100) + "px";
					t.rule.style[n.name] = n.rule.replace("%value%", i);
				});
			});
		}
		function u(t, e) {
			var n = null,
				i = function () {
					n = null;
				},
				r = function s() {
					n && (t(), requestAnimationFrame(s));
				};
			return function () {
				n || ((n = setTimeout(i, e)), requestAnimationFrame(r));
			};
		}
		var c = n(3),
			h = i(c),
			d = ["min-height", "height", "max-height"],
			f = /(\d+)vh/,
			p = /-([a-z])/g;
		(0, h["default"])(function () {
			if (/iPad|iPhone|iPod|Android/.test(navigator.userAgent) && !window.MSStream) {
				var t = function () {
					var t = window.innerHeight;
					t !== n && ((n = t), l(e, t));
				},
					e = a(),
					n = window.innerHeight;
				l(e, n);
				var i = u(t, 1e3);
				(0, h["default"])(window).on("resize", i), (0, h["default"])(document).on("touchmove", i), setTimeout(t, 16), t();
			}
		});
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		var r = n(11),
			s = i(r);
		"function" != typeof Object.assign && (Object.assign = s["default"]);
	},
	function (t, e, n) {
		var i = n(12),
			r = n(30),
			s = n(31),
			o = n(41),
			a = n(44),
			l = n(45),
			u = Object.prototype,
			c = u.hasOwnProperty,
			h = s(function (t, e) {
				if (a(e) || o(e)) return void r(e, l(e), t);
				for (var n in e) c.call(e, n) && i(t, n, e[n]);
			});
		t.exports = h;
	},
	function (t, e, n) {
		function i(t, e, n) {
			var i = t[e];
			(a.call(t, e) && s(i, n) && (void 0 !== n || e in t)) || r(t, e, n);
		}
		var r = n(13),
			s = n(29),
			o = Object.prototype,
			a = o.hasOwnProperty;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n) {
			"__proto__" == e && r ? r(t, e, { configurable: !0, enumerable: !0, value: n, writable: !0 }) : (t[e] = n);
		}
		var r = n(14);
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(15),
			r = (function () {
				try {
					var t = i(Object, "defineProperty");
					return t({}, "", {}), t;
				} catch (e) { }
			})();
		t.exports = r;
	},
	function (t, e, n) {
		function i(t, e) {
			var n = s(t, e);
			return r(n) ? n : void 0;
		}
		var r = n(16),
			s = n(28);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			if (!o(t) || s(t)) return !1;
			var e = r(t) ? p : u;
			return e.test(a(t));
		}
		var r = n(17),
			s = n(25),
			o = n(24),
			a = n(27),
			l = /[\\^$.*+?()[\]{}|]/g,
			u = /^\[object .+?Constructor\]$/,
			c = Function.prototype,
			h = Object.prototype,
			d = c.toString,
			f = h.hasOwnProperty,
			p = RegExp(
				"^" +
				d
					.call(f)
					.replace(l, "\\$&")
					.replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") +
				"$"
			);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			if (!s(t)) return !1;
			var e = r(t);
			return e == a || e == l || e == o || e == u;
		}
		var r = n(18),
			s = n(24),
			o = "[object AsyncFunction]",
			a = "[object Function]",
			l = "[object GeneratorFunction]",
			u = "[object Proxy]";
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return null == t ? (void 0 === t ? l : a) : u && u in Object(t) ? s(t) : o(t);
		}
		var r = n(19),
			s = n(22),
			o = n(23),
			a = "[object Null]",
			l = "[object Undefined]",
			u = r ? r.toStringTag : void 0;
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(20),
			r = i.Symbol;
		t.exports = r;
	},
	function (t, e, n) {
		var i = n(21),
			r = "object" == typeof self && self && self.Object === Object && self,
			s = i || r || Function("return this")();
		t.exports = s;
	},
	function (t, e) {
		(function (e) {
			var n = "object" == typeof e && e && e.Object === Object && e;
			t.exports = n;
		}.call(
			e,
			(function () {
				return this;
			})()
		));
	},
	function (t, e, n) {
		function i(t) {
			var e = o.call(t, l),
				n = t[l];
			try {
				t[l] = void 0;
				var i = !0;
			} catch (r) { }
			var s = a.call(t);
			return i && (e ? (t[l] = n) : delete t[l]), s;
		}
		var r = n(19),
			s = Object.prototype,
			o = s.hasOwnProperty,
			a = s.toString,
			l = r ? r.toStringTag : void 0;
		t.exports = i;
	},
	function (t, e) {
		function n(t) {
			return r.call(t);
		}
		var i = Object.prototype,
			r = i.toString;
		t.exports = n;
	},
	function (t, e) {
		function n(t) {
			var e = typeof t;
			return null != t && ("object" == e || "function" == e);
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t) {
			return !!s && s in t;
		}
		var r = n(26),
			s = (function () {
				var t = /[^.]+$/.exec((r && r.keys && r.keys.IE_PROTO) || "");
				return t ? "Symbol(src)_1." + t : "";
			})();
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(20),
			r = i["__core-js_shared__"];
		t.exports = r;
	},
	function (t, e) {
		function n(t) {
			if (null != t) {
				try {
					return r.call(t);
				} catch (e) { }
				try {
					return t + "";
				} catch (e) { }
			}
			return "";
		}
		var i = Function.prototype,
			r = i.toString;
		t.exports = n;
	},
	function (t, e) {
		function n(t, e) {
			return null == t ? void 0 : t[e];
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t, e) {
			return t === e || (t !== t && e !== e);
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t, e, n, i) {
			var o = !n;
			n || (n = {});
			for (var a = -1, l = e.length; ++a < l;) {
				var u = e[a],
					c = i ? i(n[u], t[u], u, n, t) : void 0;
				void 0 === c && (c = t[u]), o ? s(n, u, c) : r(n, u, c);
			}
			return n;
		}
		var r = n(12),
			s = n(13);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return r(function (e, n) {
				var i = -1,
					r = n.length,
					o = r > 1 ? n[r - 1] : void 0,
					a = r > 2 ? n[2] : void 0;
				for (o = t.length > 3 && "function" == typeof o ? (r--, o) : void 0, a && s(n[0], n[1], a) && ((o = r < 3 ? void 0 : o), (r = 1)), e = Object(e); ++i < r;) {
					var l = n[i];
					l && t(e, l, i, o);
				}
				return e;
			});
		}
		var r = n(32),
			s = n(40);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			return o(s(t, e, r), t + "");
		}
		var r = n(33),
			s = n(34),
			o = n(36);
		t.exports = i;
	},
	function (t, e) {
		function n(t) {
			return t;
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t, e, n) {
			return (
				(e = s(void 0 === e ? t.length - 1 : e, 0)),
				function () {
					for (var i = arguments, o = -1, a = s(i.length - e, 0), l = Array(a); ++o < a;) l[o] = i[e + o];
					o = -1;
					for (var u = Array(e + 1); ++o < e;) u[o] = i[o];
					return (u[e] = n(l)), r(t, this, u);
				}
			);
		}
		var r = n(35),
			s = Math.max;
		t.exports = i;
	},
	function (t, e) {
		function n(t, e, n) {
			switch (n.length) {
				case 0:
					return t.call(e);
				case 1:
					return t.call(e, n[0]);
				case 2:
					return t.call(e, n[0], n[1]);
				case 3:
					return t.call(e, n[0], n[1], n[2]);
			}
			return t.apply(e, n);
		}
		t.exports = n;
	},
	function (t, e, n) {
		var i = n(37),
			r = n(39),
			s = r(i);
		t.exports = s;
	},
	function (t, e, n) {
		var i = n(38),
			r = n(14),
			s = n(33),
			o = r
				? function (t, e) {
					return r(t, "toString", { configurable: !0, enumerable: !1, value: i(e), writable: !0 });
				}
				: s;
		t.exports = o;
	},
	function (t, e) {
		function n(t) {
			return function () {
				return t;
			};
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t) {
			var e = 0,
				n = 0;
			return function () {
				var o = s(),
					a = r - (o - n);
				if (((n = o), a > 0)) {
					if (++e >= i) return arguments[0];
				} else e = 0;
				return t.apply(void 0, arguments);
			};
		}
		var i = 800,
			r = 16,
			s = Date.now;
		t.exports = n;
	},
	function (t, e, n) {
		function i(t, e, n) {
			if (!a(n)) return !1;
			var i = typeof e;
			return !!("number" == i ? s(n) && o(e, n.length) : "string" == i && e in n) && r(n[e], t);
		}
		var r = n(29),
			s = n(41),
			o = n(43),
			a = n(24);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return null != t && s(t.length) && !r(t);
		}
		var r = n(17),
			s = n(42);
		t.exports = i;
	},
	function (t, e) {
		function n(t) {
			return "number" == typeof t && t > -1 && t % 1 == 0 && t <= i;
		}
		var i = 9007199254740991;
		t.exports = n;
	},
	function (t, e) {
		function n(t, e) {
			var n = typeof t;
			return (e = null == e ? i : e), !!e && ("number" == n || ("symbol" != n && r.test(t))) && t > -1 && t % 1 == 0 && t < e;
		}
		var i = 9007199254740991,
			r = /^(?:0|[1-9]\d*)$/;
		t.exports = n;
	},
	function (t, e) {
		function n(t) {
			var e = t && t.constructor,
				n = ("function" == typeof e && e.prototype) || i;
			return t === n;
		}
		var i = Object.prototype;
		t.exports = n;
	},
	function (t, e, n) {
		function i(t) {
			return o(t) ? r(t) : s(t);
		}
		var r = n(46),
			s = n(59),
			o = n(41);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			var n = o(t),
				i = !n && s(t),
				c = !n && !i && a(t),
				d = !n && !i && !c && u(t),
				f = n || i || c || d,
				p = f ? r(t.length, String) : [],
				v = p.length;
			for (var g in t) (!e && !h.call(t, g)) || (f && ("length" == g || (c && ("offset" == g || "parent" == g)) || (d && ("buffer" == g || "byteLength" == g || "byteOffset" == g)) || l(g, v))) || p.push(g);
			return p;
		}
		var r = n(47),
			s = n(48),
			o = n(51),
			a = n(52),
			l = n(43),
			u = n(55),
			c = Object.prototype,
			h = c.hasOwnProperty;
		t.exports = i;
	},
	function (t, e) {
		function n(t, e) {
			for (var n = -1, i = Array(t); ++n < t;) i[n] = e(n);
			return i;
		}
		t.exports = n;
	},
	function (t, e, n) {
		var i = n(49),
			r = n(50),
			s = Object.prototype,
			o = s.hasOwnProperty,
			a = s.propertyIsEnumerable,
			l = i(
				(function () {
					return arguments;
				})()
			)
				? i
				: function (t) {
					return r(t) && o.call(t, "callee") && !a.call(t, "callee");
				};
		t.exports = l;
	},
	function (t, e, n) {
		function i(t) {
			return s(t) && r(t) == o;
		}
		var r = n(18),
			s = n(50),
			o = "[object Arguments]";
		t.exports = i;
	},
	function (t, e) {
		function n(t) {
			return null != t && "object" == typeof t;
		}
		t.exports = n;
	},
	function (t, e) {
		var n = Array.isArray;
		t.exports = n;
	},
	function (t, e, n) {
		(function (t) {
			var i = n(20),
				r = n(54),
				s = "object" == typeof e && e && !e.nodeType && e,
				o = s && "object" == typeof t && t && !t.nodeType && t,
				a = o && o.exports === s,
				l = a ? i.Buffer : void 0,
				u = l ? l.isBuffer : void 0,
				c = u || r;
			t.exports = c;
		}.call(e, n(53)(t)));
	},
	function (t, e) {
		t.exports = function (t) {
			return t.webpackPolyfill || ((t.deprecate = function () { }), (t.paths = []), (t.children = []), (t.webpackPolyfill = 1)), t;
		};
	},
	function (t, e) {
		function n() {
			return !1;
		}
		t.exports = n;
	},
	function (t, e, n) {
		var i = n(56),
			r = n(57),
			s = n(58),
			o = s && s.isTypedArray,
			a = o ? r(o) : i;
		t.exports = a;
	},
	function (t, e, n) {
		function i(t) {
			return o(t) && s(t.length) && !!D[r(t)];
		}
		var r = n(18),
			s = n(42),
			o = n(50),
			a = "[object Arguments]",
			l = "[object Array]",
			u = "[object Boolean]",
			c = "[object Date]",
			h = "[object Error]",
			d = "[object Function]",
			f = "[object Map]",
			p = "[object Number]",
			v = "[object Object]",
			g = "[object RegExp]",
			m = "[object Set]",
			y = "[object String]",
			b = "[object WeakMap]",
			x = "[object ArrayBuffer]",
			w = "[object DataView]",
			_ = "[object Float32Array]",
			C = "[object Float64Array]",
			k = "[object Int8Array]",
			T = "[object Int16Array]",
			S = "[object Int32Array]",
			E = "[object Uint8Array]",
			j = "[object Uint8ClampedArray]",
			O = "[object Uint16Array]",
			$ = "[object Uint32Array]",
			D = {};
		(D[_] = D[C] = D[k] = D[T] = D[S] = D[E] = D[j] = D[O] = D[$] = !0), (D[a] = D[l] = D[x] = D[u] = D[w] = D[c] = D[h] = D[d] = D[f] = D[p] = D[v] = D[g] = D[m] = D[y] = D[b] = !1), (t.exports = i);
	},
	function (t, e) {
		function n(t) {
			return function (e) {
				return t(e);
			};
		}
		t.exports = n;
	},
	function (t, e, n) {
		(function (t) {
			var i = n(21),
				r = "object" == typeof e && e && !e.nodeType && e,
				s = r && "object" == typeof t && t && !t.nodeType && t,
				o = s && s.exports === r,
				a = o && i.process,
				l = (function () {
					try {
						var t = s && s.require && s.require("util").types;
						return t ? t : a && a.binding && a.binding("util");
					} catch (e) { }
				})();
			t.exports = l;
		}.call(e, n(53)(t)));
	},
	function (t, e, n) {
		function i(t) {
			if (!r(t)) return s(t);
			var e = [];
			for (var n in Object(t)) a.call(t, n) && "constructor" != n && e.push(n);
			return e;
		}
		var r = n(44),
			s = n(60),
			o = Object.prototype,
			a = o.hasOwnProperty;
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(61),
			r = i(Object.keys, Object);
		t.exports = r;
	},
	function (t, e) {
		function n(t, e) {
			return function (n) {
				return t(e(n));
			};
		}
		t.exports = n;
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		} /*! svg4everybody v2.0.0 | github.com/jonathantneal/svg4everybody */
		function r(t) {
			(t.onreadystatechange = function () {
				if (4 === t.readyState) {
					var e = document.createElement("x");
					e.innerHTML = t.responseText;
					var n = e.getElementsByTagName("svg");
					n.length && (n[0].setAttribute("class", "is-out-of-screen"), document.body.insertBefore(n[0], document.body.firstChild));
				}
			}),
				t.onreadystatechange();
		}
		function s(t) {
			t = t || {};
			var e = (t.element || document).getElementsByTagName("use"),
				n = "polyfill" in t ? t.polyfill : /\bEdge\/12\b|\bTrident\/[567]\b|\bVersion\/7.0 Safari\b/.test(navigator.userAgent) || (navigator.userAgent.match(/AppleWebKit\/(\d+)/) || [])[1] < 537;
			t.validate;
			if (n)
				for (var i, s, o = 0, a = e.length; o < a; o++) {
					for (i = e[o], s = i ? i.parentNode : null; s && !/svg/i.test(s.nodeName);) s = s.parentNode;
					if (s && /svg/i.test(s.nodeName)) {
						var u = i.getAttribute("xlink:href"),
							c = u.split("#"),
							h = c[0],
							d = c[1];
						if ((i.setAttribute("xlink:href", "#" + d), h.length && !l[h])) {
							var f = new XMLHttpRequest();
							f.open("GET", h), f.send(), r(f), (l[h] = !0);
						}
					}
				}
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var o = n(3),
			a = i(o),
			l = {};
		(a["default"].fn.svg4everybody = function () {
			return this.each(function () {
				s({ element: this });
			});
		}),
			(e["default"] = s);
	},
	function (t, e) {
		/*! npm.im/object-fit-images 3.2.4 */
		"use strict";
		function n(t, e) {
			return "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='" + t + "' height='" + e + "'%3E%3C/svg%3E";
		}
		function i(t) {
			if (t.srcset && !m && window.picturefill) {
				var e = window.picturefill._;
				(t[e.ns] && t[e.ns].evaled) || e.fillImg(t, { reselect: !0 }), t[e.ns].curSrc || ((t[e.ns].supported = !1), e.fillImg(t, { reselect: !0 })), (t.currentSrc = t[e.ns].curSrc || t.src);
			}
		}
		function r(t) {
			for (var e, n = getComputedStyle(t).fontFamily, i = {}; null !== (e = d.exec(n));) i[e[1]] = e[2];
			return i;
		}
		function s(t, e, i) {
			var r = n(e || 1, i || 0);
			y.call(t, "src") !== r && b.call(t, "src", r);
		}
		function o(t, e) {
			t.naturalWidth ? e(t) : setTimeout(o, 100, t, e);
		}
		function a(t) {
			var e = r(t),
				n = t[h];
			if (((e["object-fit"] = e["object-fit"] || "fill"), !n.img)) {
				if ("fill" === e["object-fit"]) return;
				if (!n.skipTest && p && !e["object-position"]) return;
			}
			if (!n.img) {
				(n.img = new Image(t.width, t.height)),
					(n.img.srcset = y.call(t, "data-ofi-srcset") || t.srcset),
					(n.img.src = y.call(t, "data-ofi-src") || t.src),
					b.call(t, "data-ofi-src", t.src),
					t.srcset && b.call(t, "data-ofi-srcset", t.srcset),
					s(t, t.naturalWidth || t.width, t.naturalHeight || t.height),
					t.srcset && (t.srcset = "");
				try {
					l(t);
				} catch (a) {
					window.console && console.warn("https://bit.ly/ofi-old-browser");
				}
			}
			i(n.img),
				(t.style.backgroundImage = 'url("' + (n.img.currentSrc || n.img.src).replace(/"/g, '\\"') + '")'),
				(t.style.backgroundPosition = e["object-position"] || "center"),
				(t.style.backgroundRepeat = "no-repeat"),
				(t.style.backgroundOrigin = "content-box"),
				/scale-down/.test(e["object-fit"])
					? o(n.img, function () {
						n.img.naturalWidth > t.width || n.img.naturalHeight > t.height ? (t.style.backgroundSize = "contain") : (t.style.backgroundSize = "auto");
					})
					: (t.style.backgroundSize = e["object-fit"].replace("none", "auto").replace("fill", "100% 100%")),
				o(n.img, function (e) {
					s(t, e.naturalWidth, e.naturalHeight);
				});
		}
		function l(t) {
			var e = {
				get: function (e) {
					return t[h].img[e ? e : "src"];
				},
				set: function (e, n) {
					return (t[h].img[n ? n : "src"] = e), b.call(t, "data-ofi-" + n, e), a(t), e;
				},
			};
			Object.defineProperty(t, "src", e),
				Object.defineProperty(t, "currentSrc", {
					get: function () {
						return e.get("currentSrc");
					},
				}),
				Object.defineProperty(t, "srcset", {
					get: function () {
						return e.get("srcset");
					},
					set: function (t) {
						return e.set(t, "srcset");
					},
				});
		}
		function u() {
			function t(t, e) {
				return t[h] && t[h].img && ("src" === e || "srcset" === e) ? t[h].img : t;
			}
			v ||
				((HTMLImageElement.prototype.getAttribute = function (e) {
					return y.call(t(this, e), e);
				}),
					(HTMLImageElement.prototype.setAttribute = function (e, n) {
						return b.call(t(this, e), e, String(n));
					}));
		}
		function c(t, e) {
			var n = !x && !t;
			if (((e = e || {}), (t = t || "img"), (v && !e.skipTest) || !g)) return !1;
			"img" === t ? (t = document.getElementsByTagName("img")) : "string" == typeof t ? (t = document.querySelectorAll(t)) : "length" in t || (t = [t]);
			for (var i = 0; i < t.length; i++) (t[i][h] = t[i][h] || { skipTest: e.skipTest }), a(t[i]);
			n &&
				(document.body.addEventListener(
					"load",
					function (t) {
						"IMG" === t.target.tagName && c(t.target, { skipTest: e.skipTest });
					},
					!0
				),
					(x = !0),
					(t = "img")),
				e.watchMQ && window.addEventListener("resize", c.bind(null, t, { skipTest: e.skipTest }));
		}
		var h = "bfred-it:object-fit-images",
			d = /(object-fit|object-position)\s*:\s*([-.\w\s%]+)/g,
			f = "undefined" == typeof Image ? { style: { "object-position": 1 } } : new Image(),
			p = "object-fit" in f.style,
			v = "object-position" in f.style,
			g = "background-size" in f.style,
			m = "string" == typeof f.currentSrc,
			y = f.getAttribute,
			b = f.setAttribute,
			x = !1;
		(c.supportsObjectFit = p), (c.supportsObjectPosition = v), u(), (t.exports = c);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(65),
			u = i(l),
			c = 0,
			h = 1,
			d = 2,
			f = (function () {
				function t(e, n) {
					r(this, t);
					var i = ((this.options = a["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, a["default"])(e)));
					(this.height = i.height()),
						(this.scrollDelta = 0),
						(this.scrollLast = 0),
						(this.scrollMin = a["default"].scroller.getViewOffset(i).top),
						(this.state = c),
						this.options.scrollable && a["default"].scroller.onpassive("scroll", this.handleScroll.bind(this));
				}
				return (
					s(t, null, [
						{
							key: "Defaults",
							get: function () {
								return { transparent: !1, scrollable: !0 };
							},
						},
					]),
					s(t, [
						{
							key: "update",
							value: function () {
								this.updateUI(!0);
							},
						},
						{
							key: "handleScroll",
							value: function () {
								this.updateUI(!1);
							},
						},
						{
							key: "updateUI",
							value: function (t) {
								var e = this.$container,
									n = this.height,
									i = this.state,
									r = a["default"].scroller.scrollTop(),
									s = Math.max(0, Math.min(n, this.scrollDelta + (r - this.scrollLast))),
									o = 0,
									l = this.options.transparent,
									u = !1;
								(o = a["default"].scroller.custom ? r : -s),
									this.isSubNavigationVisible() && ((s = a["default"].scroller.custom ? 0 : (o = 0)), (l = !1), (u = !0)),
									r <= this.scrollMin + n && i === c && !u
										? t === !0 && e.toggleClass("ui-header", l)
										: r <= this.scrollMin && !u
											? i !== c &&
											((this.state = c),
												e
													.addClass("header--static")
													.addClass(l ? "ui-header" : null)
													.removeClass("header--fixed-free")
													.removeClass("header--fixed-top")
													.css("transform", ""))
											: s <= 0
												? (i !== h && ((this.state = h), e.removeClass("header--fixed-free").removeClass("header--static").removeClass("ui-header").addClass("header--fixed-top")),
													e.css("transform", "translateY(" + o + "px)"))
												: s > 0 &&
												(i !== d
													? ((this.state = d),
														e.removeClass("header--fixed-top").removeClass("header--static").removeClass("ui-header").addClass("header--fixed-free"),
														i === c ? e.css("transform", "translateY(" + (o - n) + "px)") : e.css("transform", "translateY(" + o + "px)"))
													: this.scrollDelta === n && s < n
														? e.css("transform", "translateY(" + (o - n) + "px)")
														: a["default"].scroller.custom || this.scrollDelta === s || e.css("transform", "translateY(" + o + "px)")),
									(this.scrollLast = r),
									(this.scrollDelta = s);
							},
						},
						{
							key: "isSubNavigationVisible",
							value: function () {
								for (var t = this.$container, e = t.find(".js-subnavigation"), n = 0; n < e.length; n++) if (e.eq(n).headerSubNavigation("isVisible")) return !0;
								return t.headerMobile("isVisible");
							},
						},
					]),
					t
				);
			})();
		(e["default"] = f), (a["default"].fn.headerFixed = (0, u["default"])(f, { api: ["update"] }));
	},
	function (t, e) {
		function n() {
			return "ns" + l++;
		}
		function i(t, e, n, i) {
			var r = t.data(i.namespace);
			if (r) i.optionsSetter && "function" == typeof r[i.optionsSetter] && r[i.optionsSetter].apply(r, n);
			else {
				var s = e.bind.apply(e, [e, t].concat(n));
				if (((r = new s()), !r || "object" != typeof r)) return;
				t.data(i.namespace, r);
			}
			return r;
		}
		function r(t, e) {
			if ("string" == typeof t[0]) {
				var n = t[0],
					i = e.api;
				if (!i || i.indexOf(n) !== -1) return { apiName: n, apiParams: t.slice(1), params: [] };
			}
			return { apiName: null, apiParams: null, params: t };
		}
		function s(t, e, n, s) {
			var o = r(n, s),
				a = o.apiName,
				l = o.apiParams,
				u = o.params;
			if ("instance" === a) return t.data(s.namespace) || null;
			var c = i(t, e, u, s);
			return c && a ? c[a].apply(c, l) : void 0;
		}
		function o(t, e, n, i) {
			for (var r = t, o = 0, a = t.length; o < a; o++) {
				var l = s(t.eq(o), e, n, i);
				void 0 !== l && (r = l);
			}
			return r;
		}
		function a(t, e) {
			void 0 === e && (e = {});
			var i = Object.assign({ api: null, namespace: n(), optionsSetter: "setOptions" }, e);
			if ("function" == typeof t)
				return function () {
					for (var e = [], n = arguments.length; n--;) e[n] = arguments[n];
					return o(this, t, e, i);
				};
			throw "fn is required field for jquery-plugin-generator";
		}
        /*!
         * jquery-plugin-generator <https://github.com/kasparsz/jquery-plugin-generator>
         *
         * Copyright (c) 2019, Kaspars Zuks.
         * Licensed under the MIT License.
         */
		var l = 0;
		t.exports = a;
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(65),
			u = i(l);
		n(67);
		var c = (function () {
			function t(e, n) {
				r(this, t);
				var i = (this.options = a["default"].extend({}, this.constructor.Defaults, n)),
					s = (this.$subnav = (0, a["default"])(e)),
					o = (this.$header = s.closest(".header")),
					l = (this.$trigger = o.find(i.triggerSelector));
				(this.visible = !1), (this.animating = null), (this.timer = null), l.hover(this.showDelayed.bind(this), this.hideDelayed.bind(this)), s.hover(this.show.bind(this), this.hideDelayed.bind(this));
			}
			return (
				s(t, null, [
					{
						key: "Defaults",
						get: function () {
							return { triggerSelector: ".js-subnavigation-trigger", showDelay: 260, hideDelay: 60 };
						},
					},
				]),
				s(t, [
					{
						key: "isVisible",
						value: function () {
							return this.visible;
						},
					},
					{
						key: "show",
						value: function () {
							if (!this.visible) {
								this.visible = !0;
								var t = this.$header,
									e = this.$trigger,
									n = this.$subnav;
								e.addClass("is-active"),
									n.removeClass("is-hidden"),
									".js-subnavigation-trigger-language" !== this.options.triggerSelector && (t.addClass("header--expanded"), t.headerFixed("update")),
									setTimeout(function () {
										n.addClass("animation--fade--active");
									}, 16),
									clearTimeout(this.animation);
							}
							clearTimeout(this.timer);
						},
					},
					{
						key: "hide",
						value: function () {
							if (this.visible) {
								this.visible = !1;
								var t = this.$header,
									e = this.$trigger,
									n = this.$subnav;
								e.removeClass("is-active"),
									t.removeClass("header--expanded"),
									t.headerFixed("update"),
									setTimeout(function () {
										n.removeClass("animation--fade--active");
									}, 16),
									clearTimeout(this.animation),
									(this.animation = setTimeout(function () {
										n.addClass("is-hidden");
									}, a["default"].durationNormal));
							}
							clearTimeout(this.timer);
						},
					},
					{
						key: "showDelayed",
						value: function () {
							clearTimeout(this.timer), (this.timer = setTimeout(this.show.bind(this), this.options.showDelay));
						},
					},
					{
						key: "hideDelayed",
						value: function () {
							clearTimeout(this.timer), (this.timer = setTimeout(this.hide.bind(this), this.options.hideDelay));
						},
					},
				]),
				t
			);
		})();
		(e["default"] = c), (a["default"].fn.headerSubNavigation = (0, u["default"])(c));
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			for (var n = 0, i = t.length; n < i; n++) e(t[n]);
		}
		function s(t, e, n) {
			var i = (0, l["default"])(e),
				s = l["default"].Deferred();
			return (
				r(t.before, function (t) {
					return t(i, n);
				}),
				t.transition.length
					? requestAnimationFrame(function () {
						setTimeout(function () {
							r(t.transition, function (t) {
								return t(i, n);
							}),
								i.transitionend().done(function () {
									r(t.after, function (t) {
										return t(i, n);
									}),
										s.resolve();
								});
						}, t.delay);
					})
					: s.resolve(),
				s.promise()
			);
		}
		var o =
			"function" == typeof Symbol && "symbol" == typeof Symbol.iterator
				? function (t) {
					return typeof t;
				}
				: function (t) {
					return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
				},
			a = n(3),
			l = i(a);
		n(68),
			(l["default"].fn.transition = function () {
				for (var t = [].concat(Array.prototype.slice.call(arguments)), e = t[t.length - 1], n = "function" == typeof e ? e : null, i = { before: [], transition: [], after: [], delay: 16 }, r = 0, a = t.length; r < a; r++) {
					var u = t[r];
					"string" == typeof u && (u = l["default"].transition.sequences[u]),
						u &&
						"object" === ("undefined" == typeof u ? "undefined" : o(u)) &&
						(u.before && i.before.push(u.before), u.transition && i.transition.push(u.transition), u.after && i.after.push(u.after), u.delay && (i.delay = Math.max(i.delay, u.delay)));
				}
				return l["default"].when.apply(l["default"], l["default"].map(this, s.bind(this, i))).done(n), this;
			}),
			(l["default"].transition = { sequences: {} });
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t) {
			if (t) {
				var e = parseFloat(t);
				if (e) {
					if ("ms" === t.substr(-2)) return e;
					if ("s" === t.substr(-1)) return 1e3 * e;
				}
			}
			return 0;
		}
		var s = n(3),
			o = i(s),
			a = "WebkitTransition" in document.body.style ? "webkitTransitionEnd" : "transitionend",
			l = "WebkitAnimation" in document.body.style ? "webkitAnimationEnd" : "animationend",
			u = 0;
		(o["default"].fn.transitionduration = function (t) {
			var e = r((0, o["default"])(this).css("transition-duration"));
			e && (e += r((0, o["default"])(this).css("transition-delay")));
			var n = r((0, o["default"])(this).css("transition-duration"));
			return n && (n += r((0, o["default"])(this).css("animation-delay"))), e || n || t || 0;
		}),
			(o["default"].fn.transitionend = function () {
				return o["default"].when.apply(
					o["default"],
					o["default"].map(this, function (t) {
						var e = (0, o["default"])(t),
							n = ++u,
							i = a + ".ns" + n + " " + l + ".ns" + n,
							r = o["default"].Deferred(),
							s = e.transitionduration(),
							c = setTimeout(function () {
								r.resolve();
							}, s + 16);
						return (
							e.on(i, function (t) {
								e.is(t.target) && (clearTimeout(c), e.off(i), r.resolve());
							}),
							r.promise()
						);
					})
				);
			});
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(65),
			u = i(l);
		n(67);
		var c = {},
			h = (function () {
				function t(e, n) {
					r(this, t);
					var i = ((this.options = a["default"].extend({}, c, n)), (this.$container = (0, a["default"])(e))),
						s = ((this.$scrollable = i.find(".js-mobile-scroller-content")), (this.$toggle = i.find(".js-mobile-toggle"))),
						o = (this.$navigation = i.find(".js-mobile"));
					this.$inner = i.find(".js-mobile-inner");
					(this.visible = !1), s.on("tap", this.toggle.bind(this)), o.on("tap", this.handleOverlayClick.bind(this));
				}
				return (
					s(t, [
						{
							key: "isVisible",
							value: function () {
								return this.visible;
							},
						},
						{
							key: "toggle",
							value: function () {
								this.visible ? this.hide() : this.show();
							},
						},
						{
							key: "handleOverlayClick",
							value: function (t) {
								(0, a["default"])(t.target).closest(this.$inner).length || this.toggle();
							},
						},
						{
							key: "show",
							value: function () {
								if (!this.visible) {
									this.visible = !0;
									var t = this.$container;
									t.headerFixed("update");
									var e = this.$navigation;
									e.transition({
										before: function () {
											return e.removeClass("is-hidden");
										},
										transition: function () {
											return e.addClass("nav-mobile--open");
										},
									}),
										(0, a["default"])("html").addClass("with-mobile-menu"),
										a["default"].scroller.setDisabled(!0);
								}
							},
						},
						{
							key: "hide",
							value: function () {
								if (this.visible) {
									this.visible = !1;
									var t = this.$container;
									t.headerFixed("update");
									var e = this.$navigation;
									e.transition({
										before: function () {
											return e.removeClass("nav-mobile--open");
										},
										transition: function () {
											return e.addClass("is-hidden");
										},
									}),
										(0, a["default"])("html").removeClass("with-mobile-menu"),
										a["default"].scroller.setDisabled(!1);
								}
							},
						},
					]),
					t
				);
			})();
		(e["default"] = h), (a["default"].fn.headerMobile = (0, u["default"])(h));
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(65),
			u = i(l),
			c = n(71),
			h = i(c),
			d = n(156),
			f = i(d),
			p = n(160),
			v = i(p);
		n(169);
		var g = n(170),
			m = i(g),
			y = "accordion",
			b = "tabs",
			x = (function () {
				function t(e, n) {
					r(this, t);
					var i = (this.options = a["default"].extend({}, this.constructor.Defaults, n)),
						s = (this.$container = (0, a["default"])(e)),
						o = (this.$headings = s.find(i.headingSelector)),
						l = (this.$contents = s.find(i.contentSelector));
					if (
						((this.active = (0, h["default"])(l.filter("." + this.options.activeClassName), function (t) {
							return l.index(t);
						})),
							document.location.hash)
					) {
						var u = l.filter(document.location.hash);
						u.length && this.expand(l.index(u), !0);
					}
					o.on("tap", this.handleHeadingClick.bind(this));
				}
				return (
					s(t, null, [
						{
							key: "Defaults",
							get: function () {
								return { mode: y, transition: "height", modeMdDown: null, transitionMdDown: null, activeClassName: "is-active", headingSelector: ".js-accordion-heading", contentSelector: ".js-accordion-content" };
							},
						},
					]),
					s(t, [
						{
							key: "getCurrentMode",
							value: function () {
								var t = this.options;
								return t.modeMdDown && m["default"].matches("md-down") ? t.modeMdDown : t.mode;
							},
						},
						{
							key: "getCurrentTransition",
							value: function () {
								var t = this.options;
								return t.transitionMdDown && m["default"].matches("md-down") ? t.transitionMdDown : t.transition;
							},
						},
						{
							key: "handleHeadingClick",
							value: function (t) {
								var e = this.$headings,
									n = (0, a["default"])(t.target).closest(e),
									i = this.getIndexByHeading(n);
								this.toggle(i), t.preventDefault();
							},
						},
						{
							key: "getIndexByHeading",
							value: function (t) {
								var e = t.data("index");
								return e || 0 === e || (e = this.$headings.index(t)), e;
							},
						},
						{
							key: "getHeadingByIndex",
							value: function (t) {
								var e = this.$headings.filter('[data-index="' + t + '"]');
								return e.length || (e = this.$headings.eq(t)), e;
							},
						},
						{
							key: "toggle",
							value: function (t) {
								var e = this.active.indexOf(t) !== -1;
								e ? this.getCurrentMode() !== b && this.collapse(t) : this.expand(t);
							},
						},
						{
							key: "expand",
							value: function (t) {
								var e = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
									n = this.getHeadingByIndex(t),
									i = this.$contents.eq(t),
									r = this.options.activeClassName,
									s = this.getCurrentTransition();
								if (this.active.length && this.active.indexOf(t) !== -1) {
									if (e !== !0) return;
									this.hide(t);
								}
								this.active.length && (0, f["default"])(this.active, this.collapse.bind(this)),
									n.addClass(r),
									"height" === s
										? i.slideDown({
											duration: a["default"].durationNormal,
											easing: a["default"].easeOut,
											complete: function () {
												i.addClass(r).css("display", ""), i.trigger("resize");
											},
										})
										: "fade" === s
											? i.transition(
												{
													before: function (t) {
														return t.addClass("animation--fade-in animation--fade-in--inactive " + r);
													},
													transition: function (t) {
														return t.removeClass("animation--fade-in--inactive");
													},
													after: function (t) {
														return t.removeClass("animation--fade-in");
													},
												},
												{
													transition: function (t) {
														return t.trigger("resize");
													},
												}
											)
											: "fade-out" === s && (i.removeClass("is-hidden").addClass(r), i.trigger("resize")),
									this.expandContent(t),
									this.active.push(t);
							},
						},
						{
							key: "collapse",
							value: function (t) {
								var e = this.getHeadingByIndex(t),
									n = this.$contents.eq(t),
									i = this.options.activeClassName,
									r = this.getCurrentTransition();
								e.removeClass(i),
									"height" === r
										? n.slideUp({
											duration: a["default"].durationNormal,
											easing: a["default"].easeOut,
											complete: function () {
												n.removeClass(i).css("display", ""), n.trigger("resize");
											},
										})
										: ("fade" !== r && "fade-out" !== r) ||
										n.transition(
											{
												before: function (t) {
													return t.addClass("animation--fade-out cover");
												},
												transition: function (t) {
													return t.addClass("animation--fade-out--active");
												},
												after: function (t) {
													return t.removeClass("animation--fade-out animation--fade-out--active cover " + i);
												},
											},
											{
												after: function (t) {
													return t.trigger("resize");
												},
											}
										),
									(0, v["default"])(this.active, t);
							},
						},
						{
							key: "hide",
							value: function (t) {
								var e = this.getHeadingByIndex(t),
									n = this.$contents.eq(t),
									i = this.options.activeClassName;
								e.removeClass(i), n.removeClass(i), n.trigger("resize"), (0, v["default"])(this.active, t);
							},
						},
						{
							key: "expandContent",
							value: function (t) {
								var e = this.$contents.eq(t);
								e.find('[data-plugin*="appear"]').appear("reset");
							},
						},
					]),
					t
				);
			})();
		a["default"].fn.accordion = (0, u["default"])(x);
	},
	function (t, e, n) {
		function i(t, e) {
			var n = a(t) ? r : o;
			return n(t, s(e, 3));
		}
		var r = n(72),
			s = n(73),
			o = n(150),
			a = n(51);
		t.exports = i;
	},
	function (t, e) {
		function n(t, e) {
			for (var n = -1, i = null == t ? 0 : t.length, r = Array(i); ++n < i;) r[n] = e(t[n], n, t);
			return r;
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t) {
			return "function" == typeof t ? t : null == t ? o : "object" == typeof t ? (a(t) ? s(t[0], t[1]) : r(t)) : l(t);
		}
		var r = n(74),
			s = n(132),
			o = n(33),
			a = n(51),
			l = n(147);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			var e = s(t);
			return 1 == e.length && e[0][2]
				? o(e[0][0], e[0][1])
				: function (n) {
					return n === t || r(n, t, e);
				};
		}
		var r = n(75),
			s = n(129),
			o = n(131);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n, i) {
			var l = n.length,
				u = l,
				c = !i;
			if (null == t) return !u;
			for (t = Object(t); l--;) {
				var h = n[l];
				if (c && h[2] ? h[1] !== t[h[0]] : !(h[0] in t)) return !1;
			}
			for (; ++l < u;) {
				h = n[l];
				var d = h[0],
					f = t[d],
					p = h[1];
				if (c && h[2]) {
					if (void 0 === f && !(d in t)) return !1;
				} else {
					var v = new r();
					if (i) var g = i(f, p, d, t, e, v);
					if (!(void 0 === g ? s(p, f, o | a, i, v) : g)) return !1;
				}
			}
			return !0;
		}
		var r = n(76),
			s = n(105),
			o = 1,
			a = 2;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			var e = (this.__data__ = new r(t));
			this.size = e.size;
		}
		var r = n(77),
			s = n(84),
			o = n(85),
			a = n(86),
			l = n(87),
			u = n(88);
		(i.prototype.clear = s), (i.prototype["delete"] = o), (i.prototype.get = a), (i.prototype.has = l), (i.prototype.set = u), (t.exports = i);
	},
	function (t, e, n) {
		function i(t) {
			var e = -1,
				n = null == t ? 0 : t.length;
			for (this.clear(); ++e < n;) {
				var i = t[e];
				this.set(i[0], i[1]);
			}
		}
		var r = n(78),
			s = n(79),
			o = n(81),
			a = n(82),
			l = n(83);
		(i.prototype.clear = r), (i.prototype["delete"] = s), (i.prototype.get = o), (i.prototype.has = a), (i.prototype.set = l), (t.exports = i);
	},
	function (t, e) {
		function n() {
			(this.__data__ = []), (this.size = 0);
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t) {
			var e = this.__data__,
				n = r(e, t);
			if (n < 0) return !1;
			var i = e.length - 1;
			return n == i ? e.pop() : o.call(e, n, 1), --this.size, !0;
		}
		var r = n(80),
			s = Array.prototype,
			o = s.splice;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			for (var n = t.length; n--;) if (r(t[n][0], e)) return n;
			return -1;
		}
		var r = n(29);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			var e = this.__data__,
				n = r(e, t);
			return n < 0 ? void 0 : e[n][1];
		}
		var r = n(80);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return r(this.__data__, t) > -1;
		}
		var r = n(80);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			var n = this.__data__,
				i = r(n, t);
			return i < 0 ? (++this.size, n.push([t, e])) : (n[i][1] = e), this;
		}
		var r = n(80);
		t.exports = i;
	},
	function (t, e, n) {
		function i() {
			(this.__data__ = new r()), (this.size = 0);
		}
		var r = n(77);
		t.exports = i;
	},
	function (t, e) {
		function n(t) {
			var e = this.__data__,
				n = e["delete"](t);
			return (this.size = e.size), n;
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t) {
			return this.__data__.get(t);
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t) {
			return this.__data__.has(t);
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t, e) {
			var n = this.__data__;
			if (n instanceof r) {
				var i = n.__data__;
				if (!s || i.length < a - 1) return i.push([t, e]), (this.size = ++n.size), this;
				n = this.__data__ = new o(i);
			}
			return n.set(t, e), (this.size = n.size), this;
		}
		var r = n(77),
			s = n(89),
			o = n(90),
			a = 200;
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(15),
			r = n(20),
			s = i(r, "Map");
		t.exports = s;
	},
	function (t, e, n) {
		function i(t) {
			var e = -1,
				n = null == t ? 0 : t.length;
			for (this.clear(); ++e < n;) {
				var i = t[e];
				this.set(i[0], i[1]);
			}
		}
		var r = n(91),
			s = n(99),
			o = n(102),
			a = n(103),
			l = n(104);
		(i.prototype.clear = r), (i.prototype["delete"] = s), (i.prototype.get = o), (i.prototype.has = a), (i.prototype.set = l), (t.exports = i);
	},
	function (t, e, n) {
		function i() {
			(this.size = 0), (this.__data__ = { hash: new r(), map: new (o || s)(), string: new r() });
		}
		var r = n(92),
			s = n(77),
			o = n(89);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			var e = -1,
				n = null == t ? 0 : t.length;
			for (this.clear(); ++e < n;) {
				var i = t[e];
				this.set(i[0], i[1]);
			}
		}
		var r = n(93),
			s = n(95),
			o = n(96),
			a = n(97),
			l = n(98);
		(i.prototype.clear = r), (i.prototype["delete"] = s), (i.prototype.get = o), (i.prototype.has = a), (i.prototype.set = l), (t.exports = i);
	},
	function (t, e, n) {
		function i() {
			(this.__data__ = r ? r(null) : {}), (this.size = 0);
		}
		var r = n(94);
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(15),
			r = i(Object, "create");
		t.exports = r;
	},
	function (t, e) {
		function n(t) {
			var e = this.has(t) && delete this.__data__[t];
			return (this.size -= e ? 1 : 0), e;
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t) {
			var e = this.__data__;
			if (r) {
				var n = e[t];
				return n === s ? void 0 : n;
			}
			return a.call(e, t) ? e[t] : void 0;
		}
		var r = n(94),
			s = "__lodash_hash_undefined__",
			o = Object.prototype,
			a = o.hasOwnProperty;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			var e = this.__data__;
			return r ? void 0 !== e[t] : o.call(e, t);
		}
		var r = n(94),
			s = Object.prototype,
			o = s.hasOwnProperty;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			var n = this.__data__;
			return (this.size += this.has(t) ? 0 : 1), (n[t] = r && void 0 === e ? s : e), this;
		}
		var r = n(94),
			s = "__lodash_hash_undefined__";
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			var e = r(this, t)["delete"](t);
			return (this.size -= e ? 1 : 0), e;
		}
		var r = n(100);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			var n = t.__data__;
			return r(e) ? n["string" == typeof e ? "string" : "hash"] : n.map;
		}
		var r = n(101);
		t.exports = i;
	},
	function (t, e) {
		function n(t) {
			var e = typeof t;
			return "string" == e || "number" == e || "symbol" == e || "boolean" == e ? "__proto__" !== t : null === t;
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t) {
			return r(this, t).get(t);
		}
		var r = n(100);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return r(this, t).has(t);
		}
		var r = n(100);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			var n = r(this, t),
				i = n.size;
			return n.set(t, e), (this.size += n.size == i ? 0 : 1), this;
		}
		var r = n(100);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n, o, a) {
			return t === e || (null == t || null == e || (!s(t) && !s(e)) ? t !== t && e !== e : r(t, e, n, o, i, a));
		}
		var r = n(106),
			s = n(50);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n, i, g, y) {
			var b = u(t),
				x = u(e),
				w = b ? p : l(t),
				_ = x ? p : l(e);
			(w = w == f ? v : w), (_ = _ == f ? v : _);
			var C = w == v,
				k = _ == v,
				T = w == _;
			if (T && c(t)) {
				if (!c(e)) return !1;
				(b = !0), (C = !1);
			}
			if (T && !C) return y || (y = new r()), b || h(t) ? s(t, e, n, i, g, y) : o(t, e, w, n, i, g, y);
			if (!(n & d)) {
				var S = C && m.call(t, "__wrapped__"),
					E = k && m.call(e, "__wrapped__");
				if (S || E) {
					var j = S ? t.value() : t,
						O = E ? e.value() : e;
					return y || (y = new r()), g(j, O, n, i, y);
				}
			}
			return !!T && (y || (y = new r()), a(t, e, n, i, g, y));
		}
		var r = n(76),
			s = n(107),
			o = n(113),
			a = n(117),
			l = n(124),
			u = n(51),
			c = n(52),
			h = n(55),
			d = 1,
			f = "[object Arguments]",
			p = "[object Array]",
			v = "[object Object]",
			g = Object.prototype,
			m = g.hasOwnProperty;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n, i, u, c) {
			var h = n & a,
				d = t.length,
				f = e.length;
			if (d != f && !(h && f > d)) return !1;
			var p = c.get(t);
			if (p && c.get(e)) return p == e;
			var v = -1,
				g = !0,
				m = n & l ? new r() : void 0;
			for (c.set(t, e), c.set(e, t); ++v < d;) {
				var y = t[v],
					b = e[v];
				if (i) var x = h ? i(b, y, v, e, t, c) : i(y, b, v, t, e, c);
				if (void 0 !== x) {
					if (x) continue;
					g = !1;
					break;
				}
				if (m) {
					if (
						!s(e, function (t, e) {
							if (!o(m, e) && (y === t || u(y, t, n, i, c))) return m.push(e);
						})
					) {
						g = !1;
						break;
					}
				} else if (y !== b && !u(y, b, n, i, c)) {
					g = !1;
					break;
				}
			}
			return c["delete"](t), c["delete"](e), g;
		}
		var r = n(108),
			s = n(111),
			o = n(112),
			a = 1,
			l = 2;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			var e = -1,
				n = null == t ? 0 : t.length;
			for (this.__data__ = new r(); ++e < n;) this.add(t[e]);
		}
		var r = n(90),
			s = n(109),
			o = n(110);
		(i.prototype.add = i.prototype.push = s), (i.prototype.has = o), (t.exports = i);
	},
	function (t, e) {
		function n(t) {
			return this.__data__.set(t, i), this;
		}
		var i = "__lodash_hash_undefined__";
		t.exports = n;
	},
	function (t, e) {
		function n(t) {
			return this.__data__.has(t);
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t, e) {
			for (var n = -1, i = null == t ? 0 : t.length; ++n < i;) if (e(t[n], n, t)) return !0;
			return !1;
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t, e) {
			return t.has(e);
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t, e, n, i, r, C, T) {
			switch (n) {
				case _:
					if (t.byteLength != e.byteLength || t.byteOffset != e.byteOffset) return !1;
					(t = t.buffer), (e = e.buffer);
				case w:
					return !(t.byteLength != e.byteLength || !C(new s(t), new s(e)));
				case d:
				case f:
				case g:
					return o(+t, +e);
				case p:
					return t.name == e.name && t.message == e.message;
				case m:
				case b:
					return t == e + "";
				case v:
					var S = l;
				case y:
					var E = i & c;
					if ((S || (S = u), t.size != e.size && !E)) return !1;
					var j = T.get(t);
					if (j) return j == e;
					(i |= h), T.set(t, e);
					var O = a(S(t), S(e), i, r, C, T);
					return T["delete"](t), O;
				case x:
					if (k) return k.call(t) == k.call(e);
			}
			return !1;
		}
		var r = n(19),
			s = n(114),
			o = n(29),
			a = n(107),
			l = n(115),
			u = n(116),
			c = 1,
			h = 2,
			d = "[object Boolean]",
			f = "[object Date]",
			p = "[object Error]",
			v = "[object Map]",
			g = "[object Number]",
			m = "[object RegExp]",
			y = "[object Set]",
			b = "[object String]",
			x = "[object Symbol]",
			w = "[object ArrayBuffer]",
			_ = "[object DataView]",
			C = r ? r.prototype : void 0,
			k = C ? C.valueOf : void 0;
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(20),
			r = i.Uint8Array;
		t.exports = r;
	},
	function (t, e) {
		function n(t) {
			var e = -1,
				n = Array(t.size);
			return (
				t.forEach(function (t, i) {
					n[++e] = [i, t];
				}),
				n
			);
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t) {
			var e = -1,
				n = Array(t.size);
			return (
				t.forEach(function (t) {
					n[++e] = t;
				}),
				n
			);
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t, e, n, i, o, l) {
			var u = n & s,
				c = r(t),
				h = c.length,
				d = r(e),
				f = d.length;
			if (h != f && !u) return !1;
			for (var p = h; p--;) {
				var v = c[p];
				if (!(u ? v in e : a.call(e, v))) return !1;
			}
			var g = l.get(t);
			if (g && l.get(e)) return g == e;
			var m = !0;
			l.set(t, e), l.set(e, t);
			for (var y = u; ++p < h;) {
				v = c[p];
				var b = t[v],
					x = e[v];
				if (i) var w = u ? i(x, b, v, e, t, l) : i(b, x, v, t, e, l);
				if (!(void 0 === w ? b === x || o(b, x, n, i, l) : w)) {
					m = !1;
					break;
				}
				y || (y = "constructor" == v);
			}
			if (m && !y) {
				var _ = t.constructor,
					C = e.constructor;
				_ != C && "constructor" in t && "constructor" in e && !("function" == typeof _ && _ instanceof _ && "function" == typeof C && C instanceof C) && (m = !1);
			}
			return l["delete"](t), l["delete"](e), m;
		}
		var r = n(118),
			s = 1,
			o = Object.prototype,
			a = o.hasOwnProperty;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return r(t, o, s);
		}
		var r = n(119),
			s = n(121),
			o = n(45);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n) {
			var i = e(t);
			return s(t) ? i : r(i, n(t));
		}
		var r = n(120),
			s = n(51);
		t.exports = i;
	},
	function (t, e) {
		function n(t, e) {
			for (var n = -1, i = e.length, r = t.length; ++n < i;) t[r + n] = e[n];
			return t;
		}
		t.exports = n;
	},
	function (t, e, n) {
		var i = n(122),
			r = n(123),
			s = Object.prototype,
			o = s.propertyIsEnumerable,
			a = Object.getOwnPropertySymbols,
			l = a
				? function (t) {
					return null == t
						? []
						: ((t = Object(t)),
							i(a(t), function (e) {
								return o.call(t, e);
							}));
				}
				: r;
		t.exports = l;
	},
	function (t, e) {
		function n(t, e) {
			for (var n = -1, i = null == t ? 0 : t.length, r = 0, s = []; ++n < i;) {
				var o = t[n];
				e(o, n, t) && (s[r++] = o);
			}
			return s;
		}
		t.exports = n;
	},
	function (t, e) {
		function n() {
			return [];
		}
		t.exports = n;
	},
	function (t, e, n) {
		var i = n(125),
			r = n(89),
			s = n(126),
			o = n(127),
			a = n(128),
			l = n(18),
			u = n(27),
			c = "[object Map]",
			h = "[object Object]",
			d = "[object Promise]",
			f = "[object Set]",
			p = "[object WeakMap]",
			v = "[object DataView]",
			g = u(i),
			m = u(r),
			y = u(s),
			b = u(o),
			x = u(a),
			w = l;
		((i && w(new i(new ArrayBuffer(1))) != v) || (r && w(new r()) != c) || (s && w(s.resolve()) != d) || (o && w(new o()) != f) || (a && w(new a()) != p)) &&
			(w = function (t) {
				var e = l(t),
					n = e == h ? t.constructor : void 0,
					i = n ? u(n) : "";
				if (i)
					switch (i) {
						case g:
							return v;
						case m:
							return c;
						case y:
							return d;
						case b:
							return f;
						case x:
							return p;
					}
				return e;
			}),
			(t.exports = w);
	},
	function (t, e, n) {
		var i = n(15),
			r = n(20),
			s = i(r, "DataView");
		t.exports = s;
	},
	function (t, e, n) {
		var i = n(15),
			r = n(20),
			s = i(r, "Promise");
		t.exports = s;
	},
	function (t, e, n) {
		var i = n(15),
			r = n(20),
			s = i(r, "Set");
		t.exports = s;
	},
	function (t, e, n) {
		var i = n(15),
			r = n(20),
			s = i(r, "WeakMap");
		t.exports = s;
	},
	function (t, e, n) {
		function i(t) {
			for (var e = s(t), n = e.length; n--;) {
				var i = e[n],
					o = t[i];
				e[n] = [i, o, r(o)];
			}
			return e;
		}
		var r = n(130),
			s = n(45);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return t === t && !r(t);
		}
		var r = n(24);
		t.exports = i;
	},
	function (t, e) {
		function n(t, e) {
			return function (n) {
				return null != n && n[t] === e && (void 0 !== e || t in Object(n));
			};
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t, e) {
			return a(t) && l(e)
				? u(c(t), e)
				: function (n) {
					var i = s(n, t);
					return void 0 === i && i === e ? o(n, t) : r(e, i, h | d);
				};
		}
		var r = n(105),
			s = n(133),
			o = n(144),
			a = n(136),
			l = n(130),
			u = n(131),
			c = n(143),
			h = 1,
			d = 2;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n) {
			var i = null == t ? void 0 : r(t, e);
			return void 0 === i ? n : i;
		}
		var r = n(134);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			e = r(e, t);
			for (var n = 0, i = e.length; null != t && n < i;) t = t[s(e[n++])];
			return n && n == i ? t : void 0;
		}
		var r = n(135),
			s = n(143);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			return r(t) ? t : s(t, e) ? [t] : o(a(t));
		}
		var r = n(51),
			s = n(136),
			o = n(138),
			a = n(141);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			if (r(t)) return !1;
			var n = typeof t;
			return !("number" != n && "symbol" != n && "boolean" != n && null != t && !s(t)) || a.test(t) || !o.test(t) || (null != e && t in Object(e));
		}
		var r = n(51),
			s = n(137),
			o = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
			a = /^\w*$/;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return "symbol" == typeof t || (s(t) && r(t) == o);
		}
		var r = n(18),
			s = n(50),
			o = "[object Symbol]";
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(139),
			r = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,
			s = /\\(\\)?/g,
			o = i(function (t) {
				var e = [];
				return (
					46 === t.charCodeAt(0) && e.push(""),
					t.replace(r, function (t, n, i, r) {
						e.push(i ? r.replace(s, "$1") : n || t);
					}),
					e
				);
			});
		t.exports = o;
	},
	function (t, e, n) {
		function i(t) {
			var e = r(t, function (t) {
				return n.size === s && n.clear(), t;
			}),
				n = e.cache;
			return e;
		}
		var r = n(140),
			s = 500;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			if ("function" != typeof t || (null != e && "function" != typeof e)) throw new TypeError(s);
			var n = function () {
				var i = arguments,
					r = e ? e.apply(this, i) : i[0],
					s = n.cache;
				if (s.has(r)) return s.get(r);
				var o = t.apply(this, i);
				return (n.cache = s.set(r, o) || s), o;
			};
			return (n.cache = new (i.Cache || r)()), n;
		}
		var r = n(90),
			s = "Expected a function";
		(i.Cache = r), (t.exports = i);
	},
	function (t, e, n) {
		function i(t) {
			return null == t ? "" : r(t);
		}
		var r = n(142);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			if ("string" == typeof t) return t;
			if (o(t)) return s(t, i) + "";
			if (a(t)) return c ? c.call(t) : "";
			var e = t + "";
			return "0" == e && 1 / t == -l ? "-0" : e;
		}
		var r = n(19),
			s = n(72),
			o = n(51),
			a = n(137),
			l = 1 / 0,
			u = r ? r.prototype : void 0,
			c = u ? u.toString : void 0;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			if ("string" == typeof t || r(t)) return t;
			var e = t + "";
			return "0" == e && 1 / t == -s ? "-0" : e;
		}
		var r = n(137),
			s = 1 / 0;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			return null != t && s(t, e, r);
		}
		var r = n(145),
			s = n(146);
		t.exports = i;
	},
	function (t, e) {
		function n(t, e) {
			return null != t && e in Object(t);
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t, e, n) {
			e = r(e, t);
			for (var i = -1, c = e.length, h = !1; ++i < c;) {
				var d = u(e[i]);
				if (!(h = null != t && n(t, d))) break;
				t = t[d];
			}
			return h || ++i != c ? h : ((c = null == t ? 0 : t.length), !!c && l(c) && a(d, c) && (o(t) || s(t)));
		}
		var r = n(135),
			s = n(48),
			o = n(51),
			a = n(43),
			l = n(42),
			u = n(143);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return o(t) ? r(a(t)) : s(t);
		}
		var r = n(148),
			s = n(149),
			o = n(136),
			a = n(143);
		t.exports = i;
	},
	function (t, e) {
		function n(t) {
			return function (e) {
				return null == e ? void 0 : e[t];
			};
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t) {
			return function (e) {
				return r(e, t);
			};
		}
		var r = n(134);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			var n = -1,
				i = s(t) ? Array(t.length) : [];
			return (
				r(t, function (t, r, s) {
					i[++n] = e(t, r, s);
				}),
				i
			);
		}
		var r = n(151),
			s = n(41);
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(152),
			r = n(155),
			s = r(i);
		t.exports = s;
	},
	function (t, e, n) {
		function i(t, e) {
			return t && r(t, e, s);
		}
		var r = n(153),
			s = n(45);
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(154),
			r = i();
		t.exports = r;
	},
	function (t, e) {
		function n(t) {
			return function (e, n, i) {
				for (var r = -1, s = Object(e), o = i(e), a = o.length; a--;) {
					var l = o[t ? a : ++r];
					if (n(s[l], l, s) === !1) break;
				}
				return e;
			};
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t, e) {
			return function (n, i) {
				if (null == n) return n;
				if (!r(n)) return t(n, i);
				for (var s = n.length, o = e ? s : -1, a = Object(n); (e ? o-- : ++o < s) && i(a[o], o, a) !== !1;);
				return n;
			};
		}
		var r = n(41);
		t.exports = i;
	},
	function (t, e, n) {
		t.exports = n(157);
	},
	function (t, e, n) {
		function i(t, e) {
			var n = a(t) ? r : s;
			return n(t, o(e));
		}
		var r = n(158),
			s = n(151),
			o = n(159),
			a = n(51);
		t.exports = i;
	},
	function (t, e) {
		function n(t, e) {
			for (var n = -1, i = null == t ? 0 : t.length; ++n < i && e(t[n], n, t) !== !1;);
			return t;
		}
		t.exports = n;
	},
	function (t, e, n) {
		function i(t) {
			return "function" == typeof t ? t : r;
		}
		var r = n(33);
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(32),
			r = n(161),
			s = i(r);
		t.exports = s;
	},
	function (t, e, n) {
		function i(t, e) {
			return t && t.length && e && e.length ? r(t, e) : t;
		}
		var r = n(162);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n, i) {
			var u = i ? o : s,
				h = -1,
				d = e.length,
				f = t;
			for (t === e && (e = l(e)), n && (f = r(t, a(n))); ++h < d;) for (var p = 0, v = e[h], g = n ? n(v) : v; (p = u(f, g, p, i)) > -1;) f !== t && c.call(f, p, 1), c.call(t, p, 1);
			return t;
		}
		var r = n(72),
			s = n(163),
			o = n(167),
			a = n(57),
			l = n(168),
			u = Array.prototype,
			c = u.splice;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n) {
			return e === e ? o(t, e, n) : r(t, s, n);
		}
		var r = n(164),
			s = n(165),
			o = n(166);
		t.exports = i;
	},
	function (t, e) {
		function n(t, e, n, i) {
			for (var r = t.length, s = n + (i ? 1 : -1); i ? s-- : ++s < r;) if (e(t[s], s, t)) return s;
			return -1;
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t) {
			return t !== t;
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t, e, n) {
			for (var i = n - 1, r = t.length; ++i < r;) if (t[i] === e) return i;
			return -1;
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t, e, n, i) {
			for (var r = n - 1, s = t.length; ++r < s;) if (i(t[r], e)) return r;
			return -1;
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t, e) {
			var n = -1,
				i = t.length;
			for (e || (e = Array(i)); ++n < i;) e[n] = t[n];
			return e;
		}
		t.exports = n;
	},
	function (t, e, n) {
		(function (t) {
			"use strict";
			n(67),
				(t.transition.generateSequence = function (e) {
					(t.transition.sequences[e + "-in"] = {
						before: function (t) {
							t.removeClass("is-hidden").addClass("animation--" + e + "-in animation--" + e + "-in--inactive");
						},
						transition: function (t) {
							t.removeClass("animation--" + e + "-in--inactive");
						},
						after: function (t) {
							t.removeClass("animation--" + e + "-in");
						},
					}),
						(t.transition.sequences[e + "-out"] = {
							before: function (t) {
								t.addClass("animation--" + e + "-out");
							},
							transition: function (t) {
								t.addClass("animation--" + e + "-out--active");
							},
							after: function (t) {
								t.removeClass("animation--" + e + "-out animation--" + e + "-out--active").addClass("is-hidden");
							},
						});
				}),
				t.transition.generateSequence("fade");
		}.call(e, n(3)));
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t) {
			var e = null;
			return (
				(0, d["default"])(document.styleSheets, function (n) {
					return (0, d["default"])(n.rules || n.cssRules, function (n) {
						var i = n.cssText.indexOf(t);
						if (i !== -1) {
							var r = n.cssText[i + t.length];
							if (r in { " ": 1, "{": 1, ",": 1, "\n": 1 }) {
								if ((4 !== n.type && (n = n.parentRule), n.media && n.media.length && n.media[0])) {
									e = n.media[0];
									for (var s = 1; s < n.media.length; s++) n.media[s] && (e += ", " + n.media[s]);
								} else n.media && n.media.mediaText && (e = n.media.mediaText);
								return !0;
							}
						}
					});
				}),
				e
			);
		}
		function s(t) {
			var e = p[t];
			return null === e ? null : (e || ((e = p[t] = r(f + t)), null !== e ? (e = p[t] = matchMedia(e)) : o(t) && (e = p[t] = matchMedia(t))), e);
		}
		function o(t) {
			var e = matchMedia(t);
			return !(!e || "not all" === e.media);
		}
		function a(t, e) {
			var n = s(t);
			n && n.addListener(e);
		}
		function l(t, e) {
			a(t, function (t) {
				t.matches && e.call(this, t);
			});
			var n = s(t);
			n && n.matches && e.call(n, n);
		}
		function u(t, e) {
			a(t, function (t) {
				t.matches || e.call(this, t);
			});
			var n = s(t);
			n && !n.matches && e.call(n, n);
		}
		function c(t) {
			var e = s(t);
			return !!e && e.matches;
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var h = n(171),
			d = i(h),
			f = ".is-hidden--",
			p = {};
		e["default"] = { on: a, enter: l, leave: u, matches: c };
	},
	function (t, e, n) {
		var i = n(172),
			r = n(173),
			s = i(r);
		t.exports = s;
	},
	function (t, e, n) {
		function i(t) {
			return function (e, n, i) {
				var a = Object(e);
				if (!s(e)) {
					var l = r(n, 3);
					(e = o(e)),
						(n = function (t) {
							return l(a[t], t, a);
						});
				}
				var u = t(e, n, i);
				return u > -1 ? a[l ? e[u] : u] : void 0;
			};
		}
		var r = n(73),
			s = n(41),
			o = n(45);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n) {
			var i = null == t ? 0 : t.length;
			if (!i) return -1;
			var l = null == n ? 0 : o(n);
			return l < 0 && (l = a(i + l, 0)), r(t, s(e, 3), l);
		}
		var r = n(164),
			s = n(73),
			o = n(174),
			a = Math.max;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			var e = r(t),
				n = e % 1;
			return e === e ? (n ? e - n : e) : 0;
		}
		var r = n(175);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			if (!t) return 0 === t ? t : 0;
			if (((t = r(t)), t === s || t === -s)) {
				var e = t < 0 ? -1 : 1;
				return e * o;
			}
			return t === t ? t : 0;
		}
		var r = n(176),
			s = 1 / 0,
			o = 1.7976931348623157e308;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			if ("number" == typeof t) return t;
			if (s(t)) return o;
			if (r(t)) {
				var e = "function" == typeof t.valueOf ? t.valueOf() : t;
				t = r(e) ? e + "" : e;
			}
			if ("string" != typeof t) return 0 === t ? t : +t;
			t = t.replace(a, "");
			var n = u.test(t);
			return n || c.test(t) ? h(t.slice(2), n ? 2 : 8) : l.test(t) ? o : +t;
		}
		var r = n(24),
			s = n(137),
			o = NaN,
			a = /^\s+|\s+$/g,
			l = /^[-+]0x[0-9a-f]+$/i,
			u = /^0b[01]+$/i,
			c = /^0o[0-7]+$/i,
			h = parseInt;
		t.exports = i;
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var s = (function () {
			function t(t, e) {
				var n = [],
					i = !0,
					r = !1,
					s = void 0;
				try {
					for (var o, a = t[Symbol.iterator](); !(i = (o = a.next()).done) && (n.push(o.value), !e || n.length !== e); i = !0);
				} catch (l) {
					(r = !0), (s = l);
				} finally {
					try {
						!i && a["return"] && a["return"]();
					} finally {
						if (r) throw s;
					}
				}
				return n;
			}
			return function (e, n) {
				if (Array.isArray(e)) return e;
				if (Symbol.iterator in Object(e)) return t(e, n);
				throw new TypeError("Invalid attempt to destructure non-iterable instance");
			};
		})(),
			o = (function () {
				function t(t, e) {
					for (var n = 0; n < e.length; n++) {
						var i = e[n];
						(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
					}
				}
				return function (e, n, i) {
					return n && t(e.prototype, n), i && t(e, i), e;
				};
			})(),
			a = n(3),
			l = i(a),
			u = n(65),
			c = i(u),
			h = n(178),
			d = i(h),
			f = n(7),
			p = i(f),
			v = n(180),
			g = i(v);
		n(181);
		var m = p["default"].hasHoverSupport(),
			y = (function () {
				function t(e) {
					r(this, t),
						(this.custom = m),
						(this.$container = e),
						(this.$content = e.find(".js-main-scroller-content")),
						(this.contentHeight = 0),
						(this.viewportHeight = 0),
						(this.disabled = !1),
						(this.ready = !1),
						(this.listeners = { resize: [], scroll: [] }),
						(this.scroll = new g["default"]({ force: 0.1, precision: 0.25 })),
						this.scroll.on("step", this.update.bind(this)),
						(this.handleResizeDebounced = (0, d["default"])(this.handleResize.bind(this), 60)),
						(0, l["default"])(window).on("resize", this.handleResizeDebounced),
						(0, l["default"])(window).on("scroll", this.handleActiveScroll.bind(this)),
						this.custom ? (0, l["default"])(window).onpassive("scroll", this.handleScroll.bind(this)) : (0, l["default"])("main").onpassive("scroll", this.handleScroll.bind(this)),
						e.find("img").on("load", this.handleResizeDebounced),
						e.on("appear", this.handleResizeDebounced),
						e.on("click", 'a[href*="#"]', this.handleHashLinkClick.bind(this)),
						this.handleResize(),
						document.location.hash ? this.scrollToElement((0, l["default"])(document.location.hash)) : this.custom && this.scroll.to((0, l["default"])(window).scrollTop()),
						this.custom ? (0, l["default"])("html").addClass("with-scroller") : (0, l["default"])("html").addClass("has-native-scroll"),
						(this.ready = !0),
						(l["default"].scroller = this);
				}
				return (
					o(t, [
						{
							key: "setScrollableContent",
							value: function (t, e) {
								(this.$content = t), this.handleResize(), this.handleScroll(), this.scrollTo(0), this.scroll.reset(0);
							},
						},
						{
							key: "setDisabled",
							value: function (t) {
								this.disabled = !!t;
							},
						},
						{
							key: "on",
							value: function (t, e) {
								var n = this.listeners,
									i = t.split("."),
									r = s(i, 2),
									o = r[0],
									a = r[1];
								(n[o] = n[o] || []),
									n[o].push({
										callback: e,
										namespace: a,
									});
							},
						},
						{
							key: "off",
							value: function (t, e) {
								for (var n = t.split("."), i = s(n, 2), r = i[0], o = i[1], a = this.listeners[r] || [], l = 0; l < a.length; l++)
									e ? a[l].callback === e && ((o && a[l].namespace !== o) || (a.splice(l, 1), l--)) : o && a[l].namespace === o && (a.splice(l, 1), l--);
							},
						},
						{
							key: "trigger",
							value: function (t) {
								for (
									var e = this.listeners[t] || [], n = { contentHeight: this.contentHeight, viewportHeight: this.viewportHeight, scrollTop: this.custom ? this.scroll.value : (0, l["default"])("main").scrollTop() }, i = 0;
									i < e.length;
									i++
								)
									e[i].callback(n);
							},
						},
						{
							key: "onpassive",
							value: function (t, e) {
								this.on(t, e);
							},
						},
						{
							key: "offpassive",
							value: function (t, e) {
								this.off(t, e);
							},
						},
						{
							key: "getViewOffset",
							value: function (t) {
								var e = (0, l["default"])(t).get(0).getBoundingClientRect(),
									n = this.$content.get(0).getBoundingClientRect();
								return { left: e.left - n.left, top: e.top - n.top };
							},
						},
						{
							key: "handleHashLinkClick",
							value: function (t) {
								var e = (0, l["default"])(t.target).closest("a").attr("href").replace(/.*#/, "");
								if (e) {
									var n = null;
									try {
										n = (0, l["default"])("#" + e);
									} catch (i) { }
									n && n.length && (t.preventDefault(), this.scrollToElement(n));
								}
							},
						},
						{
							key: "scrollTo",
							value: function (t) {
								var e = Math.max(this.contentHeight - this.viewportHeight, 0);
								if (this.custom) {
									var n = Math.min(Math.max(t, 0), e);
									(0, l["default"])(window).scrollTop(n), this.ready || this.handleScroll();
								} else (0, l["default"])("main").animate({ scrollTop: t }, l["default"].durationSlow, l["default"].easeInOut);
							},
						},
						{
							key: "scrollToElement",
							value: function (t) {
								if (t.length) {
									var e = this.getViewOffset(t).top;
									this.scrollTo(e);
								}
							},
						},
						{
							key: "scrollIntoView",
							value: function (t) {
								var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
								if (t.length) {
									var n = this.scrollTop(),
										i = this.viewportHeight,
										r = this.getViewOffset(t).top,
										s = t.outerHeight();
									n + i < r + s ? this.scrollTo(r + s - i + e) : n > r && this.scrollTo(r - e);
								}
							},
						},
						{
							key: "scrollTop",
							value: function () {
								return this.custom ? this.scroll.value : (0, l["default"])("main").scrollTop();
							},
						},
						{
							key: "handleResize",
							value: function () {
								var t = this.$content.height(),
									e = (0, l["default"])(window).height(),
									n = !1;
								t !== this.contentHeight && ((this.contentHeight = t), (n = !0), this.custom && this.$container.height(this.contentHeight)),
									e !== this.viewportHeight && ((this.viewportHeight = e), (n = !0)),
									n && this.trigger("resize");
							},
						},
						{
							key: "handleScroll",
							value: function () {
								var t = (0, l["default"])(window).scrollTop();
								this.custom ? this.disabled || this.scroll.to(t) : this.trigger("scroll");
							},
						},
						{
							key: "handleActiveScroll",
							value: function (t) {
								this.custom && this.disabled && t.preventDefault();
							},
						},
						{
							key: "update",
							value: function (t) {
								this.$content.css("transform", "translateY(" + -t + "px)"), this.trigger("scroll");
							},
						},
						{
							key: "resized",
							value: function () {
								this.handleResizeDebounced();
							},
						},
					]),
					t
				);
			})();
		(e["default"] = y), (l["default"].fn.scroller = (0, c["default"])(y));
	},
	function (t, e, n) {
		function i(t, e, n) {
			function i(e) {
				var n = y,
					i = b;
				return (y = b = void 0), (k = e), (w = t.apply(i, n));
			}
			function c(t) {
				return (k = t), (_ = setTimeout(f, e)), T ? i(t) : w;
			}
			function h(t) {
				var n = t - C,
					i = t - k,
					r = e - n;
				return S ? u(r, x - i) : r;
			}
			function d(t) {
				var n = t - C,
					i = t - k;
				return void 0 === C || n >= e || n < 0 || (S && i >= x);
			}
			function f() {
				var t = s();
				return d(t) ? p(t) : void (_ = setTimeout(f, h(t)));
			}
			function p(t) {
				return (_ = void 0), E && y ? i(t) : ((y = b = void 0), w);
			}
			function v() {
				void 0 !== _ && clearTimeout(_), (k = 0), (y = C = b = _ = void 0);
			}
			function g() {
				return void 0 === _ ? w : p(s());
			}
			function m() {
				var t = s(),
					n = d(t);
				if (((y = arguments), (b = this), (C = t), n)) {
					if (void 0 === _) return c(C);
					if (S) return clearTimeout(_), (_ = setTimeout(f, e)), i(C);
				}
				return void 0 === _ && (_ = setTimeout(f, e)), w;
			}
			var y,
				b,
				x,
				w,
				_,
				C,
				k = 0,
				T = !1,
				S = !1,
				E = !0;
			if ("function" != typeof t) throw new TypeError(a);
			return (e = o(e) || 0), r(n) && ((T = !!n.leading), (S = "maxWait" in n), (x = S ? l(o(n.maxWait) || 0, e) : x), (E = "trailing" in n ? !!n.trailing : E)), (m.cancel = v), (m.flush = g), m;
		}
		var r = n(24),
			s = n(179),
			o = n(176),
			a = "Expected a function",
			l = Math.max,
			u = Math.min;
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(20),
			r = function () {
				return i.Date.now();
			};
		t.exports = r;
	},
	function (t, e) {
        /*!
         * ease-value <https://github.com/kasparsz/ease-value>
         *
         * Copyright (c) 2017, Kaspars Zuks.
         * Licensed under the MIT License.
         */
		var n = (function () {
			return "undefined" != typeof performance ? performance : Date;
		})(),
			i = function () {
				this.listeners = {};
			};
		(i.prototype.on = function (t, e) {
			var n = this.listeners;
			"function" == typeof e && ((n[t] = n[t] || []), n[t].push(e));
		}),
			(i.prototype.off = function (t, e) {
				var n = this.listeners[t],
					i = n ? n.indexOf(e) : -1;
				i !== -1 && n.splice(i, 1);
			}),
			(i.prototype.trigger = function (t, e) {
				for (var n = this.listeners[t], i = 0, r = n ? n.length : 0; i < r; i++) n[i](e);
			});
		var r = (function (t) {
			function e(e) {
				void 0 === e && (e = {}), t.call(this);
				var n = (this.options = Object.assign({}, this.constructor.Defaults, e));
				(this.value = null),
					(this.valueRaw = null),
					(this.valueInitial = null),
					(this.valueTarget = null),
					(this.hasInitialValueSet = !1),
					(this.isRunning = !1),
					(this.time = null),
					(this.timer = null),
					(this.stepBinded = this.step.bind(this)),
					n.step && this.on("step", n.step),
					n.start && this.on("start", n.start),
					n.stop && this.on("stop", n.stop),
					null !== n.value && this.to(n.value);
			}
			t && (e.__proto__ = t), (e.prototype = Object.create(t && t.prototype)), (e.prototype.constructor = e);
			var i = { Defaults: {} };
			return (
				(i.Defaults.get = function () {
					return { value: null, force: e.defaultForce, precision: e.defaultPrecision, easing: e.defaultEasing };
				}),
				(e.prototype.destroy = function () {
					(this.listeners = this.options = {}), this.timer && cancelAnimationFrame(this.timer);
				}),
				(e.prototype.to = function (t) {
					this.hasInitialValueSet ? ((this.valueInitial = this.value), (this.valueTarget = t), this.isRunning || ((this.time = n.now()), this.trigger("start", this.value), this.step())) : this.reset(t);
				}),
				(e.prototype.reset = function (t) {
					var e = this.options.precision;
					(t === this.valueRaw && t === this.valueTarget) ||
						((this.valueRaw = this.valueInitial = this.valueTarget = t),
							(this.value = Math.round(t / e) * e),
							(this.hasInitialValueSet = !0),
							(this.time = n.now()),
							this.trigger("start", this.value),
							this.trigger("step", this.value),
							this.trigger("stop", this.value));
				}),
				(e.prototype.step = function () {
					var t,
						i = this.options.precision,
						r = e.easings[this.options.easing],
						s = !this.isRunning;
					if (((this.isRunning = t = !0), this.hasInitialValueSet)) {
						var o = this.valueTarget,
							a = this.value,
							l = n.now(),
							u = l - this.time,
							c = r.call(this, this, u),
							h = Math.abs(o - c) < i;
						(this.valueRaw = h ? o : c), (this.value = Math.round(this.valueRaw / i) * i), (this.time = l);
						var d = a - this.value;
						(d || s) && this.trigger("step", this.value), h && ((this.isRunning = t = !1), this.trigger("stop", this.value));
					}
					t && (this.timer = requestAnimationFrame(this.stepBinded));
				}),
				Object.defineProperties(e, i),
				e
			);
		})(i),
			s = (function (t) {
				function e(e) {
					var n = this;
					t.call(this),
						(this.easeValues = e),
						(this.keys = Object.keys(e)),
						(this.value = this.getValue()),
						(this.isRunning = this.getIsRunning()),
						(this.reqStart = this.reqStop = this.reqStep = null),
						(this.triggerStart = this.triggerStart.bind(this)),
						(this.triggerStop = this.triggerStop.bind(this)),
						(this.triggerStep = this.triggerStep.bind(this)),
						this.keys.forEach(function (t) {
							e[t].on("start", n.handleStart.bind(n)), e[t].on("stop", n.handleStop.bind(n)), e[t].on("step", n.handleStep.bind(n)), n[t] || (n[t] = e[t]);
						});
				}
				return (
					t && (e.__proto__ = t),
					(e.prototype = Object.create(t && t.prototype)),
					(e.prototype.constructor = e),
					(e.prototype.to = function (t) {
						var e = this.easeValues;
						this.keys.forEach(function (n) {
							n in t && e[n].to(t[n]);
						});
					}),
					(e.prototype.reset = function (t) {
						var e = this.easeValues;
						this.keys.forEach(function (n) {
							n in t && e[n].reset(t[n]);
						});
					}),
					(e.prototype.getIsRunning = function () {
						var t = this.easeValues,
							e = this.keys.filter(function (e) {
								return t[e].isRunning;
							});
						return e.length > 0;
					}),
					(e.prototype.getValue = function () {
						var t = this.easeValues,
							e = {};
						return (
							this.keys.forEach(function (n) {
								e[n] = t[n].value;
							}),
							e
						);
					}),
					(e.prototype.triggerStart = function () {
						(this.reqStart = null), this.trigger("start", this.value);
					}),
					(e.prototype.triggerStop = function () {
						(this.reqStop = null), this.trigger("stop", this.value);
					}),
					(e.prototype.triggerStep = function () {
						(this.reqStep = null), this.trigger("step", this.value);
					}),
					(e.prototype.handleStart = function () {
						this.isRunning || (this.reqStart && cancelAnimationFrame(this.reqStart), (this.value = this.getValue()), (this.isRunning = this.getIsRunning()), (this.reqStart = requestAnimationFrame(this.triggerStart)));
					}),
					(e.prototype.handleStop = function () {
						(this.isRunning = this.getIsRunning()), this.isRunning || (this.reqStop && cancelAnimationFrame(this.reqStop), (this.value = this.getValue()), (this.reqStop = requestAnimationFrame(this.triggerStop)));
					}),
					(e.prototype.handleStep = function () {
						(this.value = this.getValue()), this.reqStep || (this.reqStep = requestAnimationFrame(this.triggerStep));
					}),
					e
				);
			})(i);
		(r.Events = i),
			(r.EaseValueMultiple = s),
			(r.multiple = function (t) {
				return new s(t);
			}),
			(r.defaultForce = 0.1),
			(r.defaultPrecision = 0.01),
			(r.defaultEasing = "easeOut"),
			(r.easings = {
				easeOut: function (t, e) {
					var n = t.valueTarget - t.valueRaw,
						i = (t.options.force * e) / 16;
					return n > 0 ? Math.min(t.valueTarget, t.valueRaw + n * i) : Math.max(t.valueTarget, t.valueRaw + n * i);
				},
				linear: function (t, e) {
					var n = t.valueTarget - t.valueRaw,
						i = (t.options.force * e) / 16;
					return n > 0 ? Math.min(t.valueTarget, t.valueRaw + i) : Math.max(t.valueTarget, t.valueRaw - i);
				},
			}),
			(t.exports = r);
	},
	function (t, e, n) {
		(function (t) {
			"use strict";
			var e = !1;
			try {
				var n = Object.defineProperty({}, "passive", {
					get: function () {
						e = !0;
					},
				});
				window.addEventListener("test", null, n);
			} catch (i) { }
			(t.fn.onpassive = function (t, n) {
				return e
					? this.each(function () {
						this.addEventListener(t, n, { passive: !0 });
					})
					: this.on(t, n);
			}),
				(t.fn.offpassive = function (t, n) {
					return e
						? this.each(function () {
							this.removeEventListener(t, n, { passive: !0 });
						})
						: this.off(t, n);
				});
		}.call(e, n(3)));
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var s =
			"function" == typeof Symbol && "symbol" == typeof Symbol.iterator
				? function (t) {
					return typeof t;
				}
				: function (t) {
					return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
				},
			o = (function () {
				function t(t, e) {
					for (var n = 0; n < e.length; n++) {
						var i = e[n];
						(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
					}
				}
				return function (e, n, i) {
					return n && t(e.prototype, n), i && t(e, i), e;
				};
			})(),
			a = n(3),
			l = i(a),
			u = n(65),
			c = i(u),
			h = n(183),
			d = (i(h), n(71)),
			f = i(d),
			p = n(63),
			v = i(p),
			g = n(187),
			m = i(g),
			y = n(188),
			b = i(y),
			x = n(189),
			w = i(x),
			_ = n(194),
			C = i(_),
			k = n(195),
			T = i(k),
			S = n(196),
			E = i(S),
			j = n(197),
			O = i(j),
			$ = n(198),
			D = i($),
			I = (function () {
				function t(e, n) {
					r(this, t);
					(this.options = l["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, l["default"])(e));
					(this.effects = []),
						(this.inview = !1),
						(this.visible = !1),
						(this.loaded = !1),
						this.setupEffects(),
						(this.updateConstraints = this.updateConstraints.bind(this)),
						(this.update = this.update.bind(this)),
						l["default"].scroller.onpassive("resize", this.updateConstraints),
						l["default"].scroller.onpassive("scroll", this.update),
						this.updateConstraints();
				}
				return (
					o(t, null, [
						{
							key: "Defaults",
							get: function () {
								return { margin: -100, delay: 0, effects: "fade", slideinProperty: null, slideinOffset: 30, duration: 750, easing: "cubic-bezier(.25,  .74, .22, .99)", callback: null, complete: null, destroyOnEnd: !0 };
							},
						},
					]),
					o(t, [
						{
							key: "reset",
							value: function () {
								(this.inview = !1), this.$container.addClass("invisible");
								for (var t = this.effects, e = 0, n = t.length; e < n; e++) t[e].reset && t[e].reset();
								setTimeout(this.update, 60);
							},
						},
						{
							key: "animate",
							value: function () {
								this.animateEffect();
							},
						},
						{
							key: "destroy",
							value: function () {
								l["default"].scroller.offpassive("resize", this.updateConstraints),
									l["default"].scroller.offpassive("scroll", this.update),
									this.$temp && this.$temp.remove(),
									this.$container.removeData("appear"),
									(this.$container = this.$temp = this.options = this.updateConstraints = this.update = null),
									(this.effects = []);
							},
						},
						{
							key: "convertToValue",
							value: function (t) {
								if ("string" == typeof t) {
									var e = t.match(/^(\-?[\d\.]+)(vw|vh|vmax|vmin|px)/);
									if (e) {
										var n = 1;
										return (
											"vw" === e[2]
												? (n = window.innerWidth / 100)
												: "vh" === e[2]
													? (n = window.innerHeight / 100)
													: "vmax" === e[2]
														? (n = Math.max(window.innerWidth, window.innerHeight) / 100)
														: "vmin" === e[2] && (n = Math.min(window.innerWidth, window.innerHeight) / 100),
											parseFloat(e[1]) * n
										);
									}
								}
								return parseFloat(t);
							},
						},
						{
							key: "updateConstraints",
							value: function () {
								var t = this.convertToValue(this.options.margin),
									e = l["default"].scroller.getViewOffset(this.$container).top - window.innerHeight - t;
								if (!this.visible) {
									var n = this.$container.css("display");
									this.visible = "none" !== n;
								}
								this.offset !== e && ((this.offset = e), this.update());
							},
						},
						{
							key: "update",
							value: function () {
								var t = l["default"].scroller.scrollTop();
								!this.inview && this.visible && t >= this.offset && ((this.inview = !0), this.load());
							},
						},
						{
							key: "load",
							value: function () {
								var t = this,
									e = this.$container;
								if (!this.loaded && e.is("picture")) {
									e.find("source, img").each(function (t, e) {
										var n = (0, l["default"])(e),
											i = n.data("srcset"),
											r = n.data("src");
										i && n.attr("srcset", i), r && n.attr("src", r);
									});
									var n = e.find("img");
									"function" == typeof picturefill && picturefill({ reevaluate: !0, elements: [n.get(0)] }),
										n.get(0).complete && n.attr("src")
											? this.ready()
											: e.find("img").one("load error", function () {
												t.ready();
											});
								} else if (!this.loaded && e.is("img")) {
									var i = this.$container.data("src"),
										r = (this.$temp = (0, l["default"])('<img src="' + i + '" alt="" style="position: absolute; left: -9000px;" />').appendTo("body"));
									r.one("load error", function () {
										e.attr("src", e.data("src")), t.$temp.remove(), (t.$temp = null), t.ready();
									});
								} else this.animateEffect();
							},
						},
						{
							key: "ready",
							value: function (t) {
								(0, v["default"])(this.$container.get(0)), (this.loaded = !0), this.animateEffect();
							},
						},
						{
							key: "setupEffects",
							value: function () {
								for (var e = this.options.effects.split(/\s*,\s*/g), n = [], i = 0, r = e.length; i < r; i++)
									if (e[i].length && e[i] in t.effects) {
										var s = t.effects[e[i]](this);
										n.push(s), s.reset && s.reset();
									}
								this.effects = n;
							},
						},
						{
							key: "animateEffect",
							value: function () {
								var t = this,
									e = this.$container,
									n = this.options,
									i = this.effects,
									r = n.easing,
									o = n.delay,
									a = n.duration / 1e3;
								a
									? setTimeout(function () {
										e.removeClass("invisible");
										for (var n = {}, o = [], u = 0, c = i.length; u < c; u++)
											if (i[u].animate) {
												var h = i[u].animate();
												h && "object" === ("undefined" == typeof h ? "undefined" : s(h)) && "function" == typeof h.then ? o.push(h) : l["default"].extend(n, h);
											}
										var d = Object.keys(n);
										if (d.length) {
											var p = l["default"].Deferred();
											o.push(p),
												(n.transition = (0, f["default"])(d, function (t) {
													return t + " " + a + "s " + r;
												}).join(", ")),
												e
													.css(n)
													.transitionend()
													.done(function () {
														p.resolve();
													});
										}
										o.length
											? l["default"].when.apply(l["default"], o).then(function () {
												t.finalizEffect();
											})
											: t.finalizEffect();
									}, o || 16)
									: (e.removeClass("invisible"), this.finalizEffect()),
									n.callback && n.callback(e);
							},
						},
						{
							key: "finalizEffect",
							value: function () {
								if (this.$container) {
									for (var t = this.effects, e = 0, n = t.length; e < n; e++) {
										var i = t[e];
										i.finalize && i.finalize();
									}
									this.$container.css("transition", "").trigger("appear"), this.options.complete && this.options.complete(this.$container), this.options.destroyOnEnd && this.destroy();
								}
							},
						},
					]),
					t
				);
			})();
		(e["default"] = I),
			(I.effects = { fade: m["default"], slidein: b["default"], text: w["default"], line: C["default"], "line-content": T["default"], bodymovin: E["default"], button: D["default"], "slidein-left": O["default"] }),
			(l["default"].fn.appear = (0, c["default"])(I, { namespace: "appear", api: ["reset", "animate", "update"] }));
	},
	function (t, e, n) {
		function i(t, e, n, i) {
			(t = s(t) ? t : l(t)), (n = n && !i ? a(n) : 0);
			var c = t.length;
			return n < 0 && (n = u(c + n, 0)), o(t) ? n <= c && t.indexOf(e, n) > -1 : !!c && r(t, e, n) > -1;
		}
		var r = n(163),
			s = n(41),
			o = n(184),
			a = n(174),
			l = n(185),
			u = Math.max;
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return "string" == typeof t || (!s(t) && o(t) && r(t) == a);
		}
		var r = n(18),
			s = n(51),
			o = n(50),
			a = "[object String]";
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return null == t ? [] : r(t, s(t));
		}
		var r = n(186),
			s = n(45);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e) {
			return r(e, function (e) {
				return t[e];
			});
		}
		var r = n(72);
		t.exports = i;
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		Object.defineProperty(e, "__esModule", { value: !0 }),
			(e["default"] = function (t) {
				return {
					reset: function () {
						t.$container.css("opacity", 0);
					},
					animate: function () {
						return { opacity: 1 };
					},
					finalize: function () {
						t.$container.css("opacity", "");
					},
				};
			});
		var r = n(3);
		i(r);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		Object.defineProperty(e, "__esModule", { value: !0 }),
			(e["default"] = function (t) {
				var e = t.options.slideinProperty;
				if (e) "none" === e && (e = "x");
				else {
					var n = t.$container.css("transform");
					if (n && "none" !== n) {
						var i = t.$container.css("position");
						e = "absolute" === i || "fixed" === i ? "margin-top" : "top";
					} else e = "transform";
				}
				return {
					reset: function () {
						var n = t.options.slideinOffset;
						"transform" === e ? t.$container.css(e, "translateY(" + n + "px)") : "margin-top" === e || "top" === e ? t.$container.css(e, n + "px") : ("margin-bottom" !== e && "bottom" !== e) || t.$container.css(e, -n + "px"),
							("top" !== e && "bottom" != e) || t.$container.css("position", "relative");
					},
					animate: function () {
						var t = {};
						return (t[e] = "transform" === e ? "translateY(0px)" : "0px"), t;
					},
					finalize: function () {
						t.$container.css(e, "").css("position", "");
					},
				};
			});
		var r = n(3);
		i(r);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t) {
			var e = [],
				n = t.html(),
				i = [];
			(n = n.replace(/\n/g, " ")),
				(n = n.replace(/(<br[^>]*?>)/g, function (t) {
					return " #{$" + (e.push(t) - 1) + "} ";
				})),
				(i = n.split(" "));
			for (var r = 0, s = i.length; r < s; r++) "" != i[r] && " " != i[r] && (i[r] = '<span class="word-measure">' + i[r] + "</span>");
			return (
				(n = i.join(" ")),
				(n = (0, u["default"])(
					e,
					function (t, e, n) {
						return t.replace("#{$" + n + "}", e);
					},
					n
				))
			);
		}
		function s(t) {
			var e = t.children(".word-measure"),
				n = [],
				i = [],
				r = 0;
			i.push(e.eq(0).html()), (r = e.get(0).getBoundingClientRect().top);
			for (var s = 1, o = e.length; s < o; s++) {
				var a = e.get(s).getBoundingClientRect().top,
					l = e.eq(s).html(),
					u = !!l.match(/^<br\s*\/?>$/);
				((null !== r && r !== a) || u) && (n.push('<div class="text-line">' + i.join(" ") + (u ? "<br />" : "") + "</div>"), (i = []), (r = u ? null : a)), u || i.push(l);
			}
			return n.push('<div class="text-line">' + i.join(" ") + "</div>"), n.join(" ");
		}
		Object.defineProperty(e, "__esModule", { value: !0 }),
			(e["default"] = function (t) {
				var e = t.options.delay;
				t.options.delay = 0;
				var n = void 0;
				return {
					reset: function () {
						if (h["default"].isPhone()) t.$container.removeClass("invisible");
						else {
							var e = t.$container;
							e.find("br").each(function (t, e) {
								var n = (0, a["default"])(e);
								n.is(":visible") || n.remove();
							}),
								(n = e.html()),
								e.html(r(e)),
								e.html(s(e)),
								e.find(".text-line").addClass("invisible");
						}
					},
					animate: function () {
						if (h["default"].isPhone()) return a["default"].Deferred().resolve();
						var n = t.$container.find(".text-line"),
							i = [];
						return (
							n.each(function (t, n) {
								var r = (0, a["default"])(n),
									s = a["default"].Deferred();
								i.push(s),
									r
										.encapsulate()
										.inner.css("transition-delay", e + 60 * t + "ms")
										.transition({
											before: function (t) {
												r.removeClass("is-hidden invisible"), t.addClass("animation--appear-line animation--appear-line--inactive");
											},
											transition: function (t) {
												t.removeClass("animation--appear-line--inactive");
											},
											after: function (t) {
												r.deencapsulate(), s.resolve();
											},
										});
							}),
							a["default"].when.apply(a["default"], i)
						);
					},
					finalize: function () {
						n && t.$container.html(n);
					},
				};
			});
		var o = n(3),
			a = i(o),
			l = n(190),
			u = i(l);
		n(193);
		var c = n(7),
			h = i(c);
	},
	function (t, e, n) {
		function i(t, e, n) {
			var i = l(t) ? r : a,
				u = arguments.length < 3;
			return i(t, o(e, 4), n, u, s);
		}
		var r = n(191),
			s = n(151),
			o = n(73),
			a = n(192),
			l = n(51);
		t.exports = i;
	},
	function (t, e) {
		function n(t, e, n, i) {
			var r = -1,
				s = null == t ? 0 : t.length;
			for (i && s && (n = t[++r]); ++r < s;) n = e(n, t[r], r, t);
			return n;
		}
		t.exports = n;
	},
	function (t, e) {
		function n(t, e, n, i, r) {
			return (
				r(t, function (t, r, s) {
					n = i ? ((i = !1), t) : e(n, t, r, s);
				}),
				n
			);
		}
		t.exports = n;
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			var n = l["default"].extend({ border: !0 }, e),
				i = t.hasClass("is-hidden");
			i && t.removeClass("is-hidden");
			var r = t.prop("tagName"),
				a = t.css(["margin-top", "margin-right", "margin-bottom", "margin-left", "position", "z-index", "left", "top", "right", "bottom", "flex", "border-left", "border-right"]),
				h = t.outerWidth(),
				d = t.outerHeight();
			"static" === a.position && (a.position = "relative"), n.border === !1 && (delete a["border-left"], delete a["border-right"]);
			var f = (0, l["default"])("<" + (u[r] || "div") + " />").css(o({}, a, { padding: 0, width: h, "min-width": h, "max-width": h, height: d, overflow: "hidden" })),
				p = (0, l["default"])("<" + (c[r] || "div") + " />").css({ position: "relative", width: h, height: d, overflow: "hidden" });
			return (
				f.insertBefore(t),
				f.append(p.append(t)),
				t.css({ margin: 0, left: "auto", right: "auto", top: "auto", bottom: "auto", flex: "initial", position: "relative", height: d, width: h }),
				n.border !== !1 && t.css({ "border-left": 0, "border-right": 0 }),
				i && t.addClass("is-hidden"),
				{ outer: f, inner: p, element: t, restore: s.bind(this, f, t) }
			);
		}
		function s(t, e) {
			e.css({ position: "", width: "", height: "", margin: "", padding: "", left: "", right: "", top: "", bottom: "", flex: "", "border-left": "", "border-right": "" }).insertBefore(t), t.remove();
		}
		var o =
			Object.assign ||
			function (t) {
				for (var e = 1; e < arguments.length; e++) {
					var n = arguments[e];
					for (var i in n) Object.prototype.hasOwnProperty.call(n, i) && (t[i] = n[i]);
				}
				return t;
			},
			a = n(3),
			l = i(a),
			u = { LI: "LI" },
			c = { LI: "UL" };
		(l["default"].fn.encapsulate = function (t) {
			var e = this.data("encapsulation");
			return e || ((e = r(this, t)), this.data("encapsulation", e)), e;
		}),
			(l["default"].fn.deencapsulate = function () {
				var t = this.data("encapsulation");
				return t && (t.restore(), this.data("encapsulation", null)), this;
			});
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		Object.defineProperty(e, "__esModule", { value: !0 }),
			(e["default"] = function (t) {
				var e = t.options.delay;
				return (
					(t.options.delay = 0),
					{
						reset: function () {
							var e = t.$container;
							e.addClass("invisible");
						},
						animate: function () {
							var n = t.$container,
								i = s["default"].Deferred();
							return (
								n
									.encapsulate()
									.inner.css("transition-delay", e + "ms")
									.transition({
										before: function (t) {
											n.removeClass("is-hidden invisible"), t.addClass("animation--appear-line animation--appear-line--inactive");
										},
										transition: function (t) {
											t.removeClass("animation--appear-line--inactive");
										},
										after: function (t) {
											n.deencapsulate(), i.resolve();
										},
									}),
								i.promise()
							);
						},
						finalize: function () { },
					}
				);
			});
		var r = n(3),
			s = i(r);
		n(193);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		Object.defineProperty(e, "__esModule", { value: !0 }),
			(e["default"] = function (t) {
				var e = t.options.delay,
					n = void 0;
				return (
					(t.options.delay = 0),
					{
						reset: function () {
							var e = t.$container;
							e.addClass("invisible");
						},
						animate: function () {
							var i = t.$container,
								r = s["default"].Deferred();
							return (
								i.wrapInner("<div />"),
								(n = i.children()),
								i.css("transition-delay", e + "ms"),
								n.css("transition-delay", e + "ms").transition({
									before: function (t) {
										i.removeClass("is-hidden invisible").addClass("animation--fade-in animation--fade-in--inactive"), n.addClass("animation--appear-line animation--appear-line--inactive");
									},
									transition: function (t) {
										i.removeClass("animation--fade-in--inactive"), n.removeClass("animation--appear-line--inactive");
									},
									after: function (t) {
										for (var e = n.get(0); e.firstChild;) i.get(0).appendChild(e.firstChild);
										i.removeClass("animation--fade-in").css("transition-delay", ""), n.remove(), r.resolve();
									},
								}),
								r.promise()
							);
						},
						finalize: function () { },
					}
				);
			});
		var r = n(3),
			s = i(r);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		Object.defineProperty(e, "__esModule", { value: !0 }),
			(e["default"] = function (t) {
				function e() {
					var e = t.$container,
						r = e.data(),
						s = null;
					s = r.bmCover === !1 ? i(r, e) : n(r, e);
					var o = bodymovin.loadAnimation({ wrapper: s.get(0), path: r.bmPath || null, loop: r.bmLoop || !1, animType: r.bmRenderer || "svg", autoplay: r.bmAutoplay !== !1, assetsPath: r.bmAssetsPath || null });
					o.addEventListener("complete", function () {
						return a.resolve();
					});
				}
				function n(t, e) {
					var n = (0, s["default"])('<span style="display: inline-block; position: absolute; left: 0; top: 0; width: 100%; height: 100%"></span>').appendTo(e);
					return (t.bmWidth || t.bmHeight) && e.css({ width: t.bmWidth || "100%", height: t.bmHeight || "100%" }), n;
				}
				function i(t, e) {
					return e;
				}
				function r() {
					return a || (a = s["default"].Deferred()), "undefined" != typeof bodymovin ? e() : o ? (o--, setTimeout(r, 60)) : a.reject(), a.promise();
				}
				var o = 16,
					a = null;
				return { animate: r };
			});
		var r = n(3),
			s = i(r);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		Object.defineProperty(e, "__esModule", { value: !0 }),
			(e["default"] = function (t) {
				var e = t.options.delay;
				return (
					(t.options.delay = 0),
					{
						reset: function () {
							var e = t.$container;
							e.addClass("invisible");
						},
						animate: function () {
							var n = t.$container,
								i = s["default"].Deferred();
							return (
								n
									.encapsulate()
									.inner.css("transition-delay", e + "ms")
									.transition({
										before: function (t) {
											n.removeClass("is-hidden invisible"), t.addClass("animation--slidein-left animation--slidein-left--inactive");
										},
										transition: function (t) {
											t.removeClass("animation--slidein-left--inactive");
										},
										after: function (t) {
											n.deencapsulate(), i.resolve();
										},
									}),
								i.promise()
							);
						},
						finalize: function () { },
					}
				);
			});
		var r = n(3),
			s = i(r);
		n(193);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		Object.defineProperty(e, "__esModule", { value: !0 }),
			(e["default"] = function (t) {
				var e = t.options.delay;
				return (
					(t.options.delay = 0),
					{
						reset: function () {
							var e = t.$container;
							e.addClass("invisible");
						},
						animate: function () {
							var n = t.$container,
								i = s["default"].Deferred();
							return (
								n.encapsulate({ border: !1 }).inner.transition({
									before: function (t) {
										n.removeClass("is-hidden invisible"), t.addClass("animation--button animation--button--inactive");
									},
									transition: function (t) {
										t.removeClass("animation--button--inactive");
									},
									after: function (t) {
										t.removeClass("animation--button"), n.deencapsulate(), i.resolve();
									},
									delay: e,
								}),
								i.promise()
							);
						},
						finalize: function () { },
					}
				);
			});
		var r = n(3),
			s = i(r);
		n(193);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(65),
			u = i(l),
			c = n(7),
			h = i(c),
			d = n(200),
			f = i(d),
			p = /\-(\d+)\-?(\d+|vh)/i,
			v = /[\-\d\.]+/g,
			g = {
				from: { viewport: 0, element: null, properties: [{ property: "transform", string: ["translateY(", "vh)"], values: [0] }] },
				to: { viewport: 1, element: null, properties: [{ property: "transform", string: ["translateY(", "vh)"], values: [100] }] },
			},
			m = (function () {
				function t(e, n) {
					r(this, t);
					var i = (this.options = a["default"].extend({}, this.constructor.Defaults, n)),
						s = (this.$container = (0, a["default"])(e)),
						o = (this.config = this.processConfiguration(s.get(0).dataset)),
						l = i.phone || !h["default"].isPhone();
					(this.position = null),
						o &&
						l &&
						(!a["default"].scroller.custom && (0, f["default"])(g, o)
							? this.nativeFixedPosition()
							: (this.updateConstraints(), a["default"].scroller.on("resize", this.updateConstraints.bind(this)), a["default"].scroller.onpassive("scroll", this.update.bind(this))));
				}
				return (
					s(t, null, [
						{
							key: "Defaults",
							get: function () {
								return { phone: !0 };
							},
						},
					]),
					s(t, [
						{
							key: "processProperty",
							value: function (t, e) {
								var n = [],
									i = String(e).replace(v, function (t) {
										return n.push(parseFloat(t)), "%d";
									});
								return { property: t, string: i.split("%d"), values: n };
							},
						},
						{
							key: "processProperties",
							value: function (t) {
								var e = [];
								for (var n in t) e.push(this.processProperty(n, t[n]));
								return e;
							},
						},
						{
							key: "processConfiguration",
							value: function (t) {
								var e = [],
									n = [];
								for (var i in t) {
									var r = i.match(p),
										s = void 0;
									if (r) {
										try {
											s = JSON.parse(t[i]);
										} catch (o) {
											s = {};
										}
										if ((e.push({ viewport: parseFloat(r[1]) / 100, element: "vh" === r[2].toLowerCase() ? null : parseFloat(r[2]) / 100, properties: this.processProperties(s) }), !n.length)) for (var a in s) n.push(a);
									}
								}
								return 2 === e.length ? (this.$container.css("will-change", n.join(", ")), { from: e[0], to: e[1] }) : null;
							},
						},
						{
							key: "nativeFixedPosition",
							value: function () {
								var t = this.$container,
									e = t.closest(".background");
								e.addClass("background--fixed");
							},
						},
						{
							key: "updateConstraints",
							value: function () {
								var t = this.config,
									e = this.$container.css(this.reset()),
									n = window.innerHeight,
									i = e.outerHeight(),
									r = Math.floor(a["default"].scroller.getViewOffset(e).top),
									s = void 0,
									o = void 0;
								if (((s = null === t.from.element ? t.from.viewport * n : i * t.from.element + r - t.from.viewport * n), (o = null === t.to.element ? t.to.viewport * n : i * t.to.element + r - t.to.viewport * n), s > o)) {
									var l = t.from;
									(t.from = t.to), (t.to = l), (this.constraints = { from: o, to: s });
								} else this.constraints = { from: s, to: o };
								(this.position = null), this.update();
							},
						},
						{
							key: "reset",
							value: function () {
								for (var t = this.config.from.properties, e = {}, n = 0, i = t.length; n < i; n++) e[t[n].property] = "";
								return e;
							},
						},
						{
							key: "interpolate",
							value: function (t) {
								for (var e = this.config, n = {}, i = e.from.properties, r = e.to.properties, s = 0, o = i.length; s < o; s++) {
									for (var a = i[s].values, l = r[s].values, u = [i[s].string[0]], c = 0, h = a.length; c < h; c++) u.push((l[c] - a[c]) * t + a[c]), u.push(i[s].string[c + 1]);
									n[i[s].property] = u.join("");
								}
								return n;
							},
						},
						{
							key: "update",
							value: function () {
								var t = a["default"].scroller.scrollTop(),
									e = this.constraints,
									n = Math.min(Math.max((t - e.from) / (e.to - e.from), 0), 1);
								this.position !== n && ((this.position = n), this.$container.css(this.interpolate(n)));
							},
						},
					]),
					t
				);
			})();
		(e["default"] = m), (a["default"].fn.parallax = (0, u["default"])(m));
	},
	function (t, e, n) {
		function i(t, e) {
			return r(t, e);
		}
		var r = n(105);
		t.exports = i;
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		var r = n(3),
			s = i(r),
			o = n(65),
			a = i(o);
		s["default"].fn.labelPlaceholder = (0, a["default"])(function (t) {
			function e() {
				r.addClass("form-label--up");
			}
			function n() {
				r.toggleClass("form-label--up", t.val().length > 0);
			}
			function i(t) {
				(0, s["default"])(t.target).val().length ? e() : n();
			}
			var r = t.siblings(".form-label");
			t.on("focus", e), t.on("focusout", n), t.on("change", i), n();
		});
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(65),
			u = i(l),
			c = n(71),
			h = i(c),
			d = (function () {
				function t(e, n) {
					r(this, t);
					var i = ((this.options = a["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, a["default"])(e)));
					(this.$widget = null), (this.$input = i.closest("input, select").add(i.find("input, select")));
					i.is("select") ? this.createWidget() : (this.$widget = this.$container), this.$widget.on("tap", "[data-id]", this.handleItemClick.bind(this)), this.$input.on("change", this.update.bind(this));
				}
				return (
					s(t, null, [
						{
							key: "Defaults",
							get: function () {
								return { buttonClassName: "btn btn--switch btn--text-top btn--text-left" };
							},
						},
					]),
					s(t, [
						{
							key: "createWidget",
							value: function () {
								var t = this,
									e = this.$container.prop("className"),
									n = this.$container.val(),
									i = this.$container.find("option").toArray(),
									r = (0, h["default"])(i, function (e) {
										return (
											'\n            <a class="' +
											t.options.buttonClassName +
											" " +
											(e.value == n ? "is-active" : "") +
											'" data-id="' +
											e.value +
											'">\n                <span>' +
											e.text.replace(/\s\//g, "&nbsp;/") +
											"</span>\n            </a>"
										);
									}).join("");
								(this.$widget = (0, a["default"])('<div class="form-control-switch ' + e + '">' + r + "</div>").insertAfter(this.$container)), this.$container.addClass("is-out-of-screen");
							},
						},
						{
							key: "update",
							value: function () {
								var t = this.$input.val(),
									e = this.$widget.find(".btn");
								e.removeClass("is-active"), e.filter('[data-id="' + t + '"]').addClass("is-active");
							},
						},
						{
							key: "handleItemClick",
							value: function (t) {
								var e = (0, a["default"])(t.target).closest(".btn").data("id");
								this.$input.val(e).change();
							},
						},
					]),
					t
				);
			})();
		(e["default"] = d), (a["default"].fn["switch"] = (0, u["default"])(d));
	},
	function (t, e, n) {
		(function (t) {
			"use strict";
			function e(t) {
				return t && t.__esModule ? t : { default: t };
			}
			function i(t, e) {
				if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
			}
			var r = (function () {
				function t(t, e) {
					for (var n = 0; n < e.length; n++) {
						var i = e[n];
						(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
					}
				}
				return function (e, n, i) {
					return n && t(e.prototype, n), i && t(e, i), e;
				};
			})(),
				s = n(3),
				o = e(s),
				a = n(204),
				l = e(a),
				u = n(71),
				c = e(u),
				h = n(156),
				d = (e(h), n(207)),
				f = e(d),
				p = n(171),
				v = e(p),
				g = n(65),
				m = e(g),
				y = n(7),
				b = e(y);
			n(215);
			var x = (function () {
				function e(t, n) {
					i(this, e);
					var r = (this.options = o["default"].extend({}, this.constructor.Defaults, n));
					(this.$container = (0, o["default"])(t)), (this.$replacement = null), "string" == typeof r.templates && r.templates in e && (r.templates = e[r.templates]), b["default"].isMobile() || this.create();
				}
				return (
					r(e, null, [
						{
							key: "Defaults",
							get: function () {
								return { allowClear: !1, showSearchInputInDropdown: !1, templates: void 0 };
							},
						},
					]),
					r(e, [
						{
							key: "setOptions",
							value: function (t) {
								var e = this.$container,
									n = this.value(),
									i = !1;
								(0, v["default"])(t, n) || (0, v["default"])(t, { id: n }) || ((n = t[0] && "id" in t[0] ? t[0].id : t[0]), (i = !0)),
									e.html(
										(0, c["default"])(t, function (t) {
											return '<option value="' + (0, l["default"])(t.id) + '">' + (0, l["default"])(t.text) + "</option>";
										}).join("")
									),
									b["default"].isMobile() || (e.selectivity("setOptions", { items: t }), i && e.selectivity("value", n)),
									e.val(n);
							},
						},
						{
							key: "create",
							value: function () {
								var t = this.options,
									e = this.$container,
									n = (0, f["default"])(t, ["allowClear", "showSearchInputInDropdown", "templates"]),
									i = (this.$replacement = e.removeClass("form-control").selectivity(n));
								e.css("display", "block").attr("tabindex", "-1").addClass("is-out-of-screen"), i.on("change", this.triggerTraditionalEvent.bind(this)), this.updateEmptyUIState();
							},
						},
						{
							key: "triggerTraditionalEvent",
							value: function (e) {
								var n = this;
								this.$container.trigger(new t.Event({ type: "change", originalEvent: e })),
									this.updateEmptyUIState(),
									this.$container.trigger("keyup"),
									b["default"].isIE() &&
									!this.options.showSearchInputInDropdown &&
									setTimeout(function () {
										n.$replacement.selectivity("close");
									}, 16);
							},
						},
						{
							key: "updateEmptyUIState",
							value: function () {
								this.$replacement.toggleClass("form-control--not-empty", !!this.$container.val());
							},
						},
						{
							key: "value",
							value: function () {
								return b["default"].isMobile() ? this.$container.val() : this.$container.selectivity("value");
							},
						},
					]),
					e
				);
			})();
			(x.IconTemplates = {
				singleSelectedItem: function (t) {
					var e = String(t.id)
						.toLowerCase()
						.replace(/[^a-z0-9_-]/g, "");
					return (
						'<span class="selectivity-single-selected-item" data-item-id="' +
						(0, l["default"])(t.id) +
						'"><span class="icon icon--' +
						e +
						'"></span>' +
						(t.removable ? '<a class="selectivity-single-selected-item-remove"><i class="fa fa-remove"></i></a>' : "") +
						"<span>" +
						(0, l["default"])(t.text) +
						"</span></span>"
					);
				},
			}),
				(o["default"].fn.select = (0, m["default"])(x, { optionsSetter: "_setOpts" }));
		}.call(e, n(3)));
	},
	function (t, e, n) {
		function i(t) {
			return (t = s(t)), t && a.test(t) ? t.replace(o, r) : t;
		}
		var r = n(205),
			s = n(141),
			o = /[&<>"']/g,
			a = RegExp(o.source);
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(206),
			r = { "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#39;" },
			s = i(r);
		t.exports = s;
	},
	function (t, e) {
		function n(t) {
			return function (e) {
				return null == t ? void 0 : t[e];
			};
		}
		t.exports = n;
	},
	function (t, e, n) {
		var i = n(208),
			r = n(211),
			s = r(function (t, e) {
				return null == t ? {} : i(t, e);
			});
		t.exports = s;
	},
	function (t, e, n) {
		function i(t, e) {
			return r(t, e, function (e, n) {
				return s(t, n);
			});
		}
		var r = n(209),
			s = n(144);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n) {
			for (var i = -1, a = e.length, l = {}; ++i < a;) {
				var u = e[i],
					c = r(t, u);
				n(c, u) && s(l, o(u, t), c);
			}
			return l;
		}
		var r = n(134),
			s = n(210),
			o = n(135);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n, i) {
			if (!a(t)) return t;
			e = s(e, t);
			for (var u = -1, c = e.length, h = c - 1, d = t; null != d && ++u < c;) {
				var f = l(e[u]),
					p = n;
				if (u != h) {
					var v = d[f];
					(p = i ? i(v, f, d) : void 0), void 0 === p && (p = a(v) ? v : o(e[u + 1]) ? [] : {});
				}
				r(d, f, p), (d = d[f]);
			}
			return t;
		}
		var r = n(12),
			s = n(135),
			o = n(43),
			a = n(24),
			l = n(143);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return o(s(t, void 0, r), t + "");
		}
		var r = n(212),
			s = n(34),
			o = n(36);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			var e = null == t ? 0 : t.length;
			return e ? r(t, 1) : [];
		}
		var r = n(213);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t, e, n, o, a) {
			var l = -1,
				u = t.length;
			for (n || (n = s), a || (a = []); ++l < u;) {
				var c = t[l];
				e > 0 && n(c) ? (e > 1 ? i(c, e - 1, n, o, a) : r(a, c)) : o || (a[a.length] = c);
			}
			return a;
		}
		var r = n(120),
			s = n(214);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			return o(t) || s(t) || !!(a && t && t[a]);
		}
		var r = n(19),
			s = n(48),
			o = n(51),
			a = r ? r.isConcatSpreadable : void 0;
		t.exports = i;
	},
	function (t, e, n) {
		n(216), n(221), n(218), n(226), n(227), n(229), n(230), n(231), n(232), n(233), n(234), n(235), n(236), n(238), n(239), n(240), n(217), n(241), (t.exports = n(217));
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			r.each(a, function (e, n) {
				t.on(e, function (t) {
					t.originalEvent &&
						n.forEach(function (e) {
							t[e] = t.originalEvent[e];
						});
				});
			});
		}
		var r = n(3),
			s = n(184),
			o = n(217),
			a = { change: ["added", "removed", "value"], "selectivity-change": ["added", "removed", "value"], "selectivity-highlight": ["id", "item"], "selectivity-selected": ["id", "item"], "selectivity-selecting": ["id", "item"] };
		(r.fn.selectivity = function (t, e) {
			var n,
				a = Array.prototype.slice.call(arguments, 1);
			return (
				this.each(function () {
					var l = this.selectivity;
					if (l) {
						if (("data" === t ? (t = a.length ? "setData" : "getData") : "val" === t || "value" === t ? (t = a.length ? "setValue" : "getValue") : s(t) || ((a = [t]), (t = "setOptions")), !r.isFunction(l[t])))
							throw new Error("Unknown method: " + t);
						void 0 === n && (n = l[t].apply(l, a));
					} else if (s(t)) {
						if ("destroy" !== t) throw new Error("Cannot call method on element without Selectivity instance");
					} else {
						e = r.extend({}, t, { element: this });
						var u = r(this);
						u.is("select") && u.prop("multiple") && (e.multiple = !0);
						var c = o.Inputs,
							h = e.inputType || (e.multiple ? "Multiple" : "Single");
						if (!r.isFunction(h)) {
							if (!c[h]) throw new Error("Unknown Selectivity input type: " + h);
							h = c[h];
						}
						(this.selectivity = new h(e)), (u = r(this.selectivity.el)), i(u), void 0 === n && (n = u);
					}
				}),
				void 0 === n ? this : n
			);
		}),
			(o.patchEvents = i),
			(r.Selectivity = o);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			(this.dropdown = null),
				(this.el = t.element),
				(this.enabled = !t.readOnly && !t.removeOnly),
				(this.input = null),
				(this.items = null),
				(this.options = {}),
				(this.templates = r({}, i.Templates)),
				(this.term = ""),
				this.setOptions(t),
				t.value ? this.setValue(t.value, { triggerChange: !1 }) : this.setData(t.data || null, { triggerChange: !1 }),
				this.el.setAttribute("tabindex", t.tabIndex || 0),
				(this.events = new o(this.el, this)),
				this.events.on({ blur: this._blur, mouseenter: this._mouseenter, mouseleave: this._mouseleave, "selectivity-close": this._closed });
		}
		var r = n(11),
			s = n(184),
			o = n(218),
			a = n(220);
		r(i.prototype, {
			$: function (t) {
				return this.el.querySelector(t);
			},
			close: function () {
				this._clearCloseTimeout(), this.dropdown && (this.dropdown.close(), (this.dropdown = null));
			},
			destroy: function () {
				this.events.destruct();
				for (var t = this.el; t.firstChild;) t.removeChild(t.firstChild);
				t.selectivity = null;
			},
			filterResults: function (t) {
				return t;
			},
			focus: function () {
				this._clearCloseTimeout(), (this._focusing = !0), this.input && this.input.focus(), (this._focusing = !1);
			},
			getData: function () {
				return this._data;
			},
			getItemForId: function (t) {
				var e = this.items;
				return e ? i.findNestedById(e, t) : null === t ? null : { id: t, text: "" + t };
			},
			getRelatedItemId: function (t) {
				for (var e = t.target || t; e && !e.hasAttribute("data-item-id");) e = e.parentNode;
				if (!e) return null;
				var n = e.getAttribute("data-item-id");
				if (i.findById(this._data || [], n)) return n;
				for (var r = this.dropdown; r;) {
					if (i.findNestedById(r.results, n)) return n;
					r = r.submenu;
				}
				var s = parseInt(n, 10);
				return "" + s === n ? s : n;
			},
			getValue: function () {
				return this._value;
			},
			initInput: function (t, e) {
				this.input = t;
				var n = this,
					r = this.options.inputListeners || i.InputListeners;
				r.forEach(function (i) {
					i(n, t, e);
				}),
					(e && e.search === !1) ||
					t.addEventListener("keyup", function (t) {
						t.defaultPrevented || n.search(t.target.value);
					});
			},
			open: function () {
				if (!this._opening && !this.dropdown && this.triggerEvent("selectivity-opening")) {
					this._opening = !0;
					var t = this.options.dropdown || i.Dropdown;
					t && (this.dropdown = new t(this, { items: this.items, position: this.options.positionDropdown, query: this.options.query, showSearchInput: this.options.showSearchInputInDropdown !== !1 })),
						this.search(""),
						this.focus(),
						a(this.el, "open", !0),
						(this._opening = !1);
				}
			},
			positionDropdown: function () {
				this.dropdown && this.dropdown.position();
			},
			search: function (t) {
				this.open(), this.dropdown && this.dropdown.search(t);
			},
			setData: function (t, e) {
				(e = e || {}), (t = this.validateData(t)), (this._data = t), (this._value = this.getValueForData(t)), e.triggerChange !== !1 && this.triggerChange();
			},
			setOptions: function (t) {
				t = t || {};
				var e = this;
				i.OptionListeners.forEach(function (n) {
					n(e, t);
				}),
					"items" in t && (this.items = t.items ? i.processItems(t.items) : null),
					"templates" in t && r(this.templates, t.templates),
					r(this.options, t),
					(this.enabled = !this.options.readOnly && !this.options.removeOnly);
			},
			setValue: function (t, e) {
				(e = e || {}),
					(t = this.validateValue(t)),
					(this._value = t),
					this.options.initSelection
						? this.options.initSelection(
							t,
							function (n) {
								this._value === t && ((this._data = this.validateData(n)), e.triggerChange !== !1 && this.triggerChange());
							}.bind(this)
						)
						: ((this._data = this.getDataForValue(t)), e.triggerChange !== !1 && this.triggerChange());
			},
			template: function (t, e) {
				var n = this.templates[t];
				if (!n) throw new Error("Unknown template: " + t);
				if ("function" == typeof n) {
					var i = n(e);
					return "string" == typeof i ? i.trim() : i;
				}
				return n.render ? n.render(e).trim() : n.toString().trim();
			},
			triggerChange: function (t) {
				var e = r({ data: this._data, value: this._value }, t);
				this.triggerEvent("change", e), this.triggerEvent("selectivity-change", e);
			},
			triggerEvent: function (t, e) {
				var n = document.createEvent("Event");
				return n.initEvent(t, !1, !0), r(n, e), this.el.dispatchEvent(n), !n.defaultPrevented;
			},
			validateItem: function (t) {
				if (t && i.isValidId(t.id) && s(t.text)) return t;
				throw new Error("Item should have id (number or string) and text (string) properties");
			},
			_blur: function () {
				this._focusing || this.el.classList.contains("hover") || (this._clearCloseTimeout(), (this._closeTimeout = setTimeout(this.close.bind(this), 166)));
			},
			_clearCloseTimeout: function () {
				this._closeTimeout && (clearTimeout(this._closeTimeout), (this._closeTimeout = 0));
			},
			_closed: function () {
				(this.dropdown = null), a(this.el, "open", !1);
			},
			_mouseleave: function (t) {
				this.el.contains(t.relatedTarget) || a(this.el, "hover", !1);
			},
			_mouseenter: function () {
				a(this.el, "hover", !0);
			},
		}),
			(i.Dropdown = null),
			(i.InputListeners = []),
			(i.Inputs = {}),
			(i.OptionListeners = []),
			(i.Templates = {}),
			(i.findById = function (t, e) {
				var n = i.findIndexById(t, e);
				return n > -1 ? t[n] : null;
			}),
			(i.findIndexById = function (t, e) {
				for (var n = 0, i = t.length; n < i; n++) if (t[n].id === e) return n;
				return -1;
			}),
			(i.findNestedById = function (t, e) {
				for (var n = 0, r = t.length; n < r; n++) {
					var s,
						o = t[n];
					if ((o.id === e ? (s = o) : o.children ? (s = i.findNestedById(o.children, e)) : o.submenu && o.submenu.items && (s = i.findNestedById(o.submenu.items, e)), s)) return s;
				}
				return null;
			}),
			(i.inherits = function (t, e, n) {
				return (
					(t.prototype = r(Object.create(e.prototype), { constructor: t }, n)),
					function (t, n) {
						e.prototype[n].apply(t, Array.prototype.slice.call(arguments, 2));
					}
				);
			}),
			(i.isValidId = function (t) {
				return "number" == typeof t || s(t);
			}),
			(i.matcher = function (t, e) {
				var n = null;
				if (i.transformText(t.text).indexOf(e) > -1) n = t;
				else if (t.children) {
					var r = t.children
						.map(function (t) {
							return i.matcher(t, e);
						})
						.filter(function (t) {
							return !!t;
						});
					r.length && (n = { id: t.id, text: t.text, children: r });
				}
				return n;
			}),
			(i.processItem = function (t) {
				if (i.isValidId(t)) return { id: t, text: "" + t };
				if (t && (i.isValidId(t.id) || t.children) && s(t.text)) return t.children && (t.children = i.processItems(t.children)), t;
				throw new Error("invalid item");
			}),
			(i.processItems = function (t) {
				if (Array.isArray(t)) return t.map(i.processItem);
				throw new Error("invalid items");
			}),
			(i.transformText = function (t) {
				return t.toLowerCase();
			}),
			(t.exports = i);
	},
	function (t, e, n) {
		"use strict";
		function i(t, e) {
			(this.context = e || null), (this.el = t), (this.events = {}), (this._onEvent = this._onEvent.bind(this));
		}
		var r = n(11),
			s = n(184),
			o = n(219),
			a = ["blur", "focus", "mouseenter", "mouseleave", "scroll"];
		r(i.prototype, {
			destruct: function () {
				Object.keys(this.events).forEach(function (t) {
					var e = a.indexOf(t) > -1;
					this.el.removeEventListener(t, this._onEvent, e);
				}, this),
					(this.context = null),
					(this.el = null),
					(this.events = null);
			},
			off: function (t, e, n) {
				if ((s(e) || ((n = e), (e = "")), n)) {
					var i = this.events[t];
					if (i && (i = i[e])) for (var r = 0; r < i.length; r++) i[r] === n && (i.splice(r, 1), r--);
				} else this.events[t][e] = [];
			},
			on: function (t, e, n) {
				if (s(t)) {
					if ((s(e) || ((n = e), (e = "")), !this.events.hasOwnProperty(t))) {
						var i = a.indexOf(t) > -1;
						this.el.addEventListener(t, this._onEvent, i), (this.events[t] = {});
					}
					this.events[t].hasOwnProperty(e) || (this.events[t][e] = []), this.events[t][e].indexOf(n) < 0 && this.events[t][e].push(n);
				} else {
					var r = t;
					for (var o in r)
						if (r.hasOwnProperty(o)) {
							var l = o.split(" ");
							l.length > 1 ? this.on(l[0], l[1], r[o]) : this.on(l[0], r[o]);
						}
				}
			},
			_onEvent: function (t) {
				function e(e) {
					for (var n = 0; n < e.length; n++) e[n].call(r, t);
				}
				var n = !1,
					i = t.stopPropagation;
				t.stopPropagation = function () {
					i.call(t), (n = !0);
				};
				for (var r = this.context, s = t.target, a = this.events[t.type.toLowerCase()]; s && s !== this.el && !n;) {
					for (var l in a) l && a.hasOwnProperty(l) && o(s, l) && e(a[l]);
					s = s.parentElement;
				}
				!n && a.hasOwnProperty("") && e(a[""]);
			},
		}),
			(t.exports = i);
	},
	function (t, e) {
		"use strict";
		t.exports = function (t, e) {
			var n = t.matches || t.webkitMatchesSelector || t.mozMatchesSelector || t.msMatchesSelector;
			return n.call(t, e);
		};
	},
	function (t, e) {
		"use strict";
		t.exports = function (t, e, n) {
			t && t.classList[n ? "add" : "remove"](e);
		};
	},
	function (t, e, n) {
		"use strict";
		function i(t, e) {
			for (; t && !l(t, e);) t = t.parentElement;
			return t || null;
		}
		function r(t, e) {
			(this.el = u(t.template("dropdown", { dropdownCssClass: t.options.dropdownCssClass, searchInputPlaceholder: t.options.searchInputPlaceholder, showSearchInput: e.showSearchInput }))),
				(this.resultsContainer = this.$(".selectivity-results-container")),
				(this.hasMore = !1),
				(this.highlightedResult = null),
				(this.loadMoreHighlighted = !1),
				(this.options = e),
				(this.results = []),
				(this.selectivity = t),
				(this._closed = !1),
				(this._lastMousePosition = {}),
				(this.close = this.close.bind(this)),
				(this.position = this.position.bind(this)),
				t.options.closeOnSelect !== !1 && t.events.on("selectivity-selecting", this.close),
				this.addToDom(),
				this.showLoading(),
				e.showSearchInput && (t.initInput(this.$(".selectivity-search-input")), t.focus());
			var n = {};
			(n["click " + g] = this._loadMoreClicked),
				(n["click " + m] = this._resultClicked),
				(n["mouseenter " + g] = this._loadMoreHovered),
				(n["mouseenter " + m] = this._resultHovered),
				(this.events = new o(this.el, this)),
				this.events.on(n),
				this._attachScrollListeners(),
				this._suppressWheel(),
				setTimeout(this.triggerOpen.bind(this), 1);
		}
		var s = n(11),
			o = n(218),
			a = n(222),
			l = n(219),
			u = n(223),
			c = n(224),
			h = n(225),
			d = n(220),
			f = n(217),
			p = "highlight",
			v = "." + p,
			g = ".selectivity-load-more",
			m = ".selectivity-result-item",
			y = ["scroll", "touchend", "touchmove"];
		s(r.prototype, {
			$: function (t) {
				return this.el.querySelector(t);
			},
			addToDom: function () {
				this.selectivity.el.appendChild(this.el);
			},
			close: function () {
				this._closed || ((this._closed = !0), c(this.el), this.selectivity.events.off("selectivity-selecting", this.close), this.triggerClose(), this._removeScrollListeners());
			},
			highlight: function (t, e) {
				d(this.$(v), p, !1),
					d(this.$(a(m, t.id)), p, !0),
					(this.highlightedResult = t),
					(this.loadMoreHighlighted = !1),
					this.selectivity.triggerEvent("selectivity-highlight", { item: t, id: t.id, reason: (e && e.reason) || "unspecified" });
			},
			highlightLoadMore: function () {
				d(this.$(v), p, !1), d(this.$(g), p, !0), (this.highlightedResult = null), (this.loadMoreHighlighted = !0);
			},
			loadMore: function () {
				c(this.$(g)),
					(this.resultsContainer.innerHTML += this.selectivity.template("loading")),
					this.options.query({
						callback: function (t) {
							if (!t || !t.results) throw new Error("callback must be passed a response object");
							this._showResults(f.processItems(t.results), { add: !0, hasMore: !!t.more });
						}.bind(this),
						error: this._showResults.bind(this, [], { add: !0 }),
						offset: this.results.length,
						selectivity: this.selectivity,
						term: this.term,
					});
			},
			position: function () {
				var t = this.options.position;
				t && t(this.el, this.selectivity.el), this._scrolled();
			},
			renderItems: function (t) {
				var e = this.selectivity;
				return t
					.map(function (t) {
						var n = e.template(t.id ? "resultItem" : "resultLabel", t);
						return t.children && (n += e.template("resultChildren", { childrenHtml: this.renderItems(t.children) })), n;
					}, this)
					.join("");
			},
			search: function (t) {
				if (((this.term = t), this.options.items)) {
					t = f.transformText(t);
					var e = this.selectivity.options.matcher || f.matcher;
					this._showResults(
						this.options.items
							.map(function (n) {
								return e(n, t);
							})
							.filter(function (t) {
								return !!t;
							}),
						{ term: t }
					);
				} else
					this.options.query &&
						this.options.query({
							callback: function (e) {
								if (!e || !e.results) throw new Error("callback must be passed a response object");
								this._showResults(f.processItems(e.results), { hasMore: !!e.more, term: t });
							}.bind(this),
							error: this.showError.bind(this),
							offset: 0,
							selectivity: this.selectivity,
							term: t,
						});
			},
			selectHighlight: function () {
				this.highlightedResult ? this.selectItem(this.highlightedResult.id) : this.loadMoreHighlighted && this.loadMore();
			},
			selectItem: function (t) {
				var e = f.findNestedById(this.results, t);
				if (e && !e.disabled && e.selectable !== !1) {
					var n = { id: t, item: e };
					this.selectivity.triggerEvent("selectivity-selecting", n) && this.selectivity.triggerEvent("selectivity-selected", n);
				}
			},
			showError: function (t, e) {
				(this.resultsContainer.innerHTML = this.selectivity.template("error", { escape: !e || e.escape !== !1, message: t })),
					(this.hasMore = !1),
					(this.results = []),
					(this.highlightedResult = null),
					(this.loadMoreHighlighted = !1),
					this.position();
			},
			showLoading: function () {
				(this.resultsContainer.innerHTML = this.selectivity.template("loading")), (this.hasMore = !1), (this.results = []), (this.highlightedResult = null), (this.loadMoreHighlighted = !1), this.position();
			},
			showResults: function (t, e) {
				e.add ? c(this.$(".selectivity-loading")) : (this.resultsContainer.innerHTML = "");
				var n = this.selectivity.filterResults(t),
					i = this.renderItems(n);
				e.hasMore ? (i += this.selectivity.template("loadMore")) : i || e.add || (i = this.selectivity.template("noResults", { term: e.term })),
					(this.resultsContainer.innerHTML += i),
					(this.results = e.add ? this.results.concat(t) : t),
					(this.hasMore = e.hasMore);
				var r = this.selectivity.getValue();
				if (r && !Array.isArray(r)) {
					var s = f.findNestedById(t, r);
					s && this.highlight(s, { reason: "current_value" });
				} else this.options.highlightFirstItem === !1 || (e.add && !this.loadMoreHighlighted) || this._highlightFirstItem(n);
				this.position();
			},
			triggerClose: function () {
				this.selectivity.triggerEvent("selectivity-close");
			},
			triggerOpen: function () {
				this.selectivity.triggerEvent("selectivity-open");
			},
			_attachScrollListeners: function () {
				for (var t = 0; t < y.length; t++) window.addEventListener(y[t], this.position, !0);
				window.addEventListener("resize", this.position);
			},
			_highlightFirstItem: function (t) {
				function e(t) {
					for (var n = 0, i = t.length; n < i; n++) {
						var r = t[n];
						if (r.id) return r;
						if (r.children) {
							var s = e(r.children);
							if (s) return s;
						}
					}
				}
				var n = e(t);
				n ? this.highlight(n, { reason: "first_result" }) : ((this.highlightedResult = null), (this.loadMoreHighlighted = !1));
			},
			_loadMoreClicked: function (t) {
				this.loadMore(), h(t);
			},
			_loadMoreHovered: function (t) {
				(void 0 !== t.screenX && t.screenX === this._lastMousePosition.x && void 0 !== t.screenY && t.screenY === this._lastMousePosition.y) || (this.highlightLoadMore(), this._recordMousePosition(t));
			},
			_recordMousePosition: function (t) {
				this._lastMousePosition = { x: t.screenX, y: t.screenY };
			},
			_removeScrollListeners: function () {
				for (var t = 0; t < y.length; t++) window.removeEventListener(y[t], this.position, !0);
				window.removeEventListener("resize", this.position);
			},
			_resultClicked: function (t) {
				this.selectItem(this.selectivity.getRelatedItemId(t)), h(t);
			},
			_resultHovered: function (t) {
				if (!t.screenX || t.screenX !== this._lastMousePosition.x || !t.screenY || t.screenY !== this._lastMousePosition.y) {
					var e = this.selectivity.getRelatedItemId(t),
						n = f.findNestedById(this.results, e);
					n && !n.disabled && this.highlight(n, { reason: "hovered" }), this._recordMousePosition(t);
				}
			},
			_scrolled: function () {
				var t = this.$(g);
				t && t.offsetTop - this.resultsContainer.scrollTop < this.el.clientHeight && this.loadMore();
			},
			_showResults: function (t, e) {
				this.showResults(t, s({ dropdown: this }, e));
			},
			_suppressWheel: function () {
				var t = this.selectivity.options.suppressWheelSelector;
				if (null !== t) {
					var e = t || ".selectivity-results-container";
					this.events.on("wheel", e, function (t) {
						function n() {
							h(t), t.preventDefault();
						}
						var r = 0 === t.deltaMode ? t.deltaY : 40 * t.deltaY,
							s = i(t.target, e),
							o = s.clientHeight,
							a = s.scrollHeight,
							l = s.scrollTop;
						a > o && (r < -l ? ((s.scrollTop = 0), n()) : r > a - o - l && ((s.scrollTop = a), n()));
					});
				}
			},
		}),
			(t.exports = f.Dropdown = r);
	},
	function (t, e) {
		"use strict";
		t.exports = function (t, e) {
			var n = '"' + ("" + e).replace(/\\/g, "\\\\").replace(/"/g, '\\"') + '"';
			return t + "[data-item-id=" + n + "]";
		};
	},
	function (t, e) {
		"use strict";
		t.exports = function (t) {
			var e = document.createElement("div");
			return (e.innerHTML = t), e.firstChild;
		};
	},
	function (t, e) {
		"use strict";
		t.exports = function (t) {
			t && t.parentNode && t.parentNode.removeChild(t);
		};
	},
	function (t, e) {
		"use strict";
		t.exports = function (t) {
			t.stopPropagation();
		};
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			var e = t.indexOf("@");
			if (e === -1 || t.indexOf(" ") > -1) return !1;
			var n = t.lastIndexOf(".");
			return n === -1 ? e < t.length - 2 : !(n > e) || n < t.length - 2;
		}
		function r(t, e) {
			e = void 0 === e ? t.length : e;
			for (var n = e - 1; n >= 0; n--) if (/\s/.test(t[n])) return t.slice(n + 1, e);
			return t.slice(0, e);
		}
		function s(t, e) {
			return t.charAt(0) === e[0] && t.slice(-1) === e[1] ? t.slice(1, -1).trim() : t.trim();
		}
		function o(t) {
			var e = r(t),
				n = t.slice(0, -e.length).trim();
			return i(e) ? ((e = s(s(e, "()"), "<>")), (n = s(n, '""').trim() || e), { id: e, text: n }) : t.trim() ? { id: t, text: t } : null;
		}
		function a(t, e, n) {
			function s(t) {
				if (t)
					for (var e = 0, n = t.length; e < n; e++)
						switch (t[e]) {
							case ";":
							case ",":
							case "\n":
								return !0;
							case " ":
							case "\t":
								if (i(r(t, e))) return !0;
								break;
							case '"':
								do e++;
								while (e < n && '"' !== t[e]);
								break;
							default:
								continue;
						}
				return !1;
			}
			function a(t) {
				for (var e = 0, n = t.length; e < n; e++)
					switch (t[e]) {
						case ";":
						case ",":
						case "\n":
							return { term: t.slice(0, e), input: t.slice(e + 1) };
						case " ":
						case "\t":
							if (i(r(t, e))) return { term: t.slice(0, e), input: t.slice(e + 1) };
							break;
						case '"':
							do e++;
							while (e < n && '"' !== t[e]);
							break;
						default:
							continue;
					}
				return {};
			}
			for (; s(t);) {
				var l = a(t);
				if (l.term) {
					var u = o(l.term);
					!u || (u.id && h.findById(e, u.id)) || n(u);
				}
				t = l.input;
			}
			return t;
		}
		function l(t) {
			c.call(this, u({ createTokenItem: o, showDropdown: !1, tokenizer: a }, t)),
				this.events.on("blur", function () {
					var t = this.input;
					t && i(r(t.value)) && this.add(o(t.value));
				});
		}
		var u = n(11),
			c = n(227),
			h = n(217);
		h.inherits(l, c), (t.exports = h.Inputs.Email = l);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			o.call(
				this,
				r(
					{
						positionDropdown: function (t, e) {
							var n = e.getBoundingClientRect(),
								i = t.clientHeight,
								s = n.bottom + i > window.innerHeight && n.top - i > 0;
							r(t.style, { left: n.left + "px", top: (s ? n.top - i : n.bottom) + "px", width: n.width + "px" });
						},
						showSearchInputInDropdown: !1,
					},
					t
				)
			),
				this._reset();
			var e = { change: this.rerenderSelection, click: this._clicked, "selectivity-selected": this._resultSelected };
			(e["change " + g] = h),
				(e["click " + m] = this._itemClicked),
				(e["click " + m + "-remove"] = this._itemRemoveClicked),
				(e["keydown " + g] = this._keyHeld),
				(e["keyup " + g] = this._keyReleased),
				(e["paste " + g] = this._onPaste),
				this.events.on(e);
		}
		var r = n(11),
			s = n(184),
			o = n(217),
			a = n(222),
			l = n(228),
			u = n(223),
			c = n(224),
			h = n(225),
			d = n(220),
			f = 8,
			p = 46,
			v = 13,
			g = ".selectivity-multiple-input",
			m = ".selectivity-multiple-selected-item",
			y = "ontouchstart" in window,
			b = o.inherits(i, o, {
				add: function (t) {
					var e = o.isValidId(t),
						n = e ? t : this.validateItem(t) && t.id;
					this._value.indexOf(n) === -1 &&
						(this._value.push(n),
							e && this.options.initSelection
								? this.options.initSelection(
									[n],
									function (e) {
										this._value.indexOf(n) > -1 && ((t = this.validateItem(e[0])), this._data.push(t), this.triggerChange({ added: t }));
									}.bind(this)
								)
								: (e && (t = this.getItemForId(n)), this._data.push(t), this.triggerChange({ added: t }))),
						(this.input.value = ""),
						this._updateInputWidth();
				},
				clear: function () {
					this.setData([]);
				},
				filterResults: function (t) {
					return (
						(t = t.map(function (t) {
							var e = { id: t.id, text: t.text };
							return t.children && (e.children = this.filterResults(t.children)), e;
						}, this)),
						t.filter(function (t) {
							return !o.findById(this._data, t.id);
						}, this)
					);
				},
				getDataForValue: function (t) {
					return t.map(this.getItemForId, this).filter(function (t) {
						return !!t;
					});
				},
				getValueForData: function (t) {
					return t.map(function (t) {
						return t.id;
					});
				},
				remove: function (t) {
					var e,
						n = t.id || t,
						i = o.findIndexById(this._data, n);
					i > -1 && ((e = this._data[i]), this._data.splice(i, 1)),
						this._value[i] !== n && (i = this._value.indexOf(n)),
						i > -1 && this._value.splice(i, 1),
						e && this.triggerChange({ removed: e }),
						n === this._highlightedItemId && (this._highlightedItemId = null),
						this._updateInputWidth();
				},
				rerenderSelection: function (t) {
					(t = t || {}),
						t.added
							? (this._renderSelectedItem(t.added), this._scrollToBottom())
							: t.removed
								? c(this.$(a(m, t.removed.id)))
								: (this._forEachSelectedItem(c), this._data.forEach(this._renderSelectedItem, this), this._updateInputWidth()),
						(t.added || t.removed) && (this.dropdown && this.dropdown.showResults(this.filterResults(this.dropdown.results), { hasMore: this.dropdown.hasMore }), y || this.focus()),
						this.positionDropdown(),
						this._updatePlaceholder();
				},
				search: function (t) {
					this.options.tokenizer && ((t = this.options.tokenizer(t, this._data, this.add.bind(this), this.options)), s(t) && t !== this.input.value && (this.input.value = t)),
						this._updateInputWidth(),
						this.dropdown && b(this, "search", t);
				},
				setOptions: function (t) {
					var e = this.enabled;
					b(this, "setOptions", t), e !== this.enabled && this._reset();
				},
				validateData: function (t) {
					if (null === t) return [];
					if (Array.isArray(t)) return t.map(this.validateItem, this);
					throw new Error("Data for MultiSelectivity instance should be an array");
				},
				validateValue: function (t) {
					if (null === t) return [];
					if (Array.isArray(t)) {
						if (t.every(o.isValidId)) return t;
						throw new Error("Value contains invalid IDs");
					}
					throw new Error("Value for MultiSelectivity instance should be an array");
				},
				_backspacePressed: function () {
					this.options.backspaceHighlightsBeforeDelete
						? this._highlightedItemId
							? this._deletePressed()
							: this._value.length && this._highlightItem(this._value.slice(-1)[0])
						: this._value.length && this.remove(this._value.slice(-1)[0]);
				},
				_clicked: function (t) {
					this.enabled && (this.options.showDropdown !== !1 ? this.open() : this.focus(), h(t));
				},
				_createToken: function () {
					var t = this.input.value,
						e = this.options.createTokenItem;
					if (t && e) {
						var n = e(t);
						n && this.add(n);
					}
				},
				_deletePressed: function () {
					this._highlightedItemId && this.remove(this._highlightedItemId);
				},
				_forEachSelectedItem: function (t) {
					Array.prototype.forEach.call(this.el.querySelectorAll(m), t);
				},
				_highlightItem: function (t) {
					(this._highlightedItemId = t),
						this._forEachSelectedItem(function (e) {
							d(e, "highlighted", e.getAttribute("data-item-id") === t);
						}),
						y || this.focus();
				},
				_itemClicked: function (t) {
					this.enabled && this._highlightItem(this.getRelatedItemId(t));
				},
				_itemRemoveClicked: function (t) {
					this.remove(this.getRelatedItemId(t)), h(t);
				},
				_keyHeld: function (t) {
					(this._originalValue = this.input.value), l(t) !== v || t.ctrlKey || t.preventDefault();
				},
				_keyReleased: function (t) {
					var e = !!this._originalValue,
						n = l(t);
					n !== v || t.ctrlKey ? (n !== f || e ? n !== p || e || this._deletePressed() : this._backspacePressed()) : this._createToken();
				},
				_onPaste: function () {
					setTimeout(
						function () {
							this.search(this.input.value), this._createToken();
						}.bind(this),
						10
					);
				},
				_renderSelectedItem: function (t) {
					var e = u(this.template("multipleSelectedItem", r({ highlighted: t.id === this._highlightedItemId, removable: !this.options.readOnly }, t)));
					this.input.parentNode.insertBefore(e, this.input);
				},
				_reset: function () {
					(this.el.innerHTML = this.template("multipleSelectInput", { enabled: this.enabled })), (this._highlightedItemId = null), this.initInput(this.$(g)), this.rerenderSelection();
				},
				_resultSelected: function (t) {
					this._value.indexOf(t.id) === -1 ? this.add(t.item) : this.remove(t.item);
				},
				_scrollToBottom: function () {
					var t = this.$(g + "-container");
					t.scrollTop = t.clientHeight;
				},
				_updateInputWidth: function () {
					if (this.enabled) {
						var t = this.input.value || (!this._data.length && this.options.placeholder) || "";
						this.input.setAttribute("size", t.length + 2), this.positionDropdown();
					}
				},
				_updatePlaceholder: function () {
					var t = (!this._data.length && this.options.placeholder) || "";
					this.enabled ? this.input.setAttribute("placeholder", t) : (this.$(".selectivity-placeholder").textContent = t);
				},
			});
		t.exports = o.Inputs.Multiple = i;
	},
	function (t, e) {
		"use strict";
		t.exports = function (t) {
			return t.which || t.keyCode || 0;
		};
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			s.call(
				this,
				r(
					{
						positionDropdown: function (t, e) {
							var n = e.getBoundingClientRect(),
								i = n.bottom,
								s = Math.min(Math.max(i + t.clientHeight - window.innerHeight, 0), n.top + n.height);
							r(t.style, { left: n.left + "px", top: i - s + "px", width: n.width + "px" });
						},
					},
					t
				)
			),
				this.rerender(),
				t.showSearchInputInDropdown === !1 && this.initInput(this.$(".selectivity-single-select-input"), { search: !1 }),
				this.events.on({
					change: this.rerenderSelection,
					click: this._clicked,
					"click .selectivity-search-input": o,
					"click .selectivity-single-selected-item-remove": this._itemRemoveClicked,
					"focus .selectivity-single-select-input": this._focused,
					"selectivity-selected": this._resultSelected,
				});
		}
		var r = n(11),
			s = n(217),
			o = n(225),
			a = s.inherits(i, s, {
				clear: function () {
					this.setData(null);
				},
				close: function (t) {
					(this._closing = !0), a(this, "close"), t && t.keepFocus && this.input && this.input.focus(), (this._closing = !1);
				},
				getDataForValue: function (t) {
					return this.getItemForId(t);
				},
				getValueForData: function (t) {
					return t ? t.id : null;
				},
				rerender: function () {
					(this.el.innerHTML = this.template("singleSelectInput", this.options)), this.rerenderSelection();
				},
				rerenderSelection: function () {
					var t = this._data ? "singleSelectedItem" : "singleSelectPlaceholder",
						e = this._data ? r({ removable: this.options.allowClear && !this.options.readOnly }, this._data) : { placeholder: this.options.placeholder };
					(this.el.querySelector("input").value = this._value), (this.$(".selectivity-single-result-container").innerHTML = this.template(t, e));
				},
				setOptions: function (t) {
					var e = this.enabled;
					a(this, "setOptions", t), e !== this.enabled && this.rerender();
				},
				validateData: function (t) {
					return null === t ? t : this.validateItem(t);
				},
				validateValue: function (t) {
					if (null === t || s.isValidId(t)) return t;
					throw new Error("Value for SingleSelectivity instance should be a valid ID or null");
				},
				_clicked: function () {
					this.enabled && (this.dropdown ? this.close({ keepFocus: !0 }) : this.options.showDropdown !== !1 && this.open());
				},
				_focused: function () {
					!this.enabled || this._closing || this._opening || this.options.showDropdown === !1 || this.open();
				},
				_itemRemoveClicked: function (t) {
					this.setData(null), o(t);
				},
				_resultSelected: function (t) {
					this.setData(t.item), this.close({ keepFocus: !0 });
				},
			});
		t.exports = s.Inputs.Single = i;
	},
	function (t, e, n) {
		"use strict";
		var i = n(204),
			r = n(217);
		t.exports = r.Locale = {
			loading: "Loading...",
			loadMore: "Load more...",
			noResults: "No results found",
			ajaxError: function (t) {
				return t ? "Failed to fetch results for <b>" + i(t) + "</b>" : "Failed to fetch results";
			},
			needMoreCharacters: function (t) {
				return "Enter " + t + " more characters to search";
			},
			noResultsForTerm: function (t) {
				return "No results for <b>" + i(t) + "</b>";
			},
		};
	},
	function (t, e, n) {
		"use strict";
		function i(t, e, n) {
			return t + (t.indexOf("?") > -1 ? "&" : "?") + e + "=" + encodeURIComponent(n);
		}
		function r(t, e) {
			var n = {};
			return (
				e.forEach(function (e) {
					void 0 !== t[e] && (n[e] = t[e]);
				}),
				n
			);
		}
		function s(t, e) {
			var n = t.fetch || window.fetch,
				s = e.term,
				o = "function" == typeof t.url ? t.url(e) : t.url;
			if (t.params) {
				var a = t.params(s, e.offset || 0);
				for (var u in a) a.hasOwnProperty(u) && (o = i(o, u, a[u]));
			}
			var c = r(t, ["body", "cache", "credentials", "headers", "integrity", "method", "mode", "redirect", "referrer", "referrerPolicy"]);
			n(o, c, e)
				.then(function (t) {
					if (t.ok) return t.json();
					if (Array.isArray(t) || t.results) return t;
					throw new Error("Unexpected AJAX response");
				})
				.then(function (t) {
					Array.isArray(t) ? e.callback({ results: t, more: !1 }) : e.callback({ results: t.results, more: !!t.more });
				})
			["catch"](function (n) {
				var i = t.formatError || l.ajaxError;
				e.error(i(s, n), { escape: !1 });
			});
		}
		var o = n(178),
			a = n(217),
			l = n(230);
		a.OptionListeners.unshift(function (t, e) {
			var n = e.ajax;
			if (n && n.url) {
				var i = n.quietMillis ? o(s, n.quietMillis) : s;
				e.query = function (t) {
					var e = n.minimumInputLength - t.term.length;
					return e > 0 ? void t.error(l.needMoreCharacters(e)) : void i(n, t);
				};
			}
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(217),
			r = 0;
		i.OptionListeners.push(function (t, e) {
			var n = e.query;
			n &&
				!n._async &&
				((e.query = function (t) {
					r++;
					var e = r,
						i = t.callback,
						s = t.error;
					(t.callback = function () {
						e === r && i.apply(null, arguments);
					}),
						(t.error = function () {
							e === r && s.apply(null, arguments);
						}),
						n(t);
				}),
					(e.query._async = !0));
		});
	},
	function (t, e, n) {
		"use strict";
		var i = {
			"Ⓐ": "A",
			Ａ: "A",
			À: "A",
			Á: "A",
			Â: "A",
			Ầ: "A",
			Ấ: "A",
			Ẫ: "A",
			Ẩ: "A",
			Ã: "A",
			Ā: "A",
			Ă: "A",
			Ằ: "A",
			Ắ: "A",
			Ẵ: "A",
			Ẳ: "A",
			Ȧ: "A",
			Ǡ: "A",
			Ä: "A",
			Ǟ: "A",
			Ả: "A",
			Å: "A",
			Ǻ: "A",
			Ǎ: "A",
			Ȁ: "A",
			Ȃ: "A",
			Ạ: "A",
			Ậ: "A",
			Ặ: "A",
			Ḁ: "A",
			Ą: "A",
			Ⱥ: "A",
			Ɐ: "A",
			Ꜳ: "AA",
			Æ: "AE",
			Ǽ: "AE",
			Ǣ: "AE",
			Ꜵ: "AO",
			Ꜷ: "AU",
			Ꜹ: "AV",
			Ꜻ: "AV",
			Ꜽ: "AY",
			"Ⓑ": "B",
			Ｂ: "B",
			Ḃ: "B",
			Ḅ: "B",
			Ḇ: "B",
			Ƀ: "B",
			Ƃ: "B",
			Ɓ: "B",
			"Ⓒ": "C",
			Ｃ: "C",
			Ć: "C",
			Ĉ: "C",
			Ċ: "C",
			Č: "C",
			Ç: "C",
			Ḉ: "C",
			Ƈ: "C",
			Ȼ: "C",
			Ꜿ: "C",
			"Ⓓ": "D",
			Ｄ: "D",
			Ḋ: "D",
			Ď: "D",
			Ḍ: "D",
			Ḑ: "D",
			Ḓ: "D",
			Ḏ: "D",
			Đ: "D",
			Ƌ: "D",
			Ɗ: "D",
			Ɖ: "D",
			Ꝺ: "D",
			Ǳ: "DZ",
			Ǆ: "DZ",
			ǲ: "Dz",
			ǅ: "Dz",
			"Ⓔ": "E",
			Ｅ: "E",
			È: "E",
			É: "E",
			Ê: "E",
			Ề: "E",
			Ế: "E",
			Ễ: "E",
			Ể: "E",
			Ẽ: "E",
			Ē: "E",
			Ḕ: "E",
			Ḗ: "E",
			Ĕ: "E",
			Ė: "E",
			Ë: "E",
			Ẻ: "E",
			Ě: "E",
			Ȅ: "E",
			Ȇ: "E",
			Ẹ: "E",
			Ệ: "E",
			Ȩ: "E",
			Ḝ: "E",
			Ę: "E",
			Ḙ: "E",
			Ḛ: "E",
			Ɛ: "E",
			Ǝ: "E",
			"Ⓕ": "F",
			Ｆ: "F",
			Ḟ: "F",
			Ƒ: "F",
			Ꝼ: "F",
			"Ⓖ": "G",
			Ｇ: "G",
			Ǵ: "G",
			Ĝ: "G",
			Ḡ: "G",
			Ğ: "G",
			Ġ: "G",
			Ǧ: "G",
			Ģ: "G",
			Ǥ: "G",
			Ɠ: "G",
			Ꞡ: "G",
			Ᵹ: "G",
			Ꝿ: "G",
			"Ⓗ": "H",
			Ｈ: "H",
			Ĥ: "H",
			Ḣ: "H",
			Ḧ: "H",
			Ȟ: "H",
			Ḥ: "H",
			Ḩ: "H",
			Ḫ: "H",
			Ħ: "H",
			Ⱨ: "H",
			Ⱶ: "H",
			Ɥ: "H",
			"Ⓘ": "I",
			Ｉ: "I",
			Ì: "I",
			Í: "I",
			Î: "I",
			Ĩ: "I",
			Ī: "I",
			Ĭ: "I",
			İ: "I",
			Ï: "I",
			Ḯ: "I",
			Ỉ: "I",
			Ǐ: "I",
			Ȉ: "I",
			Ȋ: "I",
			Ị: "I",
			Į: "I",
			Ḭ: "I",
			Ɨ: "I",
			"Ⓙ": "J",
			Ｊ: "J",
			Ĵ: "J",
			Ɉ: "J",
			"Ⓚ": "K",
			Ｋ: "K",
			Ḱ: "K",
			Ǩ: "K",
			Ḳ: "K",
			Ķ: "K",
			Ḵ: "K",
			Ƙ: "K",
			Ⱪ: "K",
			Ꝁ: "K",
			Ꝃ: "K",
			Ꝅ: "K",
			Ꞣ: "K",
			"Ⓛ": "L",
			Ｌ: "L",
			Ŀ: "L",
			Ĺ: "L",
			Ľ: "L",
			Ḷ: "L",
			Ḹ: "L",
			Ļ: "L",
			Ḽ: "L",
			Ḻ: "L",
			Ł: "L",
			Ƚ: "L",
			Ɫ: "L",
			Ⱡ: "L",
			Ꝉ: "L",
			Ꝇ: "L",
			Ꞁ: "L",
			Ǉ: "LJ",
			ǈ: "Lj",
			"Ⓜ": "M",
			Ｍ: "M",
			Ḿ: "M",
			Ṁ: "M",
			Ṃ: "M",
			Ɱ: "M",
			Ɯ: "M",
			"Ⓝ": "N",
			Ｎ: "N",
			Ǹ: "N",
			Ń: "N",
			Ñ: "N",
			Ṅ: "N",
			Ň: "N",
			Ṇ: "N",
			Ņ: "N",
			Ṋ: "N",
			Ṉ: "N",
			Ƞ: "N",
			Ɲ: "N",
			Ꞑ: "N",
			Ꞥ: "N",
			Ǌ: "NJ",
			ǋ: "Nj",
			"Ⓞ": "O",
			Ｏ: "O",
			Ò: "O",
			Ó: "O",
			Ô: "O",
			Ồ: "O",
			Ố: "O",
			Ỗ: "O",
			Ổ: "O",
			Õ: "O",
			Ṍ: "O",
			Ȭ: "O",
			Ṏ: "O",
			Ō: "O",
			Ṑ: "O",
			Ṓ: "O",
			Ŏ: "O",
			Ȯ: "O",
			Ȱ: "O",
			Ö: "O",
			Ȫ: "O",
			Ỏ: "O",
			Ő: "O",
			Ǒ: "O",
			Ȍ: "O",
			Ȏ: "O",
			Ơ: "O",
			Ờ: "O",
			Ớ: "O",
			Ỡ: "O",
			Ở: "O",
			Ợ: "O",
			Ọ: "O",
			Ộ: "O",
			Ǫ: "O",
			Ǭ: "O",
			Ø: "O",
			Ǿ: "O",
			Ɔ: "O",
			Ɵ: "O",
			Ꝋ: "O",
			Ꝍ: "O",
			Ƣ: "OI",
			Ꝏ: "OO",
			Ȣ: "OU",
			"Ⓟ": "P",
			Ｐ: "P",
			Ṕ: "P",
			Ṗ: "P",
			Ƥ: "P",
			Ᵽ: "P",
			Ꝑ: "P",
			Ꝓ: "P",
			Ꝕ: "P",
			"Ⓠ": "Q",
			Ｑ: "Q",
			Ꝗ: "Q",
			Ꝙ: "Q",
			Ɋ: "Q",
			"Ⓡ": "R",
			Ｒ: "R",
			Ŕ: "R",
			Ṙ: "R",
			Ř: "R",
			Ȑ: "R",
			Ȓ: "R",
			Ṛ: "R",
			Ṝ: "R",
			Ŗ: "R",
			Ṟ: "R",
			Ɍ: "R",
			Ɽ: "R",
			Ꝛ: "R",
			Ꞧ: "R",
			Ꞃ: "R",
			"Ⓢ": "S",
			Ｓ: "S",
			ẞ: "S",
			Ś: "S",
			Ṥ: "S",
			Ŝ: "S",
			Ṡ: "S",
			Š: "S",
			Ṧ: "S",
			Ṣ: "S",
			Ṩ: "S",
			Ș: "S",
			Ş: "S",
			Ȿ: "S",
			Ꞩ: "S",
			Ꞅ: "S",
			"Ⓣ": "T",
			Ｔ: "T",
			Ṫ: "T",
			Ť: "T",
			Ṭ: "T",
			Ț: "T",
			Ţ: "T",
			Ṱ: "T",
			Ṯ: "T",
			Ŧ: "T",
			Ƭ: "T",
			Ʈ: "T",
			Ⱦ: "T",
			Ꞇ: "T",
			Ꜩ: "TZ",
			"Ⓤ": "U",
			Ｕ: "U",
			Ù: "U",
			Ú: "U",
			Û: "U",
			Ũ: "U",
			Ṹ: "U",
			Ū: "U",
			Ṻ: "U",
			Ŭ: "U",
			Ü: "U",
			Ǜ: "U",
			Ǘ: "U",
			Ǖ: "U",
			Ǚ: "U",
			Ủ: "U",
			Ů: "U",
			Ű: "U",
			Ǔ: "U",
			Ȕ: "U",
			Ȗ: "U",
			Ư: "U",
			Ừ: "U",
			Ứ: "U",
			Ữ: "U",
			Ử: "U",
			Ự: "U",
			Ụ: "U",
			Ṳ: "U",
			Ų: "U",
			Ṷ: "U",
			Ṵ: "U",
			Ʉ: "U",
			"Ⓥ": "V",
			Ｖ: "V",
			Ṽ: "V",
			Ṿ: "V",
			Ʋ: "V",
			Ꝟ: "V",
			Ʌ: "V",
			Ꝡ: "VY",
			"Ⓦ": "W",
			Ｗ: "W",
			Ẁ: "W",
			Ẃ: "W",
			Ŵ: "W",
			Ẇ: "W",
			Ẅ: "W",
			Ẉ: "W",
			Ⱳ: "W",
			"Ⓧ": "X",
			Ｘ: "X",
			Ẋ: "X",
			Ẍ: "X",
			"Ⓨ": "Y",
			Ｙ: "Y",
			Ỳ: "Y",
			Ý: "Y",
			Ŷ: "Y",
			Ỹ: "Y",
			Ȳ: "Y",
			Ẏ: "Y",
			Ÿ: "Y",
			Ỷ: "Y",
			Ỵ: "Y",
			Ƴ: "Y",
			Ɏ: "Y",
			Ỿ: "Y",
			"Ⓩ": "Z",
			Ｚ: "Z",
			Ź: "Z",
			Ẑ: "Z",
			Ż: "Z",
			Ž: "Z",
			Ẓ: "Z",
			Ẕ: "Z",
			Ƶ: "Z",
			Ȥ: "Z",
			Ɀ: "Z",
			Ⱬ: "Z",
			Ꝣ: "Z",
			"ⓐ": "a",
			ａ: "a",
			ẚ: "a",
			à: "a",
			á: "a",
			â: "a",
			ầ: "a",
			ấ: "a",
			ẫ: "a",
			ẩ: "a",
			ã: "a",
			ā: "a",
			ă: "a",
			ằ: "a",
			ắ: "a",
			ẵ: "a",
			ẳ: "a",
			ȧ: "a",
			ǡ: "a",
			ä: "a",
			ǟ: "a",
			ả: "a",
			å: "a",
			ǻ: "a",
			ǎ: "a",
			ȁ: "a",
			ȃ: "a",
			ạ: "a",
			ậ: "a",
			ặ: "a",
			ḁ: "a",
			ą: "a",
			ⱥ: "a",
			ɐ: "a",
			ꜳ: "aa",
			æ: "ae",
			ǽ: "ae",
			ǣ: "ae",
			ꜵ: "ao",
			ꜷ: "au",
			ꜹ: "av",
			ꜻ: "av",
			ꜽ: "ay",
			"ⓑ": "b",
			ｂ: "b",
			ḃ: "b",
			ḅ: "b",
			ḇ: "b",
			ƀ: "b",
			ƃ: "b",
			ɓ: "b",
			"ⓒ": "c",
			ｃ: "c",
			ć: "c",
			ĉ: "c",
			ċ: "c",
			č: "c",
			ç: "c",
			ḉ: "c",
			ƈ: "c",
			ȼ: "c",
			ꜿ: "c",
			ↄ: "c",
			"ⓓ": "d",
			ｄ: "d",
			ḋ: "d",
			ď: "d",
			ḍ: "d",
			ḑ: "d",
			ḓ: "d",
			ḏ: "d",
			đ: "d",
			ƌ: "d",
			ɖ: "d",
			ɗ: "d",
			ꝺ: "d",
			ǳ: "dz",
			ǆ: "dz",
			"ⓔ": "e",
			ｅ: "e",
			è: "e",
			é: "e",
			ê: "e",
			ề: "e",
			ế: "e",
			ễ: "e",
			ể: "e",
			ẽ: "e",
			ē: "e",
			ḕ: "e",
			ḗ: "e",
			ĕ: "e",
			ė: "e",
			ë: "e",
			ẻ: "e",
			ě: "e",
			ȅ: "e",
			ȇ: "e",
			ẹ: "e",
			ệ: "e",
			ȩ: "e",
			ḝ: "e",
			ę: "e",
			ḙ: "e",
			ḛ: "e",
			ɇ: "e",
			ɛ: "e",
			ǝ: "e",
			"ⓕ": "f",
			ｆ: "f",
			ḟ: "f",
			ƒ: "f",
			ꝼ: "f",
			"ⓖ": "g",
			ｇ: "g",
			ǵ: "g",
			ĝ: "g",
			ḡ: "g",
			ğ: "g",
			ġ: "g",
			ǧ: "g",
			ģ: "g",
			ǥ: "g",
			ɠ: "g",
			ꞡ: "g",
			ᵹ: "g",
			ꝿ: "g",
			"ⓗ": "h",
			ｈ: "h",
			ĥ: "h",
			ḣ: "h",
			ḧ: "h",
			ȟ: "h",
			ḥ: "h",
			ḩ: "h",
			ḫ: "h",
			ẖ: "h",
			ħ: "h",
			ⱨ: "h",
			ⱶ: "h",
			ɥ: "h",
			ƕ: "hv",
			"ⓘ": "i",
			ｉ: "i",
			ì: "i",
			í: "i",
			î: "i",
			ĩ: "i",
			ī: "i",
			ĭ: "i",
			ï: "i",
			ḯ: "i",
			ỉ: "i",
			ǐ: "i",
			ȉ: "i",
			ȋ: "i",
			ị: "i",
			į: "i",
			ḭ: "i",
			ɨ: "i",
			ı: "i",
			"ⓙ": "j",
			ｊ: "j",
			ĵ: "j",
			ǰ: "j",
			ɉ: "j",
			"ⓚ": "k",
			ｋ: "k",
			ḱ: "k",
			ǩ: "k",
			ḳ: "k",
			ķ: "k",
			ḵ: "k",
			ƙ: "k",
			ⱪ: "k",
			ꝁ: "k",
			ꝃ: "k",
			ꝅ: "k",
			ꞣ: "k",
			"ⓛ": "l",
			ｌ: "l",
			ŀ: "l",
			ĺ: "l",
			ľ: "l",
			ḷ: "l",
			ḹ: "l",
			ļ: "l",
			ḽ: "l",
			ḻ: "l",
			ſ: "l",
			ł: "l",
			ƚ: "l",
			ɫ: "l",
			ⱡ: "l",
			ꝉ: "l",
			ꞁ: "l",
			ꝇ: "l",
			ǉ: "lj",
			"ⓜ": "m",
			ｍ: "m",
			ḿ: "m",
			ṁ: "m",
			ṃ: "m",
			ɱ: "m",
			ɯ: "m",
			"ⓝ": "n",
			ｎ: "n",
			ǹ: "n",
			ń: "n",
			ñ: "n",
			ṅ: "n",
			ň: "n",
			ṇ: "n",
			ņ: "n",
			ṋ: "n",
			ṉ: "n",
			ƞ: "n",
			ɲ: "n",
			ŉ: "n",
			ꞑ: "n",
			ꞥ: "n",
			ǌ: "nj",
			"ⓞ": "o",
			ｏ: "o",
			ò: "o",
			ó: "o",
			ô: "o",
			ồ: "o",
			ố: "o",
			ỗ: "o",
			ổ: "o",
			õ: "o",
			ṍ: "o",
			ȭ: "o",
			ṏ: "o",
			ō: "o",
			ṑ: "o",
			ṓ: "o",
			ŏ: "o",
			ȯ: "o",
			ȱ: "o",
			ö: "o",
			ȫ: "o",
			ỏ: "o",
			ő: "o",
			ǒ: "o",
			ȍ: "o",
			ȏ: "o",
			ơ: "o",
			ờ: "o",
			ớ: "o",
			ỡ: "o",
			ở: "o",
			ợ: "o",
			ọ: "o",
			ộ: "o",
			ǫ: "o",
			ǭ: "o",
			ø: "o",
			ǿ: "o",
			ɔ: "o",
			ꝋ: "o",
			ꝍ: "o",
			ɵ: "o",
			ƣ: "oi",
			ȣ: "ou",
			ꝏ: "oo",
			"ⓟ": "p",
			ｐ: "p",
			ṕ: "p",
			ṗ: "p",
			ƥ: "p",
			ᵽ: "p",
			ꝑ: "p",
			ꝓ: "p",
			ꝕ: "p",
			"ⓠ": "q",
			ｑ: "q",
			ɋ: "q",
			ꝗ: "q",
			ꝙ: "q",
			"ⓡ": "r",
			ｒ: "r",
			ŕ: "r",
			ṙ: "r",
			ř: "r",
			ȑ: "r",
			ȓ: "r",
			ṛ: "r",
			ṝ: "r",
			ŗ: "r",
			ṟ: "r",
			ɍ: "r",
			ɽ: "r",
			ꝛ: "r",
			ꞧ: "r",
			ꞃ: "r",
			"ⓢ": "s",
			ｓ: "s",
			ß: "s",
			ś: "s",
			ṥ: "s",
			ŝ: "s",
			ṡ: "s",
			š: "s",
			ṧ: "s",
			ṣ: "s",
			ṩ: "s",
			ș: "s",
			ş: "s",
			ȿ: "s",
			ꞩ: "s",
			ꞅ: "s",
			ẛ: "s",
			"ⓣ": "t",
			ｔ: "t",
			ṫ: "t",
			ẗ: "t",
			ť: "t",
			ṭ: "t",
			ț: "t",
			ţ: "t",
			ṱ: "t",
			ṯ: "t",
			ŧ: "t",
			ƭ: "t",
			ʈ: "t",
			ⱦ: "t",
			ꞇ: "t",
			ꜩ: "tz",
			"ⓤ": "u",
			ｕ: "u",
			ù: "u",
			ú: "u",
			û: "u",
			ũ: "u",
			ṹ: "u",
			ū: "u",
			ṻ: "u",
			ŭ: "u",
			ü: "u",
			ǜ: "u",
			ǘ: "u",
			ǖ: "u",
			ǚ: "u",
			ủ: "u",
			ů: "u",
			ű: "u",
			ǔ: "u",
			ȕ: "u",
			ȗ: "u",
			ư: "u",
			ừ: "u",
			ứ: "u",
			ữ: "u",
			ử: "u",
			ự: "u",
			ụ: "u",
			ṳ: "u",
			ų: "u",
			ṷ: "u",
			ṵ: "u",
			ʉ: "u",
			"ⓥ": "v",
			ｖ: "v",
			ṽ: "v",
			ṿ: "v",
			ʋ: "v",
			ꝟ: "v",
			ʌ: "v",
			ꝡ: "vy",
			"ⓦ": "w",
			ｗ: "w",
			ẁ: "w",
			ẃ: "w",
			ŵ: "w",
			ẇ: "w",
			ẅ: "w",
			ẘ: "w",
			ẉ: "w",
			ⱳ: "w",
			"ⓧ": "x",
			ｘ: "x",
			ẋ: "x",
			ẍ: "x",
			"ⓨ": "y",
			ｙ: "y",
			ỳ: "y",
			ý: "y",
			ŷ: "y",
			ỹ: "y",
			ȳ: "y",
			ẏ: "y",
			ÿ: "y",
			ỷ: "y",
			ẙ: "y",
			ỵ: "y",
			ƴ: "y",
			ɏ: "y",
			ỿ: "y",
			"ⓩ": "z",
			ｚ: "z",
			ź: "z",
			ẑ: "z",
			ż: "z",
			ž: "z",
			ẓ: "z",
			ẕ: "z",
			ƶ: "z",
			ȥ: "z",
			ɀ: "z",
			ⱬ: "z",
			ꝣ: "z",
			Ά: "Α",
			Έ: "Ε",
			Ή: "Η",
			Ί: "Ι",
			Ϊ: "Ι",
			Ό: "Ο",
			Ύ: "Υ",
			Ϋ: "Υ",
			Ώ: "Ω",
			ά: "α",
			έ: "ε",
			ή: "η",
			ί: "ι",
			ϊ: "ι",
			ΐ: "ι",
			ό: "ο",
			ύ: "υ",
			ϋ: "υ",
			ΰ: "υ",
			ω: "ω",
			ς: "σ",
		},
			r = n(217),
			s = r.transformText;
		r.transformText = function (t) {
			for (var e = "", n = 0, r = t.length; n < r; n++) {
				var o = t[n];
				e += i[o] || o;
			}
			return s(e);
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(3),
			r = n(217);
		r.OptionListeners.unshift(function (t, e) {
			var n = e.ajax;
			n &&
				n.url &&
				!n.fetch &&
				i.Deferred &&
				(n.fetch = function (t, e) {
					return i.ajax(t, { cache: "no-cache" !== e.cache, headers: e.headers || null, method: e.method || "GET", xhrFields: "include" === e.credentials ? { withCredentials: !0 } : null }).then(
						function (t) {
							return {
								results: i.map(t, function (t) {
									return t;
								}),
								more: !1,
							};
						},
						function (t, e, n) {
							throw new Error("AJAX request returned: " + e + (n ? ", " + n : ""));
						}
					);
				});
		});
	},
	function (t, e, n) {
		"use strict";
		function i(t, e) {
			var n = e.multiple ? [] : null,
				i = function () {
					var t = s(this);
					if (t.is("option")) {
						var r = t.text(),
							o = t.attr("value");
						if ((void 0 === o && (o = r), t.prop("selected"))) {
							var a = { id: o, text: r };
							e.multiple ? n.push(a) : (n = a);
						}
						return { id: o, text: t.attr("label") || r };
					}
					return { text: t.attr("label"), children: t.children("option,optgroup").map(i).get() };
				};
			e.allowClear = "allowClear" in e ? e.allowClear : !t.prop("required");
			var r = t.children("option,optgroup").map(i).get();
			(e.data = n), (e.items = e.query ? null : r), (e.placeholder = e.placeholder || t.data("placeholder") || ""), (e.tabIndex = void 0 === e.tabIndex ? t.attr("tabindex") || 0 : e.tabIndex);
			var o = (t.attr("class") || "selectivity-input").split(" ");
			o.indexOf("selectivity-input") < 0 && o.push("selectivity-input");
			var a = s("<div>").attr({ id: "s9y_" + t.attr("id"), class: o.join(" "), style: t.attr("style"), "data-name": t.attr("name") });
			return a.insertAfter(t), t.hide(), a[0];
		}
		function r(t) {
			var e = s(t.el);
			e.on("change", function (t) {
				var n = t.originalEvent.value;
				e.prev("select")
					.val("array" === s.type(n) ? n.slice(0) : n)
					.trigger(t);
			});
		}
		var s = n(3),
			o = n(217);
		o.OptionListeners.push(function (t, e) {
			var n = s(t.el);
			n.is("select") &&
				(n.attr("autofocus") &&
					setTimeout(function () {
						t.focus();
					}, 1),
					(t.el = i(n, e)),
					(t.el.selectivity = t),
					o.patchEvents(n),
					r(t));
		});
	},
	function (t, e, n) {
		"use strict";
		function i(t, e) {
			function n(e, i) {
				function o() {
					var t;
					e.highlightedResult ? (t = s(l, e.highlightedResult.id)) : e.loadMoreHighlighted && (t = e.$(".selectivity-load-more")), t && t.scrollIntoView && t.scrollIntoView(i < 0);
				}
				var a = e.results;
				if (a.length) {
					var l = [].slice.call(e.el.querySelectorAll(".selectivity-result-item"));
					if (e.submenu) return void n(e.submenu, i);
					var u = i > 0 ? 0 : l.length - 1,
						c = u,
						h = e.highlightedResult;
					if (h) {
						var d = s(l, h.id);
						if (((c = l.indexOf(d) + i), i > 0 ? c >= l.length : c < 0)) {
							if (e.hasMore) return e.highlightLoadMore(), void o();
							c = u;
						}
					}
					var f = l[c],
						p = r.findNestedById(a, t.getRelatedItemId(f));
					p && (e.highlight(p, { delay: !!p.submenu }), o());
				}
			}
			function i(i) {
				var r = t.dropdown;
				if (r) {
					var s = o(i);
					if (s === a) {
						if (!e.value) {
							if (r.submenu) {
								for (var c = r.submenu; c.submenu;) c = c.submenu;
								v = c;
							}
							i.preventDefault(), (p = !0);
						}
					} else
						s === l
							? n(r, 1)
							: s === d
								? n(r, -1)
								: s === h
									? setTimeout(function () {
										t.close();
									}, 1)
									: s === u && i.preventDefault();
				}
			}
			function f(e) {
				function n() {
					t.options.showDropdown !== !1 && t.open();
				}
				var i = t.dropdown,
					r = o(e);
				p
					? (e.preventDefault(), (p = !1), v && (v.close(), t.focus(), (v = null)))
					: r === a
						? !i && t.options.allowClear && t.clear()
						: r !== u || e.ctrlKey
							? r === c
								? (t.close(), e.preventDefault())
								: r === l || r === d
									? (n(), e.preventDefault())
									: n()
							: (i ? i.selectHighlight() : t.options.showDropdown !== !1 && n(), e.preventDefault());
			}
			var p = !1,
				v = null;
			e.addEventListener("keydown", i), e.addEventListener("keyup", f);
		}
		var r = n(217),
			s = n(237),
			o = n(228),
			a = 8,
			l = 40,
			u = 13,
			c = 27,
			h = 9,
			d = 38;
		r.InputListeners.push(i);
	},
	function (t, e) {
		"use strict";
		t.exports = function (t, e) {
			for (var n = 0, i = t.length; n < i; n++) {
				var r = t[n],
					s = r.getAttribute("data-item-id");
				if (("number" == typeof e ? parseInt(s, 10) : s) === e) return r;
			}
			return null;
		};
	},
	function (t, e, n) {
		"use strict";
		var i = n(217),
			r = {
				allowClear: "boolean",
				backspaceHighlightsBeforeDelete: "boolean",
				closeOnSelect: "boolean",
				createTokenItem: "function",
				dropdown: "function|null",
				initSelection: "function|null",
				inputListeners: "array",
				items: "array|null",
				matcher: "function|null",
				placeholder: "string",
				positionDropdown: "function|null",
				query: "function|null",
				readOnly: "boolean",
				removeOnly: "boolean",
				shouldOpenSubmenu: "function",
				showSearchInputInDropdown: "boolean",
				suppressWheelSelector: "string|null",
				tabIndex: "number",
				templates: "object",
				tokenizer: "function",
			};
		i.OptionListeners.unshift(function (t, e) {
			for (var n in e)
				if (e.hasOwnProperty(n)) {
					var i = e[n],
						s = r[n];
					if (
						s &&
						!s.split("|").some(function (t) {
							return "null" === t ? null === i : "array" === t ? Array.isArray(i) : null !== i && void 0 !== i && typeof i === t;
						})
					)
						throw new Error(n + " must be of type " + s);
				}
		});
	},
	function (t, e, n) {
		"use strict";
		function i(t, e) {
			(this.parentMenu = e.parentMenu), r.call(this, t, e), (this._closeSubmenuTimeout = 0), (this._openSubmenuTimeout = 0);
		}
		var r = n(221),
			s = n(217),
			o = n(237),
			a = s.inherits(i, r, {
				close: function () {
					this.submenu && this.submenu.close(), a(this, "close"), this.parentMenu && ((this.parentMenu.submenu = null), (this.parentMenu = null)), clearTimeout(this._closeSubmenuTimeout), clearTimeout(this._openSubmenuTimeout);
				},
				highlight: function (t, e) {
					e = e || {};
					var n = e.reason || "unspecified";
					e.delay
						? (a(this, "highlight", t), clearTimeout(this._openSubmenuTimeout), (this._openSubmenuTimeout = setTimeout(this._doHighlight.bind(this, t, n), 300)))
						: this.submenu
							? this.highlightedResult && this.highlightedResult.id === t.id
								? this._doHighlight(t, n)
								: (clearTimeout(this._closeSubmenuTimeout), (this._closeSubmenuTimeout = setTimeout(this._closeSubmenuAndHighlight.bind(this, t, n), 100)))
							: (this.parentMenu && this.parentMenu._closeSubmenuTimeout && (clearTimeout(this.parentMenu._closeSubmenuTimeout), (this.parentMenu._closeSubmenuTimeout = 0)),
								e.openSubmenu === !1 ? a(this, "highlight", t) : this._doHighlight(t, n));
				},
				search: function (t) {
					if (this.submenu) {
						var e = this.$(".selectivity-search-input");
						if (!e || e !== document.activeElement) return void this.submenu.search(t);
						this.submenu.close();
					}
					a(this, "search", t);
				},
				selectHighlight: function () {
					this.submenu ? this.submenu.selectHighlight() : a(this, "selectHighlight");
				},
				showResults: function (t, e) {
					function n(t) {
						t.children && t.children.forEach(n), t.submenu && (t.selectable = !!t.selectable);
					}
					this.submenu && e.dropdown !== this ? this.submenu.showResults(t, e) : (t.forEach(n), a(this, "showResults", t, e));
				},
				triggerClose: function () {
					this.parentMenu ? this.selectivity.triggerEvent("selectivity-close-submenu") : a(this, "triggerClose");
				},
				triggerOpen: function () {
					this.parentMenu ? this.selectivity.triggerEvent("selectivity-open-submenu") : a(this, "triggerOpen");
				},
				_closeSubmenuAndHighlight: function (t, e) {
					this.submenu && this.submenu.close(), this._doHighlight(t, e);
				},
				_doHighlight: function (t, e) {
					a(this, "highlight", t);
					var n = this.selectivity.options;
					if (!(!t.submenu || this.submenu || (n.shouldOpenSubmenu && n.shouldOpenSubmenu(t, e) === !1))) {
						var i = n.dropdown || s.Dropdown;
						if (i) {
							var r = this.el.querySelectorAll(".selectivity-result-item"),
								l = o(r, t.id),
								u = this.el;
							(this.submenu = new i(this.selectivity, {
								highlightFirstItem: !t.selectable,
								items: t.submenu.items || null,
								parentMenu: this,
								position: function (e, n) {
									if (t.submenu.positionDropdown) t.submenu.positionDropdown(e, n, l, u);
									else {
										var i = u.getBoundingClientRect(),
											r = i.right,
											s = i.width;
										r + s > document.body.clientWidth && i.left - s > 0 && (r = i.left - s + 10);
										var o = l.getBoundingClientRect().top,
											a = Math.min(Math.max(o + e.clientHeight - window.innerHeight, 0), i.top + i.height);
										(e.style.left = r + "px"), (e.style.top = o - a + "px"), (e.style.width = s + "px");
									}
								},
								query: t.submenu.query || null,
								showSearchInput: t.submenu.showSearchInput,
							})),
								this.submenu.search("");
						}
					}
				},
			});
		(s.Dropdown = i), (t.exports = i);
	},
	function (t, e, n) {
		"use strict";
		function i(t, e, n, i) {
			function r(t) {
				return (
					!!t &&
					l.some(function (e) {
						return t.indexOf(e) > -1;
					})
				);
			}
			function o(t) {
				for (var e = 0, n = t.length; e < n; e++) if (l.indexOf(t[e]) > -1) return { term: t.slice(0, e), input: t.slice(e + 1) };
				return {};
			}
			for (
				var a =
					i.createTokenItem ||
					function (t) {
						return t ? { id: t, text: t } : null;
					},
				l = i.tokenSeparators;
				r(t);

			) {
				var u = o(t);
				if (u.term) {
					var c = a(u.term);
					c && !s.findById(e, c.id) && n(c);
				}
				t = u.input;
			}
			return t;
		}
		var r = n(11),
			s = n(217);
		s.OptionListeners.push(function (t, e) {
			e.tokenSeparators && ((e.allowedTypes = r({ tokenSeparators: "array" }, e.allowedTypes)), (e.tokenizer = e.tokenizer || i));
		});
	},
	function (t, e, n) {
		"use strict";
		var i = n(204),
			r = n(217),
			s = n(230);
		r.Templates = {
			dropdown: function (t) {
				var e = t.dropdownCssClass ? " " + t.dropdownCssClass : "",
					n = "";
				if (t.showSearchInput) {
					e += " has-search-input";
					var r = t.searchInputPlaceholder;
					n = '<div class="selectivity-search-input-container"><input type="text" class="selectivity-search-input"' + (r ? ' placeholder="' + i(r) + '"' : "") + "></div>";
				}
				return '<div class="selectivity-dropdown' + e + '">' + n + '<div class="selectivity-results-container"></div></div>';
			},
			error: function (t) {
				return '<div class="selectivity-error">' + (t.escape ? i(t.message) : t.message) + "</div>";
			},
			loading: function () {
				return '<div class="selectivity-loading">' + s.loading + "</div>";
			},
			loadMore: function () {
				return '<div class="selectivity-load-more">' + s.loadMore + "</div>";
			},
			multipleSelectInput: function (t) {
				return (
					'<div class="selectivity-multiple-input-container">' +
					(t.enabled ? '<input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" class="selectivity-multiple-input">' : '<div class="selectivity-multiple-input selectivity-placeholder"></div>') +
					'<div class="selectivity-clearfix"></div></div>'
				);
			},
			multipleSelectedItem: function (t) {
				var e = t.highlighted ? " highlighted" : "";
				return (
					'<span class="selectivity-multiple-selected-item' +
					e +
					'" data-item-id="' +
					i(t.id) +
					'">' +
					(t.removable ? '<a class="selectivity-multiple-selected-item-remove"><i class="fa fa-remove"></i></a>' : "") +
					i(t.text) +
					"</span>"
				);
			},
			noResults: function (t) {
				return '<div class="selectivity-error">' + (t.term ? s.noResultsForTerm(t.term) : s.noResults) + "</div>";
			},
			resultChildren: function (t) {
				return '<div class="selectivity-result-children">' + t.childrenHtml + "</div>";
			},
			resultItem: function (t) {
				return '<div class="selectivity-result-item' + (t.disabled ? " disabled" : "") + '" data-item-id="' + i(t.id) + '">' + i(t.text) + (t.submenu ? '<i class="selectivity-submenu-icon fa fa-chevron-right"></i>' : "") + "</div>";
			},
			resultLabel: function (t) {
				return '<div class="selectivity-result-label">' + i(t.text) + "</div>";
			},
			singleSelectInput: function (t) {
				return (
					'<div class="selectivity-single-select"><input type="text" class="selectivity-single-select-input"' +
					(t.required ? " required" : "") +
					'><div class="selectivity-single-result-container"></div><i class="fa fa-sort-desc selectivity-caret"></i></div>'
				);
			},
			singleSelectPlaceholder: function (t) {
				return '<div class="selectivity-placeholder">' + i(t.placeholder) + "</div>";
			},
			singleSelectedItem: function (t) {
				return '<span class="selectivity-single-selected-item" data-item-id="' + i(t.id) + '">' + (t.removable ? '<a class="selectivity-single-selected-item-remove"><i class="fa fa-remove"></i></a>' : "") + i(t.text) + "</span>";
			},
			selectCompliance: function (t) {
				var e = t.mode,
					n = t.name;
				return "multiple" === e && "[]" !== n.slice(-2) && (n += "[]"), '<select name="' + n + '"' + ("multiple" === e ? " multiple" : "") + "></select>";
			},
			selectOptionCompliance: function (t) {
				return '<option value="' + i(t.id) + '" selected>' + i(t.text) + "</option>";
			},
		};
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		var r = n(3),
			s = i(r),
			o = n(65),
			a = i(o);
		s["default"].fn.textareaSize = (0, a["default"])(function (t) {
			function e() {
				t.height(0).height(t.get(0).scrollHeight);
			}
			t.on("input change", e), e();
		});
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(65),
			u = i(l);
		n(217);
		var c = (function () {
			function t(e, n) {
				r(this, t);
				var i = ((this.options = a["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, a["default"])(e))),
					s = ((this.$scrollable = i.find(".js-lightbox-scroller-content")), (this.$close = i.find(".js-lightbox-close"))),
					o = (this.$back = i.find(".js-lightbox-back"));
				(this.callee = null), (this.openState = !1), s.on("tap", this.close.bind(this)), o.on("tap", this.back.bind(this));
			}
			return (
				s(t, null, [
					{
						key: "Defaults",
						get: function () {
							return {};
						},
					},
				]),
				s(t, [
					{
						key: "open",
						value: function () {
							var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
							if (!this.openState) {
								(this.openState = !0), (this.callee = t.callee || null), (this.animation = t.animation || null);
								var e = this.animation || "lightbox-in";
								this.$container.transition(
									{
										before: function (t) {
											return t.removeClass("is-hidden").addClass("animation--" + e + " animation--" + e + "--inactive");
										},
										transition: function (t) {
											return t.removeClass("animation--" + e + "--inactive");
										},
										after: function (t) {
											return t.removeClass("animation--" + e);
										},
									},
									{
										after: function () {
											(0, a["default"])("html").addClass("with-lightbox");
										},
									}
								),
									this.$back.toggleClass("is-hidden", !this.callee),
									a["default"].scroller.setScrollableContent(this.$scrollable),
									(0, a["default"])(document).on("keydown.lightbox", this.handleKeyDown.bind(this)),
									this.$container.trigger("open.lightbox");
							}
						},
					},
					{
						key: "back",
						value: function () {
							this.callee ? (this.close({ animation: "slideout-right" }), (0, a["default"])(this.callee).lightbox("open", { animation: "slidein-left" })) : this.close();
						},
					},
					{
						key: "close",
						value: function () {
							var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
							if (this.openState) {
								this.openState = !1;
								var e = t.animation || "fade-out";
								this.$container.transition({
									before: function (t) {
										t.addClass("animation--" + e);
									},
									transition: function (t) {
										t.addClass("animation--" + e + "--active");
									},
									after: function (t) {
										t.removeClass("animation--" + e + " animation--" + e + "--active").addClass("is-hidden");
									},
								});
								var n = (0, a["default"])("html");
								n.removeClass("with-lightbox");
								var i = (0, a["default"])(".js-main-scroller-content");
								a["default"].scroller.setScrollableContent(i, !1), (0, a["default"])(document).off("keydown.lightbox"), this.$container.trigger("close.lightbox");
							}
						},
					},
					{
						key: "handleKeyDown",
						value: function (t) {
							27 !== t.which || (0, a["default"])(document.activeElement).is("input, textarea, select") || this.close();
						},
					},
				]),
				t
			);
		})(),
			h = (function () {
				function t(e, n) {
					r(this, t);
					var i = (this.options = a["default"].extend({}, this.constructor.Defaults, n)),
						s = (this.$container = (0, a["default"])(e));
					this.$target = (0, a["default"])(i.target);
					s.on("tap", this.open.bind(this));
				}
				return (
					s(t, null, [
						{
							key: "Defaults",
							get: function () {
								return { target: "", callee: "", animation: null, params: null, position: "" };
							},
						},
					]),
					s(t, [
						{
							key: "open",
							value: function () {
								var t = this.options.callee,
									e = this.options.params;
								t ? ((0, a["default"])(t).lightbox("close", { animation: "slideout-left" }), this.$target.lightbox("open", { callee: (0, a["default"])(t), animation: "slidein-right" })) : this.$target.lightbox("open"),
									e && this.setParams(e),
									this.options.position.length &&
									((0, a["default"])("#apply_career_position").selectivity("data", { id: 1, text: this.options.position }), (0, a["default"])("#apply_career_position").val(this.options.position));
							},
						},
						{
							key: "setParams",
							value: function (t) {
								var e = this.$target;
								for (var n in t)
									e.find('[name="' + n + '"]')
										.val(t[n])
										.change();
							},
						},
					]),
					t
				);
			})();
		(a["default"].fn.lightbox = (0, u["default"])(c)), (a["default"].fn.lightboxTrigger = (0, u["default"])(h));
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o);
		n(245), n(246);
		var l = n(190),
			u = i(l),
			c = n(65),
			h = i(c);
		n(169),
			a["default"].validator.addMethod(
				"tel",
				function (t, e) {
					return this.optional(e) || /^\+?[0-9\s]{8,}$/.test(t);
				},
				"Please enter a valid phone number"
			),
			a["default"].validator.addMethod(
				"url",
				function (t, e) {
					return (
						this.optional(e) ||
						/^(?:(?:(?:https?|ftp):)?\/\/)?(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i.test(
							t
						)
					);
				},
				"Please enter a valid URL"
			),
			a["default"].validator.addMethod("step", function (t, e) {
				return !0;
			});
		var d = (function () {
			function t(e) {
				var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
				r(this, t);
				var i = ((this.options = a["default"].extend({}, this.constructor.Defaults, n)), (this.$form = (0, a["default"])(e)));
				this.$errorMessage = i.find(".js-form-error-message");
				(this.isLoading = !1),
					(this.formName = i.attr("name") || ""),
					i.on("reset", this.reset.bind(this)),
					(this.validator = i.validate(
						a["default"].extend(
							{
								submitHandler: this.onsuccess.bind(this),
								invalidHandler: this.onerror.bind(this),
								errorPlacement: this.errorPlacement.bind(this),
								highlight: this.errorHighlight.bind(this),
								unhighlight: this.errorUnhighlight.bind(this),
							},
							this.getValidationOptions()
						)
					));
			}
			return (
				s(t, null, [
					{
						key: "Defaults",
						get: function () {
							return { formSuccessRedirect: !1 };
						},
					},
				]),
				s(t, [
					{
						key: "getValidationOptions",
						value: function () {
							return { rules: {} };
						},
					},
					{
						key: "getFormValues",
						value: function () {
							var t = this.$form;
							return t.serializeJSON();
						},
					},
					{
						key: "reset",
						value: function () {
							this.validator.resetForm(), this.hideSuccessMessage();
						},
					},
					{
						key: "disable",
						value: function () {
							var t = this.$form;
							t.find("input, select, textarea").prop("readonly", !0).addClass("readonly"), t.find('button[type="submit"], input[type="submit"]').prop("disabled", !0);
						},
					},
					{
						key: "enable",
						value: function () {
							var t = this.$form;
							t.find("input, select, textarea").prop("readonly", !1).removeClass("readonly"), t.find('button[type="submit"], input[type="submit"]').prop("disabled", !1);
						},
					},
					{
						key: "onsuccess",
						value: function (t) {
							var e = this.$form,
								n = e.valid();
							n && ((n = this.validate()), e.find(".js-upload-files").length && (e.find(".js-upload-files").hasClass("is-uploaded") || (n = !1))),
								n ? (this.hideGenericErrorMessage(), this.submit()) : (this.showGenericErrorMessage(), this.hideSuccessMessage()),
								a["default"].scroller.resized();
						},
					},
					{
						key: "onerror",
						value: function (t, e) {
							if (e.errorList.length) {
								var n = (0, a["default"])(e.errorList[0].element);
								a["default"].scroller.scrollIntoView(n, 40);
							}
							this.showGenericErrorMessage(), a["default"].scroller.resized();
						},
					},
					{
						key: "validate",
						value: function () {
							return !0;
						},
					},
					{
						key: "setLoading",
						value: function (t) {
							this.isLoading = t;
						},
					},
					{
						key: "submit",
						value: function () {
							if (!this.isLoading) {
								var t = this.$form,
									e = { url: t.attr("action"), method: t.attr("method"), cache: !1 };
								"multipart/form-data" === t.attr("enctype") ? ((e.data = new FormData(t[0])), (e.processData = !1), (e.contentType = !1)) : (e.data = this.getFormValues()),
									this.setLoading(!0),
									this.disable(),
									a["default"].ajax(e).always(this.handleResponseComplete.bind(this)).done(this.handleResponseSuccess.bind(this, e.data));
							}
						},
					},
					{
						key: "handleResponseComplete",
						value: function () {
							this.setLoading(!1), this.enable();
						},
					},
					{
						key: "handleResponseSuccess",
						value: function (t, e) {
							e.status ? this.handleSuccess(t, e) : this.handleErrorResponse(e.errors || e.error || []);
						},
					},
					{
						key: "handleErrorResponse",
						value: function (t) {
							var e = this,
								n = this.$form,
								i = (0, u["default"])(
									t,
									function (t, i) {
										var r = e.getInputName(i.id);
										return n.find('[name="' + r + '"]').length && (t[r] = i.message), t;
									},
									{}
								);
							a["default"].isEmptyObject(i) && this.showGenericErrorMessage(), this.setErrors(i), a["default"].scroller.resized();
						},
					},
					{
						key: "getInputName",
						value: function (t) {
							var e = this.$form,
								n = this.formName,
								i = t.replace(new RegExp("^" + n + "\\[(.+?)\\]"), "$1"),
								r = n + i.replace(/^([^\[]+)/, "[$1]");
							return n && e.find('[name="' + r + '"]').length ? r : e.find('[name="' + i + '"]').length ? i : this.checkInputNamesForPrefix() ? r : i;
						},
					},
					{
						key: "checkInputNamesForPrefix",
						value: function () {
							for (var t = this.formName, e = t.length, n = (0, a["default"])(this.form).find("input, textarea, select"), i = 0; i < n.length; i++) if (n.eq(i).attr("name").substring(0, e) === t) return !0;
							return !1;
						},
					},
					{
						key: "handleSuccess",
						value: function (t, e) {
							var n = this.options;
							n.formSuccessRedirect ? (console.log("test"), (document.location = n.formSuccessRedirect)) : this.showSuccessMessage(t, e);
						},
					},
					{
						key: "errorPlacement",
						value: function (t, e) {
							e.is(":radio") ? e.closest(".form-group").append(t) : e.is('[name="g-recaptcha-response"]') ? e.closest(".g-recaptcha").append(t) : e.parent().append(t);
						},
					},
					{
						key: "getErrorElement",
						value: function (t) {
							var e = (0, a["default"])(t);
							return e.is("select") && e.next(".selectivity-input") ? e.next() : e;
						},
					},
					{
						key: "getLabelElement",
						value: function (t) {
							return (0, a["default"])(t.form)
								.find('label[for="' + t.id + '"]')
								.not(".error");
						},
					},
					{
						key: "errorHighlight",
						value: function (t, e, n) {
							var i = this.getErrorElement(t),
								r = this.getLabelElement(t),
								s = i.closest(".form-group, .form-row");
							s.removeClass("has-success").addClass("has-error"), i.addClass("form-control--" + e).removeClass("form-control--" + n), r.removeClass("form-label--" + e);
						},
					},
					{
						key: "errorUnhighlight",
						value: function (t, e, n) {
							var i = this.getErrorElement(t),
								r = this.getLabelElement(t),
								s = i.closest(".form-group, .form-row");
							s.removeClass("has-error").addClass("has-success"), i.removeClass("form-control--" + e).addClass("form-control--" + n), r.removeClass("form-label--" + e), a["default"].scroller.resized();
						},
					},
					{
						key: "showGenericErrorMessage",
						value: function () {
							this.$errorMessage.removeClass("is-hidden"),
								this.$form.find(".js-upload-files").length &&
								(this.$form.find(".js-upload-files").hasClass("is-uploaded") || this.$form.find(".js-upload-error").text(a["default"].validator.messages.required).transition("fade-in"));
						},
					},
					{
						key: "hideGenericErrorMessage",
						value: function () {
							this.$errorMessage.addClass("is-hidden");
						},
					},
					{
						key: "setErrors",
						value: function (t) {
							this.validator.showErrors(t);
						},
					},
					{
						key: "showSuccessMessage",
						value: function () {
							var t = this.$form,
								e = t.find(".js-form-content"),
								n = t.find(".js-form-success");
							e.addClass("is-hidden"), n.removeClass("is-hidden"), a["default"].scroller.resized(), a["default"].scroller.scrollIntoView(n, 100);
						},
					},
					{
						key: "hideSuccessMessage",
						value: function () {
							var t = this.$form,
								e = t.find(".js-form-success"),
								n = t.find(".js-form-content");
							e.addClass("is-hidden"), n.removeClass("is-hidden");
						},
					},
				]),
				t
			);
		})();
		(e["default"] = d), (a["default"].fn.form = (0, h["default"])(d, { api: ["reset", "enable", "disable", "instance"] }));
	},
	function (t, e, n) {
		var i, r, s;
        /*!
         * jQuery Validation Plugin v1.19.1
         *
         * https://jqueryvalidation.org/
         *
         * Copyright (c) 2019 Jörn Zaefferer
         * Released under the MIT license
         */
		!(function (o) {
			(r = [n(3)]), (i = o), (s = "function" == typeof i ? i.apply(e, r) : i), !(void 0 !== s && (t.exports = s));
		})(function (t) {
			t.extend(t.fn, {
				validate: function (e) {
					if (!this.length) return void (e && e.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing."));
					var n = t.data(this[0], "validator");
					return n
						? n
						: (this.attr("novalidate", "novalidate"),
							(n = new t.validator(e, this[0])),
							t.data(this[0], "validator", n),
							n.settings.onsubmit &&
							(this.on("click.validate", ":submit", function (e) {
								(n.submitButton = e.currentTarget), t(this).hasClass("cancel") && (n.cancelSubmit = !0), void 0 !== t(this).attr("formnovalidate") && (n.cancelSubmit = !0);
							}),
								this.on("submit.validate", function (e) {
									function i() {
										var i, r;
										return (
											n.submitButton && (n.settings.submitHandler || n.formSubmitted) && (i = t("<input type='hidden'/>").attr("name", n.submitButton.name).val(t(n.submitButton).val()).appendTo(n.currentForm)),
											!(n.settings.submitHandler && !n.settings.debug) || ((r = n.settings.submitHandler.call(n, n.currentForm, e)), i && i.remove(), void 0 !== r && r)
										);
									}
									return n.settings.debug && e.preventDefault(), n.cancelSubmit ? ((n.cancelSubmit = !1), i()) : n.form() ? (n.pendingRequest ? ((n.formSubmitted = !0), !1) : i()) : (n.focusInvalid(), !1);
								})),
							n);
				},
				valid: function () {
					var e, n, i;
					return (
						t(this[0]).is("form")
							? (e = this.validate().form())
							: ((i = []),
								(e = !0),
								(n = t(this[0].form).validate()),
								this.each(function () {
									(e = n.element(this) && e), e || (i = i.concat(n.errorList));
								}),
								(n.errorList = i)),
						e
					);
				},
				rules: function (e, n) {
					var i,
						r,
						s,
						o,
						a,
						l,
						u = this[0],
						c = "undefined" != typeof this.attr("contenteditable") && "false" !== this.attr("contenteditable");
					if (null != u && (!u.form && c && ((u.form = this.closest("form")[0]), (u.name = this.attr("name"))), null != u.form)) {
						if (e)
							switch (((i = t.data(u.form, "validator").settings), (r = i.rules), (s = t.validator.staticRules(u)), e)) {
								case "add":
									t.extend(s, t.validator.normalizeRule(n)), delete s.messages, (r[u.name] = s), n.messages && (i.messages[u.name] = t.extend(i.messages[u.name], n.messages));
									break;
								case "remove":
									return n
										? ((l = {}),
											t.each(n.split(/\s/), function (t, e) {
												(l[e] = s[e]), delete s[e];
											}),
											l)
										: (delete r[u.name], s);
							}
						return (
							(o = t.validator.normalizeRules(t.extend({}, t.validator.classRules(u), t.validator.attributeRules(u), t.validator.dataRules(u), t.validator.staticRules(u)), u)),
							o.required && ((a = o.required), delete o.required, (o = t.extend({ required: a }, o))),
							o.remote && ((a = o.remote), delete o.remote, (o = t.extend(o, { remote: a }))),
							o
						);
					}
				},
			}),
				t.extend(t.expr.pseudos || t.expr[":"], {
					blank: function (e) {
						return !t.trim("" + t(e).val());
					},
					filled: function (e) {
						var n = t(e).val();
						return null !== n && !!t.trim("" + n);
					},
					unchecked: function (e) {
						return !t(e).prop("checked");
					},
				}),
				(t.validator = function (e, n) {
					(this.settings = t.extend(!0, {}, t.validator.defaults, e)), (this.currentForm = n), this.init();
				}),
				(t.validator.format = function (e, n) {
					return 1 === arguments.length
						? function () {
							var n = t.makeArray(arguments);
							return n.unshift(e), t.validator.format.apply(this, n);
						}
						: void 0 === n
							? e
							: (arguments.length > 2 && n.constructor !== Array && (n = t.makeArray(arguments).slice(1)),
								n.constructor !== Array && (n = [n]),
								t.each(n, function (t, n) {
									e = e.replace(new RegExp("\\{" + t + "\\}", "g"), function () {
										return n;
									});
								}),
								e);
				}),
				t.extend(t.validator, {
					defaults: {
						messages: {},
						groups: {},
						rules: {},
						errorClass: "error",
						pendingClass: "pending",
						validClass: "valid",
						errorElement: "label",
						focusCleanup: !1,
						focusInvalid: !0,
						errorContainer: t([]),
						errorLabelContainer: t([]),
						onsubmit: !0,
						ignore: ":hidden",
						ignoreTitle: !1,
						onfocusin: function (t) {
							(this.lastActive = t), this.settings.focusCleanup && (this.settings.unhighlight && this.settings.unhighlight.call(this, t, this.settings.errorClass, this.settings.validClass), this.hideThese(this.errorsFor(t)));
						},
						onfocusout: function (t) {
							this.checkable(t) || (!(t.name in this.submitted) && this.optional(t)) || this.element(t);
						},
						onkeyup: function (e, n) {
							var i = [16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225];
							(9 === n.which && "" === this.elementValue(e)) || t.inArray(n.keyCode, i) !== -1 || ((e.name in this.submitted || e.name in this.invalid) && this.element(e));
						},
						onclick: function (t) {
							t.name in this.submitted ? this.element(t) : t.parentNode.name in this.submitted && this.element(t.parentNode);
						},
						highlight: function (e, n, i) {
							"radio" === e.type ? this.findByName(e.name).addClass(n).removeClass(i) : t(e).addClass(n).removeClass(i);
						},
						unhighlight: function (e, n, i) {
							"radio" === e.type ? this.findByName(e.name).removeClass(n).addClass(i) : t(e).removeClass(n).addClass(i);
						},
					},
					setDefaults: function (e) {
						t.extend(t.validator.defaults, e);
					},
					messages: {
						required: "This field is required.",
						remote: "Please fix this field.",
						email: "Please enter a valid email address.",
						url: "Please enter a valid URL.",
						date: "Please enter a valid date.",
						dateISO: "Please enter a valid date (ISO).",
						number: "Please enter a valid number.",
						digits: "Please enter only digits.",
						equalTo: "Please enter the same value again.",
						maxlength: t.validator.format("Please enter no more than {0} characters."),
						minlength: t.validator.format("Please enter at least {0} characters."),
						rangelength: t.validator.format("Please enter a value between {0} and {1} characters long."),
						range: t.validator.format("Please enter a value between {0} and {1}."),
						max: t.validator.format("Please enter a value less than or equal to {0}."),
						min: t.validator.format("Please enter a value greater than or equal to {0}."),
						step: t.validator.format("Please enter a multiple of {0}."),
					},
					autoCreateRanges: !1,
					prototype: {
						init: function () {
							function e(e) {
								var n = "undefined" != typeof t(this).attr("contenteditable") && "false" !== t(this).attr("contenteditable");
								if ((!this.form && n && ((this.form = t(this).closest("form")[0]), (this.name = t(this).attr("name"))), i === this.form)) {
									var r = t.data(this.form, "validator"),
										s = "on" + e.type.replace(/^validate/, ""),
										o = r.settings;
									o[s] && !t(this).is(o.ignore) && o[s].call(r, this, e);
								}
							}
							(this.labelContainer = t(this.settings.errorLabelContainer)),
								(this.errorContext = (this.labelContainer.length && this.labelContainer) || t(this.currentForm)),
								(this.containers = t(this.settings.errorContainer).add(this.settings.errorLabelContainer)),
								(this.submitted = {}),
								(this.valueCache = {}),
								(this.pendingRequest = 0),
								(this.pending = {}),
								(this.invalid = {}),
								this.reset();
							var n,
								i = this.currentForm,
								r = (this.groups = {});
							t.each(this.settings.groups, function (e, n) {
								"string" == typeof n && (n = n.split(/\s/)),
									t.each(n, function (t, n) {
										r[n] = e;
									});
							}),
								(n = this.settings.rules),
								t.each(n, function (e, i) {
									n[e] = t.validator.normalizeRule(i);
								}),
								t(this.currentForm)
									.on(
										"focusin.validate focusout.validate keyup.validate",
										":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable], [type='button']",
										e
									)
									.on("click.validate", "select, option, [type='radio'], [type='checkbox']", e),
								this.settings.invalidHandler && t(this.currentForm).on("invalid-form.validate", this.settings.invalidHandler);
						},
						form: function () {
							return (
								this.checkForm(),
								t.extend(this.submitted, this.errorMap),
								(this.invalid = t.extend({}, this.errorMap)),
								this.valid() || t(this.currentForm).triggerHandler("invalid-form", [this]),
								this.showErrors(),
								this.valid()
							);
						},
						checkForm: function () {
							this.prepareForm();
							for (var t = 0, e = (this.currentElements = this.elements()); e[t]; t++) this.check(e[t]);
							return this.valid();
						},
						element: function (e) {
							var n,
								i,
								r = this.clean(e),
								s = this.validationTargetFor(r),
								o = this,
								a = !0;
							return (
								void 0 === s
									? delete this.invalid[r.name]
									: (this.prepareElement(s),
										(this.currentElements = t(s)),
										(i = this.groups[s.name]),
										i &&
										t.each(this.groups, function (t, e) {
											e === i && t !== s.name && ((r = o.validationTargetFor(o.clean(o.findByName(t)))), r && r.name in o.invalid && (o.currentElements.push(r), (a = o.check(r) && a)));
										}),
										(n = this.check(s) !== !1),
										(a = a && n),
										n ? (this.invalid[s.name] = !1) : (this.invalid[s.name] = !0),
										this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)),
										this.showErrors(),
										t(e).attr("aria-invalid", !n)),
								a
							);
						},
						showErrors: function (e) {
							if (e) {
								var n = this;
								t.extend(this.errorMap, e),
									(this.errorList = t.map(this.errorMap, function (t, e) {
										return { message: t, element: n.findByName(e)[0] };
									})),
									(this.successList = t.grep(this.successList, function (t) {
										return !(t.name in e);
									}));
							}
							this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors();
						},
						resetForm: function () {
							t.fn.resetForm && t(this.currentForm).resetForm(), (this.invalid = {}), (this.submitted = {}), this.prepareForm(), this.hideErrors();
							var e = this.elements().removeData("previousValue").removeAttr("aria-invalid");
							this.resetElements(e);
						},
						resetElements: function (t) {
							var e;
							if (this.settings.unhighlight) for (e = 0; t[e]; e++) this.settings.unhighlight.call(this, t[e], this.settings.errorClass, ""), this.findByName(t[e].name).removeClass(this.settings.validClass);
							else t.removeClass(this.settings.errorClass).removeClass(this.settings.validClass);
						},
						numberOfInvalids: function () {
							return this.objectLength(this.invalid);
						},
						objectLength: function (t) {
							var e,
								n = 0;
							for (e in t) void 0 !== t[e] && null !== t[e] && t[e] !== !1 && n++;
							return n;
						},
						hideErrors: function () {
							this.hideThese(this.toHide);
						},
						hideThese: function (t) {
							t.not(this.containers).text(""), this.addWrapper(t).hide();
						},
						valid: function () {
							return 0 === this.size();
						},
						size: function () {
							return this.errorList.length;
						},
						focusInvalid: function () {
							if (this.settings.focusInvalid)
								try {
									t(this.findLastActive() || (this.errorList.length && this.errorList[0].element) || [])
										.filter(":visible")
										.trigger("focus")
										.trigger("focusin");
								} catch (e) { }
						},
						findLastActive: function () {
							var e = this.lastActive;
							return (
								e &&
								1 ===
								t.grep(this.errorList, function (t) {
									return t.element.name === e.name;
								}).length &&
								e
							);
						},
						elements: function () {
							var e = this,
								n = {};
							return t(this.currentForm)
								.find("input, select, textarea, [contenteditable]")
								.not(":submit, :reset, :image, :disabled")
								.not(this.settings.ignore)
								.filter(function () {
									var i = this.name || t(this).attr("name"),
										r = "undefined" != typeof t(this).attr("contenteditable") && "false" !== t(this).attr("contenteditable");
									return (
										!i && e.settings.debug && window.console && console.error("%o has no name assigned", this),
										r && ((this.form = t(this).closest("form")[0]), (this.name = i)),
										this.form === e.currentForm && !(i in n || !e.objectLength(t(this).rules())) && ((n[i] = !0), !0)
									);
								});
						},
						clean: function (e) {
							return t(e)[0];
						},
						errors: function () {
							var e = this.settings.errorClass.split(" ").join(".");
							return t(this.settings.errorElement + "." + e, this.errorContext);
						},
						resetInternals: function () {
							(this.successList = []), (this.errorList = []), (this.errorMap = {}), (this.toShow = t([])), (this.toHide = t([]));
						},
						reset: function () {
							this.resetInternals(), (this.currentElements = t([]));
						},
						prepareForm: function () {
							this.reset(), (this.toHide = this.errors().add(this.containers));
						},
						prepareElement: function (t) {
							this.reset(), (this.toHide = this.errorsFor(t));
						},
						elementValue: function (e) {
							var n,
								i,
								r = t(e),
								s = e.type,
								o = "undefined" != typeof r.attr("contenteditable") && "false" !== r.attr("contenteditable");
							return "radio" === s || "checkbox" === s
								? this.findByName(e.name).filter(":checked").val()
								: "number" === s && "undefined" != typeof e.validity
									? e.validity.badInput
										? "NaN"
										: r.val()
									: ((n = o ? r.text() : r.val()),
										"file" === s
											? "C:\\fakepath\\" === n.substr(0, 12)
												? n.substr(12)
												: ((i = n.lastIndexOf("/")), i >= 0 ? n.substr(i + 1) : ((i = n.lastIndexOf("\\")), i >= 0 ? n.substr(i + 1) : n))
											: "string" == typeof n
												? n.replace(/\r/g, "")
												: n);
						},
						check: function (e) {
							e = this.validationTargetFor(this.clean(e));
							var n,
								i,
								r,
								s,
								o = t(e).rules(),
								a = t.map(o, function (t, e) {
									return e;
								}).length,
								l = !1,
								u = this.elementValue(e);
							"function" == typeof o.normalizer ? (s = o.normalizer) : "function" == typeof this.settings.normalizer && (s = this.settings.normalizer), s && ((u = s.call(e, u)), delete o.normalizer);
							for (i in o) {
								r = { method: i, parameters: o[i] };
								try {
									if (((n = t.validator.methods[i].call(this, u, e, r.parameters)), "dependency-mismatch" === n && 1 === a)) {
										l = !0;
										continue;
									}
									if (((l = !1), "pending" === n)) return void (this.toHide = this.toHide.not(this.errorsFor(e)));
									if (!n) return this.formatAndAdd(e, r), !1;
								} catch (c) {
									throw (
										(this.settings.debug && window.console && console.log("Exception occurred when checking element " + e.id + ", check the '" + r.method + "' method.", c),
											c instanceof TypeError && (c.message += ".  Exception occurred when checking element " + e.id + ", check the '" + r.method + "' method."),
											c)
									);
								}
							}
							if (!l) return this.objectLength(o) && this.successList.push(e), !0;
						},
						customDataMessage: function (e, n) {
							return t(e).data("msg" + n.charAt(0).toUpperCase() + n.substring(1).toLowerCase()) || t(e).data("msg");
						},
						customMessage: function (t, e) {
							var n = this.settings.messages[t];
							return n && (n.constructor === String ? n : n[e]);
						},
						findDefined: function () {
							for (var t = 0; t < arguments.length; t++) if (void 0 !== arguments[t]) return arguments[t];
						},
						defaultMessage: function (e, n) {
							"string" == typeof n && (n = { method: n });
							var i = this.findDefined(
								this.customMessage(e.name, n.method),
								this.customDataMessage(e, n.method),
								(!this.settings.ignoreTitle && e.title) || void 0,
								t.validator.messages[n.method],
								"<strong>Warning: No message defined for " + e.name + "</strong>"
							),
								r = /\$?\{(\d+)\}/g;
							return "function" == typeof i ? (i = i.call(this, n.parameters, e)) : r.test(i) && (i = t.validator.format(i.replace(r, "{$1}"), n.parameters)), i;
						},
						formatAndAdd: function (t, e) {
							var n = this.defaultMessage(t, e);
							this.errorList.push({ message: n, element: t, method: e.method }), (this.errorMap[t.name] = n), (this.submitted[t.name] = n);
						},
						addWrapper: function (t) {
							return this.settings.wrapper && (t = t.add(t.parent(this.settings.wrapper))), t;
						},
						defaultShowErrors: function () {
							var t, e, n;
							for (t = 0; this.errorList[t]; t++)
								(n = this.errorList[t]), this.settings.highlight && this.settings.highlight.call(this, n.element, this.settings.errorClass, this.settings.validClass), this.showLabel(n.element, n.message);
							if ((this.errorList.length && (this.toShow = this.toShow.add(this.containers)), this.settings.success)) for (t = 0; this.successList[t]; t++) this.showLabel(this.successList[t]);
							if (this.settings.unhighlight) for (t = 0, e = this.validElements(); e[t]; t++) this.settings.unhighlight.call(this, e[t], this.settings.errorClass, this.settings.validClass);
							(this.toHide = this.toHide.not(this.toShow)), this.hideErrors(), this.addWrapper(this.toShow).show();
						},
						validElements: function () {
							return this.currentElements.not(this.invalidElements());
						},
						invalidElements: function () {
							return t(this.errorList).map(function () {
								return this.element;
							});
						},
						showLabel: function (e, n) {
							var i,
								r,
								s,
								o,
								a = this.errorsFor(e),
								l = this.idOrName(e),
								u = t(e).attr("aria-describedby");
							a.length
								? (a.removeClass(this.settings.validClass).addClass(this.settings.errorClass), a.html(n))
								: ((a = t("<" + this.settings.errorElement + ">")
									.attr("id", l + "-error")
									.addClass(this.settings.errorClass)
									.html(n || "")),
									(i = a),
									this.settings.wrapper &&
									(i = a
										.hide()
										.show()
										.wrap("<" + this.settings.wrapper + "/>")
										.parent()),
									this.labelContainer.length ? this.labelContainer.append(i) : this.settings.errorPlacement ? this.settings.errorPlacement.call(this, i, t(e)) : i.insertAfter(e),
									a.is("label")
										? a.attr("for", l)
										: 0 === a.parents("label[for='" + this.escapeCssMeta(l) + "']").length &&
										((s = a.attr("id")),
											u ? u.match(new RegExp("\\b" + this.escapeCssMeta(s) + "\\b")) || (u += " " + s) : (u = s),
											t(e).attr("aria-describedby", u),
											(r = this.groups[e.name]),
											r &&
											((o = this),
												t.each(o.groups, function (e, n) {
													n === r && t("[name='" + o.escapeCssMeta(e) + "']", o.currentForm).attr("aria-describedby", a.attr("id"));
												})))),
								!n && this.settings.success && (a.text(""), "string" == typeof this.settings.success ? a.addClass(this.settings.success) : this.settings.success(a, e)),
								(this.toShow = this.toShow.add(a));
						},
						errorsFor: function (e) {
							var n = this.escapeCssMeta(this.idOrName(e)),
								i = t(e).attr("aria-describedby"),
								r = "label[for='" + n + "'], label[for='" + n + "'] *";
							return i && (r = r + ", #" + this.escapeCssMeta(i).replace(/\s+/g, ", #")), this.errors().filter(r);
						},
						escapeCssMeta: function (t) {
							return t.replace(/([\\!"#$%&'()*+,.\/:;<=>?@\[\]^`{|}~])/g, "\\$1");
						},
						idOrName: function (t) {
							return this.groups[t.name] || (this.checkable(t) ? t.name : t.id || t.name);
						},
						validationTargetFor: function (e) {
							return this.checkable(e) && (e = this.findByName(e.name)), t(e).not(this.settings.ignore)[0];
						},
						checkable: function (t) {
							return /radio|checkbox/i.test(t.type);
						},
						findByName: function (e) {
							return t(this.currentForm).find("[name='" + this.escapeCssMeta(e) + "']");
						},
						getLength: function (e, n) {
							switch (n.nodeName.toLowerCase()) {
								case "select":
									return t("option:selected", n).length;
								case "input":
									if (this.checkable(n)) return this.findByName(n.name).filter(":checked").length;
							}
							return e.length;
						},
						depend: function (t, e) {
							return !this.dependTypes[typeof t] || this.dependTypes[typeof t](t, e);
						},
						dependTypes: {
							boolean: function (t) {
								return t;
							},
							string: function (e, n) {
								return !!t(e, n.form).length;
							},
							function: function (t, e) {
								return t(e);
							},
						},
						optional: function (e) {
							var n = this.elementValue(e);
							return !t.validator.methods.required.call(this, n, e) && "dependency-mismatch";
						},
						startRequest: function (e) {
							this.pending[e.name] || (this.pendingRequest++, t(e).addClass(this.settings.pendingClass), (this.pending[e.name] = !0));
						},
						stopRequest: function (e, n) {
							this.pendingRequest--,
								this.pendingRequest < 0 && (this.pendingRequest = 0),
								delete this.pending[e.name],
								t(e).removeClass(this.settings.pendingClass),
								n && 0 === this.pendingRequest && this.formSubmitted && this.form()
									? (t(this.currentForm).submit(), this.submitButton && t("input:hidden[name='" + this.submitButton.name + "']", this.currentForm).remove(), (this.formSubmitted = !1))
									: !n && 0 === this.pendingRequest && this.formSubmitted && (t(this.currentForm).triggerHandler("invalid-form", [this]), (this.formSubmitted = !1));
						},
						previousValue: function (e, n) {
							return (n = ("string" == typeof n && n) || "remote"), t.data(e, "previousValue") || t.data(e, "previousValue", { old: null, valid: !0, message: this.defaultMessage(e, { method: n }) });
						},
						destroy: function () {
							this.resetForm(),
								t(this.currentForm)
									.off(".validate")
									.removeData("validator")
									.find(".validate-equalTo-blur")
									.off(".validate-equalTo")
									.removeClass("validate-equalTo-blur")
									.find(".validate-lessThan-blur")
									.off(".validate-lessThan")
									.removeClass("validate-lessThan-blur")
									.find(".validate-lessThanEqual-blur")
									.off(".validate-lessThanEqual")
									.removeClass("validate-lessThanEqual-blur")
									.find(".validate-greaterThanEqual-blur")
									.off(".validate-greaterThanEqual")
									.removeClass("validate-greaterThanEqual-blur")
									.find(".validate-greaterThan-blur")
									.off(".validate-greaterThan")
									.removeClass("validate-greaterThan-blur");
						},
					},
					classRuleSettings: { required: { required: !0 }, email: { email: !0 }, url: { url: !0 }, date: { date: !0 }, dateISO: { dateISO: !0 }, number: { number: !0 }, digits: { digits: !0 }, creditcard: { creditcard: !0 } },
					addClassRules: function (e, n) {
						e.constructor === String ? (this.classRuleSettings[e] = n) : t.extend(this.classRuleSettings, e);
					},
					classRules: function (e) {
						var n = {},
							i = t(e).attr("class");
						return (
							i &&
							t.each(i.split(" "), function () {
								this in t.validator.classRuleSettings && t.extend(n, t.validator.classRuleSettings[this]);
							}),
							n
						);
					},
					normalizeAttributeRule: function (t, e, n, i) {
						/min|max|step/.test(n) && (null === e || /number|range|text/.test(e)) && ((i = Number(i)), isNaN(i) && (i = void 0)), i || 0 === i ? (t[n] = i) : e === n && "range" !== e && (t[n] = !0);
					},
					attributeRules: function (e) {
						var n,
							i,
							r = {},
							s = t(e),
							o = e.getAttribute("type");
						for (n in t.validator.methods) "required" === n ? ((i = e.getAttribute(n)), "" === i && (i = !0), (i = !!i)) : (i = s.attr(n)), this.normalizeAttributeRule(r, o, n, i);
						return r.maxlength && /-1|2147483647|524288/.test(r.maxlength) && delete r.maxlength, r;
					},
					dataRules: function (e) {
						var n,
							i,
							r = {},
							s = t(e),
							o = e.getAttribute("type");
						for (n in t.validator.methods) (i = s.data("rule" + n.charAt(0).toUpperCase() + n.substring(1).toLowerCase())), "" === i && (i = !0), this.normalizeAttributeRule(r, o, n, i);
						return r;
					},
					staticRules: function (e) {
						var n = {},
							i = t.data(e.form, "validator");
						return i.settings.rules && (n = t.validator.normalizeRule(i.settings.rules[e.name]) || {}), n;
					},
					normalizeRules: function (e, n) {
						return (
							t.each(e, function (i, r) {
								if (r === !1) return void delete e[i];
								if (r.param || r.depends) {
									var s = !0;
									switch (typeof r.depends) {
										case "string":
											s = !!t(r.depends, n.form).length;
											break;
										case "function":
											s = r.depends.call(n, n);
									}
									s ? (e[i] = void 0 === r.param || r.param) : (t.data(n.form, "validator").resetElements(t(n)), delete e[i]);
								}
							}),
							t.each(e, function (i, r) {
								e[i] = t.isFunction(r) && "normalizer" !== i ? r(n) : r;
							}),
							t.each(["minlength", "maxlength"], function () {
								e[this] && (e[this] = Number(e[this]));
							}),
							t.each(["rangelength", "range"], function () {
								var n;
								e[this] &&
									(t.isArray(e[this])
										? (e[this] = [Number(e[this][0]), Number(e[this][1])])
										: "string" == typeof e[this] && ((n = e[this].replace(/[\[\]]/g, "").split(/[\s,]+/)), (e[this] = [Number(n[0]), Number(n[1])])));
							}),
							t.validator.autoCreateRanges &&
							(null != e.min && null != e.max && ((e.range = [e.min, e.max]), delete e.min, delete e.max),
								null != e.minlength && null != e.maxlength && ((e.rangelength = [e.minlength, e.maxlength]), delete e.minlength, delete e.maxlength)),
							e
						);
					},
					normalizeRule: function (e) {
						if ("string" == typeof e) {
							var n = {};
							t.each(e.split(/\s/), function () {
								n[this] = !0;
							}),
								(e = n);
						}
						return e;
					},
					addMethod: function (e, n, i) {
						(t.validator.methods[e] = n), (t.validator.messages[e] = void 0 !== i ? i : t.validator.messages[e]), n.length < 3 && t.validator.addClassRules(e, t.validator.normalizeRule(e));
					},
					methods: {
						required: function (e, n, i) {
							if (!this.depend(i, n)) return "dependency-mismatch";
							if ("select" === n.nodeName.toLowerCase()) {
								var r = t(n).val();
								return r && r.length > 0;
							}
							return this.checkable(n) ? this.getLength(e, n) > 0 : void 0 !== e && null !== e && e.length > 0;
						},
						email: function (t, e) {
							return this.optional(e) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(t);
						},
						url: function (t, e) {
							return (
								this.optional(e) ||
								/^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i.test(
									t
								)
							);
						},
						date: (function () {
							var t = !1;
							return function (e, n) {
								return (
									t ||
									((t = !0),
										this.settings.debug &&
										window.console &&
										console.warn(
											"The `date` method is deprecated and will be removed in version '2.0.0'.\nPlease don't use it, since it relies on the Date constructor, which\nbehaves very differently across browsers and locales. Use `dateISO`\ninstead or one of the locale specific methods in `localizations/`\nand `additional-methods.js`."
										)),
									this.optional(n) || !/Invalid|NaN/.test(new Date(e).toString())
								);
							};
						})(),
						dateISO: function (t, e) {
							return this.optional(e) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(t);
						},
						number: function (t, e) {
							return this.optional(e) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(t);
						},
						digits: function (t, e) {
							return this.optional(e) || /^\d+$/.test(t);
						},
						minlength: function (e, n, i) {
							var r = t.isArray(e) ? e.length : this.getLength(e, n);
							return this.optional(n) || r >= i;
						},
						maxlength: function (e, n, i) {
							var r = t.isArray(e) ? e.length : this.getLength(e, n);
							return this.optional(n) || r <= i;
						},
						rangelength: function (e, n, i) {
							var r = t.isArray(e) ? e.length : this.getLength(e, n);
							return this.optional(n) || (r >= i[0] && r <= i[1]);
						},
						min: function (t, e, n) {
							return this.optional(e) || t >= n;
						},
						max: function (t, e, n) {
							return this.optional(e) || t <= n;
						},
						range: function (t, e, n) {
							return this.optional(e) || (t >= n[0] && t <= n[1]);
						},
						step: function (e, n, i) {
							var r,
								s = t(n).attr("type"),
								o = "Step attribute on input type " + s + " is not supported.",
								a = ["text", "number", "range"],
								l = new RegExp("\\b" + s + "\\b"),
								u = s && !l.test(a.join()),
								c = function (t) {
									var e = ("" + t).match(/(?:\.(\d+))?$/);
									return e && e[1] ? e[1].length : 0;
								},
								h = function (t) {
									return Math.round(t * Math.pow(10, r));
								},
								d = !0;
							if (u) throw new Error(o);
							return (r = c(i)), (c(e) > r || h(e) % h(i) !== 0) && (d = !1), this.optional(n) || d;
						},
						equalTo: function (e, n, i) {
							var r = t(i);
							return (
								this.settings.onfocusout &&
								r.not(".validate-equalTo-blur").length &&
								r.addClass("validate-equalTo-blur").on("blur.validate-equalTo", function () {
									t(n).valid();
								}),
								e === r.val()
							);
						},
						remote: function (e, n, i, r) {
							if (this.optional(n)) return "dependency-mismatch";
							r = ("string" == typeof r && r) || "remote";
							var s,
								o,
								a,
								l = this.previousValue(n, r);
							return (
								this.settings.messages[n.name] || (this.settings.messages[n.name] = {}),
								(l.originalMessage = l.originalMessage || this.settings.messages[n.name][r]),
								(this.settings.messages[n.name][r] = l.message),
								(i = ("string" == typeof i && { url: i }) || i),
								(a = t.param(t.extend({ data: e }, i.data))),
								l.old === a
									? l.valid
									: ((l.old = a),
										(s = this),
										this.startRequest(n),
										(o = {}),
										(o[n.name] = e),
										t.ajax(
											t.extend(
												!0,
												{
													mode: "abort",
													port: "validate" + n.name,
													dataType: "json",
													data: o,
													context: s.currentForm,
													success: function (t) {
														var i,
															o,
															a,
															u = t === !0 || "true" === t;
														(s.settings.messages[n.name][r] = l.originalMessage),
															u
																? ((a = s.formSubmitted), s.resetInternals(), (s.toHide = s.errorsFor(n)), (s.formSubmitted = a), s.successList.push(n), (s.invalid[n.name] = !1), s.showErrors())
																: ((i = {}), (o = t || s.defaultMessage(n, { method: r, parameters: e })), (i[n.name] = l.message = o), (s.invalid[n.name] = !0), s.showErrors(i)),
															(l.valid = u),
															s.stopRequest(n, u);
													},
												},
												i
											)
										),
										"pending")
							);
						},
					},
				});
			var e,
				n = {};
			return (
				t.ajaxPrefilter
					? t.ajaxPrefilter(function (t, e, i) {
						var r = t.port;
						"abort" === t.mode && (n[r] && n[r].abort(), (n[r] = i));
					})
					: ((e = t.ajax),
						(t.ajax = function (i) {
							var r = ("mode" in i ? i : t.ajaxSettings).mode,
								s = ("port" in i ? i : t.ajaxSettings).port;
							return "abort" === r ? (n[s] && n[s].abort(), (n[s] = e.apply(this, arguments)), n[s]) : e.apply(this, arguments);
						})),
				t
			);
		});
	},
	function (t, e, n) {
		var i,
			r,
			s; /*!
	  SerializeJSON jQuery plugin.
	  https://github.com/marioizquierdo/jquery.serializeJSON
	  version 2.9.0 (Jan, 2018)
	
	  Copyright (c) 2012-2018 Mario Izquierdo
	  Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
	  and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
	*/
		!(function (o) {
			(r = [n(3)]), (i = o), (s = "function" == typeof i ? i.apply(e, r) : i), !(void 0 !== s && (t.exports = s));
		})(function (t) {
			"use strict";
			(t.fn.serializeJSON = function (e) {
				var n, i, r, s, o, a, l, u, c, h, d, f, p;
				return (
					(n = t.serializeJSON),
					(i = this),
					(r = n.setupOpts(e)),
					(s = i.serializeArray()),
					n.readCheckboxUncheckedValues(s, r, i),
					(o = {}),
					t.each(s, function (t, e) {
						(a = e.name),
							(l = e.value),
							(c = n.extractTypeAndNameWithNoType(a)),
							(h = c.nameWithNoType),
							(d = c.type),
							d || (d = n.attrFromInputWithName(i, a, "data-value-type")),
							n.validateType(a, d, r),
							"skip" !== d && ((f = n.splitInputNameIntoKeysArray(h)), (u = n.parseValue(l, a, d, r)), (p = !u && n.shouldSkipFalsy(i, a, h, d, r)), p || n.deepSet(o, f, u, r));
					}),
					o
				);
			}),
				(t.serializeJSON = {
					defaultOptions: {
						checkboxUncheckedValue: void 0,
						parseNumbers: !1,
						parseBooleans: !1,
						parseNulls: !1,
						parseAll: !1,
						parseWithFunction: null,
						skipFalsyValuesForTypes: [],
						skipFalsyValuesForFields: [],
						customTypes: {},
						defaultTypes: {
							string: function (t) {
								return String(t);
							},
							number: function (t) {
								return Number(t);
							},
							boolean: function (t) {
								var e = ["false", "null", "undefined", "", "0"];
								return e.indexOf(t) === -1;
							},
							null: function (t) {
								var e = ["false", "null", "undefined", "", "0"];
								return e.indexOf(t) === -1 ? t : null;
							},
							array: function (t) {
								return JSON.parse(t);
							},
							object: function (t) {
								return JSON.parse(t);
							},
							auto: function (e) {
								return t.serializeJSON.parseValue(e, null, null, { parseNumbers: !0, parseBooleans: !0, parseNulls: !0 });
							},
							skip: null,
						},
						useIntKeysAsArrayIndex: !1,
					},
					setupOpts: function (e) {
						var n, i, r, s, o, a;
						(a = t.serializeJSON),
							null == e && (e = {}),
							(r = a.defaultOptions || {}),
							(i = [
								"checkboxUncheckedValue",
								"parseNumbers",
								"parseBooleans",
								"parseNulls",
								"parseAll",
								"parseWithFunction",
								"skipFalsyValuesForTypes",
								"skipFalsyValuesForFields",
								"customTypes",
								"defaultTypes",
								"useIntKeysAsArrayIndex",
							]);
						for (n in e) if (i.indexOf(n) === -1) throw new Error("serializeJSON ERROR: invalid option '" + n + "'. Please use one of " + i.join(", "));
						return (
							(s = function (t) {
								return e[t] !== !1 && "" !== e[t] && (e[t] || r[t]);
							}),
							(o = s("parseAll")),
							{
								checkboxUncheckedValue: s("checkboxUncheckedValue"),
								parseNumbers: o || s("parseNumbers"),
								parseBooleans: o || s("parseBooleans"),
								parseNulls: o || s("parseNulls"),
								parseWithFunction: s("parseWithFunction"),
								skipFalsyValuesForTypes: s("skipFalsyValuesForTypes"),
								skipFalsyValuesForFields: s("skipFalsyValuesForFields"),
								typeFunctions: t.extend({}, s("defaultTypes"), s("customTypes")),
								useIntKeysAsArrayIndex: s("useIntKeysAsArrayIndex"),
							}
						);
					},
					parseValue: function (e, n, i, r) {
						var s, o;
						return (
							(s = t.serializeJSON),
							(o = e),
							r.typeFunctions && i && r.typeFunctions[i]
								? (o = r.typeFunctions[i](e))
								: r.parseNumbers && s.isNumeric(e)
									? (o = Number(e))
									: !r.parseBooleans || ("true" !== e && "false" !== e)
										? r.parseNulls && "null" == e
											? (o = null)
											: r.typeFunctions && r.typeFunctions.string && (o = r.typeFunctions.string(e))
										: (o = "true" === e),
							r.parseWithFunction && !i && (o = r.parseWithFunction(o, n)),
							o
						);
					},
					isObject: function (t) {
						return t === Object(t);
					},
					isUndefined: function (t) {
						return void 0 === t;
					},
					isValidArrayIndex: function (t) {
						return /^[0-9]+$/.test(String(t));
					},
					isNumeric: function (t) {
						return t - parseFloat(t) >= 0;
					},
					optionKeys: function (t) {
						if (Object.keys) return Object.keys(t);
						var e,
							n = [];
						for (e in t) n.push(e);
						return n;
					},
					readCheckboxUncheckedValues: function (e, n, i) {
						var r, s, o, a, l;
						null == n && (n = {}),
							(l = t.serializeJSON),
							(r = "input[type=checkbox][name]:not(:checked):not([disabled])"),
							(s = i.find(r).add(i.filter(r))),
							s.each(function (i, r) {
								if (((o = t(r)), (a = o.attr("data-unchecked-value")), null == a && (a = n.checkboxUncheckedValue), null != a)) {
									if (r.name && r.name.indexOf("[][") !== -1)
										throw new Error(
											"serializeJSON ERROR: checkbox unchecked values are not supported on nested arrays of objects like '" + r.name + "'. See https://github.com/marioizquierdo/jquery.serializeJSON/issues/67"
										);
									e.push({ name: r.name, value: a });
								}
							});
					},
					extractTypeAndNameWithNoType: function (t) {
						var e;
						return (e = t.match(/(.*):([^:]+)$/)) ? { nameWithNoType: e[1], type: e[2] } : { nameWithNoType: t, type: null };
					},
					shouldSkipFalsy: function (e, n, i, r, s) {
						var o = t.serializeJSON,
							a = o.attrFromInputWithName(e, n, "data-skip-falsy");
						if (null != a) return "false" !== a;
						var l = s.skipFalsyValuesForFields;
						if (l && (l.indexOf(i) !== -1 || l.indexOf(n) !== -1)) return !0;
						var u = s.skipFalsyValuesForTypes;
						return null == r && (r = "string"), !(!u || u.indexOf(r) === -1);
					},
					attrFromInputWithName: function (t, e, n) {
						var i, r, s;
						return (i = e.replace(/(:|\.|\[|\]|\s)/g, "\\$1")), (r = '[name="' + i + '"]'), (s = t.find(r).add(t.filter(r))), s.attr(n);
					},
					validateType: function (e, n, i) {
						var r, s;
						if (((s = t.serializeJSON), (r = s.optionKeys(i ? i.typeFunctions : s.defaultOptions.defaultTypes)), n && r.indexOf(n) === -1))
							throw new Error("serializeJSON ERROR: Invalid type " + n + " found in input name '" + e + "', please use one of " + r.join(", "));
						return !0;
					},
					splitInputNameIntoKeysArray: function (e) {
						var n, i;
						return (
							(i = t.serializeJSON),
							(n = e.split("[")),
							(n = t.map(n, function (t) {
								return t.replace(/\]/g, "");
							})),
							"" === n[0] && n.shift(),
							n
						);
					},
					deepSet: function (e, n, i, r) {
						var s, o, a, l, u, c;
						if ((null == r && (r = {}), (c = t.serializeJSON), c.isUndefined(e))) throw new Error("ArgumentError: param 'o' expected to be an object or array, found undefined");
						if (!n || 0 === n.length) throw new Error("ArgumentError: param 'keys' expected to be an array with least one element");
						(s = n[0]),
							1 === n.length
								? "" === s
									? e.push(i)
									: (e[s] = i)
								: ((o = n[1]),
									"" === s && ((l = e.length - 1), (u = e[l]), (s = c.isObject(u) && (c.isUndefined(u[o]) || n.length > 2) ? l : l + 1)),
									"" === o
										? (!c.isUndefined(e[s]) && t.isArray(e[s])) || (e[s] = [])
										: r.useIntKeysAsArrayIndex && c.isValidArrayIndex(o)
											? (!c.isUndefined(e[s]) && t.isArray(e[s])) || (e[s] = [])
											: (!c.isUndefined(e[s]) && c.isObject(e[s])) || (e[s] = {}),
									(a = n.slice(1)),
									c.deepSet(e[s], a, i, r));
					},
				});
		});
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		function s(t, e, n, i) {
			var r = arguments.length > 4 && void 0 !== arguments[4] ? arguments[4] : 1,
				s = t / e,
				o = n * r,
				a = o / s;
			return a < i * r && ((a = i * r), (o = a * s)), { left: (n - o) / 2, top: (i - a) / 2, width: o, height: a };
		}
		var o = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			a = n(3),
			l = i(a),
			u = n(65),
			c = i(u),
			h = n(63),
			d = i(h),
			f = (function () {
				function t(e) {
					r(this, t), (this.$container = (0, l["default"])(e)), d["default"].supportsObjectFit || (l["default"].scroller.onpassive("resize", this.update.bind(this)), this.update());
				}
				return (
					o(t, [
						{
							key: "update",
							value: function () {
								var t = this.$container,
									e = t.get(0),
									n = e.videoWidth || 1280,
									i = e.videoHeight || 720,
									r = (0, l["default"])("body").width(),
									o = window.innerHeight,
									a = s(n, i, r, o);
								this.$container.css(a);
							},
						},
					]),
					t
				);
			})();
		l["default"].fn.objectFitVideo = (0, c["default"])(f);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(65),
			u = i(l);
		n(169);
		var c = (function () {
			function t(e, n) {
				r(this, t);
				var i = (this.options = a["default"].extend({}, this.constructor.Defaults, n)),
					s = (this.$container = (0, a["default"])(e)),
					o = (this.$cookiesClose = s.find(i.cookiesClose));
				o.on("tap", this.handleClose.bind(this)), void 0 === this.getCookie("cookiesNotification") && this.showNotification();
			}
			return (
				s(t, null, [
					{
						key: "Defaults",
						get: function () {
							return { cookiesClose: ".js-cookies-close" };
						},
					},
				]),
				s(t, [
					{
						key: "handleClose",
						value: function () {
							this.hideNotification(), this.setCookie("cookiesNotification", "isShown", { expires: 2592e3, path: "/" });
						},
					},
					{
						key: "showNotification",
						value: function () {
							this.$container.transition("fade-in");
						},
					},
					{
						key: "hideNotification",
						value: function () {
							this.$container.transition("fade-out");
						},
					},
					{
						key: "setCookie",
						value: function (t, e, n) {
							n = n || {};
							var i = n.expires;
							if ("number" == typeof i && i) {
								var r = new Date();
								r.setTime(r.getTime() + 1e3 * i), (i = n.expires = r);
							}
							i && i.toUTCString && (n.expires = i.toUTCString()), (e = encodeURIComponent(e));
							var s = t + "=" + e;
							for (var o in n) {
								s += "; " + o;
								var a = n[o];
								a !== !0 && (s += "=" + a);
							}
							document.cookie = s;
						},
					},
					{
						key: "getCookie",
						value: function (t) {
							var e = document.cookie.match(new RegExp("(?:^|; )" + t.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"));
							return e ? decodeURIComponent(e[1]) : void 0;
						},
					},
				]),
				t
			);
		})();
		a["default"].fn.cookiesNotification = (0, u["default"])(c);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(65),
			u = i(l),
			c = (function () {
				function t(e, n) {
					var i = this;
					r(this, t), (this.$container = e);
					var s = ((this.$elements = e.find("[data-questionnaire-visible]")), (this.$input = e.find(".js-questionnaire-type"))),
						o = (this.$lightbox = e.closest(".lightbox"));
					s.on("change", function () {
						i.updateUI(), i.handleInputChange();
					}),
						this.$lightbox.length &&
						(o.on("open.lightbox", this.handleLightboxOpen.bind(this)),
							o.on("close.lightbox", this.handleLightboxClose.bind(this)),
							(0, a["default"])(window).on("hashchange", this.handleHashChange.bind(this)),
							document.location.hash && this.handleHashChange()),
						this.updateUI();
				}
				return (
					s(t, [
						{
							key: "updateUI",
							value: function () {
								var t = this.$input.val();
								this.$elements.each(function () {
									var e = (0, a["default"])(this).data("questionnaireVisible"),
										n = (0, a["default"])(this),
										i = n.find("input,select,textarea"),
										r = e.indexOf(t) !== -1;
									n.toggleClass("is-hidden", !r), i.prop("disabled", !r);
								}),
									this.$container.trigger("resize");
							},
						},
						{
							key: "handleInputChange",
							value: function () {
								var t = this.$input.val(),
									e = this.sanitizeValue(document.location.hash);
								e !== t && (document.location.hash = t);
							},
						},
						{
							key: "handleLightboxOpen",
							value: function () {
								this.handleInputChange();
							},
						},
						{
							key: "handleLightboxClose",
							value: function () {
								document.location.hash = "";
							},
						},
						{
							key: "handleHashChange",
							value: function () {
								var t = this.sanitizeValue(document.location.hash);
								t ? (this.$lightbox.lightbox("open"), t !== this.$input.val() && this.$input.val(t).change()) : this.$lightbox.lightbox("close");
							},
						},
						{
							key: "sanitizeValue",
							value: function (t) {
								var e = (t || "").replace(/^#/, ""),
									n = this.$input.find("option").filter(function (t, n) {
										return (0, a["default"])(n).val() === e;
									});
								return n.length ? e : "";
							},
						},
					]),
					t
				);
			})();
		a["default"].fn.questionnaire = (0, u["default"])(c);
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		var r = n(3),
			s = i(r),
			o = n(65),
			a = i(o),
			l = s["default"].Deferred();
		(window.recaptchaOnloadCallback = function () {
			console.log("recaptcha loaded"), l.resolve();
		}),
			(s["default"].fn.recaptcha = (0, a["default"])(function (t, e) {
				l.then(function () {
					var e = (grecaptcha.render(t.get(0), { sitekey: t.data("sitekey") }), t.find("input, textarea"));
					e.rules("add", { required: !0 });
				});
			}));
	},
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		Object.defineProperty(e, "__esModule", { value: !0 });
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(178),
			u = i(l),
			c = n(65),
			h = i(c),
			d = n(62),
			f = i(d);
		n(267), n(283);
		var p = (function () {
			function t(e, n) {
				r(this, t);
				var i = (this.options = a["default"].extend({}, this.constructor.Defaults, n)),
					s = (this.$container = (0, a["default"])(e)),
					o = (this.$list = this.findElement(s, i.listSelector) || s),
					l = (this.$filters = this.findElement(s, i.filtersSelector) || s),
					c = (this.$more = this.findElement(s, i.moreSelector) || (0, a["default"])()),
					h = (this.$pagination = this.findElement(s, i.paginationSelector) || (0, a["default"])());
				(this.$header = this.findElement(s, i.headerSelector) || (0, a["default"])()),
					(this.$empty = this.findElement(s, i.emptySelector) || (0, a["default"])()),
					(this.$template = this.findElement(o, i.templateSelector).template(i));
				(this.loading = !1),
					(this.offset = this.getItemCountFromDOM()),
					h.length && (h.addClass("is-hidden"), c.removeClass("is-hidden")),
					c.on("click", this.load.bind(this)),
					i.reloadOnFilterChange
						? l.on("change", (0, u["default"])(this.handleFilterChange.bind(this), 60))
						: l.on("submit", this.handleFilterChange.bind(this)).on("submit", function (t) {
							return t.preventDefault();
						});
			}
			return (
				s(t, null, [
					{
						key: "Defaults",
						get: function () {
							return {
								filtersSelector: ".js-ajax-filters",
								listSelector: ".js-ajax-list",
								headerSelector: ".js-ajax-list-header",
								emptySelector: ".js-ajax-empty",
								emptyMessageSelector: ".js-ajax-empty-message",
								moreSelector: ".js-ajax-load-more",
								paginationSelector: ".js-ajax-load-more-pagination",
								templateSelector: 'script[type="text/template"]',
								endpoint: null,
								endpointFormat: "json",
								removeSiblings: !1,
								reloadOnFilterChange: !0,
							};
						},
					},
				]),
				s(t, [
					{
						key: "findElement",
						value: function (t, e) {
							var n = !(arguments.length > 2 && void 0 !== arguments[2]) || arguments[2],
								i = t.filter(e);
							return (
								i.length || ((i = t.find(e)), n && (i = i.not(t.find(this.options.emptySelector + " " + e)))),
								i.length || (i = t.nextAll(e)),
								i.length || (i = t.prevAll(e)),
								i.length || (i = t.parent().nextAll(e)),
								i.length ? i : null
							);
						},
					},
					{
						key: "load",
						value: function () {
							if (!this.loading) {
								this.loading = !0;
								var t = this.options;
								a["default"].ajax({ url: t.endpoint, data: this.getFilterValues(), dataType: t.endpointFormat }).done(this.handleLoadResponse.bind(this)).always(this.handleLoadComplete.bind(this));
							}
						},
					},
					{
						key: "handleLoadComplete",
						value: function () {
							this.loading = !1;
						},
					},
					{
						key: "handleLoadResponse",
						value: function (t) {
							var e = this.$more,
								n = this.$list,
								i = this.total,
								r = this.offset;
							if (("total" in t && (i = this.total = t.total), "data" in t && t.data)) {
								var s = r ? "append" : "replace";
								this.$template.template(s, t.data), (r = this.offset = r + t.data.length), !t.data.length && i && i > r && (r = this.offset = i);
							}
							i && i > r ? e.removeClass("is-hidden") : e.addClass("is-hidden"),
								i ? (this.hideEmptyMessage(), this.$header.template("replace", { total: this.total, offset: this.offset })) : this.showEmptyMessage(t.message),
								(0, f["default"])(),
								n.app(),
								n.trigger("resize");
						},
					},
					{
						key: "getItemCountFromDOM",
						value: function () {
							return this.$list.children().length;
						},
					},
					{
						key: "showEmptyMessage",
						value: function (t) {
							var e = this.$empty,
								n = this.$header,
								i = this.$list,
								r = this.$more;
							if ((e.removeClass("is-hidden"), n.addClass("is-hidden"), r.addClass("is-hidden"), i.addClass("is-hidden"), t && "string" == typeof t)) {
								var s = e.find(this.options.emptyMessageSelector);
								s.text(t);
							}
						},
					},
					{
						key: "hideEmptyMessage",
						value: function () {
							var t = this.$empty,
								e = this.$header,
								n = this.$list;
							t.addClass("is-hidden"), e.removeClass("is-hidden"), n.removeClass("is-hidden");
						},
					},
					{
						key: "getFilterValues",
						value: function () {
							var t = this.$filters.serializeObject();
							return (t.locale = (0, a["default"])("html").attr("lang")), (t.offset = this.offset), t;
						},
					},
					{
						key: "handleFilterChange",
						value: function () {
							(this.offset = 0), this.load();
						},
					},
				]),
				t
			);
		})();
		(e["default"] = p), (a["default"].fn.ajaxlist = (0, h["default"])(p));
	},
	function (t, e, n) {
		(function (t) {
			"use strict";
			function i(t) {
				return t && t.__esModule ? t : { default: t };
			}
			function r(t, e) {
				if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
			}
			Object.defineProperty(e, "__esModule", { value: !0 });
			var s = (function () {
				function t(t, e) {
					for (var n = 0; n < e.length; n++) {
						var i = e[n];
						(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
					}
				}
				return function (e, n, i) {
					return n && t(e.prototype, n), i && t(e, i), e;
				};
			})(),
				o = n(268),
				a = i(o),
				l = n(65),
				u = i(l),
				c = (function () {
					function e(n, i) {
						r(this, e);
						var s = t.extend({}, this.constructor.Defaults, i),
							o = t(n),
							l = o.find('script[type="text/template"]').addBack('script[type="text/template"]'),
							u = !s.removeSiblings && l.prev(),
							c = !s.removeSiblings && l.next(),
							h = s.removeSiblings && l.prev(),
							d = l.parent(),
							f = (0, a["default"])(l.remove().html());
						(this.$prev = u.length ? u : null), (this.$next = c.length ? c : null), (this.$parent = d.length ? d : null), (this.$last = h.length ? h : null), (this.template = f), (this.options = s);
					}
					return (
						s(e, null, [
							{
								key: "Defaults",
								get: function () {
									return { removeSiblings: !1, postFilter: null };
								},
							},
						]),
						s(e, [
							{
								key: "reset",
								value: function () {
									var t = this.$prev,
										e = this.$next,
										n = this.$parent,
										i = t ? t.next() : null;
									if ((!t && e && (i = n.children().eq(0)), i)) for (var r = void 0; i.length && (!e || !i.is(e));) (r = i), (i = i.next()), r.remove();
									else n && n.empty();
									this.$last = null;
								},
							},
							{
								key: "append",
								value: function (e) {
									for (
										var n = this.$prev, i = this.$next, r = this.$parent, s = this.$last, o = this.template, a = this.options.postFilter, l = t.isArray(e) ? e : e ? [e] : [], u = 0, c = l.length, h = "", d = void 0;
										u < c;
										u++
									)
										try {
											h += o(l[u]);
										} catch (f) {
											console.error(f);
										}
									"function" == typeof a && (h = String(a(h)));
									try {
										(d = t(h)), (this.$last = d.eq(-1));
									} catch (p) {
										this.$last = null;
									}
									s ? s.after(d.length ? d : h) : n ? n.after(d.length ? d : h) : i ? i.before(d.length ? d : h) : r && r.empty().append(d.length ? d : h);
								},
							},
							{
								key: "replace",
								value: function (t) {
									this.reset(), this.append(t);
								},
							},
						]),
						e
					);
				})();
			(t.fn.template = (0, u["default"])(c)), (e["default"] = c);
		}.call(e, n(3)));
	},
	function (t, e, n) {
		function i(t, e, n) {
			var i = f.imports._.templateSettings || f;
			n && c(t, e, n) && (e = void 0), (t = p(t)), (e = r({}, e, i, a));
			var w,
				C,
				k = r({}, e.imports, i.imports, a),
				T = h(k),
				S = o(k, T),
				E = 0,
				j = e.interpolate || b,
				O = "__p += '",
				$ = RegExp((e.escape || b).source + "|" + j.source + "|" + (j === d ? y : b).source + "|" + (e.evaluate || b).source + "|$", "g"),
				D = _.call(e, "sourceURL") ? "//# sourceURL=" + (e.sourceURL + "").replace(/[\r\n]/g, " ") + "\n" : "";
			t.replace($, function (e, n, i, r, s, o) {
				return (
					i || (i = r),
					(O += t.slice(E, o).replace(x, l)),
					n && ((w = !0), (O += "' +\n__e(" + n + ") +\n'")),
					s && ((C = !0), (O += "';\n" + s + ";\n__p += '")),
					i && (O += "' +\n((__t = (" + i + ")) == null ? '' : __t) +\n'"),
					(E = o + e.length),
					e
				);
			}),
				(O += "';\n");
			var I = _.call(e, "variable") && e.variable;
			I || (O = "with (obj) {\n" + O + "\n}\n"),
				(O = (C ? O.replace(v, "") : O).replace(g, "$1").replace(m, "$1;")),
				(O =
					"function(" +
					(I || "obj") +
					") {\n" +
					(I ? "" : "obj || (obj = {});\n") +
					"var __t, __p = ''" +
					(w ? ", __e = _.escape" : "") +
					(C ? ", __j = Array.prototype.join;\nfunction print() { __p += __j.call(arguments, '') }\n" : ";\n") +
					O +
					"return __p\n}");
			var A = s(function () {
				return Function(T, D + "return " + O).apply(void 0, S);
			});
			if (((A.source = O), u(A))) throw A;
			return A;
		}
		var r = n(269),
			s = n(273),
			o = n(186),
			a = n(277),
			l = n(278),
			u = n(274),
			c = n(40),
			h = n(45),
			d = n(279),
			f = n(280),
			p = n(141),
			v = /\b__p \+= '';/g,
			g = /\b(__p \+=) '' \+/g,
			m = /(__e\(.*?\)|\b__t\)) \+\n'';/g,
			y = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g,
			b = /($^)/,
			x = /['\n\r\u2028\u2029\\]/g,
			w = Object.prototype,
			_ = w.hasOwnProperty;
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(30),
			r = n(31),
			s = n(270),
			o = r(function (t, e, n, r) {
				i(e, s(e), t, r);
			});
		t.exports = o;
	},
	function (t, e, n) {
		function i(t) {
			return o(t) ? r(t, !0) : s(t);
		}
		var r = n(46),
			s = n(271),
			o = n(41);
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			if (!r(t)) return o(t);
			var e = s(t),
				n = [];
			for (var i in t) ("constructor" != i || (!e && l.call(t, i))) && n.push(i);
			return n;
		}
		var r = n(24),
			s = n(44),
			o = n(272),
			a = Object.prototype,
			l = a.hasOwnProperty;
		t.exports = i;
	},
	function (t, e) {
		function n(t) {
			var e = [];
			if (null != t) for (var n in Object(t)) e.push(n);
			return e;
		}
		t.exports = n;
	},
	function (t, e, n) {
		var i = n(35),
			r = n(32),
			s = n(274),
			o = r(function (t, e) {
				try {
					return i(t, void 0, e);
				} catch (n) {
					return s(n) ? n : new Error(n);
				}
			});
		t.exports = o;
	},
	function (t, e, n) {
		function i(t) {
			if (!s(t)) return !1;
			var e = r(t);
			return e == l || e == a || ("string" == typeof t.message && "string" == typeof t.name && !o(t));
		}
		var r = n(18),
			s = n(50),
			o = n(275),
			a = "[object DOMException]",
			l = "[object Error]";
		t.exports = i;
	},
	function (t, e, n) {
		function i(t) {
			if (!o(t) || r(t) != a) return !1;
			var e = s(t);
			if (null === e) return !0;
			var n = h.call(e, "constructor") && e.constructor;
			return "function" == typeof n && n instanceof n && c.call(n) == d;
		}
		var r = n(18),
			s = n(276),
			o = n(50),
			a = "[object Object]",
			l = Function.prototype,
			u = Object.prototype,
			c = l.toString,
			h = u.hasOwnProperty,
			d = c.call(Object);
		t.exports = i;
	},
	function (t, e, n) {
		var i = n(61),
			r = i(Object.getPrototypeOf, Object);
		t.exports = r;
	},
	function (t, e, n) {
		function i(t, e, n, i) {
			return void 0 === t || (r(t, s[n]) && !o.call(i, n)) ? e : t;
		}
		var r = n(29),
			s = Object.prototype,
			o = s.hasOwnProperty;
		t.exports = i;
	},
	function (t, e) {
		function n(t) {
			return "\\" + i[t];
		}
		var i = { "\\": "\\", "'": "'", "\n": "n", "\r": "r", "\u2028": "u2028", "\u2029": "u2029" };
		t.exports = n;
	},
	function (t, e) {
		var n = /<%=([\s\S]+?)%>/g;
		t.exports = n;
	},
	function (t, e, n) {
		var i = n(204),
			r = n(281),
			s = n(282),
			o = n(279),
			a = { escape: r, evaluate: s, interpolate: o, variable: "", imports: { _: { escape: i } } };
		t.exports = a;
	},
	function (t, e) {
		var n = /<%-([\s\S]+?)%>/g;
		t.exports = n;
	},
	function (t, e) {
		var n = /<%([\s\S]+?)%>/g;
		t.exports = n;
	},
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		var r = n(3),
			s = i(r),
			o = n(190),
			a = i(o);
		s["default"].fn.serializeObject = function () {
			var t = this.is("form,input,select,textarea") ? this : this.find("form,input,select,textarea"),
				e = t.serializeArray();
			return (0, a["default"])(
				e,
				function (t, e) {
					var n = e.value || 0 === e.value ? e.value : "";
					return void 0 !== t[e.name] ? (t[e.name].push || (t[e.name] = [t[e.name]]), t[e.name].push(n)) : (t[e.name] = n), t;
				},
				{}
			);
		};
	},
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	,
	function (t, e, n) {
		(function (t, e) {
			!(function (t, n, i, r) {
				function s(e, n) {
					(this.settings = null),
						(this.options = t.extend({}, s.Defaults, n)),
						(this.$element = t(e)),
						(this._handlers = {}),
						(this._plugins = {}),
						(this._supress = {}),
						(this._current = null),
						(this._speed = null),
						(this._coordinates = []),
						(this._breakpoint = null),
						(this._width = null),
						(this._items = []),
						(this._clones = []),
						(this._mergers = []),
						(this._widths = []),
						(this._invalidated = {}),
						(this._pipe = []),
						(this._drag = { time: null, target: null, pointer: null, stage: { start: null, current: null }, direction: null }),
						(this._states = { current: {}, tags: { initializing: ["busy"], animating: ["busy"], dragging: ["interacting"] } }),
						t.each(
							["onResize", "onThrottledResize"],
							t.proxy(function (e, n) {
								this._handlers[n] = t.proxy(this[n], this);
							}, this)
						),
						t.each(
							s.Plugins,
							t.proxy(function (t, e) {
								this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this);
							}, this)
						),
						t.each(
							s.Workers,
							t.proxy(function (e, n) {
								this._pipe.push({ filter: n.filter, run: t.proxy(n.run, this) });
							}, this)
						),
						this.setup(),
						this.initialize();
				}
				(s.Defaults = {
					items: 3,
					loop: !1,
					center: !1,
					rewind: !1,
					mouseDrag: !0,
					touchDrag: !0,
					pullDrag: !0,
					freeDrag: !1,
					margin: 0,
					stagePadding: 0,
					merge: !1,
					mergeFit: !0,
					autoWidth: !1,
					startPosition: 0,
					rtl: !1,
					smartSpeed: 250,
					fluidSpeed: !1,
					dragEndSpeed: !1,
					responsive: {},
					responsiveRefreshRate: 200,
					responsiveBaseElement: n,
					fallbackEasing: "swing",
					info: !1,
					nestedItemSelector: !1,
					itemElement: "div",
					stageElement: "div",
					refreshClass: "owl-refresh",
					loadedClass: "owl-loaded",
					loadingClass: "owl-loading",
					rtlClass: "owl-rtl",
					responsiveClass: "owl-responsive",
					dragClass: "owl-drag",
					itemClass: "owl-item",
					stageClass: "owl-stage",
					stageOuterClass: "owl-stage-outer",
					grabClass: "owl-grab",
				}),
					(s.Width = { Default: "default", Inner: "inner", Outer: "outer" }),
					(s.Type = { Event: "event", State: "state" }),
					(s.Plugins = {}),
					(s.Workers = [
						{
							filter: ["width", "settings"],
							run: function () {
								this._width = this.$element.width();
							},
						},
						{
							filter: ["width", "items", "settings"],
							run: function (t) {
								t.current = this._items && this._items[this.relative(this._current)];
							},
						},
						{
							filter: ["items", "settings"],
							run: function () {
								this.$stage.children(".cloned").remove();
							},
						},
						{
							filter: ["width", "items", "settings"],
							run: function (t) {
								var e = this.settings.margin || "",
									n = !this.settings.autoWidth,
									i = this.settings.rtl,
									r = { width: "auto", "margin-left": i ? e : "", "margin-right": i ? "" : e };
								!n && this.$stage.children().css(r), (t.css = r);
							},
						},
						{
							filter: ["width", "items", "settings"],
							run: function (t) {
								var e = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
									n = null,
									i = this._items.length,
									r = !this.settings.autoWidth,
									s = [];
								for (t.items = { merge: !1, width: e }; i--;)
									(n = this._mergers[i]), (n = (this.settings.mergeFit && Math.min(n, this.settings.items)) || n), (t.items.merge = n > 1 || t.items.merge), (s[i] = r ? e * n : this._items[i].width());
								this._widths = s;
							},
						},
						{
							filter: ["items", "settings"],
							run: function () {
								var e = [],
									n = this._items,
									i = this.settings,
									r = Math.max(2 * i.items, 4),
									s = 2 * Math.ceil(n.length / 2),
									o = i.loop && n.length ? (i.rewind ? r : Math.max(r, s)) : 0,
									a = "",
									l = "";
								for (o /= 2; o--;) e.push(this.normalize(e.length / 2, !0)), (a += n[e[e.length - 1]][0].outerHTML), e.push(this.normalize(n.length - 1 - (e.length - 1) / 2, !0)), (l = n[e[e.length - 1]][0].outerHTML + l);
								(this._clones = e), t(a).addClass("cloned").appendTo(this.$stage), t(l).addClass("cloned").prependTo(this.$stage);
							},
						},
						{
							filter: ["width", "items", "settings"],
							run: function () {
								for (var t = this.settings.rtl ? 1 : -1, e = this._clones.length + this._items.length, n = -1, i = 0, r = 0, s = []; ++n < e;)
									(i = s[n - 1] || 0), (r = this._widths[this.relative(n)] + this.settings.margin), s.push(i + r * t);
								this._coordinates = s;
							},
						},
						{
							filter: ["width", "items", "settings"],
							run: function () {
								var t = this.settings.stagePadding,
									e = this._coordinates,
									n = { width: Math.ceil(Math.abs(e[e.length - 1])) + 2 * t, "padding-left": t || "", "padding-right": t || "" };
								this.$stage.css(n);
							},
						},
						{
							filter: ["width", "items", "settings"],
							run: function (t) {
								var e = this._coordinates.length,
									n = !this.settings.autoWidth,
									i = this.$stage.children();
								if (n && t.items.merge) for (; e--;) (t.css.width = this._widths[this.relative(e)]), i.eq(e).css(t.css);
								else n && ((t.css.width = t.items.width), i.css(t.css));
							},
						},
						{
							filter: ["items"],
							run: function () {
								this._coordinates.length < 1 && this.$stage.removeAttr("style");
							},
						},
						{
							filter: ["width", "items", "settings"],
							run: function (t) {
								(t.current = t.current ? this.$stage.children().index(t.current) : 0), (t.current = Math.max(this.minimum(), Math.min(this.maximum(), t.current))), this.reset(t.current);
							},
						},
						{
							filter: ["position"],
							run: function () {
								this.animate(this.coordinates(this._current));
							},
						},
						{
							filter: ["width", "position", "items", "settings"],
							run: function () {
								var t,
									e,
									n,
									i,
									r = this.settings.rtl ? 1 : -1,
									s = 2 * this.settings.stagePadding,
									o = this.coordinates(this.current()) + s,
									a = o + this.width() * r,
									l = [];
								for (n = 0, i = this._coordinates.length; n < i; n++)
									(t = this._coordinates[n - 1] || 0), (e = Math.abs(this._coordinates[n]) + s * r), ((this.op(t, "<=", o) && this.op(t, ">", a)) || (this.op(e, "<", o) && this.op(e, ">", a))) && l.push(n);
								this.$stage.children(".active").removeClass("active"),
									this.$stage.children(":eq(" + l.join("), :eq(") + ")").addClass("active"),
									this.settings.center && (this.$stage.children(".center").removeClass("center"), this.$stage.children().eq(this.current()).addClass("center"));
							},
						},
					]),
					(s.prototype.initialize = function () {
						if ((this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading"))) {
							var e, n, i;
							(e = this.$element.find("img")), (n = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : r), (i = this.$element.children(n).width()), e.length && i <= 0 && this.preloadAutoWidthImages(e);
						}
						this.$element.addClass(this.options.loadingClass),
							(this.$stage = t("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>')),
							this.$element.append(this.$stage.parent()),
							this.replace(this.$element.children().not(this.$stage.parent())),
							this.$element.is(":visible") ? this.refresh() : this.invalidate("width"),
							this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass),
							this.registerEventHandlers(),
							this.leave("initializing"),
							this.trigger("initialized");
					}),
					(s.prototype.setup = function () {
						var e = this.viewport(),
							n = this.options.responsive,
							i = -1,
							r = null;
						n
							? (t.each(n, function (t) {
								t <= e && t > i && (i = Number(t));
							}),
								(r = t.extend({}, this.options, n[i])),
								delete r.responsive,
								r.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + i)))
							: (r = t.extend({}, this.options)),
							(null !== this.settings && this._breakpoint === i) ||
							(this.trigger("change", { property: { name: "settings", value: r } }),
								(this._breakpoint = i),
								(this.settings = r),
								this.invalidate("settings"),
								this.trigger("changed", { property: { name: "settings", value: this.settings } }));
					}),
					(s.prototype.optionsLogic = function () {
						this.settings.autoWidth && ((this.settings.stagePadding = !1), (this.settings.merge = !1));
					}),
					(s.prototype.prepare = function (e) {
						var n = this.trigger("prepare", { content: e });
						return (
							n.data ||
							(n.data = t("<" + this.settings.itemElement + "/>")
								.addClass(this.options.itemClass)
								.append(e)),
							this.trigger("prepared", { content: n.data }),
							n.data
						);
					}),
					(s.prototype.update = function () {
						for (
							var e = 0,
							n = this._pipe.length,
							i = t.proxy(function (t) {
								return this[t];
							}, this._invalidated),
							r = {};
							e < n;

						)
							(this._invalidated.all || t.grep(this._pipe[e].filter, i).length > 0) && this._pipe[e].run(r), e++;
						(this._invalidated = {}), !this.is("valid") && this.enter("valid");
					}),
					(s.prototype.width = function (t) {
						switch ((t = t || s.Width.Default)) {
							case s.Width.Inner:
							case s.Width.Outer:
								return this._width;
							default:
								return this._width - 2 * this.settings.stagePadding + this.settings.margin;
						}
					}),
					(s.prototype.refresh = function () {
						this.enter("refreshing"),
							this.trigger("refresh"),
							this.setup(),
							this.optionsLogic(),
							this.$element.addClass(this.options.refreshClass),
							this.update(),
							this.$element.removeClass(this.options.refreshClass),
							this.leave("refreshing"),
							this.trigger("refreshed");
					}),
					(s.prototype.onThrottledResize = function () {
						n.clearTimeout(this.resizeTimer), (this.resizeTimer = n.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate));
					}),
					(s.prototype.onResize = function () {
						return (
							!!this._items.length &&
							this._width !== this.$element.width() &&
							!!this.$element.is(":visible") &&
							(this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))
						);
					}),
					(s.prototype.registerEventHandlers = function () {
						t.support.transition && this.$stage.on(t.support.transition.end + ".owl.core", t.proxy(this.onTransitionEnd, this)),
							this.settings.responsive !== !1 && this.on(n, "resize", this._handlers.onThrottledResize),
							this.settings.mouseDrag &&
							(this.$element.addClass(this.options.dragClass),
								this.$stage.on("mousedown.owl.core", t.proxy(this.onDragStart, this)),
								this.$stage.on("dragstart.owl.core selectstart.owl.core", function () {
									return !1;
								})),
							this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", t.proxy(this.onDragEnd, this)));
					}),
					(s.prototype.onDragStart = function (e) {
						var n = null;
						3 !== e.which &&
							(t.support.transform
								? ((n = this.$stage
									.css("transform")
									.replace(/.*\(|\)| /g, "")
									.split(",")),
									(n = { x: n[16 === n.length ? 12 : 4], y: n[16 === n.length ? 13 : 5] }))
								: ((n = this.$stage.position()), (n = { x: this.settings.rtl ? n.left + this.$stage.width() - this.width() + this.settings.margin : n.left, y: n.top })),
								this.is("animating") && (t.support.transform ? this.animate(n.x) : this.$stage.stop(), this.invalidate("position")),
								this.$element.toggleClass(this.options.grabClass, "mousedown" === e.type),
								this.speed(0),
								(this._drag.time = new Date().getTime()),
								(this._drag.target = t(e.target)),
								(this._drag.stage.start = n),
								(this._drag.stage.current = n),
								(this._drag.pointer = this.pointer(e)),
								t(i).on("mouseup.owl.core touchend.owl.core", t.proxy(this.onDragEnd, this)),
								t(i).one(
									"mousemove.owl.core touchmove.owl.core",
									t.proxy(function (e) {
										var n = this.difference(this._drag.pointer, this.pointer(e));
										t(i).on("mousemove.owl.core touchmove.owl.core", t.proxy(this.onDragMove, this)), (Math.abs(n.x) < Math.abs(n.y) && this.is("valid")) || (e.preventDefault(), this.enter("dragging"), this.trigger("drag"));
									}, this)
								));
					}),
					(s.prototype.onDragMove = function (t) {
						var e = null,
							n = null,
							i = null,
							r = this.difference(this._drag.pointer, this.pointer(t)),
							s = this.difference(this._drag.stage.start, r);
						this.is("dragging") &&
							(t.preventDefault(),
								this.settings.loop
									? ((e = this.coordinates(this.minimum())), (n = this.coordinates(this.maximum() + 1) - e), (s.x = ((((s.x - e) % n) + n) % n) + e))
									: ((e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum())),
										(n = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum())),
										(i = this.settings.pullDrag ? (-1 * r.x) / 5 : 0),
										(s.x = Math.max(Math.min(s.x, e + i), n + i))),
								(this._drag.stage.current = s),
								this.animate(s.x));
					}),
					(s.prototype.onDragEnd = function (e) {
						var n = this.difference(this._drag.pointer, this.pointer(e)),
							r = this._drag.stage.current,
							s = (n.x > 0) ^ this.settings.rtl ? "left" : "right";
						t(i).off(".owl.core"),
							this.$element.removeClass(this.options.grabClass),
							((0 !== n.x && this.is("dragging")) || !this.is("valid")) &&
							(this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed),
								this.current(this.closest(r.x, 0 !== n.x ? s : this._drag.direction)),
								this.invalidate("position"),
								this.update(),
								(this._drag.direction = s),
								(Math.abs(n.x) > 3 || new Date().getTime() - this._drag.time > 300) &&
								this._drag.target.one("click.owl.core", function () {
									return !1;
								})),
							this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"));
					}),
					(s.prototype.closest = function (e, n) {
						var i = -1,
							r = 30,
							s = this.width(),
							o = this.coordinates();
						return (
							this.settings.freeDrag ||
							t.each(
								o,
								t.proxy(function (t, a) {
									return e > a - r && e < a + r ? (i = t) : this.op(e, "<", a) && this.op(e, ">", o[t + 1] || a - s) && (i = "left" === n ? t + 1 : t), i === -1;
								}, this)
							),
							this.settings.loop || (this.op(e, ">", o[this.minimum()]) ? (i = e = this.minimum()) : this.op(e, "<", o[this.maximum()]) && (i = e = this.maximum())),
							i
						);
					}),
					(s.prototype.animate = function (e) {
						var n = this.speed() > 0;
						this.is("animating") && this.onTransitionEnd(),
							n && (this.enter("animating"), this.trigger("translate")),
							t.support.transform3d && t.support.transition
								? this.$stage.css({ transform: "translate3d(" + e + "px,0px,0px)", transition: this.speed() / 1e3 + "s" })
								: n
									? this.$stage.animate({ left: e + "px" }, this.speed(), this.settings.fallbackEasing, t.proxy(this.onTransitionEnd, this))
									: this.$stage.css({ left: e + "px" });
					}),
					(s.prototype.is = function (t) {
						return this._states.current[t] && this._states.current[t] > 0;
					}),
					(s.prototype.current = function (t) {
						if (t === r) return this._current;
						if (0 === this._items.length) return r;
						if (((t = this.normalize(t)), this._current !== t)) {
							var e = this.trigger("change", { property: { name: "position", value: t } });
							e.data !== r && (t = this.normalize(e.data)), (this._current = t), this.invalidate("position"), this.trigger("changed", { property: { name: "position", value: this._current } });
						}
						return this._current;
					}),
					(s.prototype.invalidate = function (e) {
						return (
							"string" === t.type(e) && ((this._invalidated[e] = !0), this.is("valid") && this.leave("valid")),
							t.map(this._invalidated, function (t, e) {
								return e;
							})
						);
					}),
					(s.prototype.reset = function (t) {
						(t = this.normalize(t)), t !== r && ((this._speed = 0), (this._current = t), this.suppress(["translate", "translated"]), this.animate(this.coordinates(t)), this.release(["translate", "translated"]));
					}),
					(s.prototype.normalize = function (e, n) {
						var i = this._items.length,
							s = n ? 0 : this._clones.length;
						return !t.isNumeric(e) || i < 1 ? (e = r) : (e < 0 || e >= i + s) && (e = ((((e - s / 2) % i) + i) % i) + s / 2), e;
					}),
					(s.prototype.relative = function (t) {
						return (t -= this._clones.length / 2), this.normalize(t, !0);
					}),
					(s.prototype.maximum = function (t) {
						var e,
							n = this.settings,
							i = this._coordinates.length,
							r = Math.abs(this._coordinates[i - 1]) - this._width,
							s = -1;
						if (n.loop) i = this._clones.length / 2 + this._items.length - 1;
						else if (n.autoWidth || n.merge) for (; i - s > 1;) Math.abs(this._coordinates[(e = (i + s) >> 1)]) < r ? (s = e) : (i = e);
						else i = n.center ? this._items.length - 1 : this._items.length - n.items;
						return t && (i -= this._clones.length / 2), Math.max(i, 0);
					}),
					(s.prototype.minimum = function (t) {
						return t ? 0 : this._clones.length / 2;
					}),
					(s.prototype.items = function (t) {
						return t === r ? this._items.slice() : ((t = this.normalize(t, !0)), this._items[t]);
					}),
					(s.prototype.mergers = function (t) {
						return t === r ? this._mergers.slice() : ((t = this.normalize(t, !0)), this._mergers[t]);
					}),
					(s.prototype.clones = function (e) {
						var n = this._clones.length / 2,
							i = n + this._items.length,
							s = function (t) {
								return t % 2 === 0 ? i + t / 2 : n - (t + 1) / 2;
							};
						return e === r
							? t.map(this._clones, function (t, e) {
								return s(e);
							})
							: t.map(this._clones, function (t, n) {
								return t === e ? s(n) : null;
							});
					}),
					(s.prototype.speed = function (t) {
						return t !== r && (this._speed = t), this._speed;
					}),
					(s.prototype.coordinates = function (e) {
						var n = null;
						return e === r
							? t.map(
								this._coordinates,
								t.proxy(function (t, e) {
									return this.coordinates(e);
								}, this)
							)
							: (this.settings.center ? ((n = this._coordinates[e]), (n += ((this.width() - n + (this._coordinates[e - 1] || 0)) / 2) * (this.settings.rtl ? -1 : 1))) : (n = this._coordinates[e - 1] || 0), n);
					}),
					(s.prototype.duration = function (t, e, n) {
						return Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs(n || this.settings.smartSpeed);
					}),
					(s.prototype.to = function (t, e) {
						var n = this.current(),
							i = null,
							r = t - this.relative(n),
							s = (r > 0) - (r < 0),
							o = this._items.length,
							a = this.minimum(),
							l = this.maximum();
						this.settings.loop
							? (!this.settings.rewind && Math.abs(r) > o / 2 && (r += s * -1 * o), (t = n + r), (i = ((((t - a) % o) + o) % o) + a), i !== t && i - r <= l && i - r > 0 && ((n = i - r), (t = i), this.reset(n)))
							: this.settings.rewind
								? ((l += 1), (t = ((t % l) + l) % l))
								: (t = Math.max(a, Math.min(l, t))),
							this.speed(this.duration(n, t, e)),
							this.current(t),
							this.$element.is(":visible") && this.update();
					}),
					(s.prototype.next = function (t) {
						(t = t || !1), this.to(this.relative(this.current()) + 1, t);
					}),
					(s.prototype.prev = function (t) {
						(t = t || !1), this.to(this.relative(this.current()) - 1, t);
					}),
					(s.prototype.onTransitionEnd = function (t) {
						return (t === r || (t.stopPropagation(), (t.target || t.srcElement || t.originalTarget) === this.$stage.get(0))) && (this.leave("animating"), void this.trigger("translated"));
					}),
					(s.prototype.viewport = function () {
						var e;
						if (this.options.responsiveBaseElement !== n) e = t(this.options.responsiveBaseElement).width();
						else if (n.innerWidth) e = n.innerWidth;
						else {
							if (!i.documentElement || !i.documentElement.clientWidth) throw "Can not detect viewport width.";
							e = i.documentElement.clientWidth;
						}
						return e;
					}),
					(s.prototype.replace = function (n) {
						this.$stage.empty(),
							(this._items = []),
							n && (n = n instanceof e ? n : t(n)),
							this.settings.nestedItemSelector && (n = n.find("." + this.settings.nestedItemSelector)),
							n
								.filter(function () {
									return 1 === this.nodeType;
								})
								.each(
									t.proxy(function (t, e) {
										(e = this.prepare(e)), this.$stage.append(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").andSelf("[data-merge]").attr("data-merge") || 1);
									}, this)
								),
							this.reset(t.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0),
							this.invalidate("items");
					}),
					(s.prototype.add = function (n, i) {
						var s = this.relative(this._current);
						(i = i === r ? this._items.length : this.normalize(i, !0)),
							(n = n instanceof e ? n : t(n)),
							this.trigger("add", { content: n, position: i }),
							(n = this.prepare(n)),
							0 === this._items.length || i === this._items.length
								? (0 === this._items.length && this.$stage.append(n),
									0 !== this._items.length && this._items[i - 1].after(n),
									this._items.push(n),
									this._mergers.push(1 * n.find("[data-merge]").andSelf("[data-merge]").attr("data-merge") || 1))
								: (this._items[i].before(n), this._items.splice(i, 0, n), this._mergers.splice(i, 0, 1 * n.find("[data-merge]").andSelf("[data-merge]").attr("data-merge") || 1)),
							this._items[s] && this.reset(this._items[s].index()),
							this.invalidate("items"),
							this.trigger("added", { content: n, position: i });
					}),
					(s.prototype.remove = function (t) {
						(t = this.normalize(t, !0)),
							t !== r &&
							(this.trigger("remove", { content: this._items[t], position: t }),
								this._items[t].remove(),
								this._items.splice(t, 1),
								this._mergers.splice(t, 1),
								this.invalidate("items"),
								this.trigger("removed", { content: null, position: t }));
					}),
					(s.prototype.preloadAutoWidthImages = function (e) {
						e.each(
							t.proxy(function (e, n) {
								this.enter("pre-loading"),
									(n = t(n)),
									t(new Image())
										.one(
											"load",
											t.proxy(function (t) {
												n.attr("src", t.target.src), n.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh();
											}, this)
										)
										.attr("src", n.attr("src") || n.attr("data-src") || n.attr("data-src-retina"));
							}, this)
						);
					}),
					(s.prototype.destroy = function () {
						this.$element.off(".owl.core"), this.$stage.off(".owl.core"), t(i).off(".owl.core"), this.settings.responsive !== !1 && (n.clearTimeout(this.resizeTimer), this.off(n, "resize", this._handlers.onThrottledResize));
						for (var e in this._plugins) this._plugins[e].destroy();
						this.$stage.children(".cloned").remove(),
							this.$stage.unwrap(),
							this.$stage.children().contents().unwrap(),
							this.$stage.children().unwrap(),
							this.$element
								.removeClass(this.options.refreshClass)
								.removeClass(this.options.loadingClass)
								.removeClass(this.options.loadedClass)
								.removeClass(this.options.rtlClass)
								.removeClass(this.options.dragClass)
								.removeClass(this.options.grabClass)
								.attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), ""))
								.removeData("owl.carousel");
					}),
					(s.prototype.op = function (t, e, n) {
						var i = this.settings.rtl;
						switch (e) {
							case "<":
								return i ? t > n : t < n;
							case ">":
								return i ? t < n : t > n;
							case ">=":
								return i ? t <= n : t >= n;
							case "<=":
								return i ? t >= n : t <= n;
						}
					}),
					(s.prototype.on = function (t, e, n, i) {
						t.addEventListener ? t.addEventListener(e, n, i) : t.attachEvent && t.attachEvent("on" + e, n);
					}),
					(s.prototype.off = function (t, e, n, i) {
						t.removeEventListener ? t.removeEventListener(e, n, i) : t.detachEvent && t.detachEvent("on" + e, n);
					}),
					(s.prototype.trigger = function (e, n, i, r, o) {
						var a = { item: { count: this._items.length, index: this.current() } },
							l = t.camelCase(
								t
									.grep(["on", e, i], function (t) {
										return t;
									})
									.join("-")
									.toLowerCase()
							),
							u = t.Event([e, "owl", i || "carousel"].join(".").toLowerCase(), t.extend({ relatedTarget: this }, a, n));
						return (
							this._supress[e] ||
							(t.each(this._plugins, function (t, e) {
								e.onTrigger && e.onTrigger(u);
							}),
								this.register({ type: s.Type.Event, name: e }),
								this.$element.trigger(u),
								this.settings && "function" == typeof this.settings[l] && this.settings[l].call(this, u)),
							u
						);
					}),
					(s.prototype.enter = function (e) {
						t.each(
							[e].concat(this._states.tags[e] || []),
							t.proxy(function (t, e) {
								this._states.current[e] === r && (this._states.current[e] = 0), this._states.current[e]++;
							}, this)
						);
					}),
					(s.prototype.leave = function (e) {
						t.each(
							[e].concat(this._states.tags[e] || []),
							t.proxy(function (t, e) {
								this._states.current[e]--;
							}, this)
						);
					}),
					(s.prototype.register = function (e) {
						if (e.type === s.Type.Event) {
							if ((t.event.special[e.name] || (t.event.special[e.name] = {}), !t.event.special[e.name].owl)) {
								var n = t.event.special[e.name]._default;
								(t.event.special[e.name]._default = function (t) {
									return !n || !n.apply || (t.namespace && t.namespace.indexOf("owl") !== -1) ? t.namespace && t.namespace.indexOf("owl") > -1 : n.apply(this, arguments);
								}),
									(t.event.special[e.name].owl = !0);
							}
						} else
							e.type === s.Type.State &&
								(this._states.tags[e.name] ? (this._states.tags[e.name] = this._states.tags[e.name].concat(e.tags)) : (this._states.tags[e.name] = e.tags),
									(this._states.tags[e.name] = t.grep(
										this._states.tags[e.name],
										t.proxy(function (n, i) {
											return t.inArray(n, this._states.tags[e.name]) === i;
										}, this)
									)));
					}),
					(s.prototype.suppress = function (e) {
						t.each(
							e,
							t.proxy(function (t, e) {
								this._supress[e] = !0;
							}, this)
						);
					}),
					(s.prototype.release = function (e) {
						t.each(
							e,
							t.proxy(function (t, e) {
								delete this._supress[e];
							}, this)
						);
					}),
					(s.prototype.pointer = function (t) {
						var e = { x: null, y: null };
						return (
							(t = t.originalEvent || t || n.event),
							(t = t.touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t),
							t.pageX ? ((e.x = t.pageX), (e.y = t.pageY)) : ((e.x = t.clientX), (e.y = t.clientY)),
							e
						);
					}),
					(s.prototype.difference = function (t, e) {
						return { x: t.x - e.x, y: t.y - e.y };
					}),
					(t.fn.owlCarousel = function (e) {
						var n = Array.prototype.slice.call(arguments, 1);
						return this.each(function () {
							var i = t(this),
								r = i.data("owl.carousel");
							r ||
								((r = new s(this, "object" == typeof e && e)),
									i.data("owl.carousel", r),
									t.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (e, n) {
										r.register({ type: s.Type.Event, name: n }),
											r.$element.on(
												n + ".owl.carousel.core",
												t.proxy(function (t) {
													t.namespace && t.relatedTarget !== this && (this.suppress([n]), r[n].apply(this, [].slice.call(arguments, 1)), this.release([n]));
												}, r)
											);
									})),
								"string" == typeof e && "_" !== e.charAt(0) && r[e].apply(r, n);
						});
					}),
					(t.fn.owlCarousel.Constructor = s);
			})(window.Zepto || t, window, document),
				(function (t, e, n, i) {
					var r = function (e) {
						(this._core = e),
							(this._interval = null),
							(this._visible = null),
							(this._handlers = {
								"initialized.owl.carousel": t.proxy(function (t) {
									t.namespace && this._core.settings.autoRefresh && this.watch();
								}, this),
							}),
							(this._core.options = t.extend({}, r.Defaults, this._core.options)),
							this._core.$element.on(this._handlers);
					};
					(r.Defaults = { autoRefresh: !0, autoRefreshInterval: 500 }),
						(r.prototype.watch = function () {
							this._interval || ((this._visible = this._core.$element.is(":visible")), (this._interval = e.setInterval(t.proxy(this.refresh, this), this._core.settings.autoRefreshInterval)));
						}),
						(r.prototype.refresh = function () {
							this._core.$element.is(":visible") !== this._visible &&
								((this._visible = !this._visible), this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh());
						}),
						(r.prototype.destroy = function () {
							var t, n;
							e.clearInterval(this._interval);
							for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
							for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null);
						}),
						(t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = r);
				})(window.Zepto || t, window, document),
				(function (t, e, n, i) {
					var r = function (e) {
						(this._core = e),
							(this._loaded = []),
							(this._handlers = {
								"initialized.owl.carousel change.owl.carousel": t.proxy(function (e) {
									if (e.namespace && this._core.settings && this._core.settings.lazyLoad && ((e.property && "position" == e.property.name) || "initialized" == e.type))
										for (
											var n = this._core.settings,
											i = (n.center && Math.ceil(n.items / 2)) || n.items,
											r = (n.center && i * -1) || 0,
											s = ((e.property && e.property.value) || this._core.current()) + r,
											o = this._core.clones().length,
											a = t.proxy(function (t, e) {
												this.load(e);
											}, this);
											r++ < i;

										)
											this.load(o / 2 + this._core.relative(s)), o && t.each(this._core.clones(this._core.relative(s)), a), s++;
								}, this),
							}),
							(this._core.options = t.extend({}, r.Defaults, this._core.options)),
							this._core.$element.on(this._handlers);
					};
					(r.Defaults = { lazyLoad: !1 }),
						(r.prototype.load = function (n) {
							var i = this._core.$stage.children().eq(n),
								r = i && i.find(".owl-lazy");
							!r ||
								t.inArray(i.get(0), this._loaded) > -1 ||
								(r.each(
									t.proxy(function (n, i) {
										var r,
											s = t(i),
											o = (e.devicePixelRatio > 1 && s.attr("data-src-retina")) || s.attr("data-src");
										this._core.trigger("load", { element: s, url: o }, "lazy"),
											s.is("img")
												? s
													.one(
														"load.owl.lazy",
														t.proxy(function () {
															s.css("opacity", 1), this._core.trigger("loaded", { element: s, url: o }, "lazy");
														}, this)
													)
													.attr("src", o)
												: ((r = new Image()),
													(r.onload = t.proxy(function () {
														s.css({ "background-image": "url(" + o + ")", opacity: "1" }), this._core.trigger("loaded", { element: s, url: o }, "lazy");
													}, this)),
													(r.src = o));
									}, this)
								),
									this._loaded.push(i.get(0)));
						}),
						(r.prototype.destroy = function () {
							var t, e;
							for (t in this.handlers) this._core.$element.off(t, this.handlers[t]);
							for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null);
						}),
						(t.fn.owlCarousel.Constructor.Plugins.Lazy = r);
				})(window.Zepto || t, window, document),
				(function (t, e, n, i) {
					var r = function (e) {
						(this._core = e),
							(this._handlers = {
								"initialized.owl.carousel refreshed.owl.carousel": t.proxy(function (t) {
									t.namespace && this._core.settings.autoHeight && this.update();
								}, this),
								"changed.owl.carousel": t.proxy(function (t) {
									t.namespace && this._core.settings.autoHeight && "position" == t.property.name && this.update();
								}, this),
								"loaded.owl.lazy": t.proxy(function (t) {
									t.namespace && this._core.settings.autoHeight && t.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update();
								}, this),
							}),
							(this._core.options = t.extend({}, r.Defaults, this._core.options)),
							this._core.$element.on(this._handlers);
					};
					(r.Defaults = { autoHeight: !1, autoHeightClass: "owl-height" }),
						(r.prototype.update = function () {
							var e = this._core._current,
								n = e + this._core.settings.items,
								i = this._core.$stage.children().toArray().slice(e, n);
							(heights = []),
								(maxheight = 0),
								t.each(i, function (e, n) {
									heights.push(t(n).height());
								}),
								(maxheight = Math.max.apply(null, heights)),
								this._core.$stage.parent().height(maxheight).addClass(this._core.settings.autoHeightClass);
						}),
						(r.prototype.destroy = function () {
							var t, e;
							for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
							for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null);
						}),
						(t.fn.owlCarousel.Constructor.Plugins.AutoHeight = r);
				})(window.Zepto || t, window, document),
				(function (t, e, n, i) {
					var r = function (e) {
						(this._core = e),
							(this._videos = {}),
							(this._playing = null),
							(this._handlers = {
								"initialized.owl.carousel": t.proxy(function (t) {
									t.namespace && this._core.register({ type: "state", name: "playing", tags: ["interacting"] });
								}, this),
								"resize.owl.carousel": t.proxy(function (t) {
									t.namespace && this._core.settings.video && this.isInFullScreen() && t.preventDefault();
								}, this),
								"refreshed.owl.carousel": t.proxy(function (t) {
									t.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove();
								}, this),
								"changed.owl.carousel": t.proxy(function (t) {
									t.namespace && "position" === t.property.name && this._playing && this.stop();
								}, this),
								"prepared.owl.carousel": t.proxy(function (e) {
									if (e.namespace) {
										var n = t(e.content).find(".owl-video");
										n.length && (n.css("display", "none"), this.fetch(n, t(e.content)));
									}
								}, this),
							}),
							(this._core.options = t.extend({}, r.Defaults, this._core.options)),
							this._core.$element.on(this._handlers),
							this._core.$element.on(
								"click.owl.video",
								".owl-video-play-icon",
								t.proxy(function (t) {
									this.play(t);
								}, this)
							);
					};
					(r.Defaults = { video: !1, videoHeight: !1, videoWidth: !1 }),
						(r.prototype.fetch = function (t, e) {
							var n = t.attr("data-vimeo-id") ? "vimeo" : "youtube",
								i = t.attr("data-vimeo-id") || t.attr("data-youtube-id"),
								r = t.attr("data-width") || this._core.settings.videoWidth,
								s = t.attr("data-height") || this._core.settings.videoHeight,
								o = t.attr("href");
							if (!o) throw new Error("Missing video URL.");
							if (((i = o.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/)), i[3].indexOf("youtu") > -1))
								n = "youtube";
							else {
								if (!(i[3].indexOf("vimeo") > -1)) throw new Error("Video URL not supported.");
								n = "vimeo";
							}
							(i = i[6]), (this._videos[o] = { type: n, id: i, width: r, height: s }), e.attr("data-video", o), this.thumbnail(t, this._videos[o]);
						}),
						(r.prototype.thumbnail = function (e, n) {
							var i,
								r,
								s,
								o = n.width && n.height ? 'style="width:' + n.width + "px;height:" + n.height + 'px;"' : "",
								a = e.find("img"),
								l = "src",
								u = "",
								c = this._core.settings,
								h = function (t) {
									(r = '<div class="owl-video-play-icon"></div>'),
										(i = c.lazyLoad ? '<div class="owl-video-tn ' + u + '" ' + l + '="' + t + '"></div>' : '<div class="owl-video-tn" style="opacity:1;background-image:url(' + t + ')"></div>'),
										e.after(i),
										e.after(r);
								};
							return (
								e.wrap('<div class="owl-video-wrapper"' + o + "></div>"),
								this._core.settings.lazyLoad && ((l = "data-src"), (u = "owl-lazy")),
								a.length
									? (h(a.attr(l)), a.remove(), !1)
									: void ("youtube" === n.type
										? ((s = "http://img.youtube.com/vi/" + n.id + "/hqdefault.jpg"), h(s))
										: "vimeo" === n.type &&
										t.ajax({
											type: "GET",
											url: "http://vimeo.com/api/v2/video/" + n.id + ".json",
											jsonp: "callback",
											dataType: "jsonp",
											success: function (t) {
												(s = t[0].thumbnail_large), h(s);
											},
										}))
							);
						}),
						(r.prototype.stop = function () {
							this._core.trigger("stop", null, "video"),
								this._playing.find(".owl-video-frame").remove(),
								this._playing.removeClass("owl-video-playing"),
								(this._playing = null),
								this._core.leave("playing"),
								this._core.trigger("stopped", null, "video");
						}),
						(r.prototype.play = function (e) {
							var n,
								i = t(e.target),
								r = i.closest("." + this._core.settings.itemClass),
								s = this._videos[r.attr("data-video")],
								o = s.width || "100%",
								a = s.height || this._core.$stage.height();
							this._playing ||
								(this._core.enter("playing"),
									this._core.trigger("play", null, "video"),
									(r = this._core.items(this._core.relative(r.index()))),
									this._core.reset(r.index()),
									"youtube" === s.type
										? (n = '<iframe width="' + o + '" height="' + a + '" src="http://www.youtube.com/embed/' + s.id + "?autoplay=1&v=" + s.id + '" frameborder="0" allowfullscreen></iframe>')
										: "vimeo" === s.type &&
										(n = '<iframe src="http://player.vimeo.com/video/' + s.id + '?autoplay=1" width="' + o + '" height="' + a + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'),
									t('<div class="owl-video-frame">' + n + "</div>").insertAfter(r.find(".owl-video")),
									(this._playing = r.addClass("owl-video-playing")));
						}),
						(r.prototype.isInFullScreen = function () {
							var e = n.fullscreenElement || n.mozFullScreenElement || n.webkitFullscreenElement;
							return e && t(e).parent().hasClass("owl-video-frame");
						}),
						(r.prototype.destroy = function () {
							var t, e;
							this._core.$element.off("click.owl.video");
							for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
							for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null);
						}),
						(t.fn.owlCarousel.Constructor.Plugins.Video = r);
				})(window.Zepto || t, window, document),
				(function (t, e, n, i) {
					var r = function (e) {
						(this.core = e),
							(this.core.options = t.extend({}, r.Defaults, this.core.options)),
							(this.swapping = !0),
							(this.previous = i),
							(this.next = i),
							(this.handlers = {
								"change.owl.carousel": t.proxy(function (t) {
									t.namespace && "position" == t.property.name && ((this.previous = this.core.current()), (this.next = t.property.value));
								}, this),
								"drag.owl.carousel dragged.owl.carousel translated.owl.carousel": t.proxy(function (t) {
									t.namespace && (this.swapping = "translated" == t.type);
								}, this),
								"translate.owl.carousel": t.proxy(function (t) {
									t.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap();
								}, this),
							}),
							this.core.$element.on(this.handlers);
					};
					(r.Defaults = { animateOut: !1, animateIn: !1 }),
						(r.prototype.swap = function () {
							if (1 === this.core.settings.items && t.support.animation && t.support.transition) {
								this.core.speed(0);
								var e,
									n = t.proxy(this.clear, this),
									i = this.core.$stage.children().eq(this.previous),
									r = this.core.$stage.children().eq(this.next),
									s = this.core.settings.animateIn,
									o = this.core.settings.animateOut;
								this.core.current() !== this.previous &&
									(o &&
										((e = this.core.coordinates(this.previous) - this.core.coordinates(this.next)),
											i
												.one(t.support.animation.end, n)
												.css({ left: e + "px" })
												.addClass("animated owl-animated-out")
												.addClass(o)),
										s && r.one(t.support.animation.end, n).addClass("animated owl-animated-in").addClass(s));
							}
						}),
						(r.prototype.clear = function (e) {
							t(e.target).css({ left: "" }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd();
						}),
						(r.prototype.destroy = function () {
							var t, e;
							for (t in this.handlers) this.core.$element.off(t, this.handlers[t]);
							for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null);
						}),
						(t.fn.owlCarousel.Constructor.Plugins.Animate = r);
				})(window.Zepto || t, window, document),
				(function (t, e, n, i) {
					var r = function (e) {
						(this._core = e),
							(this._interval = null),
							(this._paused = !1),
							(this._handlers = {
								"changed.owl.carousel": t.proxy(function (t) {
									t.namespace && "settings" === t.property.name && (this._core.settings.autoplay ? this.play() : this.stop());
								}, this),
								"initialized.owl.carousel": t.proxy(function (t) {
									t.namespace && this._core.settings.autoplay && this.play();
								}, this),
								"play.owl.autoplay": t.proxy(function (t, e, n) {
									t.namespace && this.play(e, n);
								}, this),
								"stop.owl.autoplay": t.proxy(function (t) {
									t.namespace && this.stop();
								}, this),
								"mouseover.owl.autoplay": t.proxy(function () {
									this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause();
								}, this),
								"mouseleave.owl.autoplay": t.proxy(function () {
									this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play();
								}, this),
							}),
							this._core.$element.on(this._handlers),
							(this._core.options = t.extend({}, r.Defaults, this._core.options));
					};
					(r.Defaults = { autoplay: !1, autoplayTimeout: 5e3, autoplayHoverPause: !1, autoplaySpeed: !1 }),
						(r.prototype.play = function (i, r) {
							(this._paused = !1),
								this._core.is("rotating") ||
								(this._core.enter("rotating"),
									(this._interval = e.setInterval(
										t.proxy(function () {
											this._paused || this._core.is("busy") || this._core.is("interacting") || n.hidden || this._core.next(r || this._core.settings.autoplaySpeed);
										}, this),
										i || this._core.settings.autoplayTimeout
									)));
						}),
						(r.prototype.stop = function () {
							this._core.is("rotating") && (e.clearInterval(this._interval), this._core.leave("rotating"));
						}),
						(r.prototype.pause = function () {
							this._core.is("rotating") && (this._paused = !0);
						}),
						(r.prototype.destroy = function () {
							var t, e;
							this.stop();
							for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
							for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null);
						}),
						(t.fn.owlCarousel.Constructor.Plugins.autoplay = r);
				})(window.Zepto || t, window, document),
				(function (t, e, n, i) {
					"use strict";
					var r = function (e) {
						(this._core = e),
							(this._initialized = !1),
							(this._pages = []),
							(this._controls = {}),
							(this._templates = []),
							(this.$element = this._core.$element),
							(this._overrides = { next: this._core.next, prev: this._core.prev, to: this._core.to }),
							(this._handlers = {
								"prepared.owl.carousel": t.proxy(function (e) {
									e.namespace &&
										this._core.settings.dotsData &&
										this._templates.push('<div class="' + this._core.settings.dotClass + '">' + t(e.content).find("[data-dot]").andSelf("[data-dot]").attr("data-dot") + "</div>");
								}, this),
								"added.owl.carousel": t.proxy(function (t) {
									t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 0, this._templates.pop());
								}, this),
								"remove.owl.carousel": t.proxy(function (t) {
									t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 1);
								}, this),
								"changed.owl.carousel": t.proxy(function (t) {
									t.namespace && "position" == t.property.name && this.draw();
								}, this),
								"initialized.owl.carousel": t.proxy(function (t) {
									t.namespace &&
										!this._initialized &&
										(this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), (this._initialized = !0), this._core.trigger("initialized", null, "navigation"));
								}, this),
								"refreshed.owl.carousel": t.proxy(function (t) {
									t.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"));
								}, this),
							}),
							(this._core.options = t.extend({}, r.Defaults, this._core.options)),
							this.$element.on(this._handlers);
					};
					(r.Defaults = {
						nav: !1,
						navText: ["prev", "next"],
						navSpeed: !1,
						navElement: "div",
						navContainer: !1,
						navContainerClass: "owl-nav",
						navClass: ["owl-prev", "owl-next"],
						slideBy: 1,
						dotClass: "owl-dot",
						dotsClass: "owl-dots",
						dots: !0,
						dotsEach: !1,
						dotsData: !1,
						dotsSpeed: !1,
						dotsContainer: !1,
					}),
						(r.prototype.initialize = function () {
							var e,
								n = this._core.settings;
							(this._controls.$relative = (n.navContainer ? t(n.navContainer) : t("<div>").addClass(n.navContainerClass).appendTo(this.$element)).addClass("disabled")),
								(this._controls.$previous = t("<" + n.navElement + ">")
									.addClass(n.navClass[0])
									.html(n.navText[0])
									.prependTo(this._controls.$relative)
									.on(
										"click",
										t.proxy(function (t) {
											this.prev(n.navSpeed);
										}, this)
									)),
								(this._controls.$next = t("<" + n.navElement + ">")
									.addClass(n.navClass[1])
									.html(n.navText[1])
									.appendTo(this._controls.$relative)
									.on(
										"click",
										t.proxy(function (t) {
											this.next(n.navSpeed);
										}, this)
									)),
								n.dotsData || (this._templates = [t("<div>").addClass(n.dotClass).append(t("<span>")).prop("outerHTML")]),
								(this._controls.$absolute = (n.dotsContainer ? t(n.dotsContainer) : t("<div>").addClass(n.dotsClass).appendTo(this.$element)).addClass("disabled")),
								this._controls.$absolute.on(
									"click",
									"div",
									t.proxy(function (e) {
										var i = t(e.target).parent().is(this._controls.$absolute) ? t(e.target).index() : t(e.target).parent().index();
										e.preventDefault(), this.to(i, n.dotsSpeed);
									}, this)
								);
							for (e in this._overrides) this._core[e] = t.proxy(this[e], this);
						}),
						(r.prototype.destroy = function () {
							var t, e, n, i;
							for (t in this._handlers) this.$element.off(t, this._handlers[t]);
							for (e in this._controls) this._controls[e].remove();
							for (i in this.overides) this._core[i] = this._overrides[i];
							for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null);
						}),
						(r.prototype.update = function () {
							var t,
								e,
								n,
								i = this._core.clones().length / 2,
								r = i + this._core.items().length,
								s = this._core.maximum(!0),
								o = this._core.settings,
								a = o.center || o.autoWidth || o.dotsData ? 1 : o.dotsEach || o.items;
							if (("page" !== o.slideBy && (o.slideBy = Math.min(o.slideBy, o.items)), o.dots || "page" == o.slideBy))
								for (this._pages = [], t = i, e = 0, n = 0; t < r; t++) {
									if (e >= a || 0 === e) {
										if ((this._pages.push({ start: Math.min(s, t - i), end: t - i + a - 1 }), Math.min(s, t - i) === s)) break;
										(e = 0), ++n;
									}
									e += this._core.mergers(this._core.relative(t));
								}
						}),
						(r.prototype.draw = function () {
							var e,
								n = this._core.settings,
								i = this._core.items().length <= n.items,
								r = this._core.relative(this._core.current()),
								s = n.loop || n.rewind;
							this._controls.$relative.toggleClass("disabled", !n.nav || i),
								n.nav && (this._controls.$previous.toggleClass("disabled", !s && r <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !s && r >= this._core.maximum(!0))),
								this._controls.$absolute.toggleClass("disabled", !n.dots || i),
								n.dots &&
								((e = this._pages.length - this._controls.$absolute.children().length),
									n.dotsData && 0 !== e
										? this._controls.$absolute.html(this._templates.join(""))
										: e > 0
											? this._controls.$absolute.append(new Array(e + 1).join(this._templates[0]))
											: e < 0 && this._controls.$absolute.children().slice(e).remove(),
									this._controls.$absolute.find(".active").removeClass("active"),
									this._controls.$absolute.children().eq(t.inArray(this.current(), this._pages)).addClass("active"));
						}),
						(r.prototype.onTrigger = function (e) {
							var n = this._core.settings;
							e.page = { index: t.inArray(this.current(), this._pages), count: this._pages.length, size: n && (n.center || n.autoWidth || n.dotsData ? 1 : n.dotsEach || n.items) };
						}),
						(r.prototype.current = function () {
							var e = this._core.relative(this._core.current());
							return t
								.grep(
									this._pages,
									t.proxy(function (t, n) {
										return t.start <= e && t.end >= e;
									}, this)
								)
								.pop();
						}),
						(r.prototype.getPosition = function (e) {
							var n,
								i,
								r = this._core.settings;
							return (
								"page" == r.slideBy
									? ((n = t.inArray(this.current(), this._pages)), (i = this._pages.length), e ? ++n : --n, (n = this._pages[((n % i) + i) % i].start))
									: ((n = this._core.relative(this._core.current())), (i = this._core.items().length), e ? (n += r.slideBy) : (n -= r.slideBy)),
								n
							);
						}),
						(r.prototype.next = function (e) {
							t.proxy(this._overrides.to, this._core)(this.getPosition(!0), e);
						}),
						(r.prototype.prev = function (e) {
							t.proxy(this._overrides.to, this._core)(this.getPosition(!1), e);
						}),
						(r.prototype.to = function (e, n, i) {
							var r;
							i ? t.proxy(this._overrides.to, this._core)(e, n) : ((r = this._pages.length), t.proxy(this._overrides.to, this._core)(this._pages[((e % r) + r) % r].start, n));
						}),
						(t.fn.owlCarousel.Constructor.Plugins.Navigation = r);
				})(window.Zepto || t, window, document),
				(function (t, e, n, i) {
					"use strict";
					var r = function (n) {
						(this._core = n),
							(this._hashes = {}),
							(this.$element = this._core.$element),
							(this._handlers = {
								"initialized.owl.carousel": t.proxy(function (n) {
									n.namespace && "URLHash" === this._core.settings.startPosition && t(e).trigger("hashchange.owl.navigation");
								}, this),
								"prepared.owl.carousel": t.proxy(function (e) {
									if (e.namespace) {
										var n = t(e.content).find("[data-hash]").andSelf("[data-hash]").attr("data-hash");
										if (!n) return;
										this._hashes[n] = e.content;
									}
								}, this),
								"changed.owl.carousel": t.proxy(function (n) {
									if (n.namespace && "position" === n.property.name) {
										var i = this._core.items(this._core.relative(this._core.current())),
											r = t
												.map(this._hashes, function (t, e) {
													return t === i ? e : null;
												})
												.join();
										if (!r || e.location.hash.slice(1) === r) return;
										e.location.hash = r;
									}
								}, this),
							}),
							(this._core.options = t.extend({}, r.Defaults, this._core.options)),
							this.$element.on(this._handlers),
							t(e).on(
								"hashchange.owl.navigation",
								t.proxy(function (t) {
									var n = e.location.hash.substring(1),
										r = this._core.$stage.children(),
										s = this._hashes[n] && r.index(this._hashes[n]);
									s !== i && s !== this._core.current() && this._core.to(this._core.relative(s), !1, !0);
								}, this)
							);
					};
					(r.Defaults = { URLhashListener: !1 }),
						(r.prototype.destroy = function () {
							var n, i;
							t(e).off("hashchange.owl.navigation");
							for (n in this._handlers) this._core.$element.off(n, this._handlers[n]);
							for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null);
						}),
						(t.fn.owlCarousel.Constructor.Plugins.Hash = r);
				})(window.Zepto || t, window, document),
				(function (t, e, n, i) {
					function r(e, n) {
						var r = !1,
							s = e.charAt(0).toUpperCase() + e.slice(1);
						return (
							t.each((e + " " + a.join(s + " ") + s).split(" "), function (t, e) {
								if (o[e] !== i) return (r = !n || e), !1;
							}),
							r
						);
					}
					function s(t) {
						return r(t, !0);
					}
					var o = t("<support>").get(0).style,
						a = "Webkit Moz O ms".split(" "),
						l = {
							transition: { end: { WebkitTransition: "webkitTransitionEnd", MozTransition: "transitionend", OTransition: "oTransitionEnd", transition: "transitionend" } },
							animation: { end: { WebkitAnimation: "webkitAnimationEnd", MozAnimation: "animationend", OAnimation: "oAnimationEnd", animation: "animationend" } },
						},
						u = {
							csstransforms: function () {
								return !!r("transform");
							},
							csstransforms3d: function () {
								return !!r("perspective");
							},
							csstransitions: function () {
								return !!r("transition");
							},
							cssanimations: function () {
								return !!r("animation");
							},
						};
					u.csstransitions() && ((t.support.transition = new String(s("transition"))), (t.support.transition.end = l.transition.end[t.support.transition])),
						u.cssanimations() && ((t.support.animation = new String(s("animation"))), (t.support.animation.end = l.animation.end[t.support.animation])),
						u.csstransforms() && ((t.support.transform = new String(s("transform"))), (t.support.transform3d = u.csstransforms3d()));
				})(window.Zepto || t, window, document);
		}.call(e, n(3), n(3)));
	},
	,
	,
	,
	,
	function (t, e, n) {
		"use strict";
		function i(t) {
			return t && t.__esModule ? t : { default: t };
		}
		function r(t, e) {
			if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
		}
		var s = (function () {
			function t(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (e, n, i) {
				return n && t(e.prototype, n), i && t(e, i), e;
			};
		})(),
			o = n(3),
			a = i(o),
			l = n(65),
			u = i(l);
		n(306), (a["default"].fn.andSelf = a["default"].fn.addBack);
		var c = (function () {
			function t(e, n) {
				r(this, t);
				var i = ((this.options = a["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, a["default"])(e))),
					s = i.parent().find(".js-slideshow-next"),
					o = i.parent().find(".js-slideshow-previous");
				(this.$count = i.parent().find(".js-slideshow-count")), (this.$index = i.parent().find(".js-slideshow-index"));
				this.createCarousel(), s.on("tap", this.next.bind(this)), o.on("tap", this.previous.bind(this)), i.on("changed.owl.carousel", this.updateIndex.bind(this));
			}
			return (
				s(t, null, [
					{
						key: "Defaults",
						get: function () {
							return { nav: !1, dots: !1 };
						},
					},
				]),
				s(t, [
					{
						key: "createCarousel",
						value: function () {
							var t = this.$container,
								e = t.children().length > 1;
							t.owlCarousel({ loop: e, margin: 0, nav: this.options.nav, dots: this.options.dots, lazyLoad: !0, smartSpeed: 450, mouseDrag: e, touchDrag: e, responsive: { 0: { items: 1 } } }),
								t.on("change.owl.carousel", this.animateItemOut.bind(this)),
								t.on("changed.owl.carousel", this.animateItemIn.bind(this)),
								t.on("next.owl.carousel", this.updateAnimationDirection.bind(this, "next")),
								t.on("prev.owl.carousel", this.updateAnimationDirection.bind(this, "previous")),
								t.on("dragged.owl.carousel", this.handleDrag.bind(this)),
								this.getCarouselItem().addClass("slideshow--animation slideshow--animation--active"),
								this.updateAnimationDirection("next");
						},
					},
					{
						key: "updateIndex",
						value: function (t) {
							var e = ((t.item.index - 2 + t.item.count) % t.item.count) + 1;
							this.$index.text(e), this.$count.text(t.item.count);
						},
					},
					{
						key: "getCarousel",
						value: function () {
							return this.$container.data("owl.carousel");
						},
					},
					{
						key: "getCarouselItem",
						value: function () {
							return this.$container.find(".active");
						},
					},
					{
						key: "next",
						value: function () {
							this.updateAnimationDirection("next"), this.getCarousel().next();
						},
					},
					{
						key: "previous",
						value: function () {
							this.updateAnimationDirection("previous"), this.getCarousel().prev();
						},
					},
					{
						key: "handleDrag",
						value: function (t) {
							var e = "left" === t.relatedTarget._drag.direction ? "next" : "previous";
							this.updateAnimationDirection(e);
						},
					},
					{
						key: "updateAnimationDirection",
						value: function (t) {
							this.$container.removeClass("slideshow--direction-next slideshow--direction-previous").addClass("slideshow--direction-" + t);
						},
					},
					{
						key: "animateItemOut",
						value: function () {
							var t = this,
								e = this.getCarouselItem();
							e.removeClass("slideshow--animation--active"),
								setTimeout(function () {
									e.is(t.getCarouselItem()) || e.removeClass("slideshow--animation");
								}, 800);
						},
					},
					{
						key: "animateItemIn",
						value: function () {
							var t = this;
							setTimeout(function () {
								var e = t.getCarouselItem();
								e.addClass("slideshow--animation"),
									setTimeout(function () {
										e.addClass("slideshow--animation--active");
									}, 60);
							}, 60);
						},
					},
				]),
				t
			);
		})();
		a["default"].fn.slideshow = (0, u["default"])(c);
	},
]);
webpackJsonp([1],{0:function(t,e,s){t.exports=s(256)},256:function(t,e,s){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}var a=s(257),r=i(a);window.bodymovin=r["default"]},257:function(t,e,s){var i;!function(a,r){i=function(){return r(a)}.call(e,s,e,t),!(void 0!==i&&(t.exports=i))}(window||{},function(t){function e(){return{}}function s(t){Pt=t?Math.round:function(t){return t}}function i(t){return Math.round(1e4*t)/1e4}function a(t){t.style.userSelect="none",t.style.MozUserSelect="none",t.style.webkitUserSelect="none",t.style.oUserSelect="none"}function r(t,e,s,i){this.type=t,this.currentTime=e,this.totalTime=s,this.direction=i<0?-1:1}function n(t,e){this.type=t,this.direction=e<0?-1:1}function h(t,e,s,i){this.type=t,this.currentLoop=e,this.totalLoops=s,this.direction=i<0?-1:1}function o(t,e,s){this.type=t,this.firstFrame=e,this.totalFrames=s}function l(t,e){this.type=t,this.target=e}function p(t,e){return this._cbs[t]||(this._cbs[t]=[]),this._cbs[t].push(e),function(){this.removeEventListener(t,e)}.bind(this)}function f(t,e){if(e){if(this._cbs[t]){for(var s=0,i=this._cbs[t].length;s<i;)this._cbs[t][s]===e&&(this._cbs[t].splice(s,1),s-=1,i-=1),s+=1;this._cbs[t].length||(this._cbs[t]=null)}}else this._cbs[t]=null}function m(t,e){if(this._cbs[t])for(var s=this._cbs[t].length,i=0;i<s;i++)this._cbs[t][i](e)}function d(t,e){void 0===e&&(e="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");var s,i="";for(s=t;s>0;--s)i+=e[Math.round(Math.random()*(e.length-1))];return i}function c(t,e,s){var i,a,r,n,h,o,l,p;switch(1===arguments.length&&(e=t.s,s=t.v,t=t.h),n=Math.floor(6*t),h=6*t-n,o=s*(1-e),l=s*(1-h*e),p=s*(1-(1-h)*e),n%6){case 0:i=s,a=p,r=o;break;case 1:i=l,a=s,r=o;break;case 2:i=o,a=s,r=p;break;case 3:i=o,a=l,r=s;break;case 4:i=p,a=o,r=s;break;case 5:i=s,a=o,r=l}return[i,a,r]}function u(t,e,s){1===arguments.length&&(e=t.g,s=t.b,t=t.r);var i,a=Math.max(t,e,s),r=Math.min(t,e,s),n=a-r,h=0===a?0:n/a,o=a/255;switch(a){case r:i=0;break;case t:i=e-s+n*(e<s?6:0),i/=6*n;break;case e:i=s-t+2*n,i/=6*n;break;case s:i=t-e+4*n,i/=6*n}return[i,h,o]}function g(t,e){var s=u(255*t[0],255*t[1],255*t[2]);return s[1]+=e,s[1]>1?s[1]=1:s[1]<=0&&(s[1]=0),c(s[0],s[1],s[2])}function y(t,e){var s=u(255*t[0],255*t[1],255*t[2]);return s[2]+=e,s[2]>1?s[2]=1:s[2]<0&&(s[2]=0),c(s[0],s[1],s[2])}function v(t,e){var s=u(255*t[0],255*t[1],255*t[2]);return s[0]+=e/360,s[0]>1?s[0]-=1:s[0]<0&&(s[0]+=1),c(s[0],s[1],s[2])}function b(t){var e,s,i=[],a=[],r=[],n={},h=0;t.c&&(i[0]=t.o[0],a[0]=t.i[0],r[0]=t.v[0],h=1),s=t.i.length;var o=s-1;for(e=h;e<s;e+=1)i.push(t.o[o]),a.push(t.i[o]),r.push(t.v[o]),o-=1;return n.i=i,n.o=a,n.v=r,n}function k(){}function A(t,e,s){if(!e){var i=Object.create(t.prototype,s),a={};return i&&"[object Function]"===a.toString.call(i.init)&&i.init(),i}e.prototype=Object.create(t.prototype),e.prototype.constructor=e,e.prototype._parent=t.prototype}function P(t,e){for(var s in t.prototype)t.prototype.hasOwnProperty(s)&&(e.prototype[s]=t.prototype[s])}function E(){function t(t,e,s,i,a,r){var n=t*i+e*a+s*r-a*i-r*t-s*e;return n>-1e-4&&n<1e-4}function e(e,s,i,a,r,n,h,o,l){if(0===i&&0===n&&0===l)return t(e,s,a,r,h,o);var p,f=Math.sqrt(Math.pow(a-e,2)+Math.pow(r-s,2)+Math.pow(n-i,2)),m=Math.sqrt(Math.pow(h-e,2)+Math.pow(o-s,2)+Math.pow(l-i,2)),d=Math.sqrt(Math.pow(h-a,2)+Math.pow(o-r,2)+Math.pow(l-n,2));return p=f>m?f>d?f-m-d:d-m-f:d>m?d-m-f:m-f-d,p>-1e-4&&p<1e-4}function s(t){var e,s=t.c,i=t.v,a=t.o,r=t.i,n=t._length,h=[],o=0;for(e=0;e<n-1;e+=1)h[e]=l(i[e],i[e+1],a[e],r[e+1]),o+=h[e].addedLength;return s&&(h[e]=l(i[e],i[0],a[e],r[0]),o+=h[e].addedLength),{lengths:h,totalLength:o}}function i(t){this.segmentLength=0,this.points=new Array(t)}function a(t,e){this.partialLength=t,this.point=e}function r(t,e){var s=e.segments,i=s.length,a=wt((i-1)*t),r=t*e.addedLength,n=0;if(r==s[a].l)return s[a].p;for(var h=s[a].l>r?-1:1,o=!0;o;)s[a].l<=r&&s[a+1].l>r?(n=(r-s[a].l)/(s[a+1].l-s[a].l),o=!1):a+=h,(a<0||a>=i-1)&&(o=!1);return s[a].p+(s[a+1].p-s[a].p)*n}function n(){this.pt1=new Array(2),this.pt2=new Array(2),this.pt3=new Array(2),this.pt4=new Array(2)}function h(t,e,s,i,a,n){var h=r(a,n),o=1-h,l=Math.round(1e3*(o*o*o*t[0]+(h*o*o+o*h*o+o*o*h)*s[0]+(h*h*o+o*h*h+h*o*h)*i[0]+h*h*h*e[0]))/1e3,p=Math.round(1e3*(o*o*o*t[1]+(h*o*o+o*h*o+o*o*h)*s[1]+(h*h*o+o*h*h+h*o*h)*i[1]+h*h*h*e[1]))/1e3;return[l,p]}function o(t,e,s,i,a,h,o){var l=new n;a=a<0?0:a>1?1:a;var p=r(a,o);h=h>1?1:h;var f,m=r(h,o),d=t.length,c=1-p,u=1-m;for(f=0;f<d;f+=1)l.pt1[f]=Math.round(1e3*(c*c*c*t[f]+(p*c*c+c*p*c+c*c*p)*s[f]+(p*p*c+c*p*p+p*c*p)*i[f]+p*p*p*e[f]))/1e3,l.pt3[f]=Math.round(1e3*(c*c*u*t[f]+(p*c*u+c*p*u+c*c*m)*s[f]+(p*p*u+c*p*m+p*c*m)*i[f]+p*p*m*e[f]))/1e3,l.pt4[f]=Math.round(1e3*(c*u*u*t[f]+(p*u*u+c*m*u+c*u*m)*s[f]+(p*m*u+c*m*m+p*u*m)*i[f]+p*m*m*e[f]))/1e3,l.pt2[f]=Math.round(1e3*(u*u*u*t[f]+(m*u*u+u*m*u+u*u*m)*s[f]+(m*m*u+u*m*m+m*u*m)*i[f]+m*m*m*e[f]))/1e3;return l}var l=(Math,function(){function t(t,e){this.l=t,this.p=e}return function(e,s,i,a){var r,n,h,o,l,p,f=It,m=0,d=[],c=[],u={addedLength:0,segments:[]};for(h=i.length,r=0;r<f;r+=1){for(l=r/(f-1),p=0,n=0;n<h;n+=1)o=Ft(1-l,3)*e[n]+3*Ft(1-l,2)*l*i[n]+3*(1-l)*Ft(l,2)*a[n]+Ft(l,3)*s[n],d[n]=o,null!==c[n]&&(p+=Ft(d[n]-c[n],2)),c[n]=d[n];p&&(p=xt(p),m+=p),u.segments.push(new t(m,l))}return u.addedLength=m,u}}()),p=function(){var e={};return function(s){var r=s.s,n=s.e,h=s.to,o=s.ti,l=(r.join("_")+"_"+n.join("_")+"_"+h.join("_")+"_"+o.join("_")).replace(/\./g,"p");if(e[l])return void(s.bezierData=e[l]);var p,f,m,d,c,u,g,y=It,v=0,b=null;2===r.length&&(r[0]!=n[0]||r[1]!=n[1])&&t(r[0],r[1],n[0],n[1],r[0]+h[0],r[1]+h[1])&&t(r[0],r[1],n[0],n[1],n[0]+o[0],n[1]+o[1])&&(y=2);var k=new i(y);for(m=h.length,p=0;p<y;p+=1){for(g=new Array(m),c=p/(y-1),u=0,f=0;f<m;f+=1)d=Ft(1-c,3)*r[f]+3*Ft(1-c,2)*c*(r[f]+h[f])+3*(1-c)*Ft(c,2)*(n[f]+o[f])+Ft(c,3)*n[f],g[f]=d,null!==b&&(u+=Ft(g[f]-b[f],2));u=xt(u),v+=u,k.points[p]=new a(u,g),b=g}k.segmentLength=v,s.bezierData=k,e[l]=k}}();return{getBezierLength:l,getSegmentsLength:s,getNewSegment:o,getPointInSegment:h,buildBezierData:p,pointOnLine2D:t,pointOnLine3D:e}}function M(){function t(a,r,h){var o,l,p,f,m,d,c,u,g=a.length;for(f=0;f<g;f+=1)if(o=a[f],"ks"in o&&!o.completed){if(o.completed=!0,o.tt&&(a[f-1].td=o.tt),l=[],p=-1,o.hasMask){var y=o.masksProperties;for(d=y.length,m=0;m<d;m+=1)if(y[m].pt.k.i)i(y[m].pt.k);else for(u=y[m].pt.k.length,c=0;c<u;c+=1)y[m].pt.k[c].s&&i(y[m].pt.k[c].s[0]),y[m].pt.k[c].e&&i(y[m].pt.k[c].e[0])}0===o.ty?(o.layers=e(o.refId,r),t(o.layers,r,h)):4===o.ty?s(o.shapes):5==o.ty&&n(o,h)}}function e(t,e){for(var s=0,i=e.length;s<i;){if(e[s].id===t)return e[s].layers.__used?JSON.parse(JSON.stringify(e[s].layers)):(e[s].layers.__used=!0,e[s].layers);s+=1}}function s(t){var e,a,r,n=t.length,h=!1;for(e=n-1;e>=0;e-=1)if("sh"==t[e].ty){if(t[e].ks.k.i)i(t[e].ks.k);else for(r=t[e].ks.k.length,a=0;a<r;a+=1)t[e].ks.k[a].s&&i(t[e].ks.k[a].s[0]),t[e].ks.k[a].e&&i(t[e].ks.k[a].e[0]);h=!0}else"gr"==t[e].ty&&s(t[e].it)}function i(t){var e,s=t.i.length;for(e=0;e<s;e+=1)t.i[e][0]+=t.v[e][0],t.i[e][1]+=t.v[e][1],t.o[e][0]+=t.v[e][0],t.o[e][1]+=t.v[e][1]}function a(t,e){var s=e?e.split("."):[100,100,100];return t[0]>s[0]||!(s[0]>t[0])&&(t[1]>s[1]||!(s[1]>t[1])&&(t[2]>s[2]||!(s[2]>t[2])&&void 0))}function r(e,s){e.__complete||(l(e),h(e),o(e),p(e),t(e.layers,e.assets,s),e.__complete=!0)}function n(t,e){0!==t.t.a.length||"m"in t.t.p||(t.singleShape=!0)}var h=function(){function t(t){var e=t.t.d;t.t.d={k:[{s:e,t:0}]}}function e(e){var s,i=e.length;for(s=0;s<i;s+=1)5===e[s].ty&&t(e[s])}var s=[4,4,14];return function(t){if(a(s,t.v)&&(e(t.layers),t.assets)){var i,r=t.assets.length;for(i=0;i<r;i+=1)t.assets[i].layers&&e(t.assets[i].layers)}}}(),o=function(){var t=[4,7,99];return function(e){if(e.chars&&!a(t,e.v)){var s,r,n,h,o,l=e.chars.length;for(s=0;s<l;s+=1)if(e.chars[s].data&&e.chars[s].data.shapes)for(o=e.chars[s].data.shapes[0].it,n=o.length,r=0;r<n;r+=1)h=o[r].ks.k,h.__converted||(i(o[r].ks.k),h.__converted=!0)}}}(),l=function(){function t(e){var s,i,a,r=e.length;for(s=0;s<r;s+=1)if("gr"===e[s].ty)t(e[s].it);else if("fl"===e[s].ty||"st"===e[s].ty)if(e[s].c.k&&e[s].c.k[0].i)for(a=e[s].c.k.length,i=0;i<a;i+=1)e[s].c.k[i].s&&(e[s].c.k[i].s[0]/=255,e[s].c.k[i].s[1]/=255,e[s].c.k[i].s[2]/=255,e[s].c.k[i].s[3]/=255),e[s].c.k[i].e&&(e[s].c.k[i].e[0]/=255,e[s].c.k[i].e[1]/=255,e[s].c.k[i].e[2]/=255,e[s].c.k[i].e[3]/=255);else e[s].c.k[0]/=255,e[s].c.k[1]/=255,e[s].c.k[2]/=255,e[s].c.k[3]/=255}function e(e){var s,i=e.length;for(s=0;s<i;s+=1)4===e[s].ty&&t(e[s].shapes)}var s=[4,1,9];return function(t){if(a(s,t.v)&&(e(t.layers),t.assets)){var i,r=t.assets.length;for(i=0;i<r;i+=1)t.assets[i].layers&&e(t.assets[i].layers)}}}(),p=function(){function t(e){var s,i,a,r=e.length,n=!1;for(s=r-1;s>=0;s-=1)if("sh"==e[s].ty){if(e[s].ks.k.i)e[s].ks.k.c=e[s].closed;else for(a=e[s].ks.k.length,i=0;i<a;i+=1)e[s].ks.k[i].s&&(e[s].ks.k[i].s[0].c=e[s].closed),e[s].ks.k[i].e&&(e[s].ks.k[i].e[0].c=e[s].closed);n=!0}else"gr"==e[s].ty&&t(e[s].it)}function e(e){var s,i,a,r,n,h,o=e.length;for(i=0;i<o;i+=1){if(s=e[i],s.hasMask){var l=s.masksProperties;for(r=l.length,a=0;a<r;a+=1)if(l[a].pt.k.i)l[a].pt.k.c=l[a].cl;else for(h=l[a].pt.k.length,n=0;n<h;n+=1)l[a].pt.k[n].s&&(l[a].pt.k[n].s[0].c=l[a].cl),l[a].pt.k[n].e&&(l[a].pt.k[n].e[0].c=l[a].cl)}4===s.ty&&t(s.shapes)}}var s=[4,4,18];return function(t){if(a(s,t.v)&&(e(t.layers),t.assets)){var i,r=t.assets.length;for(i=0;i<r;i+=1)t.assets[i].layers&&e(t.assets[i].layers)}}}(),f={};return f.completeData=r,f}function _(){this.c=!1,this._length=0,this._maxLength=8,this.v=Array.apply(null,{length:this._maxLength}),this.o=Array.apply(null,{length:this._maxLength}),this.i=Array.apply(null,{length:this._maxLength})}function S(){}function F(){}function x(){}function w(){}function D(){this._length=0,this._maxLength=4,this.shapes=Array.apply(null,{length:this._maxLength})}function C(t,e,s){this.mdf=!1,this._firstFrame=!0,this._hasMaskedPath=!1,this._frameId=-1,this._dynamicProperties=[],this._textData=t,this._renderType=e,this._elem=s,this._animatorsData=Array.apply(null,{length:this._textData.a.length}),this._pathData={},this._moreOptions={alignment:{}},this.renderedLetters=[],this.lettersChangedFlag=!1}function I(t,e,s,i,a,r){this.o=t,this.sw=e,this.sc=s,this.fc=i,this.m=a,this.p=r,this.mdf={o:!0,sw:!!e,sc:!!s,fc:!!i,m:!0,p:!0}}function T(t,e,s){this._frameId=-99999,this.pv="",this.v="",this.kf=!1,this.firstFrame=!0,this.mdf=!0,this.data=e,this.elem=t,this.keysIndex=-1,this.currentData={ascent:0,boxWidth:[0,0],f:"",fStyle:"",fWeight:"",fc:"",j:"",justifyOffset:"",l:[],lh:0,lineWidths:[],ls:"",of:"",s:"",sc:"",sw:0,t:0,tr:0,fillColorAnim:!1,strokeColorAnim:!1,strokeWidthAnim:!1,yOffset:0,__complete:!1},this.searchProperty()?s.push(this):this.getValue(!0)}function L(){}function N(t,e){this.animationItem=t,this.layers=null,this.renderedFrame=-1,this.globalData={frameNum:-1},this.renderConfig={preserveAspectRatio:e&&e.preserveAspectRatio||"xMidYMid meet",progressiveLoad:e&&e.progressiveLoad||!1,hideOnTransparent:!e||e.hideOnTransparent!==!1,viewBoxOnly:e&&e.viewBoxOnly||!1,className:e&&e.className||""},this.globalData.renderConfig=this.renderConfig,this.elements=[],this.pendingElements=[],this.destroyed=!1}function V(t,e,s){this.dynamicProperties=[],this.data=t,this.element=e,this.globalData=s,this.storedData=[],this.masksProperties=this.data.masksProperties,this.viewData=Array.apply(null,{length:this.masksProperties.length}),this.maskElement=null,this.firstFrame=!0;var i,a,r,n,h,o,l,p,f=this.globalData.defs,m=this.masksProperties.length,c=this.masksProperties,u=0,g=[],y=d(10),v="clipPath",b="clip-path";for(i=0;i<m;i++)if(("a"!==c[i].mode&&"n"!==c[i].mode||c[i].inv||100!==c[i].o.k)&&(v="mask",b="mask"),"s"!=c[i].mode&&"i"!=c[i].mode||0!=u?h=null:(h=document.createElementNS(Et,"rect"),h.setAttribute("fill","#ffffff"),h.setAttribute("width",this.element.comp.data.w),h.setAttribute("height",this.element.comp.data.h),g.push(h)),a=document.createElementNS(Et,"path"),"n"!=c[i].mode){if(u+=1,"s"==c[i].mode?a.setAttribute("fill","#000000"):a.setAttribute("fill","#ffffff"),a.setAttribute("clip-rule","nonzero"),0!==c[i].x.k){v="mask",b="mask",p=jt.getProp(this.element,c[i].x,0,null,this.dynamicProperties);var k="fi_"+d(10);o=document.createElementNS(Et,"filter"),o.setAttribute("id",k),l=document.createElementNS(Et,"feMorphology"),l.setAttribute("operator","dilate"),l.setAttribute("in","SourceGraphic"),l.setAttribute("radius","0"),o.appendChild(l),f.appendChild(o),"s"==c[i].mode?a.setAttribute("stroke","#000000"):a.setAttribute("stroke","#ffffff")}else l=null,p=null;if(this.storedData[i]={elem:a,x:p,expan:l,lastPath:"",lastOperator:"",filterId:k,lastRadius:0},"i"==c[i].mode){n=g.length;var A=document.createElementNS(Et,"g");for(r=0;r<n;r+=1)A.appendChild(g[r]);var P=document.createElementNS(Et,"mask");P.setAttribute("mask-type","alpha"),P.setAttribute("id",y+"_"+u),P.appendChild(a),f.appendChild(P),A.setAttribute("mask","url("+Mt+"#"+y+"_"+u+")"),g.length=0,g.push(A)}else g.push(a);c[i].inv&&!this.solidPath&&(this.solidPath=this.createLayerSolidPath()),this.viewData[i]={elem:a,lastPath:"",op:jt.getProp(this.element,c[i].o,0,.01,this.dynamicProperties),prop:Bt.getShapeProp(this.element,c[i],3,this.dynamicProperties,null)},h&&(this.viewData[i].invRect=h),this.viewData[i].prop.k||this.drawPath(c[i],this.viewData[i].prop.v,this.viewData[i])}else this.viewData[i]={op:jt.getProp(this.element,c[i].o,0,.01,this.dynamicProperties),prop:Bt.getShapeProp(this.element,c[i],3,this.dynamicProperties,null),elem:a},f.appendChild(a);for(this.maskElement=document.createElementNS(Et,v),m=g.length,i=0;i<m;i+=1)this.maskElement.appendChild(g[i]);this.maskElement.setAttribute("id",y),u>0&&this.element.maskedElement.setAttribute(b,"url("+Mt+"#"+y+")"),f.appendChild(this.maskElement)}function O(){}function z(t,e,s,i,a){this.globalData=s,this.comp=i,this.data=t,this.matteElement=null,this.transformedElement=null,this.isTransparent=!1,this.parentContainer=e,this.layerId=a?a.layerId:"ly_"+d(10),this.placeholder=a,this._sizeChanged=!1,this.init()}function R(t,e,s,i,a){this.shapes=[],this.shapesData=t.shapes,this.stylesList=[],this.itemsData=[],this.prevViewData=[],this.shapeModifiers=[],this.processedElements=[],this._parent.constructor.call(this,t,e,s,i,a)}function j(t,e,s,i){}function B(t,e,s,i,a){this.textSpans=[],this.renderType="svg",this._parent.constructor.call(this,t,e,s,i,a)}function G(t,e){this.filterManager=e;var s=document.createElementNS(Et,"feColorMatrix");if(s.setAttribute("type","matrix"),s.setAttribute("color-interpolation-filters","linearRGB"),s.setAttribute("values","0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0"),s.setAttribute("result","f1"),t.appendChild(s),s=document.createElementNS(Et,"feColorMatrix"),s.setAttribute("type","matrix"),s.setAttribute("color-interpolation-filters","sRGB"),s.setAttribute("values","1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"),s.setAttribute("result","f2"),t.appendChild(s),this.matrixFilter=s,100!==e.effectElements[2].p.v||e.effectElements[2].p.k){var i=document.createElementNS(Et,"feMerge");t.appendChild(i);var a;a=document.createElementNS(Et,"feMergeNode"),a.setAttribute("in","SourceGraphic"),i.appendChild(a),a=document.createElementNS(Et,"feMergeNode"),a.setAttribute("in","f2"),i.appendChild(a)}}function W(t,e){this.filterManager=e;var s=document.createElementNS(Et,"feColorMatrix");s.setAttribute("type","matrix"),s.setAttribute("color-interpolation-filters","sRGB"),s.setAttribute("values","1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0"),t.appendChild(s),this.matrixFilter=s}function X(t,e){this.initialized=!1,this.filterManager=e,this.elem=t,this.paths=[]}function q(t,e){this.filterManager=e;var s=document.createElementNS(Et,"feColorMatrix");s.setAttribute("type","matrix"),s.setAttribute("color-interpolation-filters","linearRGB"),s.setAttribute("values","0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0"),s.setAttribute("result","f1"),t.appendChild(s);var i=document.createElementNS(Et,"feComponentTransfer");i.setAttribute("color-interpolation-filters","sRGB"),t.appendChild(i),this.matrixFilter=i;var a=document.createElementNS(Et,"feFuncR");a.setAttribute("type","table"),i.appendChild(a),this.feFuncR=a;var r=document.createElementNS(Et,"feFuncG");r.setAttribute("type","table"),i.appendChild(r),this.feFuncG=r;var n=document.createElementNS(Et,"feFuncB");n.setAttribute("type","table"),i.appendChild(n),this.feFuncB=n}function Y(t,e){this.filterManager=e;var s=this.filterManager.effectElements,i=document.createElementNS(Et,"feComponentTransfer");(s[9].p.k||0!==s[9].p.v||s[10].p.k||1!==s[10].p.v||s[11].p.k||1!==s[11].p.v||s[12].p.k||0!==s[12].p.v||s[13].p.k||1!==s[13].p.v)&&(this.feFuncR=this.createFeFunc("feFuncR",i)),(s[16].p.k||0!==s[16].p.v||s[17].p.k||1!==s[17].p.v||s[18].p.k||1!==s[18].p.v||s[19].p.k||0!==s[19].p.v||s[20].p.k||1!==s[20].p.v)&&(this.feFuncG=this.createFeFunc("feFuncG",i)),(s[23].p.k||0!==s[23].p.v||s[24].p.k||1!==s[24].p.v||s[25].p.k||1!==s[25].p.v||s[26].p.k||0!==s[26].p.v||s[27].p.k||1!==s[27].p.v)&&(this.feFuncB=this.createFeFunc("feFuncB",i)),(s[30].p.k||0!==s[30].p.v||s[31].p.k||1!==s[31].p.v||s[32].p.k||1!==s[32].p.v||s[33].p.k||0!==s[33].p.v||s[34].p.k||1!==s[34].p.v)&&(this.feFuncA=this.createFeFunc("feFuncA",i)),(this.feFuncR||this.feFuncG||this.feFuncB||this.feFuncA)&&(i.setAttribute("color-interpolation-filters","sRGB"),t.appendChild(i),i=document.createElementNS(Et,"feComponentTransfer")),(s[2].p.k||0!==s[2].p.v||s[3].p.k||1!==s[3].p.v||s[4].p.k||1!==s[4].p.v||s[5].p.k||0!==s[5].p.v||s[6].p.k||1!==s[6].p.v)&&(i.setAttribute("color-interpolation-filters","sRGB"),t.appendChild(i),this.feFuncRComposed=this.createFeFunc("feFuncR",i),this.feFuncGComposed=this.createFeFunc("feFuncG",i),this.feFuncBComposed=this.createFeFunc("feFuncB",i))}function H(t,e){t.setAttribute("x","-100%"),t.setAttribute("y","-100%"),t.setAttribute("width","400%"),t.setAttribute("height","400%"),this.filterManager=e;var s=document.createElementNS(Et,"feGaussianBlur");s.setAttribute("in","SourceAlpha"),s.setAttribute("result","drop_shadow_1"),s.setAttribute("stdDeviation","0"),this.feGaussianBlur=s,t.appendChild(s);var i=document.createElementNS(Et,"feOffset");i.setAttribute("dx","25"),i.setAttribute("dy","0"),i.setAttribute("in","drop_shadow_1"),i.setAttribute("result","drop_shadow_2"),this.feOffset=i,t.appendChild(i);var a=document.createElementNS(Et,"feFlood");a.setAttribute("flood-color","#00ff00"),a.setAttribute("flood-opacity","1"),a.setAttribute("result","drop_shadow_3"),this.feFlood=a,t.appendChild(a);var r=document.createElementNS(Et,"feComposite");r.setAttribute("in","drop_shadow_3"),r.setAttribute("in2","drop_shadow_2"),r.setAttribute("operator","in"),r.setAttribute("result","drop_shadow_4"),t.appendChild(r);var n=document.createElementNS(Et,"feMerge");t.appendChild(n);var h;h=document.createElementNS(Et,"feMergeNode"),n.appendChild(h),h=document.createElementNS(Et,"feMergeNode"),h.setAttribute("in","SourceGraphic"),this.feMergeNode=h,this.feMerge=n,this.originalNodeAdded=!1,n.appendChild(h)}function J(t,e,s){this.initialized=!1,this.filterManager=e,this.filterElem=t,this.elem=s,s.matteElement=document.createElementNS(Et,"g"),s.matteElement.appendChild(s.layerElement),s.matteElement.appendChild(s.transformedElement),s.baseElement=s.matteElement}function U(t){var e,s=t.data.ef.length,i=d(10),a=qt.createFilter(i),r=0;this.filters=[];var n;for(e=0;e<s;e+=1)20===t.data.ef[e].ty?(r+=1,n=new G(a,t.effects.effectElements[e]),this.filters.push(n)):21===t.data.ef[e].ty?(r+=1,n=new W(a,t.effects.effectElements[e]),this.filters.push(n)):22===t.data.ef[e].ty?(n=new X(t,t.effects.effectElements[e]),this.filters.push(n)):23===t.data.ef[e].ty?(r+=1,n=new q(a,t.effects.effectElements[e]),this.filters.push(n)):24===t.data.ef[e].ty?(r+=1,n=new Y(a,t.effects.effectElements[e]),this.filters.push(n)):25===t.data.ef[e].ty?(r+=1,n=new H(a,t.effects.effectElements[e]),this.filters.push(n)):28===t.data.ef[e].ty&&(n=new J(a,t.effects.effectElements[e],t),this.filters.push(n));r&&(t.globalData.defs.appendChild(a),t.layerElement.setAttribute("filter","url("+Mt+"#"+i+")"))}function Z(t,e,s,i,a){this._parent.constructor.call(this,t,e,s,i,a),this.layers=t.layers,this.supports3d=!0,this.completeLayers=!1,this.pendingElements=[],this.elements=this.layers?Array.apply(null,{length:this.layers.length}):[],this.data.tm&&(this.tm=jt.getProp(this,this.data.tm,0,s.frameRate,this.dynamicProperties)),this.data.xt?(this.layerElement=document.createElementNS(Et,"g"),this.buildAllItems()):s.progressiveLoad||this.buildAllItems()}function K(t,e,s,i,a){this.assetData=s.getAssetData(t.refId),this._parent.constructor.call(this,t,e,s,i,a)}function Q(t,e,s,i,a){this._parent.constructor.call(this,t,e,s,i,a)}function $(t){Mt=t}function tt(t){Zt.play(t)}function et(t){Zt.pause(t)}function st(t){Zt.togglePause(t)}function it(t,e){Zt.setSpeed(t,e)}function at(t,e){Zt.setDirection(t,e)}function rt(t){Zt.stop(t)}function nt(t){Zt.moveFrame(t)}function ht(){$t===!0?Zt.searchAnimations(te,$t,ee):Zt.searchAnimations()}function ot(t){return Zt.registerAnimation(t)}function lt(){Zt.resize()}function pt(){Zt.start()}function ft(t,e,s){Zt.goToAndStop(t,e,s)}function mt(t){_t=t}function dt(t){return $t===!0&&(t.animationData=JSON.parse(te)),Zt.loadAnimation(t)}function ct(t){return Zt.destroy(t)}function ut(t){if("string"==typeof t)switch(t){case"high":It=200;break;case"medium":It=50;break;case"low":It=10}else!isNaN(t)&&t>1&&(It=t);s(!(It>=50))}function gt(){return"undefined"!=typeof navigator}function yt(t,e){"expressions"===t&&(At=e)}function vt(t){switch(t){case"propertyFactory":return jt;case"shapePropertyFactory":return Bt;case"matrix":return k}}function bt(){"complete"===document.readyState&&(clearInterval(ne),ht())}function kt(t){for(var e=re.split("&"),s=0;s<e.length;s++){var i=e[s].split("=");if(decodeURIComponent(i[0])==t)return decodeURIComponent(i[1])}}var At,Pt,Et="http://www.w3.org/2000/svg",Mt="",_t=!0,St=/^((?!chrome|android).)*safari/i.test(navigator.userAgent),Ft=(Math.round,Math.pow),xt=Math.sqrt,wt=(Math.abs,Math.floor),Dt=(Math.max,Math.min),Ct={};!function(){var t,e=Object.getOwnPropertyNames(Math),s=e.length;for(t=0;t<s;t+=1)Ct[e[t]]=Math[e[t]]}(),Ct.random=Math.random,Ct.abs=function(t){var e=typeof t;if("object"===e&&t.length){var s,i=Array.apply(null,{length:t.length}),a=t.length;for(s=0;s<a;s+=1)i[s]=Math.abs(t[s]);return i}return Math.abs(t)};var It=150,Tt=Math.PI/180,Lt=.5519;s(!1);var Nt=function(){var t,e,s=[];for(t=0;t<256;t+=1)e=t.toString(16),s[t]=1==e.length?"0"+e:e;return function(t,e,i){return t<0&&(t=0),e<0&&(e=0),i<0&&(i=0),"#"+s[t]+s[e]+s[i]}}(),k=(function(){var t=[];return function(e,s){return void 0!==s&&(e[3]=s),t[e[0]]||(t[e[0]]={}),t[e[0]][e[1]]||(t[e[0]][e[1]]={}),t[e[0]][e[1]][e[2]]||(t[e[0]][e[1]][e[2]]={}),t[e[0]][e[1]][e[2]][e[3]]||(t[e[0]][e[1]][e[2]][e[3]]="rgba("+e.join(",")+")"),t[e[0]][e[1]][e[2]][e[3]]}}(),function(){function t(){return this.props[0]=1,this.props[1]=0,this.props[2]=0,this.props[3]=0,this.props[4]=0,this.props[5]=1,this.props[6]=0,this.props[7]=0,this.props[8]=0,this.props[9]=0,this.props[10]=1,this.props[11]=0,this.props[12]=0,this.props[13]=0,this.props[14]=0,this.props[15]=1,this}function e(t){if(0===t)return this;var e=Math.cos(t),s=Math.sin(t);return this._t(e,-s,0,0,s,e,0,0,0,0,1,0,0,0,0,1)}function s(t){if(0===t)return this;var e=Math.cos(t),s=Math.sin(t);return this._t(1,0,0,0,0,e,-s,0,0,s,e,0,0,0,0,1)}function a(t){if(0===t)return this;var e=Math.cos(t),s=Math.sin(t);return this._t(e,0,s,0,0,1,0,0,-s,0,e,0,0,0,0,1)}function r(t){if(0===t)return this;var e=Math.cos(t),s=Math.sin(t);return this._t(e,-s,0,0,s,e,0,0,0,0,1,0,0,0,0,1)}function n(t,e){return this._t(1,e,t,1,0,0)}function h(t,e){return this.shear(Math.tan(t),Math.tan(e))}function o(t,e){var s=Math.cos(e),i=Math.sin(e);return this._t(s,i,0,0,-i,s,0,0,0,0,1,0,0,0,0,1)._t(1,0,0,0,Math.tan(t),1,0,0,0,0,1,0,0,0,0,1)._t(s,-i,0,0,i,s,0,0,0,0,1,0,0,0,0,1)}function l(t,e,s){return s=isNaN(s)?1:s,1==t&&1==e&&1==s?this:this._t(t,0,0,0,0,e,0,0,0,0,s,0,0,0,0,1)}function p(t,e,s,i,a,r,n,h,o,l,p,f,m,d,c,u){return this.props[0]=t,this.props[1]=e,this.props[2]=s,this.props[3]=i,this.props[4]=a,this.props[5]=r,this.props[6]=n,this.props[7]=h,this.props[8]=o,this.props[9]=l,this.props[10]=p,this.props[11]=f,this.props[12]=m,this.props[13]=d,this.props[14]=c,this.props[15]=u,this}function f(t,e,s){return s=s||0,0!==t||0!==e||0!==s?this._t(1,0,0,0,0,1,0,0,0,0,1,0,t,e,s,1):this}function m(t,e,s,i,a,r,n,h,o,l,p,f,m,d,c,u){if(1===t&&0===e&&0===s&&0===i&&0===a&&1===r&&0===n&&0===h&&0===o&&0===l&&1===p&&0===f)return 0===m&&0===d&&0===c||(this.props[12]=this.props[12]*t+this.props[13]*a+this.props[14]*o+this.props[15]*m,this.props[13]=this.props[12]*e+this.props[13]*r+this.props[14]*l+this.props[15]*d,this.props[14]=this.props[12]*s+this.props[13]*n+this.props[14]*p+this.props[15]*c,this.props[15]=this.props[12]*i+this.props[13]*h+this.props[14]*f+this.props[15]*u),this._identityCalculated=!1,this;var g=this.props[0],y=this.props[1],v=this.props[2],b=this.props[3],k=this.props[4],A=this.props[5],P=this.props[6],E=this.props[7],M=this.props[8],_=this.props[9],S=this.props[10],F=this.props[11],x=this.props[12],w=this.props[13],D=this.props[14],C=this.props[15];return this.props[0]=g*t+y*a+v*o+b*m,this.props[1]=g*e+y*r+v*l+b*d,this.props[2]=g*s+y*n+v*p+b*c,this.props[3]=g*i+y*h+v*f+b*u,this.props[4]=k*t+A*a+P*o+E*m,this.props[5]=k*e+A*r+P*l+E*d,this.props[6]=k*s+A*n+P*p+E*c,this.props[7]=k*i+A*h+P*f+E*u,this.props[8]=M*t+_*a+S*o+F*m,this.props[9]=M*e+_*r+S*l+F*d,this.props[10]=M*s+_*n+S*p+F*c,this.props[11]=M*i+_*h+S*f+F*u,this.props[12]=x*t+w*a+D*o+C*m,this.props[13]=x*e+w*r+D*l+C*d,this.props[14]=x*s+w*n+D*p+C*c,this.props[15]=x*i+w*h+D*f+C*u,this._identityCalculated=!1,this}function d(){return this._identityCalculated||(this._identity=!(1!==this.props[0]||0!==this.props[1]||0!==this.props[2]||0!==this.props[3]||0!==this.props[4]||1!==this.props[5]||0!==this.props[6]||0!==this.props[7]||0!==this.props[8]||0!==this.props[9]||1!==this.props[10]||0!==this.props[11]||0!==this.props[12]||0!==this.props[13]||0!==this.props[14]||1!==this.props[15]),this._identityCalculated=!0),this._identity}function c(t){var e;for(e=0;e<16;e+=1)t.props[e]=this.props[e]}function u(t){var e;for(e=0;e<16;e+=1)this.props[e]=t[e]}function g(t,e,s){return{x:t*this.props[0]+e*this.props[4]+s*this.props[8]+this.props[12],y:t*this.props[1]+e*this.props[5]+s*this.props[9]+this.props[13],z:t*this.props[2]+e*this.props[6]+s*this.props[10]+this.props[14]}}function y(t,e,s){return t*this.props[0]+e*this.props[4]+s*this.props[8]+this.props[12]}function v(t,e,s){return t*this.props[1]+e*this.props[5]+s*this.props[9]+this.props[13]}function b(t,e,s){return t*this.props[2]+e*this.props[6]+s*this.props[10]+this.props[14]}function k(t){var e=this.props[0]*this.props[5]-this.props[1]*this.props[4],s=this.props[5]/e,i=-this.props[1]/e,a=-this.props[4]/e,r=this.props[0]/e,n=(this.props[4]*this.props[13]-this.props[5]*this.props[12])/e,h=-(this.props[0]*this.props[13]-this.props[1]*this.props[12])/e;return[t[0]*s+t[1]*a+n,t[0]*i+t[1]*r+h,0]}function A(t){var e,s=t.length,i=[];for(e=0;e<s;e+=1)i[e]=k(t[e]);return i}function P(t,e,s,i){if(i&&2===i){var a=Ht.newPoint();return a[0]=t*this.props[0]+e*this.props[4]+s*this.props[8]+this.props[12],a[1]=t*this.props[1]+e*this.props[5]+s*this.props[9]+this.props[13],a}return[t*this.props[0]+e*this.props[4]+s*this.props[8]+this.props[12],t*this.props[1]+e*this.props[5]+s*this.props[9]+this.props[13],t*this.props[2]+e*this.props[6]+s*this.props[10]+this.props[14]]}function E(t,e){return this.isIdentity()?t+","+e:Pt(t*this.props[0]+e*this.props[4]+this.props[12])+","+Pt(t*this.props[1]+e*this.props[5]+this.props[13])}function M(){return[this.props[0],this.props[1],this.props[2],this.props[3],this.props[4],this.props[5],this.props[6],this.props[7],this.props[8],this.props[9],this.props[10],this.props[11],this.props[12],this.props[13],this.props[14],this.props[15]]}function _(){return St?"matrix3d("+i(this.props[0])+","+i(this.props[1])+","+i(this.props[2])+","+i(this.props[3])+","+i(this.props[4])+","+i(this.props[5])+","+i(this.props[6])+","+i(this.props[7])+","+i(this.props[8])+","+i(this.props[9])+","+i(this.props[10])+","+i(this.props[11])+","+i(this.props[12])+","+i(this.props[13])+","+i(this.props[14])+","+i(this.props[15])+")":(this.cssParts[1]=this.props.join(","),this.cssParts.join(""))}function S(){return"matrix("+i(this.props[0])+","+i(this.props[1])+","+i(this.props[4])+","+i(this.props[5])+","+i(this.props[12])+","+i(this.props[13])+")"}function F(){return""+this.toArray()}return function(){this.reset=t,this.rotate=e,this.rotateX=s,this.rotateY=a,this.rotateZ=r,this.skew=h,this.skewFromAxis=o,this.shear=n,this.scale=l,this.setTransform=p,this.translate=f,this.transform=m,this.applyToPoint=g,this.applyToX=y,this.applyToY=v,this.applyToZ=b,this.applyToPointArray=P,this.applyToPointStringified=E,this.toArray=M,this.toCSS=_,this.to2dCSS=S,this.toString=F,this.clone=c,this.cloneFromProps=u,this.inversePoints=A,this.inversePoint=k,this._t=this.transform,this.isIdentity=d,this._identity=!0,this._identityCalculated=!1,this.props=[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],this.cssParts=["matrix3d(","",")"]}}());!function(t,e){function s(s,l,p){var d=[];l=1==l?{entropy:!0}:l||{};var v=n(r(l.entropy?[s,o(t)]:null==s?h():s,3),d),b=new i(d),k=function(){for(var t=b.g(m),e=u,s=0;t<g;)t=(t+s)*f,e*=f,s=b.g(1);for(;t>=y;)t/=2,e/=2,s>>>=1;return(t+s)/e};return k.int32=function(){return 0|b.g(4)},k.quick=function(){return b.g(4)/4294967296},k["double"]=k,n(o(b.S),t),(l.pass||p||function(t,s,i,r){return r&&(r.S&&a(r,b),t.state=function(){return a(b,{})}),i?(e[c]=t,s):t})(k,v,"global"in l?l.global:this==e,l.state)}function i(t){var e,s=t.length,i=this,a=0,r=i.i=i.j=0,n=i.S=[];for(s||(t=[s++]);a<f;)n[a]=a++;for(a=0;a<f;a++)n[a]=n[r=v&r+t[a%s]+(e=n[a])],n[r]=e;(i.g=function(t){for(var e,s=0,a=i.i,r=i.j,n=i.S;t--;)e=n[a=v&a+1],s=s*f+n[v&(n[a]=n[r=v&r+e])+(n[r]=e)];return i.i=a,i.j=r,s})(f)}function a(t,e){return e.i=t.i,e.j=t.j,e.S=t.S.slice(),e}function r(t,e){var s,i=[],a=typeof t;if(e&&"object"==a)for(s in t)try{i.push(r(t[s],e-1))}catch(n){}return i.length?i:"string"==a?t:t+"\0"}function n(t,e){for(var s,i=t+"",a=0;a<i.length;)e[v&a]=v&(s^=19*e[v&a])+i.charCodeAt(a++);return o(e)}function h(){try{if(l)return o(l.randomBytes(f));var e=new Uint8Array(f);return(p.crypto||p.msCrypto).getRandomValues(e),o(e)}catch(s){var i=p.navigator,a=i&&i.plugins;return[+new Date,p,a,p.screen,o(t)]}}function o(t){return String.fromCharCode.apply(0,t)}var l,p=this,f=256,m=6,d=52,c="random",u=e.pow(f,m),g=e.pow(2,d),y=2*g,v=f-1;e["seed"+c]=s,n(e.random(),t)}([],Ct);var Vt=function(){function t(t,e,s,i,a){var r=a||("bez_"+t+"_"+e+"_"+s+"_"+i).replace(/\./g,"p");if(p[r])return p[r];var n=new o([t,e,s,i]);return p[r]=n,n}function e(t,e){return 1-3*e+3*t}function s(t,e){return 3*e-6*t}function i(t){return 3*t}function a(t,a,r){return((e(a,r)*t+s(a,r))*t+i(a))*t}function r(t,a,r){return 3*e(a,r)*t*t+2*s(a,r)*t+i(a)}function n(t,e,s,i,r){var n,h,o=0;do h=e+(s-e)/2,n=a(h,i,r)-t,n>0?s=h:e=h;while(Math.abs(n)>d&&++o<c);return h}function h(t,e,s,i){for(var n=0;n<f;++n){var h=r(e,s,i);if(0===h)return e;var o=a(e,s,i)-t;e-=o/h}return e}function o(t){this._p=t,this._mSampleValues=y?new Float32Array(u):new Array(u),this._precomputed=!1,this.get=this.get.bind(this)}var l={};l.getBezierEasing=t;var p={},f=4,m=.001,d=1e-7,c=10,u=11,g=1/(u-1),y="function"==typeof Float32Array;return o.prototype={get:function(t){var e=this._p[0],s=this._p[1],i=this._p[2],r=this._p[3];return this._precomputed||this._precompute(),e===s&&i===r?t:0===t?0:1===t?1:a(this._getTForX(t),s,r)},_precompute:function(){var t=this._p[0],e=this._p[1],s=this._p[2],i=this._p[3];this._precomputed=!0,t===e&&s===i||this._calcSampleValues();
},_calcSampleValues:function(){for(var t=this._p[0],e=this._p[2],s=0;s<u;++s)this._mSampleValues[s]=a(s*g,t,e)},_getTForX:function(t){for(var e=this._p[0],s=this._p[2],i=this._mSampleValues,a=0,o=1,l=u-1;o!==l&&i[o]<=t;++o)a+=g;--o;var p=(t-i[o])/(i[o+1]-i[o]),f=a+p*g,d=r(f,e,s);return d>=m?h(t,f,e,s):0===d?f:n(t,a,a+g,e,s)}},l}();!function(){for(var e=0,s=["ms","moz","webkit","o"],i=0;i<s.length&&!t.requestAnimationFrame;++i)t.requestAnimationFrame=t[s[i]+"RequestAnimationFrame"],t.cancelAnimationFrame=t[s[i]+"CancelAnimationFrame"]||t[s[i]+"CancelRequestAnimationFrame"];t.requestAnimationFrame||(t.requestAnimationFrame=function(t,s){var i=(new Date).getTime(),a=Math.max(0,16-(i-e)),r=setTimeout(function(){t(i+a)},a);return e=i+a,r}),t.cancelAnimationFrame||(t.cancelAnimationFrame=function(t){clearTimeout(t)})}();var Ot=E(),zt=M(),Rt=function(){function e(t,e){var s=document.createElement("span");s.style.fontFamily=e;var i=document.createElement("span");i.innerHTML="giItT1WQy@!-/#",s.style.position="absolute",s.style.left="-10000px",s.style.top="-10000px",s.style.fontSize="300px",s.style.fontVariant="normal",s.style.fontStyle="normal",s.style.fontWeight="normal",s.style.letterSpacing="0",s.appendChild(i),document.body.appendChild(s);var a=i.offsetWidth;return i.style.fontFamily=t+", "+e,{node:i,w:a,parent:s}}function s(){var e,i,a,r=this.fonts.length,n=r;for(e=0;e<r;e+=1)if(this.fonts[e].loaded)n-=1;else if("t"===this.fonts[e].fOrigin||2===this.fonts[e].origin){if(t.Typekit&&t.Typekit.load&&0===this.typekitLoaded){this.typekitLoaded=1;try{t.Typekit.load({async:!0,active:function(){this.typekitLoaded=2}.bind(this)})}catch(h){}}2===this.typekitLoaded&&(this.fonts[e].loaded=!0)}else"n"===this.fonts[e].fOrigin||0===this.fonts[e].origin?this.fonts[e].loaded=!0:(i=this.fonts[e].monoCase.node,a=this.fonts[e].monoCase.w,i.offsetWidth!==a?(n-=1,this.fonts[e].loaded=!0):(i=this.fonts[e].sansCase.node,a=this.fonts[e].sansCase.w,i.offsetWidth!==a&&(n-=1,this.fonts[e].loaded=!0)),this.fonts[e].loaded&&(this.fonts[e].sansCase.parent.parentNode.removeChild(this.fonts[e].sansCase.parent),this.fonts[e].monoCase.parent.parentNode.removeChild(this.fonts[e].monoCase.parent)));0!==n&&Date.now()-this.initTime<l?setTimeout(s.bind(this),20):setTimeout(function(){this.loaded=!0}.bind(this),0)}function i(t,e){var s=document.createElementNS(Et,"text");s.style.fontSize="100px",s.style.fontFamily=e.fFamily,s.textContent="1",e.fClass?(s.style.fontFamily="inherit",s.className=e.fClass):s.style.fontFamily=e.fFamily,t.appendChild(s);var i=document.createElement("canvas").getContext("2d");return i.font="100px "+e.fFamily,i}function a(t,a){if(!t)return void(this.loaded=!0);if(this.chars)return this.loaded=!0,void(this.fonts=t.list);var r,n=t.list,h=n.length;for(r=0;r<h;r+=1){if(n[r].loaded=!1,n[r].monoCase=e(n[r].fFamily,"monospace"),n[r].sansCase=e(n[r].fFamily,"sans-serif"),n[r].fPath){if("p"===n[r].fOrigin||3===n[r].origin){var o=document.createElement("style");o.type="text/css",o.innerHTML="@font-face {font-family: "+n[r].fFamily+"; font-style: normal; src: url('"+n[r].fPath+"');}",a.appendChild(o)}else if("g"===n[r].fOrigin||1===n[r].origin){var l=document.createElement("link");l.type="text/css",l.rel="stylesheet",l.href=n[r].fPath,a.appendChild(l)}else if("t"===n[r].fOrigin||2===n[r].origin){var p=document.createElement("script");p.setAttribute("src",n[r].fPath),a.appendChild(p)}}else n[r].loaded=!0;n[r].helper=i(a,n[r]),this.fonts.push(n[r])}s.bind(this)()}function r(t){if(t){this.chars||(this.chars=[]);var e,s,i,a=t.length,r=this.chars.length;for(e=0;e<a;e+=1){for(s=0,i=!1;s<r;)this.chars[s].style===t[e].style&&this.chars[s].fFamily===t[e].fFamily&&this.chars[s].ch===t[e].ch&&(i=!0),s+=1;i||(this.chars.push(t[e]),r+=1)}}}function n(t,e,s){for(var i=0,a=this.chars.length;i<a;){if(this.chars[i].ch===t&&this.chars[i].style===e&&this.chars[i].fFamily===s)return this.chars[i];i+=1}}function h(t,e,s){var i=this.getFontByName(e),a=i.helper;return a.measureText(t).width*s/100}function o(t){for(var e=0,s=this.fonts.length;e<s;){if(this.fonts[e].fName===t)return this.fonts[e];e+=1}return"sans-serif"}var l=5e3,p=function(){this.fonts=[],this.chars=null,this.typekitLoaded=0,this.loaded=!1,this.initTime=Date.now()};return p.prototype.addChars=r,p.prototype.addFonts=a,p.prototype.getCharData=n,p.prototype.getFontByName=o,p.prototype.measureText=h,p}(),jt=function(){function t(t,e,s,i){var a,r=this.offsetTime;s.constructor===Array&&(a=Array.apply(null,{length:s.length}));for(var n,h,o=e,l=this.keyframes.length-1,p=!0;p;){if(n=this.keyframes[o],h=this.keyframes[o+1],o==l-1&&t>=h.t-r){n.h&&(n=h),e=0;break}if(h.t-r>t){e=o;break}o<l-1?o+=1:(e=0,p=!1)}var f,m,d,c,u,g;if(n.to){n.bezierData||Ot.buildBezierData(n);var y=n.bezierData;if(t>=h.t-r||t<n.t-r){var v=t>=h.t-r?y.points.length-1:0;for(m=y.points[v].point.length,f=0;f<m;f+=1)a[f]=y.points[v].point[f];i._lastBezierData=null}else{n.__fnct?g=n.__fnct:(g=Vt.getBezierEasing(n.o.x,n.o.y,n.i.x,n.i.y,n.n).get,n.__fnct=g),d=g((t-(n.t-r))/(h.t-r-(n.t-r)));var b,k=y.segmentLength*d,A=i.lastFrame<t&&i._lastBezierData===y?i._lastAddedLength:0;for(u=i.lastFrame<t&&i._lastBezierData===y?i._lastPoint:0,p=!0,c=y.points.length;p;){if(A+=y.points[u].partialLength,0===k||0===d||u==y.points.length-1){for(m=y.points[u].point.length,f=0;f<m;f+=1)a[f]=y.points[u].point[f];break}if(k>=A&&k<A+y.points[u+1].partialLength){for(b=(k-A)/y.points[u+1].partialLength,m=y.points[u].point.length,f=0;f<m;f+=1)a[f]=y.points[u].point[f]+(y.points[u+1].point[f]-y.points[u].point[f])*b;break}u<c-1?u+=1:p=!1}i._lastPoint=u,i._lastAddedLength=A-y.points[u].partialLength,i._lastBezierData=y}}else{var P,E,M,_,S;for(l=n.s.length,o=0;o<l;o+=1){if(1!==n.h&&(t>=h.t-r?d=1:t<n.t-r?d=0:(n.o.x.constructor===Array?(n.__fnct||(n.__fnct=[]),n.__fnct[o]?g=n.__fnct[o]:(P=n.o.x[o]||n.o.x[0],E=n.o.y[o]||n.o.y[0],M=n.i.x[o]||n.i.x[0],_=n.i.y[o]||n.i.y[0],g=Vt.getBezierEasing(P,E,M,_).get,n.__fnct[o]=g)):n.__fnct?g=n.__fnct:(P=n.o.x,E=n.o.y,M=n.i.x,_=n.i.y,g=Vt.getBezierEasing(P,E,M,_).get,n.__fnct=g),d=g((t-(n.t-r))/(h.t-r-(n.t-r))))),this.sh&&1!==n.h){var F=n.s[o],x=n.e[o];F-x<-180?F+=360:F-x>180&&(F-=360),S=F+(x-F)*d}else S=1===n.h?n.s[o]:n.s[o]+(n.e[o]-n.s[o])*d;1===l?a=S:a[o]=S}}return{value:a,iterationIndex:e}}function e(){if(this.elem.globalData.frameId!==this.frameId){this.mdf=!1;var t=this.comp.renderedFrame-this.offsetTime,e=this.keyframes[0].t-this.offsetTime,s=this.keyframes[this.keyframes.length-1].t-this.offsetTime;if(!(t===this._caching.lastFrame||this._caching.lastFrame!==p&&(this._caching.lastFrame>=s&&t>=s||this._caching.lastFrame<e&&t<e))){var i=this._caching.lastFrame<t?this._caching.lastIndex:0,a=this.interpolateValue(t,i,this.pv,this._caching);if(this._caching.lastIndex=a.iterationIndex,this.pv.constructor===Array)for(i=0;i<this.v.length;)this.pv[i]=a.value[i],this.v[i]=this.mult?this.pv[i]*this.mult:this.pv[i],this.lastPValue[i]!==this.pv[i]&&(this.mdf=!0,this.lastPValue[i]=this.pv[i]),i+=1;else this.pv=a.value,this.v=this.mult?this.pv*this.mult:this.pv,this.lastPValue!=this.pv&&(this.mdf=!0,this.lastPValue=this.pv)}this._caching.lastFrame=t,this.frameId=this.elem.globalData.frameId}}function s(){}function i(t,e,i){this.mult=i,this.v=i?e.k*i:e.k,this.pv=e.k,this.mdf=!1,this.comp=t.comp,this.k=!1,this.kf=!1,this.vel=0,this.getValue=s}function a(t,e,i){this.mult=i,this.data=e,this.mdf=!1,this.comp=t.comp,this.k=!1,this.kf=!1,this.frameId=-1,this.v=Array.apply(null,{length:e.k.length}),this.pv=Array.apply(null,{length:e.k.length}),this.lastValue=Array.apply(null,{length:e.k.length});var a=Array.apply(null,{length:e.k.length});this.vel=a.map(function(){return 0});var r,n=e.k.length;for(r=0;r<n;r+=1)this.v[r]=i?e.k[r]*i:e.k[r],this.pv[r]=e.k[r];this.getValue=s}function r(s,i,a){this.keyframes=i.k,this.offsetTime=s.data.st,this.lastValue=-99999,this.lastPValue=-99999,this.frameId=-1,this._caching={lastFrame:p,lastIndex:0},this.k=!0,this.kf=!0,this.data=i,this.mult=a,this.elem=s,this.comp=s.comp,this.v=a?i.k[0].s[0]*a:i.k[0].s[0],this.pv=i.k[0].s[0],this.getValue=e,this.interpolateValue=t}function n(s,i,a){var r,n,h,o,l,f=i.k.length;for(r=0;r<f-1;r+=1)i.k[r].to&&i.k[r].s&&i.k[r].e&&(n=i.k[r].s,h=i.k[r].e,o=i.k[r].to,l=i.k[r].ti,(2===n.length&&(n[0]!==h[0]||n[1]!==h[1])&&Ot.pointOnLine2D(n[0],n[1],h[0],h[1],n[0]+o[0],n[1]+o[1])&&Ot.pointOnLine2D(n[0],n[1],h[0],h[1],h[0]+l[0],h[1]+l[1])||3===n.length&&(n[0]!==h[0]||n[1]!==h[1]||n[2]!==h[2])&&Ot.pointOnLine3D(n[0],n[1],n[2],h[0],h[1],h[2],n[0]+o[0],n[1]+o[1],n[2]+o[2])&&Ot.pointOnLine3D(n[0],n[1],n[2],h[0],h[1],h[2],h[0]+l[0],h[1]+l[1],h[2]+l[2]))&&(i.k[r].to=null,i.k[r].ti=null));this.keyframes=i.k,this.offsetTime=s.data.st,this.k=!0,this.kf=!0,this.mult=a,this.elem=s,this.comp=s.comp,this._caching={lastFrame:p,lastIndex:0},this.getValue=e,this.interpolateValue=t,this.frameId=-1,this.v=Array.apply(null,{length:i.k[0].s.length}),this.pv=Array.apply(null,{length:i.k[0].s.length}),this.lastValue=Array.apply(null,{length:i.k[0].s.length}),this.lastPValue=Array.apply(null,{length:i.k[0].s.length})}function h(t,e,s,h,o){var l;if(2===s)l=new f(t,e,o);else if(0===e.a)l=0===s?new i(t,e,h):new a(t,e,h);else if(1===e.a)l=0===s?new r(t,e,h):new n(t,e,h);else if(e.k.length)if("number"==typeof e.k[0])l=new a(t,e,h);else switch(s){case 0:l=new r(t,e,h);break;case 1:l=new n(t,e,h)}else l=new i(t,e,h);return l.k&&o.push(l),l}function o(t,e,s,i){return new d(t,e,s,i)}function l(t,e,s){return new c(t,e,s)}var p=-999999,f=function(){function t(){return this.p?ExpressionValue(this.p):[this.px.v,this.py.v,this.pz?this.pz.v:0]}function e(){return ExpressionValue(this.px)}function s(){return ExpressionValue(this.py)}function i(){return ExpressionValue(this.a)}function a(){return ExpressionValue(this.or)}function r(){return this.r?ExpressionValue(this.r,1/Tt):ExpressionValue(this.rz,1/Tt)}function n(){return ExpressionValue(this.s,100)}function h(){return ExpressionValue(this.o,100)}function o(){return ExpressionValue(this.sk)}function l(){return ExpressionValue(this.sa)}function p(t){var e,s=this.dynamicProperties.length;for(e=0;e<s;e+=1)this.dynamicProperties[e].getValue(),this.dynamicProperties[e].mdf&&(this.mdf=!0);this.a&&t.translate(-this.a.v[0],-this.a.v[1],this.a.v[2]),this.s&&t.scale(this.s.v[0],this.s.v[1],this.s.v[2]),this.r?t.rotate(-this.r.v):t.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]),this.data.p.s?this.data.p.z?t.translate(this.px.v,this.py.v,-this.pz.v):t.translate(this.px.v,this.py.v,0):t.translate(this.p.v[0],this.p.v[1],-this.p.v[2])}function f(){if(this.elem.globalData.frameId!==this.frameId){this.mdf=!1;var t,e=this.dynamicProperties.length;for(t=0;t<e;t+=1)this.dynamicProperties[t].getValue(),this.dynamicProperties[t].mdf&&(this.mdf=!0);if(this.mdf){if(this.v.reset(),this.a&&this.v.translate(-this.a.v[0],-this.a.v[1],this.a.v[2]),this.s&&this.v.scale(this.s.v[0],this.s.v[1],this.s.v[2]),this.sk&&this.v.skewFromAxis(-this.sk.v,this.sa.v),this.r?this.v.rotate(-this.r.v):this.v.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]),this.autoOriented&&this.p.keyframes&&this.p.getValueAtTime){var s,i;this.p._caching.lastFrame+this.p.offsetTime<=this.p.keyframes[0].t?(s=this.p.getValueAtTime((this.p.keyframes[0].t+.01)/this.elem.globalData.frameRate,0),i=this.p.getValueAtTime(this.p.keyframes[0].t/this.elem.globalData.frameRate,0)):this.p._caching.lastFrame+this.p.offsetTime>=this.p.keyframes[this.p.keyframes.length-1].t?(s=this.p.getValueAtTime(this.p.keyframes[this.p.keyframes.length-1].t/this.elem.globalData.frameRate,0),i=this.p.getValueAtTime((this.p.keyframes[this.p.keyframes.length-1].t-.01)/this.elem.globalData.frameRate,0)):(s=this.p.pv,i=this.p.getValueAtTime((this.p._caching.lastFrame+this.p.offsetTime-.01)/this.elem.globalData.frameRate,this.p.offsetTime)),this.v.rotate(-Math.atan2(s[1]-i[1],s[0]-i[0]))}this.data.p.s?this.data.p.z?this.v.translate(this.px.v,this.py.v,-this.pz.v):this.v.translate(this.px.v,this.py.v,0):this.v.translate(this.p.v[0],this.p.v[1],-this.p.v[2])}this.frameId=this.elem.globalData.frameId}}function m(){this.inverted=!0,this.iv=new k,this.k||(this.data.p.s?this.iv.translate(this.px.v,this.py.v,-this.pz.v):this.iv.translate(this.p.v[0],this.p.v[1],-this.p.v[2]),this.r?this.iv.rotate(-this.r.v):this.iv.rotateX(-this.rx.v).rotateY(-this.ry.v).rotateZ(this.rz.v),this.s&&this.iv.scale(this.s.v[0],this.s.v[1],1),this.a&&this.iv.translate(-this.a.v[0],-this.a.v[1],this.a.v[2]))}function d(){}return function(c,u,g){this.elem=c,this.frameId=-1,this.type="transform",this.dynamicProperties=[],this.mdf=!1,this.data=u,this.getValue=f,this.applyToMatrix=p,this.setInverted=m,this.autoOrient=d,this.v=new k,u.p.s?(this.px=jt.getProp(c,u.p.x,0,0,this.dynamicProperties),this.py=jt.getProp(c,u.p.y,0,0,this.dynamicProperties),u.p.z&&(this.pz=jt.getProp(c,u.p.z,0,0,this.dynamicProperties))):this.p=jt.getProp(c,u.p,1,0,this.dynamicProperties),u.r?this.r=jt.getProp(c,u.r,0,Tt,this.dynamicProperties):u.rx&&(this.rx=jt.getProp(c,u.rx,0,Tt,this.dynamicProperties),this.ry=jt.getProp(c,u.ry,0,Tt,this.dynamicProperties),this.rz=jt.getProp(c,u.rz,0,Tt,this.dynamicProperties),this.or=jt.getProp(c,u.or,1,Tt,this.dynamicProperties),this.or.sh=!0),u.sk&&(this.sk=jt.getProp(c,u.sk,0,Tt,this.dynamicProperties),this.sa=jt.getProp(c,u.sa,0,Tt,this.dynamicProperties)),u.a&&(this.a=jt.getProp(c,u.a,1,0,this.dynamicProperties)),u.s&&(this.s=jt.getProp(c,u.s,1,.01,this.dynamicProperties)),u.o?this.o=jt.getProp(c,u.o,0,.01,this.dynamicProperties):this.o={mdf:!1,v:1},this.dynamicProperties.length?g.push(this):(this.a&&this.v.translate(-this.a.v[0],-this.a.v[1],this.a.v[2]),this.s&&this.v.scale(this.s.v[0],this.s.v[1],this.s.v[2]),this.sk&&this.v.skewFromAxis(-this.sk.v,this.sa.v),this.r?this.v.rotate(-this.r.v):this.v.rotateZ(-this.rz.v).rotateY(this.ry.v).rotateX(this.rx.v).rotateZ(-this.or.v[2]).rotateY(this.or.v[1]).rotateX(this.or.v[0]),this.data.p.s?u.p.z?this.v.translate(this.px.v,this.py.v,-this.pz.v):this.v.translate(this.px.v,this.py.v,0):this.v.translate(this.p.v[0],this.p.v[1],-this.p.v[2])),Object.defineProperty(this,"position",{get:t}),Object.defineProperty(this,"xPosition",{get:e}),Object.defineProperty(this,"yPosition",{get:s}),Object.defineProperty(this,"orientation",{get:a}),Object.defineProperty(this,"anchorPoint",{get:i}),Object.defineProperty(this,"rotation",{get:r}),Object.defineProperty(this,"scale",{get:n}),Object.defineProperty(this,"opacity",{get:h}),Object.defineProperty(this,"skew",{get:o}),Object.defineProperty(this,"skewAxis",{get:l})}}(),m=function(){function t(t){if(this.prop.getValue(),this.cmdf=!1,this.omdf=!1,this.prop.mdf||t){var e,s,i,a=4*this.data.p;for(e=0;e<a;e+=1)s=e%4===0?100:255,i=Math.round(this.prop.v[e]*s),this.c[e]!==i&&(this.c[e]=i,this.cmdf=!0);if(this.o.length)for(a=this.prop.v.length,e=4*this.data.p;e<a;e+=1)s=e%2===0?100:1,i=e%2===0?Math.round(100*this.prop.v[e]):this.prop.v[e],this.o[e-4*this.data.p]!==i&&(this.o[e-4*this.data.p]=i,this.omdf=!0)}}function e(e,s,i){this.prop=h(e,s.k,1,null,[]),this.data=s,this.k=this.prop.k,this.c=Array.apply(null,{length:4*s.p});var a=s.k.k[0].s?s.k.k[0].s.length-4*s.p:s.k.k.length-4*s.p;this.o=Array.apply(null,{length:a}),this.cmdf=!1,this.omdf=!1,this.getValue=t,this.prop.k&&i.push(this),this.getValue(!0)}return function(t,s,i){return new e(t,s,i)}}(),d=function(){function t(t){var e=0,s=this.dataProps.length;if(this.elem.globalData.frameId!==this.frameId||t){for(this.mdf=!1,this.frameId=this.elem.globalData.frameId;e<s;){if(this.dataProps[e].p.mdf){this.mdf=!0;break}e+=1}if(this.mdf||t)for("svg"===this.renderer&&(this.dasharray=""),e=0;e<s;e+=1)"o"!=this.dataProps[e].n?"svg"===this.renderer?this.dasharray+=" "+this.dataProps[e].p.v:this.dasharray[e]=this.dataProps[e].p.v:this.dashoffset=this.dataProps[e].p.v}}return function(e,s,i,a){this.elem=e,this.frameId=-1,this.dataProps=new Array(s.length),this.renderer=i,this.mdf=!1,this.k=!1,"svg"===this.renderer?this.dasharray="":this.dasharray=new Array(s.length-1),this.dashoffset=0;var r,n,h=s.length;for(r=0;r<h;r+=1)n=jt.getProp(e,s[r].v,0,0,a),this.k=!!n.k||this.k,this.dataProps[r]={n:s[r].n,p:n};this.getValue=t,this.k?a.push(this):this.getValue(!0)}}(),c=function(){function t(t){if(this.mdf=t||!1,this.dynamicProperties.length){var e,s=this.dynamicProperties.length;for(e=0;e<s;e+=1)this.dynamicProperties[e].getValue(),this.dynamicProperties[e].mdf&&(this.mdf=!0)}var i=this.elem.textProperty.currentData?this.elem.textProperty.currentData.l.length:0;t&&2===this.data.r&&(this.e.v=i);var a=2===this.data.r?1:100/i,r=this.o.v/a,n=this.s.v/a+r,h=this.e.v/a+r;if(n>h){var o=n;n=h,h=o}this.finalS=n,this.finalE=h}function e(t){var e=Vt.getBezierEasing(this.ne.v/100,0,1-this.xe.v/100,1).get,r=0,n=this.finalS,h=this.finalE,o=this.data.sh;if(2==o)r=h===n?t>=h?1:0:s(0,i(.5/(h-n)+(t-n)/(h-n),1)),r=e(r);else if(3==o)r=h===n?t>=h?0:1:1-s(0,i(.5/(h-n)+(t-n)/(h-n),1)),r=e(r);else if(4==o)h===n?r=0:(r=s(0,i(.5/(h-n)+(t-n)/(h-n),1)),r<.5?r*=2:r=1-2*(r-.5)),r=e(r);else if(5==o){if(h===n)r=0;else{var l=h-n;t=i(s(0,t+.5-n),h-n);var p=-l/2+t,f=l/2;r=Math.sqrt(1-p*p/(f*f))}r=e(r)}else 6==o?(h===n?r=0:(t=i(s(0,t+.5-n),h-n),r=(1+Math.cos(Math.PI+2*Math.PI*t/(h-n)))/2),r=e(r)):(t>=a(n)&&(r=t-n<0?1-(n-t):s(0,i(h-t,1))),r=e(r));return r*this.a.v}var s=Math.max,i=Math.min,a=Math.floor;return function(s,i,a){this.mdf=!1,this.k=!1,this.data=i,this.dynamicProperties=[],this.getValue=t,this.getMult=e,this.elem=s,this.comp=s.comp,this.finalS=0,this.finalE=0,this.s=jt.getProp(s,i.s||{k:0},0,0,this.dynamicProperties),"e"in i?this.e=jt.getProp(s,i.e,0,0,this.dynamicProperties):this.e={v:100},this.o=jt.getProp(s,i.o||{k:0},0,0,this.dynamicProperties),this.xe=jt.getProp(s,i.xe||{k:0},0,0,this.dynamicProperties),this.ne=jt.getProp(s,i.ne||{k:0},0,0,this.dynamicProperties),this.a=jt.getProp(s,i.a,0,.01,this.dynamicProperties),this.dynamicProperties.length?a.push(this):this.getValue()}}(),u={getProp:h,getDashProp:o,getTextSelectorProp:l,getGradientProp:m};return u}();_.prototype.setPathData=function(t,e){this.c=t,this.setLength(e);for(var s=0;s<e;)this.v[s]=Ht.newPoint(),this.o[s]=Ht.newPoint(),this.i[s]=Ht.newPoint(),s+=1},_.prototype.setLength=function(t){for(;this._maxLength<t;)this.doubleArrayLength();this._length=t},_.prototype.doubleArrayLength=function(){this.v=this.v.concat(Array.apply(null,{length:this._maxLength})),this.i=this.i.concat(Array.apply(null,{length:this._maxLength})),this.o=this.o.concat(Array.apply(null,{length:this._maxLength})),this._maxLength*=2},_.prototype.setXYAt=function(t,e,s,i,a){var r;switch(this._length=Math.max(this._length,i+1),this._length>=this._maxLength&&this.doubleArrayLength(),s){case"v":r=this.v;break;case"i":r=this.i;break;case"o":r=this.o}(!r[i]||r[i]&&!a)&&(r[i]=Ht.newPoint()),r[i][0]=t,r[i][1]=e},_.prototype.setTripleAt=function(t,e,s,i,a,r,n,h){this.setXYAt(t,e,"v",n,h),this.setXYAt(s,i,"o",n,h),this.setXYAt(a,r,"i",n,h)};var Bt=function(){function t(t,e,s,i){var a,r,n;if(t<this.keyframes[0].t-this.offsetTime)a=this.keyframes[0].s[0],n=!0,e=0;else if(t>=this.keyframes[this.keyframes.length-1].t-this.offsetTime)a=1===this.keyframes[this.keyframes.length-2].h?this.keyframes[this.keyframes.length-1].s[0]:this.keyframes[this.keyframes.length-2].e[0],n=!0;else{for(var h,o,l,p,f,m,d=e,c=this.keyframes.length-1,u=!0;u&&(h=this.keyframes[d],o=this.keyframes[d+1],!(o.t-this.offsetTime>t));)d<c-1?d+=1:u=!1;n=1===h.h,e=d;var g;if(!n){if(t>=o.t-this.offsetTime)g=1;else if(t<h.t-this.offsetTime)g=0;else{var y;h.__fnct?y=h.__fnct:(y=Vt.getBezierEasing(h.o.x,h.o.y,h.i.x,h.i.y).get,h.__fnct=y),g=y((t-(h.t-this.offsetTime))/(o.t-this.offsetTime-(h.t-this.offsetTime)))}r=h.e[0]}a=h.s[0]}p=s._length,m=a.i[0].length;var v,b=!1;for(l=0;l<p;l+=1)for(f=0;f<m;f+=1)n?(v=a.i[l][f],s.i[l][f]!==v&&(s.i[l][f]=v,i&&(this.pv.i[l][f]=v),b=!0),v=a.o[l][f],s.o[l][f]!==v&&(s.o[l][f]=v,i&&(this.pv.o[l][f]=v),b=!0),v=a.v[l][f],s.v[l][f]!==v&&(s.v[l][f]=v,i&&(this.pv.v[l][f]=v),b=!0)):(v=a.i[l][f]+(r.i[l][f]-a.i[l][f])*g,s.i[l][f]!==v&&(s.i[l][f]=v,i&&(this.pv.i[l][f]=v),b=!0),v=a.o[l][f]+(r.o[l][f]-a.o[l][f])*g,s.o[l][f]!==v&&(s.o[l][f]=v,i&&(this.pv.o[l][f]=v),b=!0),v=a.v[l][f]+(r.v[l][f]-a.v[l][f])*g,s.v[l][f]!==v&&(s.v[l][f]=v,i&&(this.pv.v[l][f]=v),b=!0));return b&&(s.c=a.c),{iterationIndex:e,hasModified:b}}function e(){if(this.elem.globalData.frameId!==this.frameId){this.mdf=!1;var t=this.comp.renderedFrame-this.offsetTime,e=this.keyframes[0].t-this.offsetTime,s=this.keyframes[this.keyframes.length-1].t-this.offsetTime;if(this.lastFrame===l||!(this.lastFrame<e&&t<e||this.lastFrame>s&&t>s)){var i=this.lastFrame<t?this._lastIndex:0,a=this.interpolateShape(t,i,this.v,!0);this._lastIndex=a.iterationIndex,this.mdf=a.hasModified,a.hasModified&&(this.paths=this.localShapeCollection)}this.lastFrame=t,this.frameId=this.elem.globalData.frameId}}function s(){return this.v}function i(){this.paths=this.localShapeCollection,this.k||(this.mdf=!1)}function a(t,e,s){this.__shapeObject=1,this.comp=t.comp,this.k=!1,this.mdf=!1;var a=3===s?e.pt.k:e.ks.k;this.v=Jt.clone(a),this.pv=Jt.clone(this.v),this.localShapeCollection=Ut.newShapeCollection(),this.paths=this.localShapeCollection,this.paths.addShape(this.v),this.reset=i}function r(t,e,s){this.__shapeObject=1,this.comp=t.comp,this.elem=t,this.offsetTime=t.data.st,this._lastIndex=0,this.keyframes=3===s?e.pt.k:e.ks.k,this.k=!0,this.kf=!0;var a=this.keyframes[0].s[0].i.length;this.keyframes[0].s[0].i[0].length;this.v=Jt.newShape(),this.v.setPathData(this.keyframes[0].s[0].c,a),this.pv=Jt.clone(this.v),this.localShapeCollection=Ut.newShapeCollection(),this.paths=this.localShapeCollection,this.paths.addShape(this.v),this.lastFrame=l,this.reset=i}function n(t,e,s,i){var n;if(3===s||4===s){var h=3===s?e.pt:e.ks,o=h.k;n=1===h.a||o.length?new r(t,e,s):new a(t,e,s)}else 5===s?n=new m(t,e):6===s?n=new p(t,e):7===s&&(n=new f(t,e));return n.k&&i.push(n),n}function h(){return a}function o(){return r}var l=-999999;a.prototype.interpolateShape=t,a.prototype.getValue=s,r.prototype.getValue=e,r.prototype.interpolateShape=t;var p=function(){function t(){var t=this.p.v[0],e=this.p.v[1],i=this.s.v[0]/2,a=this.s.v[1]/2;3!==this.d?(this.v.v[0][0]=t,this.v.v[0][1]=e-a,this.v.v[1][0]=t+i,this.v.v[1][1]=e,this.v.v[2][0]=t,this.v.v[2][1]=e+a,this.v.v[3][0]=t-i,this.v.v[3][1]=e,this.v.i[0][0]=t-i*s,this.v.i[0][1]=e-a,this.v.i[1][0]=t+i,this.v.i[1][1]=e-a*s,this.v.i[2][0]=t+i*s,this.v.i[2][1]=e+a,this.v.i[3][0]=t-i,this.v.i[3][1]=e+a*s,this.v.o[0][0]=t+i*s,this.v.o[0][1]=e-a,this.v.o[1][0]=t+i,this.v.o[1][1]=e+a*s,this.v.o[2][0]=t-i*s,this.v.o[2][1]=e+a,this.v.o[3][0]=t-i,this.v.o[3][1]=e-a*s):(this.v.v[0][0]=t,this.v.v[0][1]=e-a,this.v.v[1][0]=t-i,this.v.v[1][1]=e,this.v.v[2][0]=t,this.v.v[2][1]=e+a,this.v.v[3][0]=t+i,this.v.v[3][1]=e,this.v.i[0][0]=t+i*s,this.v.i[0][1]=e-a,this.v.i[1][0]=t-i,this.v.i[1][1]=e-a*s,this.v.i[2][0]=t-i*s,this.v.i[2][1]=e+a,this.v.i[3][0]=t+i,this.v.i[3][1]=e+a*s,this.v.o[0][0]=t-i*s,this.v.o[0][1]=e-a,this.v.o[1][0]=t-i,this.v.o[1][1]=e+a*s,this.v.o[2][0]=t+i*s,this.v.o[2][1]=e+a,this.v.o[3][0]=t+i,this.v.o[3][1]=e-a*s)}function e(t){var e,s=this.dynamicProperties.length;if(this.elem.globalData.frameId!==this.frameId){for(this.mdf=!1,this.frameId=this.elem.globalData.frameId,e=0;e<s;e+=1)this.dynamicProperties[e].getValue(t),this.dynamicProperties[e].mdf&&(this.mdf=!0);this.mdf&&this.convertEllToPath()}}var s=Lt;return function(s,a){this.v=Jt.newShape(),this.v.setPathData(!0,4),this.localShapeCollection=Ut.newShapeCollection(),this.paths=this.localShapeCollection,this.localShapeCollection.addShape(this.v),this.d=a.d,this.dynamicProperties=[],this.elem=s,this.comp=s.comp,this.frameId=-1,this.mdf=!1,this.getValue=e,this.convertEllToPath=t,this.reset=i,this.p=jt.getProp(s,a.p,1,0,this.dynamicProperties),this.s=jt.getProp(s,a.s,1,0,this.dynamicProperties),this.dynamicProperties.length?this.k=!0:this.convertEllToPath()}}(),f=function(){function t(){var t,e=Math.floor(this.pt.v),s=2*Math.PI/e,i=this.or.v,a=this.os.v,r=2*Math.PI*i/(4*e),n=-Math.PI/2,h=3===this.data.d?-1:1;for(n+=this.r.v,this.v._length=0,t=0;t<e;t+=1){var o=i*Math.cos(n),l=i*Math.sin(n),p=0===o&&0===l?0:l/Math.sqrt(o*o+l*l),f=0===o&&0===l?0:-o/Math.sqrt(o*o+l*l);o+=+this.p.v[0],l+=+this.p.v[1],this.v.setTripleAt(o,l,o-p*r*a*h,l-f*r*a*h,o+p*r*a*h,l+f*r*a*h,t,!0),n+=s*h}this.paths.length=0,this.paths[0]=this.v}function e(){var t,e,s,i,a=2*Math.floor(this.pt.v),r=2*Math.PI/a,n=!0,h=this.or.v,o=this.ir.v,l=this.os.v,p=this.is.v,f=2*Math.PI*h/(2*a),m=2*Math.PI*o/(2*a),d=-Math.PI/2;d+=this.r.v;var c=3===this.data.d?-1:1;for(this.v._length=0,t=0;t<a;t+=1){e=n?h:o,s=n?l:p,i=n?f:m;var u=e*Math.cos(d),g=e*Math.sin(d),y=0===u&&0===g?0:g/Math.sqrt(u*u+g*g),v=0===u&&0===g?0:-u/Math.sqrt(u*u+g*g);u+=+this.p.v[0],g+=+this.p.v[1],this.v.setTripleAt(u,g,u-y*i*s*c,g-v*i*s*c,u+y*i*s*c,g+v*i*s*c,t,!0),n=!n,d+=r*c}}function s(){if(this.elem.globalData.frameId!==this.frameId){this.mdf=!1,this.frameId=this.elem.globalData.frameId;var t,e=this.dynamicProperties.length;for(t=0;t<e;t+=1)this.dynamicProperties[t].getValue(),this.dynamicProperties[t].mdf&&(this.mdf=!0);this.mdf&&this.convertToPath()}}return function(a,r){this.v=Jt.newShape(),this.v.setPathData(!0,0),this.elem=a,this.comp=a.comp,this.data=r,this.frameId=-1,this.d=r.d,this.dynamicProperties=[],this.mdf=!1,this.getValue=s,this.reset=i,1===r.sy?(this.ir=jt.getProp(a,r.ir,0,0,this.dynamicProperties),this.is=jt.getProp(a,r.is,0,.01,this.dynamicProperties),this.convertToPath=e):this.convertToPath=t,this.pt=jt.getProp(a,r.pt,0,0,this.dynamicProperties),this.p=jt.getProp(a,r.p,1,0,this.dynamicProperties),this.r=jt.getProp(a,r.r,0,Tt,this.dynamicProperties),this.or=jt.getProp(a,r.or,0,0,this.dynamicProperties),this.os=jt.getProp(a,r.os,0,.01,this.dynamicProperties),this.localShapeCollection=Ut.newShapeCollection(),this.localShapeCollection.addShape(this.v),this.paths=this.localShapeCollection,this.dynamicProperties.length?this.k=!0:this.convertToPath()}}(),m=function(){function t(t){if(this.elem.globalData.frameId!==this.frameId){this.mdf=!1,this.frameId=this.elem.globalData.frameId;var e,s=this.dynamicProperties.length;for(e=0;e<s;e+=1)this.dynamicProperties[e].getValue(t),this.dynamicProperties[e].mdf&&(this.mdf=!0);this.mdf&&this.convertRectToPath()}}function e(){var t=this.p.v[0],e=this.p.v[1],s=this.s.v[0]/2,i=this.s.v[1]/2,a=Dt(s,i,this.r.v),r=a*(1-Lt);this.v._length=0,2===this.d||1===this.d?(this.v.setTripleAt(t+s,e-i+a,t+s,e-i+a,t+s,e-i+r,0,!0),this.v.setTripleAt(t+s,e+i-a,t+s,e+i-r,t+s,e+i-a,1,!0),0!==a?(this.v.setTripleAt(t+s-a,e+i,t+s-a,e+i,t+s-r,e+i,2,!0),this.v.setTripleAt(t-s+a,e+i,t-s+r,e+i,t-s+a,e+i,3,!0),this.v.setTripleAt(t-s,e+i-a,t-s,e+i-a,t-s,e+i-r,4,!0),this.v.setTripleAt(t-s,e-i+a,t-s,e-i+r,t-s,e-i+a,5,!0),this.v.setTripleAt(t-s+a,e-i,t-s+a,e-i,t-s+r,e-i,6,!0),this.v.setTripleAt(t+s-a,e-i,t+s-r,e-i,t+s-a,e-i,7,!0)):(this.v.setTripleAt(t-s,e+i,t-s+r,e+i,t-s,e+i,2),this.v.setTripleAt(t-s,e-i,t-s,e-i+r,t-s,e-i,3))):(this.v.setTripleAt(t+s,e-i+a,t+s,e-i+r,t+s,e-i+a,0,!0),0!==a?(this.v.setTripleAt(t+s-a,e-i,t+s-a,e-i,t+s-r,e-i,1,!0),this.v.setTripleAt(t-s+a,e-i,t-s+r,e-i,t-s+a,e-i,2,!0),this.v.setTripleAt(t-s,e-i+a,t-s,e-i+a,t-s,e-i+r,3,!0),this.v.setTripleAt(t-s,e+i-a,t-s,e+i-r,t-s,e+i-a,4,!0),this.v.setTripleAt(t-s+a,e+i,t-s+a,e+i,t-s+r,e+i,5,!0),this.v.setTripleAt(t+s-a,e+i,t+s-r,e+i,t+s-a,e+i,6,!0),this.v.setTripleAt(t+s,e+i-a,t+s,e+i-a,t+s,e+i-r,7,!0)):(this.v.setTripleAt(t-s,e-i,t-s+r,e-i,t-s,e-i,1,!0),this.v.setTripleAt(t-s,e+i,t-s,e+i-r,t-s,e+i,2,!0),this.v.setTripleAt(t+s,e+i,t+s-r,e+i,t+s,e+i,3,!0)))}return function(s,a){this.v=Jt.newShape(),this.v.c=!0,this.localShapeCollection=Ut.newShapeCollection(),this.localShapeCollection.addShape(this.v),this.paths=this.localShapeCollection,this.elem=s,this.comp=s.comp,this.frameId=-1,this.d=a.d,this.dynamicProperties=[],this.mdf=!1,this.getValue=t,this.convertRectToPath=e,this.reset=i,this.p=jt.getProp(s,a.p,1,0,this.dynamicProperties),this.s=jt.getProp(s,a.s,1,0,this.dynamicProperties),this.r=jt.getProp(s,a.r,0,0,this.dynamicProperties),this.dynamicProperties.length?this.k=!0:this.convertRectToPath()}}(),d={};return d.getShapeProp=n,d.getConstructorFunction=h,d.getKeyframedConstructorFunction=o,d}(),Gt=function(){function t(t,e){i[t]||(i[t]=e)}function e(t,e,s,a){return new i[t](e,s,a)}var s={},i={};return s.registerModifier=t,s.getModifier=e,s}();S.prototype.initModifierProperties=function(){},S.prototype.addShapeToModifier=function(){},S.prototype.addShape=function(t){this.closed||(this.shapes.push({shape:t.sh,data:t,localShapeCollection:Ut.newShapeCollection()}),this.addShapeToModifier(t.sh))},S.prototype.init=function(t,e,s){this.elem=t,this.frameId=-1,this.shapes=[],this.dynamicProperties=[],this.mdf=!1,this.closed=!1,this.k=!1,this.comp=t.comp,this.initModifierProperties(t,e),this.dynamicProperties.length?(this.k=!0,s.push(this)):this.getValue(!0)},P(S,F),F.prototype.processKeys=function(t){if(this.elem.globalData.frameId!==this.frameId||t){this.mdf=!!t,this.frameId=this.elem.globalData.frameId;var e,s=this.dynamicProperties.length;for(e=0;e<s;e+=1)this.dynamicProperties[e].getValue(),this.dynamicProperties[e].mdf&&(this.mdf=!0);if(this.mdf||t){var i=this.o.v%360/360;i<0&&(i+=1);var a=this.s.v+i,r=this.e.v+i;if(a>r){var n=a;a=r,r=n}this.sValue=a,this.eValue=r,this.oValue=i}}},F.prototype.initModifierProperties=function(t,e){this.sValue=0,this.eValue=0,this.oValue=0,this.getValue=this.processKeys,this.s=jt.getProp(t,e.s,0,.01,this.dynamicProperties),this.e=jt.getProp(t,e.e,0,.01,this.dynamicProperties),this.o=jt.getProp(t,e.o,0,0,this.dynamicProperties),this.m=e.m,this.dynamicProperties.length||this.getValue(!0)},F.prototype.calculateShapeEdges=function(t,e,s,i,a){var r=[];e<=1?r.push({s:t,e:e}):t>=1?r.push({s:t-1,e:e-1}):(r.push({s:t,e:1}),r.push({s:0,e:e-1}));var n,h,o=[],l=r.length;for(n=0;n<l;n+=1)if(h=r[n],h.e*a<i||h.s*a>i+s);else{var p,f;p=h.s*a<=i?0:(h.s*a-i)/s,f=h.e*a>=i+s?1:(h.e*a-i)/s,o.push([p,f])}return o.length||o.push([0,0]),o},F.prototype.processShapes=function(t){var e,s,i,a,r,n,h,o=this.shapes.length,l=this.sValue,p=this.eValue,f=0;if(p===l)for(s=0;s<o;s+=1)this.shapes[s].localShapeCollection.releaseShapes(),this.shapes[s].shape.mdf=!0,this.shapes[s].shape.paths=this.shapes[s].localShapeCollection;else if(1===p&&0===l||0===p&&1===l){if(this.mdf)for(s=0;s<o;s+=1)this.shapes[s].shape.mdf=!0}else{var m,d,c=[];for(s=0;s<o;s+=1)if(m=this.shapes[s],m.shape.mdf||this.mdf||t||2===this.m){if(e=m.shape.paths,a=e._length,h=0,!m.shape.mdf&&m.pathsData)h=m.totalShapeLength;else{for(r=[],i=0;i<a;i+=1)n=Ot.getSegmentsLength(e.shapes[i]),r.push(n),h+=n.totalLength;m.totalShapeLength=h,m.pathsData=r}f+=h,m.shape.mdf=!0}else m.shape.paths=m.localShapeCollection;var i,a,u=l,g=p,y=0;for(s=o-1;s>=0;s-=1)if(m=this.shapes[s],m.shape.mdf){if(d=m.localShapeCollection,d.releaseShapes(),2===this.m&&o>1){var v=this.calculateShapeEdges(l,p,m.totalShapeLength,y,f);y+=m.totalShapeLength}else v=[[u,g]];for(a=v.length,i=0;i<a;i+=1){u=v[i][0],g=v[i][1],c.length=0,g<=1?c.push({s:m.totalShapeLength*u,e:m.totalShapeLength*g}):u>=1?c.push({s:m.totalShapeLength*(u-1),e:m.totalShapeLength*(g-1)}):(c.push({s:m.totalShapeLength*u,e:m.totalShapeLength}),c.push({s:0,e:m.totalShapeLength*(g-1)}));var b=this.addShapes(m,c[0]);if(c[0].s!==c[0].e){if(c.length>1)if(m.shape.v.c){var k=b.pop();this.addPaths(b,d),b=this.addShapes(m,c[1],k)}else this.addPaths(b,d),b=this.addShapes(m,c[1]);this.addPaths(b,d)}}m.shape.paths=d}}this.dynamicProperties.length||(this.mdf=!1)},F.prototype.addPaths=function(t,e){var s,i=t.length;for(s=0;s<i;s+=1)e.addShape(t[s])},F.prototype.addSegment=function(t,e,s,i,a,r,n){a.setXYAt(e[0],e[1],"o",r),a.setXYAt(s[0],s[1],"i",r+1),n&&a.setXYAt(t[0],t[1],"v",r),a.setXYAt(i[0],i[1],"v",r+1)},F.prototype.addShapes=function(t,e,s){var i,a,r,n,h,o,l,p,f=t.pathsData,m=t.shape.paths.shapes,d=t.shape.paths._length,c=0,u=[],g=!0;
for(s?(h=s._length,p=s._length):(s=Jt.newShape(),h=0,p=0),u.push(s),i=0;i<d;i+=1){for(o=f[i].lengths,s.c=m[i].c,r=m[i].c?o.length:o.length+1,a=1;a<r;a+=1)if(n=o[a-1],c+n.addedLength<e.s)c+=n.addedLength,s.c=!1;else{if(c>e.e){s.c=!1;break}e.s<=c&&e.e>=c+n.addedLength?(this.addSegment(m[i].v[a-1],m[i].o[a-1],m[i].i[a],m[i].v[a],s,h,g),g=!1):(l=Ot.getNewSegment(m[i].v[a-1],m[i].v[a],m[i].o[a-1],m[i].i[a],(e.s-c)/n.addedLength,(e.e-c)/n.addedLength,o[a-1]),this.addSegment(l.pt1,l.pt3,l.pt4,l.pt2,s,h,g),g=!1,s.c=!1),c+=n.addedLength,h+=1}if(m[i].c){if(n=o[a-1],c<=e.e){var y=o[a-1].addedLength;e.s<=c&&e.e>=c+y?(this.addSegment(m[i].v[a-1],m[i].o[a-1],m[i].i[0],m[i].v[0],s,h,g),g=!1):(l=Ot.getNewSegment(m[i].v[a-1],m[i].v[0],m[i].o[a-1],m[i].i[0],(e.s-c)/y,(e.e-c)/y,o[a-1]),this.addSegment(l.pt1,l.pt3,l.pt4,l.pt2,s,h,g),g=!1,s.c=!1)}else s.c=!1;c+=n.addedLength,h+=1}if(s._length&&(s.setXYAt(s.v[p][0],s.v[p][1],"i",p),s.setXYAt(s.v[s._length-1][0],s.v[s._length-1][1],"o",s._length-1)),c>e.e)break;i<d-1&&(s=Jt.newShape(),g=!0,u.push(s),h=0)}return u},Gt.registerModifier("tm",F),P(S,x),x.prototype.processKeys=function(t){if(this.elem.globalData.frameId!==this.frameId||t){this.mdf=!!t,this.frameId=this.elem.globalData.frameId;var e,s=this.dynamicProperties.length;for(e=0;e<s;e+=1)this.dynamicProperties[e].getValue(),this.dynamicProperties[e].mdf&&(this.mdf=!0)}},x.prototype.initModifierProperties=function(t,e){this.getValue=this.processKeys,this.rd=jt.getProp(t,e.r,0,null,this.dynamicProperties),this.dynamicProperties.length||this.getValue(!0)},x.prototype.processPath=function(t,e){var s=Jt.newShape();s.c=t.c;var i,a,r,n,h,o,l,p,f,m,d,c,u,g=t._length,y=0;for(i=0;i<g;i+=1)a=t.v[i],n=t.o[i],r=t.i[i],a[0]===n[0]&&a[1]===n[1]&&a[0]===r[0]&&a[1]===r[1]?0!==i&&i!==g-1||t.c?(h=0===i?t.v[g-1]:t.v[i-1],o=Math.sqrt(Math.pow(a[0]-h[0],2)+Math.pow(a[1]-h[1],2)),l=o?Math.min(o/2,e)/o:0,p=c=a[0]+(h[0]-a[0])*l,f=u=a[1]-(a[1]-h[1])*l,m=p-(p-a[0])*Lt,d=f-(f-a[1])*Lt,s.setTripleAt(p,f,m,d,c,u,y),y+=1,h=i===g-1?t.v[0]:t.v[i+1],o=Math.sqrt(Math.pow(a[0]-h[0],2)+Math.pow(a[1]-h[1],2)),l=o?Math.min(o/2,e)/o:0,p=m=a[0]+(h[0]-a[0])*l,f=d=a[1]+(h[1]-a[1])*l,c=p-(p-a[0])*Lt,u=f-(f-a[1])*Lt,s.setTripleAt(p,f,m,d,c,u,y),y+=1):(s.setTripleAt(a[0],a[1],n[0],n[1],r[0],r[1],y),y+=1):(s.setTripleAt(t.v[i][0],t.v[i][1],t.o[i][0],t.o[i][1],t.i[i][0],t.i[i][1],y),y+=1);return s},x.prototype.processShapes=function(t){var e,s,i,a,r=this.shapes.length,n=this.rd.v;if(0!==n){var h,o,l;for(s=0;s<r;s+=1){if(h=this.shapes[s],o=h.shape.paths,l=h.localShapeCollection,h.shape.mdf||this.mdf||t)for(l.releaseShapes(),h.shape.mdf=!0,e=h.shape.paths.shapes,a=h.shape.paths._length,i=0;i<a;i+=1)l.addShape(this.processPath(e[i],n));h.shape.paths=h.localShapeCollection}}this.dynamicProperties.length||(this.mdf=!1)},Gt.registerModifier("rd",x),w.prototype.processKeys=function(t){if(this.elem.globalData.frameId!==this.frameId||t){this.mdf=!!t;var e,s=this.dynamicProperties.length;for(e=0;e<s;e+=1)this.dynamicProperties[e].getValue(),this.dynamicProperties[e].mdf&&(this.mdf=!0)}},w.prototype.initModifierProperties=function(t,e){this.getValue=this.processKeys,this.c=jt.getProp(t,e.c,0,null,this.dynamicProperties),this.o=jt.getProp(t,e.o,0,null,this.dynamicProperties),this.tr=jt.getProp(t,e.tr,2,null,this.dynamicProperties),this.data=e,this.dynamicProperties.length||this.getValue(!0),this.pMatrix=new k,this.rMatrix=new k,this.sMatrix=new k,this.tMatrix=new k,this.matrix=new k},w.prototype.applyTransforms=function(t,e,s,i,a,r){var n=r?-1:1,h=i.s.v[0]+(1-i.s.v[0])*(1-a),o=i.s.v[1]+(1-i.s.v[1])*(1-a);t.translate(i.p.v[0]*n*a,i.p.v[1]*n*a,i.p.v[2]),e.translate(-i.a.v[0],-i.a.v[1],i.a.v[2]),e.rotate(-i.r.v*n*a),e.translate(i.a.v[0],i.a.v[1],i.a.v[2]),s.translate(-i.a.v[0],-i.a.v[1],i.a.v[2]),s.scale(r?1/h:h,r?1/o:o),s.translate(i.a.v[0],i.a.v[1],i.a.v[2])},w.prototype.init=function(t,e,s,i,a){this.elem=t,this.arr=e,this.pos=s,this.elemsData=i,this._currentCopies=0,this._elements=[],this._groups=[],this.dynamicProperties=[],this.frameId=-1,this.initModifierProperties(t,e[s]);for(var r=0;s>0;)s-=1,this._elements.unshift(e[s]),r+=1;this.dynamicProperties.length?(this.k=!0,a.push(this)):this.getValue(!0)},w.prototype.resetElements=function(t){var e,s=t.length;for(e=0;e<s;e+=1)t[e]._processed=!1,"gr"===t[e].ty&&this.resetElements(t[e].it)},w.prototype.cloneElements=function(t){var e=(t.length,JSON.parse(JSON.stringify(t)));return this.resetElements(e),e},w.prototype.changeGroupRender=function(t,e){var s,i=t.length;for(s=0;s<i;s+=1)t[s]._render=e,"gr"===t[s].ty&&this.changeGroupRender(t[s].it,e)},w.prototype.processShapes=function(t){if(this.elem.globalData.frameId!==this.frameId&&(this.frameId=this.elem.globalData.frameId,this.dynamicProperties.length||t||(this.mdf=!1),this.mdf)){var e=Math.ceil(this.c.v);if(this._groups.length<e){for(;this._groups.length<e;){var s={it:this.cloneElements(this._elements),ty:"gr"};s.it.push({a:{a:0,ix:1,k:[0,0]},nm:"Transform",o:{a:0,ix:7,k:100},p:{a:0,ix:2,k:[0,0]},r:{a:0,ix:6,k:0},s:{a:0,ix:3,k:[100,100]},sa:{a:0,ix:5,k:0},sk:{a:0,ix:4,k:0},ty:"tr"}),this.arr.splice(0,0,s),this._groups.splice(0,0,s),this._currentCopies+=1}this.elem.reloadShapes()}var i,a,r=0;for(i=0;i<=this._groups.length-1;i+=1)a=r<e,this._groups[i]._render=a,this.changeGroupRender(this._groups[i].it,a),r+=1;this._currentCopies=e,this.elem.firstFrame=!0;var n=this.o.v,h=n%1,o=n>0?Math.floor(n):Math.ceil(n),l=(this.tr.v.props,this.pMatrix.props),p=this.rMatrix.props,f=this.sMatrix.props;this.pMatrix.reset(),this.rMatrix.reset(),this.sMatrix.reset(),this.tMatrix.reset(),this.matrix.reset();var m=0;if(n>0){for(;m<o;)this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,1,!1),m+=1;h&&(this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,h,!1),m+=h)}else if(n<0){for(;m>o;)this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,1,!0),m-=1;h&&(this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,-h,!0),m-=h)}i=1===this.data.m?0:this._currentCopies-1;var d=1===this.data.m?1:-1;for(r=this._currentCopies;r;){if(0!==m){(0!==i&&1===d||i!==this._currentCopies-1&&d===-1)&&this.applyTransforms(this.pMatrix,this.rMatrix,this.sMatrix,this.tr,1,!1),this.matrix.transform(p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7],p[8],p[9],p[10],p[11],p[12],p[13],p[14],p[15]),this.matrix.transform(f[0],f[1],f[2],f[3],f[4],f[5],f[6],f[7],f[8],f[9],f[10],f[11],f[12],f[13],f[14],f[15]),this.matrix.transform(l[0],l[1],l[2],l[3],l[4],l[5],l[6],l[7],l[8],l[9],l[10],l[11],l[12],l[13],l[14],l[15]);var c,u=this.elemsData[i].it,g=u[u.length-1].transform.mProps.v.props,y=g.length;for(c=0;c<y;c+=1)g[c]=this.matrix.props[c];this.matrix.reset()}else{this.matrix.reset();var c,u=this.elemsData[i].it,g=u[u.length-1].transform.mProps.v.props,y=g.length;for(c=0;c<y;c+=1)g[c]=this.matrix.props[c]}m+=1,r-=1,i+=d}}},w.prototype.addShape=function(){},Gt.registerModifier("rp",w),D.prototype.addShape=function(t){this._length===this._maxLength&&(this.shapes=this.shapes.concat(Array.apply(null,{length:this._maxLength})),this._maxLength*=2),this.shapes[this._length]=t,this._length+=1},D.prototype.releaseShapes=function(){var t;for(t=0;t<this._length;t+=1)Jt.release(this.shapes[t]);this._length=0};var Wt=function(){function t(){this.loadedAssets+=1,this.loadedAssets===this.totalImages&&n&&n(null)}function e(t){var e="";if(this.assetsPath){var s=t.p;s.indexOf("images/")!==-1&&(s=s.split("/")[1]),e=this.assetsPath+s}else e=this.path,e+=t.u?t.u:"",e+=t.p;return e}function s(e){var s=document.createElement("img");s.addEventListener("load",t.bind(this),!1),s.addEventListener("error",t.bind(this),!1),s.src=e}function i(t,i){n=i,this.totalAssets=t.length;var a;for(a=0;a<this.totalAssets;a+=1)t[a].layers||(s.bind(this)(e.bind(this)(t[a])),this.totalImages+=1)}function a(t){this.path=t||""}function r(t){this.assetsPath=t||""}var n;return function(){this.loadAssets=i,this.setAssetsPath=r,this.setPath=a,this.assetsPath="",this.path="",this.totalAssets=0,this.totalImages=0,this.loadedAssets=0}}(),Xt=function(){var t={maskType:!0};return(/MSIE 10/i.test(navigator.userAgent)||/MSIE 9/i.test(navigator.userAgent)||/rv:11.0/i.test(navigator.userAgent)||/Edge\/\d./i.test(navigator.userAgent))&&(t.maskType=!1),t}(),qt=function(){function t(t){var e=document.createElementNS(Et,"filter");return e.setAttribute("id",t),e.setAttribute("filterUnits","objectBoundingBox"),e.setAttribute("x","0%"),e.setAttribute("y","0%"),e.setAttribute("width","100%"),e.setAttribute("height","100%"),e}function e(){var t=document.createElementNS(Et,"feColorMatrix");return t.setAttribute("type","matrix"),t.setAttribute("color-interpolation-filters","sRGB"),t.setAttribute("values","0 0 0 1 0  0 0 0 1 0  0 0 0 1 0  0 0 0 1 1"),t}var s={};return s.createFilter=t,s.createAlphaToLuminanceFilter=e,s}();C.prototype.searchProperties=function(t){var e,s,i,a=this._textData.a.length,r=jt.getProp;for(e=0;e<a;e+=1)i=this._textData.a[e],s={a:{},s:{}},"r"in i.a&&(s.a.r=r(this._elem,i.a.r,0,Tt,this._dynamicProperties)),"rx"in i.a&&(s.a.rx=r(this._elem,i.a.rx,0,Tt,this._dynamicProperties)),"ry"in i.a&&(s.a.ry=r(this._elem,i.a.ry,0,Tt,this._dynamicProperties)),"sk"in i.a&&(s.a.sk=r(this._elem,i.a.sk,0,Tt,this._dynamicProperties)),"sa"in i.a&&(s.a.sa=r(this._elem,i.a.sa,0,Tt,this._dynamicProperties)),"s"in i.a&&(s.a.s=r(this._elem,i.a.s,1,.01,this._dynamicProperties)),"a"in i.a&&(s.a.a=r(this._elem,i.a.a,1,0,this._dynamicProperties)),"o"in i.a&&(s.a.o=r(this._elem,i.a.o,0,.01,this._dynamicProperties)),"p"in i.a&&(s.a.p=r(this._elem,i.a.p,1,0,this._dynamicProperties)),"sw"in i.a&&(s.a.sw=r(this._elem,i.a.sw,0,0,this._dynamicProperties)),"sc"in i.a&&(s.a.sc=r(this._elem,i.a.sc,1,0,this._dynamicProperties)),"fc"in i.a&&(s.a.fc=r(this._elem,i.a.fc,1,0,this._dynamicProperties)),"fh"in i.a&&(s.a.fh=r(this._elem,i.a.fh,0,0,this._dynamicProperties)),"fs"in i.a&&(s.a.fs=r(this._elem,i.a.fs,0,.01,this._dynamicProperties)),"fb"in i.a&&(s.a.fb=r(this._elem,i.a.fb,0,.01,this._dynamicProperties)),"t"in i.a&&(s.a.t=r(this._elem,i.a.t,0,0,this._dynamicProperties)),s.s=jt.getTextSelectorProp(this._elem,i.s,this._dynamicProperties),s.s.t=i.s.t,this._animatorsData[e]=s;this._textData.p&&"m"in this._textData.p?(this._pathData={f:r(this._elem,this._textData.p.f,0,0,this._dynamicProperties),l:r(this._elem,this._textData.p.l,0,0,this._dynamicProperties),r:this._textData.p.r,m:this._elem.maskManager.getMaskProperty(this._textData.p.m)},this._hasMaskedPath=!0):this._hasMaskedPath=!1,this._moreOptions.alignment=r(this._elem,this._textData.m.a,1,0,this._dynamicProperties),this._dynamicProperties.length&&t.push(this)},C.prototype.getMeasures=function(t,e){if(this.lettersChangedFlag=e,this.mdf||this._firstFrame||e||this._hasMaskedPath&&this._pathData.m.mdf){this._firstFrame=!1;var s,i,a,r,n=this._moreOptions.alignment.v,h=this._animatorsData,o=this._textData,l=this.mHelper,p=this._renderType,f=this.renderedLetters.length,m=(this.data,t.l);if(this._hasMaskedPath){var d=this._pathData.m;if(!this._pathData.n||this._pathData.mdf){var c=d.v;this._pathData.r&&(c=b(c));var u={tLength:0,segments:[]};r=c._length-1;var k,A=0;for(a=0;a<r;a+=1)k={s:c.v[a],e:c.v[a+1],to:[c.o[a][0]-c.v[a][0],c.o[a][1]-c.v[a][1]],ti:[c.i[a+1][0]-c.v[a+1][0],c.i[a+1][1]-c.v[a+1][1]]},Ot.buildBezierData(k),u.tLength+=k.bezierData.segmentLength,u.segments.push(k),A+=k.bezierData.segmentLength;a=r,d.v.c&&(k={s:c.v[a],e:c.v[0],to:[c.o[a][0]-c.v[a][0],c.o[a][1]-c.v[a][1]],ti:[c.i[0][0]-c.v[0][0],c.i[0][1]-c.v[0][1]]},Ot.buildBezierData(k),u.tLength+=k.bezierData.segmentLength,u.segments.push(k),A+=k.bezierData.segmentLength),this._pathData.pi=u}var P,E,M,u=this._pathData.pi,_=this._pathData.f.v,S=0,F=1,x=0,w=!0,D=u.segments;if(_<0&&d.v.c)for(u.tLength<Math.abs(_)&&(_=-Math.abs(_)%u.tLength),S=D.length-1,M=D[S].bezierData.points,F=M.length-1;_<0;)_+=M[F].partialLength,F-=1,F<0&&(S-=1,M=D[S].bezierData.points,F=M.length-1);M=D[S].bezierData.points,E=M[F-1],P=M[F];var C,T,L=P.partialLength}r=m.length,s=0,i=0;var N,V,O,z,R,j=1.2*t.s*.714,B=!0;if(z=h.length,e)for(O=0;O<z;O+=1)V=h[O].s,V.getValue(!0);var G,W,X,q,Y,H,J,U,Z,K,Q,$,tt,et=-1,st=_,it=S,at=F,rt=-1,nt=0,ht="",ot=this.defaultPropsArray;for(a=0;a<r;a+=1){if(l.reset(),Y=1,m[a].n)s=0,i+=t.yOffset,i+=B?1:0,_=st,B=!1,nt=0,this._hasMaskedPath&&(S=it,F=at,M=D[S].bezierData.points,E=M[F-1],P=M[F],L=P.partialLength,x=0),tt=K=$=ht="",ot=this.defaultPropsArray;else{if(this._hasMaskedPath){if(rt!==m[a].line){switch(t.j){case 1:_+=A-t.lineWidths[m[a].line];break;case 2:_+=(A-t.lineWidths[m[a].line])/2}rt=m[a].line}et!==m[a].ind&&(m[et]&&(_+=m[et].extra),_+=m[a].an/2,et=m[a].ind),_+=n[0]*m[a].an/200;var lt=0;for(O=0;O<z;O+=1)N=h[O].a,"p"in N&&(V=h[O].s,G=V.getMult(m[a].anIndexes[O],o.a[O].s.totalChars),lt+=G.length?N.p.v[0]*G[0]:N.p.v[0]*G),"a"in N&&(V=h[O].s,G=V.getMult(m[a].anIndexes[O],o.a[O].s.totalChars),lt+=G.length?N.a.v[0]*G[0]:N.a.v[0]*G);for(w=!0;w;)x+L>=_+lt||!M?(C=(_+lt-x)/P.partialLength,X=E.point[0]+(P.point[0]-E.point[0])*C,q=E.point[1]+(P.point[1]-E.point[1])*C,l.translate(-n[0]*m[a].an/200,-(n[1]*j/100)),w=!1):M&&(x+=P.partialLength,F+=1,F>=M.length&&(F=0,S+=1,D[S]?M=D[S].bezierData.points:d.v.c?(F=0,S=0,M=D[S].bezierData.points):(x-=P.partialLength,M=null)),M&&(E=P,P=M[F],L=P.partialLength));W=m[a].an/2-m[a].add,l.translate(-W,0,0)}else W=m[a].an/2-m[a].add,l.translate(-W,0,0),l.translate(-n[0]*m[a].an/200,-n[1]*j/100,0);for(nt+=m[a].l/2,O=0;O<z;O+=1)N=h[O].a,"t"in N&&(V=h[O].s,G=V.getMult(m[a].anIndexes[O],o.a[O].s.totalChars),this._hasMaskedPath?_+=G.length?N.t*G[0]:N.t*G:s+=G.length?N.t.v*G[0]:N.t.v*G);for(nt+=m[a].l/2,t.strokeWidthAnim&&(J=t.sw||0),t.strokeColorAnim&&(H=t.sc?[t.sc[0],t.sc[1],t.sc[2]]:[0,0,0]),t.fillColorAnim&&t.fc&&(U=[t.fc[0],t.fc[1],t.fc[2]]),O=0;O<z;O+=1)N=h[O].a,"a"in N&&(V=h[O].s,G=V.getMult(m[a].anIndexes[O],o.a[O].s.totalChars),G.length?l.translate(-N.a.v[0]*G[0],-N.a.v[1]*G[1],N.a.v[2]*G[2]):l.translate(-N.a.v[0]*G,-N.a.v[1]*G,N.a.v[2]*G));for(O=0;O<z;O+=1)N=h[O].a,"s"in N&&(V=h[O].s,G=V.getMult(m[a].anIndexes[O],o.a[O].s.totalChars),G.length?l.scale(1+(N.s.v[0]-1)*G[0],1+(N.s.v[1]-1)*G[1],1):l.scale(1+(N.s.v[0]-1)*G,1+(N.s.v[1]-1)*G,1));for(O=0;O<z;O+=1){if(N=h[O].a,V=h[O].s,G=V.getMult(m[a].anIndexes[O],o.a[O].s.totalChars),"sk"in N&&(G.length?l.skewFromAxis(-N.sk.v*G[0],N.sa.v*G[1]):l.skewFromAxis(-N.sk.v*G,N.sa.v*G)),"r"in N&&(G.length?l.rotateZ(-N.r.v*G[2]):l.rotateZ(-N.r.v*G)),"ry"in N&&(G.length?l.rotateY(N.ry.v*G[1]):l.rotateY(N.ry.v*G)),"rx"in N&&(G.length?l.rotateX(N.rx.v*G[0]):l.rotateX(N.rx.v*G)),"o"in N&&(Y+=G.length?(N.o.v*G[0]-Y)*G[0]:(N.o.v*G-Y)*G),t.strokeWidthAnim&&"sw"in N&&(J+=G.length?N.sw.v*G[0]:N.sw.v*G),t.strokeColorAnim&&"sc"in N)for(Z=0;Z<3;Z+=1)G.length?H[Z]=H[Z]+(N.sc.v[Z]-H[Z])*G[0]:H[Z]=H[Z]+(N.sc.v[Z]-H[Z])*G;if(t.fillColorAnim&&t.fc){if("fc"in N)for(Z=0;Z<3;Z+=1)G.length?U[Z]=U[Z]+(N.fc.v[Z]-U[Z])*G[0]:U[Z]=U[Z]+(N.fc.v[Z]-U[Z])*G;"fh"in N&&(U=G.length?v(U,N.fh.v*G[0]):v(U,N.fh.v*G)),"fs"in N&&(U=G.length?g(U,N.fs.v*G[0]):g(U,N.fs.v*G)),"fb"in N&&(U=G.length?y(U,N.fb.v*G[0]):y(U,N.fb.v*G))}}for(O=0;O<z;O+=1)N=h[O].a,"p"in N&&(V=h[O].s,G=V.getMult(m[a].anIndexes[O],o.a[O].s.totalChars),this._hasMaskedPath?G.length?l.translate(0,N.p.v[1]*G[0],-N.p.v[2]*G[1]):l.translate(0,N.p.v[1]*G,-N.p.v[2]*G):G.length?l.translate(N.p.v[0]*G[0],N.p.v[1]*G[1],-N.p.v[2]*G[2]):l.translate(N.p.v[0]*G,N.p.v[1]*G,-N.p.v[2]*G));if(t.strokeWidthAnim&&(K=J<0?0:J),t.strokeColorAnim&&(Q="rgb("+Math.round(255*H[0])+","+Math.round(255*H[1])+","+Math.round(255*H[2])+")"),t.fillColorAnim&&t.fc&&($="rgb("+Math.round(255*U[0])+","+Math.round(255*U[1])+","+Math.round(255*U[2])+")"),this._hasMaskedPath){if(l.translate(0,-t.ls),l.translate(0,n[1]*j/100+i,0),o.p.p){T=(P.point[1]-E.point[1])/(P.point[0]-E.point[0]);var pt=180*Math.atan(T)/Math.PI;P.point[0]<E.point[0]&&(pt+=180),l.rotate(-pt*Math.PI/180)}l.translate(X,q,0),_-=n[0]*m[a].an/200,m[a+1]&&et!==m[a+1].ind&&(_+=m[a].an/2,_+=t.tr/1e3*t.s)}else{switch(l.translate(s,i,0),t.ps&&l.translate(t.ps[0],t.ps[1]+t.ascent,0),t.j){case 1:l.translate(t.justifyOffset+(t.boxWidth-t.lineWidths[m[a].line]),0,0);break;case 2:l.translate(t.justifyOffset+(t.boxWidth-t.lineWidths[m[a].line])/2,0,0)}l.translate(0,-t.ls),l.translate(W,0,0),l.translate(n[0]*m[a].an/200,n[1]*j/100,0),s+=m[a].l+t.tr/1e3*t.s}"html"===p?ht=l.toCSS():"svg"===p?ht=l.to2dCSS():ot=[l.props[0],l.props[1],l.props[2],l.props[3],l.props[4],l.props[5],l.props[6],l.props[7],l.props[8],l.props[9],l.props[10],l.props[11],l.props[12],l.props[13],l.props[14],l.props[15]],tt=Y}f<=a?(R=new I(tt,K,Q,$,ht,ot),this.renderedLetters.push(R),f+=1,this.lettersChangedFlag=!0):(R=this.renderedLetters[a],this.lettersChangedFlag=R.update(tt,K,Q,$,ht,ot)||this.lettersChangedFlag)}}},C.prototype.getValue=function(){if(this._elem.globalData.frameId!==this._frameId){this._frameId=this._elem.globalData.frameId;var t,e=this._dynamicProperties.length;for(this.mdf=!1,t=0;t<e;t+=1)this._dynamicProperties[t].getValue(),this.mdf=this._dynamicProperties[t].mdf||this.mdf}},C.prototype.mHelper=new k,C.prototype.defaultPropsArray=[],I.prototype.update=function(t,e,s,i,a,r){this.mdf.o=!1,this.mdf.sw=!1,this.mdf.sc=!1,this.mdf.fc=!1,this.mdf.m=!1,this.mdf.p=!1;var n=!1;return this.o!==t&&(this.o=t,this.mdf.o=!0,n=!0),this.sw!==e&&(this.sw=e,this.mdf.sw=!0,n=!0),this.sc!==s&&(this.sc=s,this.mdf.sc=!0,n=!0),this.fc!==i&&(this.fc=i,this.mdf.fc=!0,n=!0),this.m!==a&&(this.m=a,this.mdf.m=!0,n=!0),!r.length||this.p[0]===r[0]&&this.p[1]===r[1]&&this.p[4]===r[4]&&this.p[5]===r[5]&&this.p[12]===r[12]&&this.p[13]===r[13]||(this.p=r,this.mdf.p=!0,n=!0),n},T.prototype.setCurrentData=function(t){var e=this.currentData;e.ascent=t.ascent,e.boxWidth=t.boxWidth?t.boxWidth:e.boxWidth,e.f=t.f,e.fStyle=t.fStyle,e.fWeight=t.fWeight,e.fc=t.fc,e.j=t.j,e.justifyOffset=t.justifyOffset,e.l=t.l,e.lh=t.lh,e.lineWidths=t.lineWidths,e.ls=t.ls,e.of=t.of,e.s=t.s,e.sc=t.sc,e.sw=t.sw,e.t=t.t,e.tr=t.tr,e.fillColorAnim=t.fillColorAnim||e.fillColorAnim,e.strokeColorAnim=t.strokeColorAnim||e.strokeColorAnim,e.strokeWidthAnim=t.strokeWidthAnim||e.strokeWidthAnim,e.yOffset=t.yOffset,e.__complete=!1},T.prototype.searchProperty=function(){return this.kf=this.data.d.k.length>1,this.kf},T.prototype.getValue=function(){this.mdf=!1;var t=this.elem.globalData.frameId;if(t!==this._frameId&&this.kf||this.firstFrame){for(var e,s=this.data.d.k,i=0,a=s.length;i<=a-1&&(e=s[i].s,!(i===a-1||s[i+1].t>t));)i+=1;this.keysIndex!==i&&(e.__complete||this.completeTextData(e),this.setCurrentData(e),this.mdf=!this.firstFrame,this.pv=this.v=this.currentData.t,this.keysIndex=i),this._frameId=t}},T.prototype.completeTextData=function(t){t.__complete=!0;var e,s,i,a,r,n,h,o=this.elem.globalData.fontManager,l=this.data,p=[],f=0,m=l.m.g,d=0,c=0,u=0,g=[],y=0,v=0,b=o.getFontByName(t.f),k=0,A=b.fStyle.split(" "),P="normal",E="normal";s=A.length;var M;for(e=0;e<s;e+=1)switch(M=A[e].toLowerCase()){case"italic":E="italic";break;case"bold":P="700";break;case"black":P="900";break;case"medium":P="500";break;case"regular":case"normal":P="400";case"light":case"thin":P="200"}t.fWeight=P,t.fStyle=E,s=t.t.length;var _=t.tr/1e3*t.s;if(t.sz){var S=t.sz[0],F=-1;for(e=0;e<s;e+=1)i=!1," "===t.t.charAt(e)?F=e:13===t.t.charCodeAt(e)&&(y=0,i=!0),o.chars?(h=o.getCharData(t.t.charAt(e),b.fStyle,b.fFamily),k=i?0:h.w*t.s/100):k=o.measureText(t.t.charAt(e),t.f,t.s),y+k>S&&" "!==t.t.charAt(e)?(F===-1?s+=1:e=F,t.t=t.t.substr(0,e)+"\r"+t.t.substr(e===F?e+1:e),F=-1,y=0):(y+=k,y+=_);s=t.t.length}y=-_,k=0;var x,w=0;for(e=0;e<s;e+=1)if(i=!1,x=t.t.charAt(e)," "===x?a=" ":13===x.charCodeAt(0)?(w=0,g.push(y),v=y>v?y:v,y=-2*_,a="",i=!0,u+=1):a=t.t.charAt(e),o.chars?(h=o.getCharData(x,b.fStyle,o.getFontByName(t.f).fFamily),k=i?0:h.w*t.s/100):k=o.measureText(a,t.f,t.s)," "===x?w+=k+_:(y+=k+_+w,w=0),p.push({l:k,an:k,add:d,n:i,anIndexes:[],val:a,line:u}),2==m){if(d+=k,""==a||" "==a||e==s-1){for(""!=a&&" "!=a||(d-=k);c<=e;)p[c].an=d,p[c].ind=f,p[c].extra=k,c+=1;f+=1,d=0}}else if(3==m){if(d+=k,""==a||e==s-1){for(""==a&&(d-=k);c<=e;)p[c].an=d,p[c].ind=f,p[c].extra=k,c+=1;d=0,f+=1}}else p[f].ind=f,p[f].extra=0,f+=1;if(t.l=p,v=y>v?y:v,g.push(y),t.sz)t.boxWidth=t.sz[0],t.justifyOffset=0;else switch(t.boxWidth=v,t.j){case 1:t.justifyOffset=-t.boxWidth;break;case 2:t.justifyOffset=-t.boxWidth/2;break;default:t.justifyOffset=0}t.lineWidths=g;var D,C,I=l.a;n=I.length;var T,L,N=[];for(r=0;r<n;r+=1){for(D=I[r],D.a.sc&&(t.strokeColorAnim=!0),D.a.sw&&(t.strokeWidthAnim=!0),(D.a.fc||D.a.fh||D.a.fs||D.a.fb)&&(t.fillColorAnim=!0),L=0,T=D.s.b,e=0;e<s;e+=1)C=p[e],C.anIndexes[r]=L,(1==T&&""!=C.val||2==T&&""!=C.val&&" "!=C.val||3==T&&(C.n||" "==C.val||e==s-1)||4==T&&(C.n||e==s-1))&&(1===D.s.rn&&N.push(L),L+=1);l.a[r].s.totalChars=L;var V,O=-1;if(1===D.s.rn)for(e=0;e<s;e+=1)C=p[e],O!=C.anIndexes[r]&&(O=C.anIndexes[r],V=N.splice(Math.floor(Math.random()*N.length),1)[0]),C.anIndexes[r]=V}t.yOffset=t.lh||1.2*t.s,t.ls=t.ls||0,t.ascent=b.ascent*t.s/100},T.prototype.updateDocumentData=function(t,e){e=void 0===e?this.keysIndex:e;var s=this.data.d.k[e].s;s.__complete=!1,s.t=t.t,this.keysIndex=-1,this.firstFrame=!0,this.getValue()};var Yt=function(){function t(t){return t.concat(Array.apply(null,{length:t.length}))}return{"double":t}}(),Ht=function(){function t(){var t;return i?(i-=1,t=r[i]):t=[.1,.1],t}function e(t){i===a&&(r=Yt["double"](r),a=2*a),r[i]=t,i+=1}var s={newPoint:t,release:e},i=0,a=8,r=Array.apply(null,{length:a});return s}(),Jt=function(){function t(){var t;return r?(r-=1,t=h[r]):t=new _,t}function e(t){r===n&&(h=Yt["double"](h),n=2*n);var e,s=t._length;for(e=0;e<s;e+=1)Ht.release(t.v[e]),Ht.release(t.i[e]),Ht.release(t.o[e]),t.v[e]=null,t.i[e]=null,t.o[e]=null;t._length=0,t.c=!1,h[r]=t,r+=1}function s(t,s){for(;s--;)e(t[s])}function i(e,s){var i,a=void 0===e._length?e.v.length:e._length,r=t();r.setLength(a),r.c=e.c;var n;for(i=0;i<a;i+=1)s?(n=s.applyToPointArray(e.v[i][0],e.v[i][1],0,2),r.setXYAt(n[0],n[1],"v",i),Ht.release(n),n=s.applyToPointArray(e.o[i][0],e.o[i][1],0,2),r.setXYAt(n[0],n[1],"o",i),Ht.release(n),n=s.applyToPointArray(e.i[i][0],e.i[i][1],0,2),r.setXYAt(n[0],n[1],"i",i),Ht.release(n)):r.setTripleAt(e.v[i][0],e.v[i][1],e.o[i][0],e.o[i][1],e.i[i][0],e.i[i][1],i);return r}var a={clone:i,newShape:t,release:e,releaseArray:s},r=0,n=4,h=Array.apply(null,{length:n});return a}(),Ut=function(){function t(){var t;return a?(a-=1,t=n[a]):t=new D,t}function e(t){var e,s=t._length;for(e=0;e<s;e+=1)Jt.release(t.shapes[e]);t._length=0,a===r&&(n=Yt["double"](n),r=2*r),n[a]=t,a+=1}function s(t,s){e(t),a===r&&(n=Yt["double"](n),r=2*r),n[a]=t,a+=1}var i={newShapeCollection:t,release:e,clone:s},a=0,r=4,n=Array.apply(null,{length:r});return i}();L.prototype.checkLayers=function(t){var e,s,i=this.layers.length;for(this.completeLayers=!0,e=i-1;e>=0;e--)this.elements[e]||(s=this.layers[e],s.ip-s.st<=t-this.layers[e].st&&s.op-s.st>t-this.layers[e].st&&this.buildItem(e)),this.completeLayers=!!this.elements[e]&&this.completeLayers;this.checkPendingElements()},L.prototype.createItem=function(t){switch(t.ty){case 2:return this.createImage(t);case 0:return this.createComp(t);case 1:return this.createSolid(t);case 4:return this.createShape(t);case 5:return this.createText(t);case 13:return this.createCamera(t);case 99:return null}return this.createBase(t)},L.prototype.createCamera=function(){throw new Error("You're using a 3d camera. Try the html renderer.")},L.prototype.buildAllItems=function(){var t,e=this.layers.length;for(t=0;t<e;t+=1)this.buildItem(t);this.checkPendingElements()},L.prototype.includeLayers=function(t){this.completeLayers=!1;var e,s,i=t.length,a=this.layers.length;for(e=0;e<i;e+=1)for(s=0;s<a;){if(this.layers[s].id==t[e].id){this.layers[s]=t[e];break}s+=1}},L.prototype.setProjectInterface=function(t){this.globalData.projectInterface=t},L.prototype.initItems=function(){this.globalData.progressiveLoad||this.buildAllItems()},L.prototype.buildElementParenting=function(t,e,s){s=s||[];for(var i=this.elements,a=this.layers,r=0,n=a.length;r<n;)a[r].ind==e&&(i[r]&&i[r]!==!0?void 0!==a[r].parent?(s.push(i[r]),i[r]._isParent=!0,this.buildElementParenting(t,a[r].parent,s)):(s.push(i[r]),i[r]._isParent=!0,t.setHierarchy(s)):(this.buildItem(r),this.addPendingElement(t))),r+=1},L.prototype.addPendingElement=function(t){this.pendingElements.push(t)},P(L,N),N.prototype.createBase=function(t){return new z(t,this.layerElement,this.globalData,this)},N.prototype.createShape=function(t){return new R(t,this.layerElement,this.globalData,this)},N.prototype.createText=function(t){return new B(t,this.layerElement,this.globalData,this)},N.prototype.createImage=function(t){return new K(t,this.layerElement,this.globalData,this)},N.prototype.createComp=function(t){return new Z(t,this.layerElement,this.globalData,this)},N.prototype.createSolid=function(t){return new Q(t,this.layerElement,this.globalData,this)},N.prototype.configAnimation=function(t){this.layerElement=document.createElementNS(Et,"svg"),this.layerElement.setAttribute("xmlns","http://www.w3.org/2000/svg"),this.layerElement.setAttribute("viewBox","0 0 "+t.w+" "+t.h),this.renderConfig.viewBoxOnly||(this.layerElement.setAttribute("width",t.w),this.layerElement.setAttribute("height",t.h),this.layerElement.style.width="100%",this.layerElement.style.height="100%"),this.renderConfig.className&&this.layerElement.setAttribute("class",this.renderConfig.className),this.layerElement.setAttribute("preserveAspectRatio",this.renderConfig.preserveAspectRatio),this.animationItem.wrapper.appendChild(this.layerElement);var e=document.createElementNS(Et,"defs");this.globalData.defs=e,this.layerElement.appendChild(e),this.globalData.getAssetData=this.animationItem.getAssetData.bind(this.animationItem),this.globalData.getAssetsPath=this.animationItem.getAssetsPath.bind(this.animationItem),this.globalData.progressiveLoad=this.renderConfig.progressiveLoad,this.globalData.frameId=0,this.globalData.nm=t.nm,this.globalData.compSize={w:t.w,h:t.h},this.data=t,this.globalData.frameRate=t.fr;var s=document.createElementNS(Et,"clipPath"),i=document.createElementNS(Et,"rect");i.setAttribute("width",t.w),i.setAttribute("height",t.h),i.setAttribute("x",0),i.setAttribute("y",0);var a="animationMask_"+d(10);s.setAttribute("id",a),s.appendChild(i);var r=document.createElementNS(Et,"g");r.setAttribute("clip-path","url("+Mt+"#"+a+")"),this.layerElement.appendChild(r),e.appendChild(s),this.layerElement=r,this.layers=t.layers,this.globalData.fontManager=new Rt,this.globalData.fontManager.addChars(t.chars),this.globalData.fontManager.addFonts(t.fonts,e),this.elements=Array.apply(null,{length:t.layers.length})},N.prototype.destroy=function(){this.animationItem.wrapper.innerHTML="",this.layerElement=null,this.globalData.defs=null;var t,e=this.layers?this.layers.length:0;for(t=0;t<e;t++)this.elements[t]&&this.elements[t].destroy();this.elements.length=0,this.destroyed=!0,this.animationItem=null},N.prototype.updateContainerSize=function(){},N.prototype.buildItem=function(t){var e=this.elements;if(!e[t]&&99!=this.layers[t].ty){e[t]=!0;var s=this.createItem(this.layers[t]);e[t]=s,At&&(0===this.layers[t].ty&&this.globalData.projectInterface.registerComposition(s),s.initExpressions()),this.appendElementInPos(s,t),this.layers[t].tt&&(this.elements[t-1]&&this.elements[t-1]!==!0?s.setMatte(e[t-1].layerId):(this.buildItem(t-1),this.addPendingElement(s)))}},N.prototype.checkPendingElements=function(){for(;this.pendingElements.length;){var t=this.pendingElements.pop();if(t.checkParenting(),t.data.tt)for(var e=0,s=this.elements.length;e<s;){if(this.elements[e]===t){t.setMatte(this.elements[e-1].layerId);break}e+=1}}},N.prototype.renderFrame=function(t){if(this.renderedFrame!=t&&!this.destroyed){null===t?t=this.renderedFrame:this.renderedFrame=t,this.globalData.frameNum=t,this.globalData.frameId+=1,this.globalData.projectInterface.currentFrame=t;var e,s=this.layers.length;for(this.completeLayers||this.checkLayers(t),e=s-1;e>=0;e--)(this.completeLayers||this.elements[e])&&this.elements[e].prepareFrame(t-this.layers[e].st);for(e=s-1;e>=0;e--)(this.completeLayers||this.elements[e])&&this.elements[e].renderFrame()}},N.prototype.appendElementInPos=function(t,e){var s=t.getBaseElement();if(s){for(var i,a=0;a<e;)this.elements[a]&&this.elements[a]!==!0&&this.elements[a].getBaseElement()&&(i=this.elements[a].getBaseElement()),a+=1;i?this.layerElement.insertBefore(s,i):this.layerElement.appendChild(s)}},N.prototype.hide=function(){this.layerElement.style.display="none"},N.prototype.show=function(){this.layerElement.style.display="block"},N.prototype.searchExtraCompositions=function(t){var e,s=t.length,i=document.createElementNS(Et,"g");for(e=0;e<s;e+=1)if(t[e].xt){var a=this.createComp(t[e],i,this.globalData.comp,null);a.initExpressions(),this.globalData.projectInterface.registerComposition(a)}},V.prototype.getMaskProperty=function(t){return this.viewData[t].prop},V.prototype.prepareFrame=function(){var t,e=this.dynamicProperties.length;for(t=0;t<e;t+=1)this.dynamicProperties[t].getValue()},V.prototype.renderFrame=function(t){var e,s=this.masksProperties.length;for(e=0;e<s;e++)if((this.viewData[e].prop.mdf||this.firstFrame)&&this.drawPath(this.masksProperties[e],this.viewData[e].prop.v,this.viewData[e]),(this.viewData[e].op.mdf||this.firstFrame)&&this.viewData[e].elem.setAttribute("fill-opacity",this.viewData[e].op.v),"n"!==this.masksProperties[e].mode&&(this.viewData[e].invRect&&(this.element.finalTransform.mProp.mdf||this.firstFrame)&&(this.viewData[e].invRect.setAttribute("x",-t.props[12]),this.viewData[e].invRect.setAttribute("y",-t.props[13])),this.storedData[e].x&&(this.storedData[e].x.mdf||this.firstFrame))){var i=this.storedData[e].expan;this.storedData[e].x.v<0?("erode"!==this.storedData[e].lastOperator&&(this.storedData[e].lastOperator="erode",this.storedData[e].elem.setAttribute("filter","url("+Mt+"#"+this.storedData[e].filterId+")")),i.setAttribute("radius",-this.storedData[e].x.v)):("dilate"!==this.storedData[e].lastOperator&&(this.storedData[e].lastOperator="dilate",this.storedData[e].elem.setAttribute("filter",null)),this.storedData[e].elem.setAttribute("stroke-width",2*this.storedData[e].x.v))}this.firstFrame=!1},V.prototype.getMaskelement=function(){return this.maskElement},V.prototype.createLayerSolidPath=function(){var t="M0,0 ";return t+=" h"+this.globalData.compSize.w,t+=" v"+this.globalData.compSize.h,t+=" h-"+this.globalData.compSize.w,t+=" v-"+this.globalData.compSize.h+" "},V.prototype.drawPath=function(t,e,s){var i,a,r=" M"+e.v[0][0]+","+e.v[0][1];for(a=e._length,i=1;i<a;i+=1)r+=" C"+Pt(e.o[i-1][0])+","+Pt(e.o[i-1][1])+" "+Pt(e.i[i][0])+","+Pt(e.i[i][1])+" "+Pt(e.v[i][0])+","+Pt(e.v[i][1]);e.c&&a>1&&(r+=" C"+Pt(e.o[i-1][0])+","+Pt(e.o[i-1][1])+" "+Pt(e.i[0][0])+","+Pt(e.i[0][1])+" "+Pt(e.v[0][0])+","+Pt(e.v[0][1])),s.lastPath!==r&&(s.elem&&(e.c?t.inv?s.elem.setAttribute("d",this.solidPath+r):s.elem.setAttribute("d",r):s.elem.setAttribute("d","")),s.lastPath=r)},V.prototype.destroy=function(){this.element=null,this.globalData=null,this.maskElement=null,this.data=null,this.masksProperties=null},O.prototype.checkMasks=function(){if(!this.data.hasMask)return!1;for(var t=0,e=this.data.masksProperties.length;t<e;){if("n"!==this.data.masksProperties[t].mode&&this.data.masksProperties[t].cl!==!1)return!0;t+=1}return!1},O.prototype.checkParenting=function(){void 0!==this.data.parent&&this.comp.buildElementParenting(this,this.data.parent)},O.prototype.prepareFrame=function(t){this.data.ip-this.data.st<=t&&this.data.op-this.data.st>t?this.isVisible!==!0&&(this.elemMdf=!0,this.globalData.mdf=!0,this.isVisible=!0,this.firstFrame=!0,this.data.hasMask&&(this.maskManager.firstFrame=!0)):this.isVisible!==!1&&(this.elemMdf=!0,this.globalData.mdf=!0,this.isVisible=!1);var e,s=this.dynamicProperties.length;for(e=0;e<s;e+=1)(this.isVisible||this._isParent&&"transform"===this.dynamicProperties[e].type)&&(this.dynamicProperties[e].getValue(),this.dynamicProperties[e].mdf&&(this.elemMdf=!0,this.globalData.mdf=!0));
return this.data.hasMask&&this.isVisible&&this.maskManager.prepareFrame(t*this.data.sr),this.currentFrameNum=t*this.data.sr,this.isVisible},O.prototype.globalToLocal=function(t){var e=[];e.push(this.finalTransform);for(var s=!0,i=this.comp;s;)i.finalTransform?(i.data.hasMask&&e.splice(0,0,i.finalTransform),i=i.comp):s=!1;var a,r,n=e.length;for(a=0;a<n;a+=1)r=e[a].mat.applyToPointArray(0,0,0),t=[t[0]-r[0],t[1]-r[1],0];return t},O.prototype.initExpressions=function(){this.layerInterface=LayerExpressionInterface(this),this.data.hasMask&&this.layerInterface.registerMaskInterface(this.maskManager);var t=EffectsExpressionInterface.createEffectsInterface(this,this.layerInterface);this.layerInterface.registerEffectsInterface(t),0===this.data.ty||this.data.xt?this.compInterface=CompExpressionInterface(this):4===this.data.ty?this.layerInterface.shapeInterface=ShapeExpressionInterface.createShapeInterface(this.shapesData,this.itemsData,this.layerInterface):5===this.data.ty&&(this.layerInterface.textInterface=TextExpressionInterface(this))},O.prototype.setBlendMode=function(){var t="";switch(this.data.bm){case 1:t="multiply";break;case 2:t="screen";break;case 3:t="overlay";break;case 4:t="darken";break;case 5:t="lighten";break;case 6:t="color-dodge";break;case 7:t="color-burn";break;case 8:t="hard-light";break;case 9:t="soft-light";break;case 10:t="difference";break;case 11:t="exclusion";break;case 12:t="hue";break;case 13:t="saturation";break;case 14:t="color";break;case 15:t="luminosity"}var e=this.baseElement||this.layerElement;e.style["mix-blend-mode"]=t},O.prototype.init=function(){this.data.sr||(this.data.sr=1),this.dynamicProperties=this.dynamicProperties||[],this.data.ef&&(this.effects=new EffectsManager(this.data,this,this.dynamicProperties)),this.hidden=!1,this.firstFrame=!0,this.isVisible=!1,this._isParent=!1,this.currentFrameNum=-99999,this.lastNum=-99999,this.data.ks&&(this.finalTransform={mProp:jt.getProp(this,this.data.ks,2,null,this.dynamicProperties),matMdf:!1,opMdf:!1,mat:new k,opacity:1},this.data.ao&&(this.finalTransform.mProp.autoOriented=!0),this.finalTransform.op=this.finalTransform.mProp.o,this.transform=this.finalTransform.mProp,11!==this.data.ty&&this.createElements(),this.data.hasMask&&this.addMasks(this.data)),this.elemMdf=!1},O.prototype.getType=function(){return this.type},O.prototype.resetHierarchy=function(){this.hierarchy?this.hierarchy.length=0:this.hierarchy=[]},O.prototype.getHierarchy=function(){return this.hierarchy||(this.hierarchy=[]),this.hierarchy},O.prototype.setHierarchy=function(t){this.hierarchy=t},O.prototype.getLayerSize=function(){return 5===this.data.ty?{w:this.data.textData.width,h:this.data.textData.height}:{w:this.data.width,h:this.data.height}},O.prototype.hide=function(){},O.prototype.sourceRectAtTime=function(){return{top:0,left:0,width:100,height:100}},O.prototype.mHelper=new k,A(O,z),z.prototype.createElements=function(){this.layerElement=document.createElementNS(Et,"g"),this.transformedElement=this.layerElement,this.data.hasMask&&(this.maskedElement=this.layerElement);var t=null;if(this.data.td){if(3==this.data.td||1==this.data.td){var e=document.createElementNS(Et,"mask");if(e.setAttribute("id",this.layerId),e.setAttribute("mask-type",3==this.data.td?"luminance":"alpha"),e.appendChild(this.layerElement),t=e,this.globalData.defs.appendChild(e),!Xt.maskType&&1==this.data.td){e.setAttribute("mask-type","luminance");var s=d(10),i=qt.createFilter(s);this.globalData.defs.appendChild(i),i.appendChild(qt.createAlphaToLuminanceFilter());var a=document.createElementNS(Et,"g");a.appendChild(this.layerElement),t=a,e.appendChild(a),a.setAttribute("filter","url("+Mt+"#"+s+")")}}else if(2==this.data.td){var r=document.createElementNS(Et,"mask");r.setAttribute("id",this.layerId),r.setAttribute("mask-type","alpha");var n=document.createElementNS(Et,"g");r.appendChild(n);var s=d(10),i=qt.createFilter(s),h=document.createElementNS(Et,"feColorMatrix");h.setAttribute("type","matrix"),h.setAttribute("color-interpolation-filters","sRGB"),h.setAttribute("values","1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 -1 1"),i.appendChild(h),this.globalData.defs.appendChild(i);var o=document.createElementNS(Et,"rect");if(o.setAttribute("width",this.comp.data.w),o.setAttribute("height",this.comp.data.h),o.setAttribute("x","0"),o.setAttribute("y","0"),o.setAttribute("fill","#ffffff"),o.setAttribute("opacity","0"),n.setAttribute("filter","url("+Mt+"#"+s+")"),n.appendChild(o),n.appendChild(this.layerElement),t=n,!Xt.maskType){r.setAttribute("mask-type","luminance"),i.appendChild(qt.createAlphaToLuminanceFilter());var a=document.createElementNS(Et,"g");n.appendChild(o),a.appendChild(this.layerElement),t=a,n.appendChild(a)}this.globalData.defs.appendChild(r)}}else(this.data.hasMask||this.data.tt)&&this.data.tt?(this.matteElement=document.createElementNS(Et,"g"),this.matteElement.appendChild(this.layerElement),t=this.matteElement,this.baseElement=this.matteElement):this.baseElement=this.layerElement;if(!this.data.ln&&!this.data.cl||4!==this.data.ty&&0!==this.data.ty||(this.data.ln&&this.layerElement.setAttribute("id",this.data.ln),this.data.cl&&this.layerElement.setAttribute("class",this.data.cl)),0===this.data.ty){var l=document.createElementNS(Et,"clipPath"),p=document.createElementNS(Et,"path");p.setAttribute("d","M0,0 L"+this.data.w+",0 L"+this.data.w+","+this.data.h+" L0,"+this.data.h+"z");var f="cp_"+d(8);if(l.setAttribute("id",f),l.appendChild(p),this.globalData.defs.appendChild(l),this.checkMasks()){var m=document.createElementNS(Et,"g");m.setAttribute("clip-path","url("+Mt+"#"+f+")"),m.appendChild(this.layerElement),this.transformedElement=m,t?t.appendChild(this.transformedElement):this.baseElement=this.transformedElement}else this.layerElement.setAttribute("clip-path","url("+Mt+"#"+f+")")}0!==this.data.bm&&this.setBlendMode(),this.layerElement!==this.parentContainer&&(this.placeholder=null),this.data.ef&&(this.effectsManager=new U(this)),this.checkParenting()},z.prototype.setBlendMode=O.prototype.setBlendMode,z.prototype.renderFrame=function(t){if(3===this.data.ty||this.data.hd||!this.isVisible)return!1;this.lastNum=this.currentFrameNum,this.finalTransform.opMdf=this.firstFrame||this.finalTransform.op.mdf,this.finalTransform.matMdf=this.firstFrame||this.finalTransform.mProp.mdf,this.finalTransform.opacity=this.finalTransform.op.v;var e,s=this.finalTransform.mat;if(this.hierarchy){var i=0,a=this.hierarchy.length;if(!this.finalTransform.matMdf)for(;i<a;){if(this.hierarchy[i].finalTransform.mProp.mdf){this.finalTransform.matMdf=!0;break}i+=1}if(this.finalTransform.matMdf)for(e=this.finalTransform.mProp.v.props,s.cloneFromProps(e),i=0;i<a;i+=1)e=this.hierarchy[i].finalTransform.mProp.v.props,s.transform(e[0],e[1],e[2],e[3],e[4],e[5],e[6],e[7],e[8],e[9],e[10],e[11],e[12],e[13],e[14],e[15])}else this.isVisible&&(s=this.finalTransform.mProp.v);return this.finalTransform.matMdf&&this.layerElement&&this.transformedElement.setAttribute("transform",s.to2dCSS()),this.finalTransform.opMdf&&this.layerElement&&(this.finalTransform.op.v<=0?!this.isTransparent&&this.globalData.renderConfig.hideOnTransparent&&(this.isTransparent=!0,this.hide()):this.hidden&&this.isTransparent&&(this.isTransparent=!1,this.show()),this.transformedElement.setAttribute("opacity",this.finalTransform.op.v)),this.data.hasMask&&this.maskManager.renderFrame(s),this.effectsManager&&this.effectsManager.renderFrame(this.firstFrame),this.isVisible},z.prototype.destroy=function(){this.layerElement=null,this.parentContainer=null,this.matteElement&&(this.matteElement=null),this.maskManager&&this.maskManager.destroy()},z.prototype.getBaseElement=function(){return this.baseElement},z.prototype.addMasks=function(t){this.maskManager=new V(t,this,this.globalData)},z.prototype.setMatte=function(t){this.matteElement&&this.matteElement.setAttribute("mask","url("+Mt+"#"+t+")")},z.prototype.hide=function(){this.hidden||(this.layerElement.style.display="none",this.hidden=!0)},z.prototype.show=function(){this.isVisible&&!this.isTransparent&&(this.hidden=!1,this.layerElement.style.display="block")},A(z,R),R.prototype.identityMatrix=new k,R.prototype.lcEnum={1:"butt",2:"round",3:"square"},R.prototype.ljEnum={1:"miter",2:"round",3:"butt"},R.prototype.searchProcessedElement=function(t){for(var e=this.processedElements.length;e;)if(e-=1,this.processedElements[e].elem===t)return this.processedElements[e].pos;return 0},R.prototype.addProcessedElement=function(t,e){for(var s=this.processedElements.length;s;)if(s-=1,this.processedElements[s].elem===t){this.processedElements[s].pos=e;break}0===s&&this.processedElements.push({elem:t,pos:e})},R.prototype.buildExpressionInterface=function(){},R.prototype.createElements=function(){this._parent.createElements.call(this),this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,this.layerElement,this.dynamicProperties,0,[],!0),this.data.hd&&!this.data.td||a(this.layerElement)},R.prototype.setGradientData=function(t,e,s){var i,a="gr_"+d(10);i=1===e.t?document.createElementNS(Et,"linearGradient"):document.createElementNS(Et,"radialGradient"),i.setAttribute("id",a),i.setAttribute("spreadMethod","pad"),i.setAttribute("gradientUnits","userSpaceOnUse");var r,n,h,o=[];for(h=4*e.g.p,n=0;n<h;n+=4)r=document.createElementNS(Et,"stop"),i.appendChild(r),o.push(r);t.setAttribute("gf"===e.ty?"fill":"stroke","url(#"+a+")"),this.globalData.defs.appendChild(i),s.gf=i,s.cst=o},R.prototype.setGradientOpacity=function(t,e,s){if(t.g.k.k[0].s&&t.g.k.k[0].s.length>4*t.g.p||t.g.k.k.length>4*t.g.p){var i,a,r,n,h=document.createElementNS(Et,"mask"),o=document.createElementNS(Et,"path");h.appendChild(o);var l="op_"+d(10),p="mk_"+d(10);h.setAttribute("id",p),i=1===t.t?document.createElementNS(Et,"linearGradient"):document.createElementNS(Et,"radialGradient"),i.setAttribute("id",l),i.setAttribute("spreadMethod","pad"),i.setAttribute("gradientUnits","userSpaceOnUse"),n=t.g.k.k[0].s?t.g.k.k[0].s.length:t.g.k.k.length;var f=[];for(r=4*t.g.p;r<n;r+=2)a=document.createElementNS(Et,"stop"),a.setAttribute("stop-color","rgb(255,255,255)"),i.appendChild(a),f.push(a);return o.setAttribute("gf"===t.ty?"fill":"stroke","url(#"+l+")"),this.globalData.defs.appendChild(i),this.globalData.defs.appendChild(h),e.of=i,e.ost=f,s.msElem=o,p}},R.prototype.createStyleElement=function(t,e,s){var i={},a={data:t,type:t.ty,d:"",ld:"",lvl:e,mdf:!1,closed:!1},r=document.createElementNS(Et,"path");if(i.o=jt.getProp(this,t.o,0,.01,s),("st"==t.ty||"gs"==t.ty)&&(r.setAttribute("stroke-linecap",this.lcEnum[t.lc]||"round"),r.setAttribute("stroke-linejoin",this.ljEnum[t.lj]||"round"),r.setAttribute("fill-opacity","0"),1==t.lj&&r.setAttribute("stroke-miterlimit",t.ml),i.w=jt.getProp(this,t.w,0,null,s),t.d)){var n=jt.getDashProp(this,t.d,"svg",s);n.k||(r.setAttribute("stroke-dasharray",n.dasharray),r.setAttribute("stroke-dashoffset",n.dashoffset)),i.d=n}if("fl"==t.ty||"st"==t.ty)i.c=jt.getProp(this,t.c,1,255,s);else{i.g=jt.getGradientProp(this,t.g,s),2==t.t&&(i.h=jt.getProp(this,t.h,0,.01,s),i.a=jt.getProp(this,t.a,0,Tt,s)),i.s=jt.getProp(this,t.s,1,null,s),i.e=jt.getProp(this,t.e,1,null,s),this.setGradientData(r,t,i,a);var h=this.setGradientOpacity(t,i,a);h&&r.setAttribute("mask","url(#"+h+")")}return i.elem=r,2===t.r&&r.setAttribute("fill-rule","evenodd"),t.ln&&r.setAttribute("id",t.ln),t.cl&&r.setAttribute("class",t.cl),a.pElem=r,this.stylesList.push(a),i.style=a,i},R.prototype.createGroupElement=function(t){var e={it:[],prevViewData:[]},s=document.createElementNS(Et,"g");return e.gr=s,t.ln&&e.gr.setAttribute("id",t.ln),e},R.prototype.createTransformElement=function(t,e){var s={transform:{op:jt.getProp(this,t.o,0,.01,e),mProps:jt.getProp(this,t,2,null,e)},elements:[]};return s},R.prototype.createShapeElement=function(t,e,s,i){var a={elements:[],caches:[],styles:[],transformers:e,lStr:""},r=4;return"rc"==t.ty?r=5:"el"==t.ty?r=6:"sr"==t.ty&&(r=7),a.sh=Bt.getShapeProp(this,t,r,i),a.lvl=s,this.shapes.push(a.sh),this.addShapeToModifiers(a),a};R.prototype.setElementStyles=function(){var t,e=this.stylesList.length,s=[];for(t=0;t<e;t+=1)this.stylesList[t].closed||s.push(this.stylesList[t]);return s},R.prototype.reloadShapes=function(){this.firstFrame=!0;var t,e=this.itemsData.length;for(t=0;t<e;t+=1)this.prevViewData[t]=this.itemsData[t];this.searchShapes(this.shapesData,this.itemsData,this.prevViewData,this.layerElement,this.dynamicProperties,0,[],!0);var t,e=this.dynamicProperties.length;for(t=0;t<e;t+=1)this.dynamicProperties[t].getValue();this.renderModifiers()},R.prototype.searchShapes=function(t,e,s,i,a,r,n,h){var o,l,p,f,m,d,c=[].concat(n),u=t.length-1,g=[],y=[];for(o=u;o>=0;o-=1){if(d=this.searchProcessedElement(t[o]),d?e[o]=s[d-1]:t[o]._render=h,"fl"==t[o].ty||"st"==t[o].ty||"gf"==t[o].ty||"gs"==t[o].ty)d?e[o].style.closed=!1:e[o]=this.createStyleElement(t[o],r,a),t[o]._render&&i.appendChild(e[o].elem),g.push(e[o].style);else if("gr"==t[o].ty){if(d)for(p=e[o].it.length,l=0;l<p;l+=1)e[o].prevViewData[l]=e[o].it[l];else e[o]=this.createGroupElement(t[o]);this.searchShapes(t[o].it,e[o].it,e[o].prevViewData,e[o].gr,a,r+1,c,h),t[o]._render&&i.appendChild(e[o].gr)}else"tr"==t[o].ty?(d||(e[o]=this.createTransformElement(t[o],a)),f=e[o].transform,c.push(f)):"sh"==t[o].ty||"rc"==t[o].ty||"el"==t[o].ty||"sr"==t[o].ty?(d||(e[o]=this.createShapeElement(t[o],c,r,a)),e[o].elements=this.setElementStyles()):"tm"==t[o].ty||"rd"==t[o].ty||"ms"==t[o].ty?(d?(m=e[o],m.closed=!1):(m=Gt.getModifier(t[o].ty),m.init(this,t[o],a),e[o]=m,this.shapeModifiers.push(m)),y.push(m)):"rp"==t[o].ty&&(d?(m=e[o],m.closed=!0):(m=Gt.getModifier(t[o].ty),e[o]=m,m.init(this,t,o,e,a),this.shapeModifiers.push(m),h=!1),y.push(m));this.addProcessedElement(t[o],o+1)}for(u=g.length,o=0;o<u;o+=1)g[o].closed=!0;for(u=y.length,o=0;o<u;o+=1)y[o].closed=!0},R.prototype.addShapeToModifiers=function(t){var e,s=this.shapeModifiers.length;for(e=0;e<s;e+=1)this.shapeModifiers[e].addShape(t)},R.prototype.renderModifiers=function(){if(this.shapeModifiers.length){var t,e=this.shapes.length;for(t=0;t<e;t+=1)this.shapes[t].reset();for(e=this.shapeModifiers.length,t=e-1;t>=0;t-=1)this.shapeModifiers[t].processShapes(this.firstFrame)}},R.prototype.renderFrame=function(t){var e=this._parent.renderFrame.call(this,t);if(e===!1)return void this.hide();this.hidden&&(this.layerElement.style.display="block",this.hidden=!1),this.renderModifiers();var s,i=this.stylesList.length;for(s=0;s<i;s+=1)this.stylesList[s].d="",this.stylesList[s].mdf=!1;for(this.renderShape(this.shapesData,this.itemsData,null),s=0;s<i;s+=1)"0"===this.stylesList[s].ld&&(this.stylesList[s].ld="1",this.stylesList[s].pElem.style.display="block"),(this.stylesList[s].mdf||this.firstFrame)&&(this.stylesList[s].pElem.setAttribute("d",this.stylesList[s].d),this.stylesList[s].msElem&&this.stylesList[s].msElem.setAttribute("d",this.stylesList[s].d));this.firstFrame&&(this.firstFrame=!1)},R.prototype.hide=function(){if(!this.hidden){this.layerElement.style.display="none";var t,e=this.stylesList.length;for(t=e-1;t>=0;t-=1)"0"!==this.stylesList[t].ld&&(this.stylesList[t].ld="0",this.stylesList[t].pElem.style.display="none",this.stylesList[t].pElem.parentNode&&(this.stylesList[t].parent=this.stylesList[t].pElem.parentNode));this.hidden=!0}},R.prototype.renderShape=function(t,e,s){var i,a,r=t.length-1;for(i=r;i>=0;i-=1)a=t[i].ty,"tr"==a?((this.firstFrame||e[i].transform.op.mdf&&s)&&s.setAttribute("opacity",e[i].transform.op.v),(this.firstFrame||e[i].transform.mProps.mdf&&s)&&s.setAttribute("transform",e[i].transform.mProps.v.to2dCSS())):"sh"==a||"el"==a||"rc"==a||"sr"==a?this.renderPath(t[i],e[i]):"fl"==a?this.renderFill(t[i],e[i]):"gf"==a?this.renderGradient(t[i],e[i]):"gs"==a?(this.renderGradient(t[i],e[i]),this.renderStroke(t[i],e[i])):"st"==a?this.renderStroke(t[i],e[i]):"gr"==a&&this.renderShape(t[i].it,e[i].it,e[i].gr)},R.prototype.buildShapeString=function(t,e,s,i){var a,r="";for(a=1;a<e;a+=1)1===a&&(r+=" M"+i.applyToPointStringified(t.v[0][0],t.v[0][1])),r+=" C"+i.applyToPointStringified(t.o[a-1][0],t.o[a-1][1])+" "+i.applyToPointStringified(t.i[a][0],t.i[a][1])+" "+i.applyToPointStringified(t.v[a][0],t.v[a][1]);return 1===e&&(r+=" M"+i.applyToPointStringified(t.v[0][0],t.v[0][1])),s&&e&&(r+=" C"+i.applyToPointStringified(t.o[a-1][0],t.o[a-1][1])+" "+i.applyToPointStringified(t.i[0][0],t.i[0][1])+" "+i.applyToPointStringified(t.v[0][0],t.v[0][1]),r+="z"),r},R.prototype.renderPath=function(t,e){var s,i,a,r,n,h,o=e.elements.length,l=e.lvl;if(t._render)for(h=0;h<o;h+=1)if(e.elements[h].data._render){r=e.sh.mdf||this.firstFrame,a="M0 0";var p=e.sh.paths;if(i=p._length,e.elements[h].lvl<l){for(var f,m=this.mHelper.reset(),d=l-e.elements[h].lvl,c=e.transformers.length-1;d>0;)r=e.transformers[c].mProps.mdf||r,f=e.transformers[c].mProps.v.props,m.transform(f[0],f[1],f[2],f[3],f[4],f[5],f[6],f[7],f[8],f[9],f[10],f[11],f[12],f[13],f[14],f[15]),d--,c--;if(r){for(s=0;s<i;s+=1)n=p.shapes[s],n&&n._length&&(a+=this.buildShapeString(n,n._length,n.c,m));e.caches[h]=a}else a=e.caches[h]}else if(r){for(s=0;s<i;s+=1)n=p.shapes[s],n&&n._length&&(a+=this.buildShapeString(n,n._length,n.c,this.identityMatrix));e.caches[h]=a}else a=e.caches[h];e.elements[h].d+=a,e.elements[h].mdf=r||e.elements[h].mdf}else e.elements[h].mdf=!0},R.prototype.renderFill=function(t,e){var s=e.style;(e.c.mdf||this.firstFrame)&&s.pElem.setAttribute("fill","rgb("+wt(e.c.v[0])+","+wt(e.c.v[1])+","+wt(e.c.v[2])+")"),(e.o.mdf||this.firstFrame)&&s.pElem.setAttribute("fill-opacity",e.o.v)},R.prototype.renderGradient=function(t,e){var s=e.gf,i=e.of,a=e.s.v,r=e.e.v;if(e.o.mdf||this.firstFrame){var n="gf"===t.ty?"fill-opacity":"stroke-opacity";e.elem.setAttribute(n,e.o.v)}if(e.s.mdf||this.firstFrame){var h=1===t.t?"x1":"cx",o="x1"===h?"y1":"cy";s.setAttribute(h,a[0]),s.setAttribute(o,a[1]),i&&(i.setAttribute(h,a[0]),i.setAttribute(o,a[1]))}var l,p,f,m;if(e.g.cmdf||this.firstFrame){l=e.cst;var d=e.g.c;for(f=l.length,p=0;p<f;p+=1)m=l[p],m.setAttribute("offset",d[4*p]+"%"),m.setAttribute("stop-color","rgb("+d[4*p+1]+","+d[4*p+2]+","+d[4*p+3]+")")}if(i&&(e.g.omdf||this.firstFrame)){l=e.ost;var c=e.g.o;for(f=l.length,p=0;p<f;p+=1)m=l[p],m.setAttribute("offset",c[2*p]+"%"),m.setAttribute("stop-opacity",c[2*p+1])}if(1===t.t)(e.e.mdf||this.firstFrame)&&(s.setAttribute("x2",r[0]),s.setAttribute("y2",r[1]),i&&(i.setAttribute("x2",r[0]),i.setAttribute("y2",r[1])));else{var u;if((e.s.mdf||e.e.mdf||this.firstFrame)&&(u=Math.sqrt(Math.pow(a[0]-r[0],2)+Math.pow(a[1]-r[1],2)),s.setAttribute("r",u),i&&i.setAttribute("r",u)),e.e.mdf||e.h.mdf||e.a.mdf||this.firstFrame){u||(u=Math.sqrt(Math.pow(a[0]-r[0],2)+Math.pow(a[1]-r[1],2)));var g=Math.atan2(r[1]-a[1],r[0]-a[0]),y=e.h.v>=1?.99:e.h.v<=-1?-.99:e.h.v,v=u*y,b=Math.cos(g+e.a.v)*v+a[0],k=Math.sin(g+e.a.v)*v+a[1];s.setAttribute("fx",b),s.setAttribute("fy",k),i&&(i.setAttribute("fx",b),i.setAttribute("fy",k))}}},R.prototype.renderStroke=function(t,e){var s=e.style,i=e.d;i&&i.k&&(i.mdf||this.firstFrame)&&(s.pElem.setAttribute("stroke-dasharray",i.dasharray),s.pElem.setAttribute("stroke-dashoffset",i.dashoffset)),e.c&&(e.c.mdf||this.firstFrame)&&s.pElem.setAttribute("stroke","rgb("+wt(e.c.v[0])+","+wt(e.c.v[1])+","+wt(e.c.v[2])+")"),(e.o.mdf||this.firstFrame)&&s.pElem.setAttribute("stroke-opacity",e.o.v),(e.w.mdf||this.firstFrame)&&(s.pElem.setAttribute("stroke-width",e.w.v),s.msElem&&s.msElem.setAttribute("stroke-width",e.w.v))},R.prototype.destroy=function(){this._parent.destroy.call(this._parent),this.shapeData=null,this.itemsData=null,this.parentContainer=null,this.placeholder=null},j.prototype.init=function(){this.lettersChangedFlag=!0,this.dynamicProperties=this.dynamicProperties||[],this.textAnimator=new C(this.data.t,this.renderType,this),this.textProperty=new T(this,this.data.t,this.dynamicProperties),this._parent.init.call(this),this.textAnimator.searchProperties(this.dynamicProperties)},j.prototype.prepareFrame=function(t){this._parent.prepareFrame.call(this,t),(this.textProperty.mdf||this.textProperty.firstFrame)&&(this.buildNewText(),this.textProperty.firstFrame=!1)},j.prototype.createPathShape=function(t,e){var s,i,a=e.length,r="";for(s=0;s<a;s+=1)i=e[s].ks.k,r+=this.buildShapeString(i,i.i.length,!0,t);return r},j.prototype.updateDocumentData=function(t,e){this.textProperty.updateDocumentData(t,e)},j.prototype.applyTextPropertiesToMatrix=function(t,e,s,i,a){switch(t.ps&&e.translate(t.ps[0],t.ps[1]+t.ascent,0),e.translate(0,-t.ls,0),t.j){case 1:e.translate(t.justifyOffset+(t.boxWidth-t.lineWidths[s]),0,0);break;case 2:e.translate(t.justifyOffset+(t.boxWidth-t.lineWidths[s])/2,0,0)}e.translate(i,a,0)},j.prototype.buildColor=function(t){return"rgb("+Math.round(255*t[0])+","+Math.round(255*t[1])+","+Math.round(255*t[2])+")"},j.prototype.buildShapeString=R.prototype.buildShapeString,j.prototype.emptyProp=new I,j.prototype.destroy=function(){this._parent.destroy.call(this._parent)},A(z,B),P(j,B),B.prototype.createElements=function(){this._parent.createElements.call(this),this.data.ln&&this.layerElement.setAttribute("id",this.data.ln),this.data.cl&&this.layerElement.setAttribute("class",this.data.cl),this.data.singleShape&&!this.globalData.fontManager.chars&&(this.textContainer=document.createElementNS(Et,"text"))},B.prototype.buildNewText=function(){var t,e,s=this.textProperty.currentData;this.renderedLetters=Array.apply(null,{length:s?s.l.length:0}),s.fc?this.layerElement.setAttribute("fill",this.buildColor(s.fc)):this.layerElement.setAttribute("fill","rgba(0,0,0,0)"),s.sc&&(this.layerElement.setAttribute("stroke",this.buildColor(s.sc)),this.layerElement.setAttribute("stroke-width",s.sw)),this.layerElement.setAttribute("font-size",s.s);var i=this.globalData.fontManager.getFontByName(s.f);if(i.fClass)this.layerElement.setAttribute("class",i.fClass);else{this.layerElement.setAttribute("font-family",i.fFamily);var a=s.fWeight,r=s.fStyle;this.layerElement.setAttribute("font-style",r),this.layerElement.setAttribute("font-weight",a)}var n=s.l||[],h=this.globalData.fontManager.chars;if(e=n.length){var o,l,p=this.mHelper,f="",m=this.data.singleShape,d=0,c=0,u=!0,g=s.tr/1e3*s.s;if(m&&!h){var y=this.textContainer,v="";switch(s.j){case 1:v="end";break;case 2:v="middle";break;case 2:v="start"}y.setAttribute("text-anchor",v),y.setAttribute("letter-spacing",g);var b=s.t.split(String.fromCharCode(13));e=b.length;var c=s.ps?s.ps[1]+s.ascent:0;for(t=0;t<e;t+=1)o=this.textSpans[t]||document.createElementNS(Et,"tspan"),o.textContent=b[t],o.setAttribute("x",0),o.setAttribute("y",c),o.style.display="inherit",y.appendChild(o),this.textSpans[t]=o,c+=s.lh;this.layerElement.appendChild(y)}else{var k,A,P=this.textSpans.length;for(t=0;t<e;t+=1)h&&m&&0!==t||(o=P>t?this.textSpans[t]:document.createElementNS(Et,h?"path":"text"),P<=t&&(o.setAttribute("stroke-linecap","butt"),o.setAttribute("stroke-linejoin","round"),o.setAttribute("stroke-miterlimit","4"),this.textSpans[t]=o,this.layerElement.appendChild(o)),o.style.display="inherit"),p.reset(),h?(p.scale(s.s/100,s.s/100),m&&(n[t].n&&(d=-g,c+=s.yOffset,c+=u?1:0,u=!1),this.applyTextPropertiesToMatrix(s,p,n[t].line,d,c),d+=n[t].l||0,d+=g),A=this.globalData.fontManager.getCharData(s.t.charAt(t),i.fStyle,this.globalData.fontManager.getFontByName(s.f).fFamily),k=A&&A.data||{},l=k.shapes?k.shapes[0].it:[],m?f+=this.createPathShape(p,l):o.setAttribute("d",this.createPathShape(p,l))):(o.textContent=n[t].val,o.setAttributeNS("http://www.w3.org/XML/1998/namespace","xml:space","preserve"));m&&o.setAttribute("d",f)}for(;t<this.textSpans.length;)this.textSpans[t].style.display="none",t+=1;this._sizeChanged=!0}},B.prototype.sourceRectAtTime=function(t){if(this.prepareFrame(this.comp.renderedFrame-this.data.st),this.renderLetters(),this._sizeChanged){this._sizeChanged=!1;var e=this.layerElement.getBBox();this.bbox={top:e.y,left:e.x,width:e.width,height:e.height}}return this.bbox},B.prototype.renderLetters=function(){if(!this.data.singleShape&&(this.textAnimator.getMeasures(this.textProperty.currentData,this.lettersChangedFlag),this.lettersChangedFlag||this.textAnimator.lettersChangedFlag)){this._sizeChanged=!0;var t,e,s=this.textAnimator.renderedLetters,i=this.textProperty.currentData.l;e=i.length;var a,r;for(t=0;t<e;t+=1)i[t].n||(a=s[t],r=this.textSpans[t],a.mdf.m&&r.setAttribute("transform",a.m),a.mdf.o&&r.setAttribute("opacity",a.o),a.mdf.sw&&r.setAttribute("stroke-width",a.sw),a.mdf.sc&&r.setAttribute("stroke",a.sc),a.mdf.fc&&r.setAttribute("fill",a.fc))}},B.prototype.renderFrame=function(t){var e=this._parent.renderFrame.call(this,t);return e===!1?void this.hide():(this.hidden&&this.show(),this.firstFrame&&(this.firstFrame=!1),void this.renderLetters())},G.prototype.renderFrame=function(t){if(t||this.filterManager.mdf){var e=this.filterManager.effectElements[0].p.v,s=this.filterManager.effectElements[1].p.v,i=this.filterManager.effectElements[2].p.v/100;this.matrixFilter.setAttribute("values",s[0]-e[0]+" 0 0 0 "+e[0]+" "+(s[1]-e[1])+" 0 0 0 "+e[1]+" "+(s[2]-e[2])+" 0 0 0 "+e[2]+" 0 0 0 "+i+" 0")}},W.prototype.renderFrame=function(t){if(t||this.filterManager.mdf){var e=this.filterManager.effectElements[2].p.v,s=this.filterManager.effectElements[6].p.v;this.matrixFilter.setAttribute("values","0 0 0 0 "+e[0]+" 0 0 0 0 "+e[1]+" 0 0 0 0 "+e[2]+" 0 0 0 "+s+" 0")}},X.prototype.initialize=function(){var t,e,s,i,a=this.elem.layerElement.children||this.elem.layerElement.childNodes;for(1===this.filterManager.effectElements[1].p.v?(i=this.elem.maskManager.masksProperties.length,s=0):(s=this.filterManager.effectElements[0].p.v-1,i=s+1),e=document.createElementNS(Et,"g"),e.setAttribute("fill","none"),e.setAttribute("stroke-linecap","round"),e.setAttribute("stroke-dashoffset",1),s;s<i;s+=1)t=document.createElementNS(Et,"path"),e.appendChild(t),this.paths.push({p:t,m:s});if(3===this.filterManager.effectElements[10].p.v){var r=document.createElementNS(Et,"mask"),n="stms_"+d(10);r.setAttribute("id",n),r.setAttribute("mask-type","alpha"),r.appendChild(e),this.elem.globalData.defs.appendChild(r);var h=document.createElementNS(Et,"g");h.setAttribute("mask","url("+Mt+"#"+n+")"),a[0]&&h.appendChild(a[0]),this.elem.layerElement.appendChild(h),this.masker=r,e.setAttribute("stroke","#fff")}else if(1===this.filterManager.effectElements[10].p.v||2===this.filterManager.effectElements[10].p.v){if(2===this.filterManager.effectElements[10].p.v)for(var a=this.elem.layerElement.children||this.elem.layerElement.childNodes;a.length;)this.elem.layerElement.removeChild(a[0]);this.elem.layerElement.appendChild(e),this.elem.layerElement.removeAttribute("mask"),e.setAttribute("stroke","#fff")}this.initialized=!0,this.pathMasker=e},X.prototype.renderFrame=function(t){this.initialized||this.initialize();var e,s,i,a=this.paths.length;for(e=0;e<a;e+=1)if(s=this.elem.maskManager.viewData[this.paths[e].m],i=this.paths[e].p,(t||this.filterManager.mdf||s.prop.mdf)&&i.setAttribute("d",s.lastPath),t||this.filterManager.effectElements[9].p.mdf||this.filterManager.effectElements[4].p.mdf||this.filterManager.effectElements[7].p.mdf||this.filterManager.effectElements[8].p.mdf||s.prop.mdf){var r;if(0!==this.filterManager.effectElements[7].p.v||100!==this.filterManager.effectElements[8].p.v){var n=Math.min(this.filterManager.effectElements[7].p.v,this.filterManager.effectElements[8].p.v)/100,h=Math.max(this.filterManager.effectElements[7].p.v,this.filterManager.effectElements[8].p.v)/100,o=i.getTotalLength();r="0 0 0 "+o*n+" ";var l,p=o*(h-n),f=1+2*this.filterManager.effectElements[4].p.v*this.filterManager.effectElements[9].p.v/100,m=Math.floor(p/f);for(l=0;l<m;l+=1)r+="1 "+2*this.filterManager.effectElements[4].p.v*this.filterManager.effectElements[9].p.v/100+" ";r+="0 "+10*o+" 0 0"}else r="1 "+2*this.filterManager.effectElements[4].p.v*this.filterManager.effectElements[9].p.v/100;i.setAttribute("stroke-dasharray",r)}if((t||this.filterManager.effectElements[4].p.mdf)&&this.pathMasker.setAttribute("stroke-width",2*this.filterManager.effectElements[4].p.v),(t||this.filterManager.effectElements[6].p.mdf)&&this.pathMasker.setAttribute("opacity",this.filterManager.effectElements[6].p.v),(1===this.filterManager.effectElements[10].p.v||2===this.filterManager.effectElements[10].p.v)&&(t||this.filterManager.effectElements[3].p.mdf)){var d=this.filterManager.effectElements[3].p.v;this.pathMasker.setAttribute("stroke","rgb("+wt(255*d[0])+","+wt(255*d[1])+","+wt(255*d[2])+")")}},q.prototype.renderFrame=function(t){if(t||this.filterManager.mdf){var e=this.filterManager.effectElements[0].p.v,s=this.filterManager.effectElements[1].p.v,i=this.filterManager.effectElements[2].p.v,a=i[0]+" "+s[0]+" "+e[0],r=i[1]+" "+s[1]+" "+e[1],n=i[2]+" "+s[2]+" "+e[2];this.feFuncR.setAttribute("tableValues",a),this.feFuncG.setAttribute("tableValues",r),this.feFuncB.setAttribute("tableValues",n)}},Y.prototype.createFeFunc=function(t,e){var s=document.createElementNS(Et,t);return s.setAttribute("type","table"),e.appendChild(s),s},Y.prototype.getTableValue=function(t,e,s,i,a){for(var r,n,h=0,o=256,l=Math.min(t,e),p=Math.max(t,e),f=Array.call(null,{length:o}),m=0,d=a-i,c=e-t;h<=256;)r=h/256,n=r<=l?c<0?a:i:r>=p?c<0?i:a:i+d*Math.pow((r-t)/c,1/s),f[m++]=n,h+=256/(o-1);return f.join(" ")},Y.prototype.renderFrame=function(t){if(t||this.filterManager.mdf){var e,s=this.filterManager.effectElements;this.feFuncRComposed&&(t||s[2].p.mdf||s[3].p.mdf||s[4].p.mdf||s[5].p.mdf||s[6].p.mdf)&&(e=this.getTableValue(s[2].p.v,s[3].p.v,s[4].p.v,s[5].p.v,s[6].p.v),this.feFuncRComposed.setAttribute("tableValues",e),this.feFuncGComposed.setAttribute("tableValues",e),this.feFuncBComposed.setAttribute("tableValues",e)),this.feFuncR&&(t||s[9].p.mdf||s[10].p.mdf||s[11].p.mdf||s[12].p.mdf||s[13].p.mdf)&&(e=this.getTableValue(s[9].p.v,s[10].p.v,s[11].p.v,s[12].p.v,s[13].p.v),this.feFuncR.setAttribute("tableValues",e)),this.feFuncG&&(t||s[16].p.mdf||s[17].p.mdf||s[18].p.mdf||s[19].p.mdf||s[20].p.mdf)&&(e=this.getTableValue(s[16].p.v,s[17].p.v,s[18].p.v,s[19].p.v,s[20].p.v),this.feFuncG.setAttribute("tableValues",e)),this.feFuncB&&(t||s[23].p.mdf||s[24].p.mdf||s[25].p.mdf||s[26].p.mdf||s[27].p.mdf)&&(e=this.getTableValue(s[23].p.v,s[24].p.v,s[25].p.v,s[26].p.v,s[27].p.v),this.feFuncB.setAttribute("tableValues",e)),this.feFuncA&&(t||s[30].p.mdf||s[31].p.mdf||s[32].p.mdf||s[33].p.mdf||s[34].p.mdf)&&(e=this.getTableValue(s[30].p.v,s[31].p.v,s[32].p.v,s[33].p.v,s[34].p.v),this.feFuncA.setAttribute("tableValues",e))}},H.prototype.renderFrame=function(t){if(t||this.filterManager.mdf){if((t||this.filterManager.effectElements[4].p.mdf)&&this.feGaussianBlur.setAttribute("stdDeviation",this.filterManager.effectElements[4].p.v/4),t||this.filterManager.effectElements[0].p.mdf){var e=this.filterManager.effectElements[0].p.v;this.feFlood.setAttribute("flood-color",Nt(Math.round(255*e[0]),Math.round(255*e[1]),Math.round(255*e[2])))}if((t||this.filterManager.effectElements[1].p.mdf)&&this.feFlood.setAttribute("flood-opacity",this.filterManager.effectElements[1].p.v/255),t||this.filterManager.effectElements[2].p.mdf||this.filterManager.effectElements[3].p.mdf){var s=this.filterManager.effectElements[3].p.v,i=(this.filterManager.effectElements[2].p.v-90)*Tt,a=s*Math.cos(i),r=s*Math.sin(i);this.feOffset.setAttribute("dx",a),this.feOffset.setAttribute("dy",r)}}},J.prototype.setElementAsMask=function(t,e){var s=document.createElementNS(Et,"mask");s.setAttribute("id",e.layerId),s.setAttribute("mask-type","alpha"),s.appendChild(e.layerElement),t.setMatte(e.layerId),e.data.hd=!1;var i=t.globalData.defs;i.appendChild(s)},J.prototype.initialize=function(){for(var t=this.filterManager.effectElements[0].p.v,e=0,s=this.elem.comp.elements.length;e<s;)this.elem.comp.elements[e].data.ind===t&&this.setElementAsMask(this.elem,this.elem.comp.elements[e]),e+=1;this.initialized=!0},J.prototype.renderFrame=function(){this.initialized||this.initialize()},U.prototype.renderFrame=function(t){var e,s=this.filters.length;for(e=0;e<s;e+=1)this.filters[e].renderFrame(t)},A(z,Z),Z.prototype.hide=function(){if(!this.hidden){this._parent.hide.call(this);
var t,e=this.elements.length;for(t=0;t<e;t+=1)this.elements[t]&&this.elements[t].hide()}},Z.prototype.prepareFrame=function(t){if(this._parent.prepareFrame.call(this,t),this.isVisible!==!1||this.data.xt){if(this.tm){var e=this.tm.v;e===this.data.op&&(e=this.data.op-1),this.renderedFrame=e}else this.renderedFrame=t/this.data.sr;var s,i=this.elements.length;for(this.completeLayers||this.checkLayers(this.renderedFrame),s=0;s<i;s+=1)(this.completeLayers||this.elements[s])&&this.elements[s].prepareFrame(this.renderedFrame-this.layers[s].st)}},Z.prototype.renderFrame=function(t){var e,s=this._parent.renderFrame.call(this,t),i=this.layers.length;if(s===!1)return void this.hide();for(this.hidden&&this.show(),e=0;e<i;e+=1)(this.completeLayers||this.elements[e])&&this.elements[e].renderFrame();this.firstFrame&&(this.firstFrame=!1)},Z.prototype.setElements=function(t){this.elements=t},Z.prototype.getElements=function(){return this.elements},Z.prototype.destroy=function(){this._parent.destroy.call(this._parent);var t,e=this.layers.length;for(t=0;t<e;t+=1)this.elements[t]&&this.elements[t].destroy()},Z.prototype.checkLayers=N.prototype.checkLayers,Z.prototype.buildItem=N.prototype.buildItem,Z.prototype.buildAllItems=N.prototype.buildAllItems,Z.prototype.buildElementParenting=N.prototype.buildElementParenting,Z.prototype.createItem=N.prototype.createItem,Z.prototype.createImage=N.prototype.createImage,Z.prototype.createComp=N.prototype.createComp,Z.prototype.createSolid=N.prototype.createSolid,Z.prototype.createShape=N.prototype.createShape,Z.prototype.createText=N.prototype.createText,Z.prototype.createBase=N.prototype.createBase,Z.prototype.appendElementInPos=N.prototype.appendElementInPos,Z.prototype.checkPendingElements=N.prototype.checkPendingElements,Z.prototype.addPendingElement=N.prototype.addPendingElement,A(z,K),K.prototype.createElements=function(){var t=this.globalData.getAssetsPath(this.assetData);this._parent.createElements.call(this),this.innerElem=document.createElementNS(Et,"image"),this.innerElem.setAttribute("width",this.assetData.w+"px"),this.innerElem.setAttribute("height",this.assetData.h+"px"),this.innerElem.setAttribute("preserveAspectRatio","xMidYMid slice"),this.innerElem.setAttributeNS("http://www.w3.org/1999/xlink","href",t),this.maskedElement=this.innerElem,this.layerElement.appendChild(this.innerElem),this.data.ln&&this.layerElement.setAttribute("id",this.data.ln),this.data.cl&&this.layerElement.setAttribute("class",this.data.cl)},K.prototype.renderFrame=function(t){var e=this._parent.renderFrame.call(this,t);return e===!1?void this.hide():(this.hidden&&this.show(),void(this.firstFrame&&(this.firstFrame=!1)))},K.prototype.destroy=function(){this._parent.destroy.call(this._parent),this.innerElem=null},A(z,Q),Q.prototype.createElements=function(){this._parent.createElements.call(this);var t=document.createElementNS(Et,"rect");t.setAttribute("width",this.data.sw),t.setAttribute("height",this.data.sh),t.setAttribute("fill",this.data.sc),this.layerElement.appendChild(t),this.innerElem=t,this.data.ln&&this.layerElement.setAttribute("id",this.data.ln),this.data.cl&&this.layerElement.setAttribute("class",this.data.cl)},Q.prototype.renderFrame=K.prototype.renderFrame,Q.prototype.destroy=K.prototype.destroy;var Zt=function(){function e(t){for(var e=0,s=t.target;e<_;)E[e].animation===s&&(E.splice(e,1),e-=1,_-=1,s.isPaused||a()),e+=1}function s(t,e){if(!t)return null;for(var s=0;s<_;){if(E[s].elem==t&&null!==E[s].elem)return E[s].animation;s+=1}var i=new Kt;return r(i,t),i.setData(t,e),i}function i(){F+=1,A()}function a(){F-=1,0===F&&(S=!0)}function r(t,s){t.addEventListener("destroy",e),t.addEventListener("_active",i),t.addEventListener("_idle",a),E.push({elem:s,animation:t}),_+=1}function n(t){var e=new Kt;return r(e,null),e.setParams(t),e}function h(t,e){var s;for(s=0;s<_;s+=1)E[s].animation.setSpeed(t,e)}function o(t,e){var s;for(s=0;s<_;s+=1)E[s].animation.setDirection(t,e)}function l(t){var e;for(e=0;e<_;e+=1)E[e].animation.play(t)}function p(t,e){M=Date.now();var s;for(s=0;s<_;s+=1)E[s].animation.moveFrame(t,e)}function f(e){var s,i=e-M;for(s=0;s<_;s+=1)E[s].animation.advanceTime(i);M=e,S||t.requestAnimationFrame(f)}function m(e){M=e,t.requestAnimationFrame(f)}function d(t){var e;for(e=0;e<_;e+=1)E[e].animation.pause(t)}function c(t,e,s){var i;for(i=0;i<_;i+=1)E[i].animation.goToAndStop(t,e,s)}function u(t){var e;for(e=0;e<_;e+=1)E[e].animation.stop(t)}function g(t){var e;for(e=0;e<_;e+=1)E[e].animation.togglePause(t)}function y(t){var e;for(e=_-1;e>=0;e-=1)E[e].animation.destroy(t)}function v(t,e,i){var a,r=document.getElementsByClassName("bodymovin"),n=r.length;for(a=0;a<n;a+=1)i&&r[a].setAttribute("data-bm-type",i),s(r[a],t);if(e&&0===n){i||(i="svg");var h=document.getElementsByTagName("body")[0];h.innerHTML="";var o=document.createElement("div");o.style.width="100%",o.style.height="100%",o.setAttribute("data-bm-type",i),h.appendChild(o),s(o,t)}}function b(){var t;for(t=0;t<_;t+=1)E[t].animation.resize()}function k(){t.requestAnimationFrame(m)}function A(){S&&(S=!1,t.requestAnimationFrame(m))}var P={},E=[],M=0,_=0,S=!0,F=0;return setTimeout(k,0),P.registerAnimation=s,P.loadAnimation=n,P.setSpeed=h,P.setDirection=o,P.play=l,P.moveFrame=p,P.pause=d,P.stop=u,P.togglePause=g,P.searchAnimations=v,P.resize=b,P.start=k,P.goToAndStop=c,P.destroy=y,P}(),Kt=function(){this._cbs=[],this.name="",this.path="",this.isLoaded=!1,this.currentFrame=0,this.currentRawFrame=0,this.totalFrames=0,this.frameRate=0,this.frameMult=0,this.playSpeed=1,this.playDirection=1,this.pendingElements=0,this.playCount=0,this.prerenderFramesFlag=!0,this.animationData={},this.layers=[],this.assets=[],this.isPaused=!0,this.autoplay=!1,this.loop=!0,this.renderer=null,this.animationID=d(10),this.scaleMode="fit",this.assetsPath="",this.timeCompleted=0,this.segmentPos=0,this.subframeEnabled=_t,this.segments=[],this.pendingSegment=!1,this._idle=!0,this.projectInterface=e()};Kt.prototype.setParams=function(t){var e=this;t.context&&(this.context=t.context),(t.wrapper||t.container)&&(this.wrapper=t.wrapper||t.container);var s=t.animType?t.animType:t.renderer?t.renderer:"svg";switch(s){case"canvas":this.renderer=new CanvasRenderer(this,t.rendererSettings);break;case"svg":this.renderer=new N(this,t.rendererSettings);break;case"hybrid":case"html":default:this.renderer=new HybridRenderer(this,t.rendererSettings)}if(this.renderer.setProjectInterface(this.projectInterface),this.animType=s,""===t.loop||null===t.loop||(t.loop===!1?this.loop=!1:t.loop===!0?this.loop=!0:this.loop=parseInt(t.loop)),this.autoplay=!("autoplay"in t)||t.autoplay,this.name=t.name?t.name:"",this.prerenderFramesFlag=!("prerender"in t)||t.prerender,this.autoloadSegments=!t.hasOwnProperty("autoloadSegments")||t.autoloadSegments,t.animationData)e.configAnimation(t.animationData);else if(t.path){"json"!=t.path.substr(-4)&&("/"!=t.path.substr(-1,1)&&(t.path+="/"),t.path+="data.json");var i=new XMLHttpRequest;t.path.lastIndexOf("\\")!=-1?this.path=t.path.substr(0,t.path.lastIndexOf("\\")+1):this.path=t.path.substr(0,t.path.lastIndexOf("/")+1),this.assetsPath=t.assetsPath,this.fileName=t.path.substr(t.path.lastIndexOf("/")+1),this.fileName=this.fileName.substr(0,this.fileName.lastIndexOf(".json")),i.open("GET",t.path,!0),i.send(),i.onreadystatechange=function(){if(4==i.readyState)if(200==i.status)e.configAnimation(JSON.parse(i.responseText));else try{var t=JSON.parse(i.responseText);e.configAnimation(t)}catch(s){}}}},Kt.prototype.setData=function(t,e){var s={wrapper:t,animationData:e?"object"==typeof e?e:JSON.parse(e):null},i=t.attributes;s.path=i.getNamedItem("data-animation-path")?i.getNamedItem("data-animation-path").value:i.getNamedItem("data-bm-path")?i.getNamedItem("data-bm-path").value:i.getNamedItem("bm-path")?i.getNamedItem("bm-path").value:"",s.animType=i.getNamedItem("data-anim-type")?i.getNamedItem("data-anim-type").value:i.getNamedItem("data-bm-type")?i.getNamedItem("data-bm-type").value:i.getNamedItem("bm-type")?i.getNamedItem("bm-type").value:i.getNamedItem("data-bm-renderer")?i.getNamedItem("data-bm-renderer").value:i.getNamedItem("bm-renderer")?i.getNamedItem("bm-renderer").value:"canvas";var a=i.getNamedItem("data-anim-loop")?i.getNamedItem("data-anim-loop").value:i.getNamedItem("data-bm-loop")?i.getNamedItem("data-bm-loop").value:i.getNamedItem("bm-loop")?i.getNamedItem("bm-loop").value:"";""===a||("false"===a?s.loop=!1:"true"===a?s.loop=!0:s.loop=parseInt(a));var r=i.getNamedItem("data-anim-autoplay")?i.getNamedItem("data-anim-autoplay").value:i.getNamedItem("data-bm-autoplay")?i.getNamedItem("data-bm-autoplay").value:!i.getNamedItem("bm-autoplay")||i.getNamedItem("bm-autoplay").value;s.autoplay="false"!==r,s.name=i.getNamedItem("data-name")?i.getNamedItem("data-name").value:i.getNamedItem("data-bm-name")?i.getNamedItem("data-bm-name").value:i.getNamedItem("bm-name")?i.getNamedItem("bm-name").value:"";var n=i.getNamedItem("data-anim-prerender")?i.getNamedItem("data-anim-prerender").value:i.getNamedItem("data-bm-prerender")?i.getNamedItem("data-bm-prerender").value:i.getNamedItem("bm-prerender")?i.getNamedItem("bm-prerender").value:"";"false"===n&&(s.prerender=!1),this.setParams(s)},Kt.prototype.includeLayers=function(t){t.op>this.animationData.op&&(this.animationData.op=t.op,this.totalFrames=Math.floor(t.op-this.animationData.ip),this.animationData.tf=this.totalFrames);var e,s,i=this.animationData.layers,a=i.length,r=t.layers,n=r.length;for(s=0;s<n;s+=1)for(e=0;e<a;){if(i[e].id==r[s].id){i[e]=r[s];break}e+=1}if((t.chars||t.fonts)&&(this.renderer.globalData.fontManager.addChars(t.chars),this.renderer.globalData.fontManager.addFonts(t.fonts,this.renderer.globalData.defs)),t.assets)for(a=t.assets.length,e=0;e<a;e+=1)this.animationData.assets.push(t.assets[e]);this.animationData.__complete=!1,zt.completeData(this.animationData,this.renderer.globalData.fontManager),this.renderer.includeLayers(t.layers),At&&At.initExpressions(this),this.renderer.renderFrame(null),this.loadNextSegment()},Kt.prototype.loadNextSegment=function(){var t=this.animationData.segments;if(!t||0===t.length||!this.autoloadSegments)return this.trigger("data_ready"),void(this.timeCompleted=this.animationData.tf);var e=t.shift();this.timeCompleted=e.time*this.frameRate;var s=new XMLHttpRequest,i=this,a=this.path+this.fileName+"_"+this.segmentPos+".json";this.segmentPos+=1,s.open("GET",a,!0),s.send(),s.onreadystatechange=function(){if(4==s.readyState)if(200==s.status)i.includeLayers(JSON.parse(s.responseText));else try{var t=JSON.parse(s.responseText);i.includeLayers(t)}catch(e){}}},Kt.prototype.loadSegments=function(){var t=this.animationData.segments;t||(this.timeCompleted=this.animationData.tf),this.loadNextSegment()},Kt.prototype.configAnimation=function(t){var e=this;this.renderer&&this.renderer.destroyed||(this.animationData=t,this.totalFrames=Math.floor(this.animationData.op-this.animationData.ip),this.animationData.tf=this.totalFrames,this.renderer.configAnimation(t),t.assets||(t.assets=[]),t.comps&&(t.assets=t.assets.concat(t.comps),t.comps=null),this.renderer.searchExtraCompositions(t.assets),this.layers=this.animationData.layers,this.assets=this.animationData.assets,this.frameRate=this.animationData.fr,this.firstFrame=Math.round(this.animationData.ip),this.frameMult=this.animationData.fr/1e3,this.trigger("config_ready"),this.imagePreloader=new Wt,this.imagePreloader.setAssetsPath(this.assetsPath),this.imagePreloader.setPath(this.path),this.imagePreloader.loadAssets(t.assets,function(t){t||e.trigger("loaded_images")}),this.loadSegments(),this.updaFrameModifier(),this.renderer.globalData.fontManager?this.waitForFontsLoaded():(zt.completeData(this.animationData,this.renderer.globalData.fontManager),this.checkLoaded()))},Kt.prototype.waitForFontsLoaded=function(){function t(){this.renderer.globalData.fontManager.loaded?(zt.completeData(this.animationData,this.renderer.globalData.fontManager),this.checkLoaded()):setTimeout(t.bind(this),20)}return function(){t.bind(this)()}}(),Kt.prototype.addPendingElement=function(){this.pendingElements+=1},Kt.prototype.elementLoaded=function(){this.pendingElements--,this.checkLoaded()},Kt.prototype.checkLoaded=function(){0===this.pendingElements&&(At&&At.initExpressions(this),this.renderer.initItems(),setTimeout(function(){this.trigger("DOMLoaded")}.bind(this),0),this.isLoaded=!0,this.gotoFrame(),this.autoplay&&this.play())},Kt.prototype.resize=function(){this.renderer.updateContainerSize()},Kt.prototype.setSubframe=function(t){this.subframeEnabled=!!t},Kt.prototype.gotoFrame=function(){this.subframeEnabled?this.currentFrame=this.currentRawFrame:this.currentFrame=Math.floor(this.currentRawFrame),this.timeCompleted!==this.totalFrames&&this.currentFrame>this.timeCompleted&&(this.currentFrame=this.timeCompleted),this.trigger("enterFrame"),this.renderFrame()},Kt.prototype.renderFrame=function(){this.isLoaded!==!1&&this.renderer.renderFrame(this.currentFrame+this.firstFrame)},Kt.prototype.play=function(t){t&&this.name!=t||this.isPaused===!0&&(this.isPaused=!1,this._idle&&(this._idle=!1,this.trigger("_active")))},Kt.prototype.pause=function(t){t&&this.name!=t||this.isPaused===!1&&(this.isPaused=!0,this.pendingSegment||(this._idle=!0,this.trigger("_idle")))},Kt.prototype.togglePause=function(t){t&&this.name!=t||(this.isPaused===!0?this.play():this.pause())},Kt.prototype.stop=function(t){t&&this.name!=t||(this.pause(),this.currentFrame=this.currentRawFrame=0,this.playCount=0,this.gotoFrame())},Kt.prototype.goToAndStop=function(t,e,s){s&&this.name!=s||(e?this.setCurrentRawFrameValue(t):this.setCurrentRawFrameValue(t*this.frameModifier),this.pause())},Kt.prototype.goToAndPlay=function(t,e,s){this.goToAndStop(t,e,s),this.play()},Kt.prototype.advanceTime=function(t){return this.pendingSegment?(this.pendingSegment=!1,this.adjustSegment(this.segments.shift()),void(this.isPaused&&this.play())):void(this.isPaused!==!0&&this.isLoaded!==!1&&this.setCurrentRawFrameValue(this.currentRawFrame+t*this.frameModifier))},Kt.prototype.updateAnimation=function(t){this.setCurrentRawFrameValue(this.totalFrames*t)},Kt.prototype.moveFrame=function(t,e){e&&this.name!=e||this.setCurrentRawFrameValue(this.currentRawFrame+t)},Kt.prototype.adjustSegment=function(t){this.playCount=0,t[1]<t[0]?(this.frameModifier>0&&(this.playSpeed<0?this.setSpeed(-this.playSpeed):this.setDirection(-1)),this.totalFrames=t[0]-t[1],this.firstFrame=t[1],this.setCurrentRawFrameValue(this.totalFrames-.01)):t[1]>t[0]&&(this.frameModifier<0&&(this.playSpeed<0?this.setSpeed(-this.playSpeed):this.setDirection(1)),this.totalFrames=t[1]-t[0],this.firstFrame=t[0],this.setCurrentRawFrameValue(0)),this.trigger("segmentStart")},Kt.prototype.setSegment=function(t,e){var s=-1;this.isPaused&&(this.currentRawFrame+this.firstFrame<t?s=t:this.currentRawFrame+this.firstFrame>e&&(s=e-t-.01)),this.firstFrame=t,this.totalFrames=e-t,s!==-1&&this.goToAndStop(s,!0)},Kt.prototype.playSegments=function(t,e){if("object"==typeof t[0]){var s,i=t.length;for(s=0;s<i;s+=1)this.segments.push(t[s])}else this.segments.push(t);e&&this.adjustSegment(this.segments.shift()),this.isPaused&&this.play()},Kt.prototype.resetSegments=function(t){this.segments.length=0,this.segments.push([this.animationData.ip*this.frameRate,Math.floor(this.animationData.op-this.animationData.ip+this.animationData.ip*this.frameRate)]),t&&this.adjustSegment(this.segments.shift())},Kt.prototype.checkSegments=function(){this.segments.length&&(this.pendingSegment=!0)},Kt.prototype.remove=function(t){t&&this.name!=t||this.renderer.destroy()},Kt.prototype.destroy=function(t){t&&this.name!=t||this.renderer&&this.renderer.destroyed||(this.renderer.destroy(),this.trigger("destroy"),this._cbs=null,this.onEnterFrame=this.onLoopComplete=this.onComplete=this.onSegmentStart=this.onDestroy=null)},Kt.prototype.setCurrentRawFrameValue=function(t){if(this.currentRawFrame=t,this.currentRawFrame>=this.totalFrames){if(this.checkSegments(),this.loop===!1)return this.currentRawFrame=this.totalFrames-.01,this.gotoFrame(),this.pause(),void this.trigger("complete");if(this.trigger("loopComplete"),this.playCount+=1,this.loop!==!0&&this.playCount==this.loop||this.pendingSegment)return this.currentRawFrame=this.totalFrames-.01,this.gotoFrame(),this.pause(),void this.trigger("complete");this.currentRawFrame=this.currentRawFrame%this.totalFrames}else if(this.currentRawFrame<0)return this.checkSegments(),this.playCount-=1,this.playCount<0&&(this.playCount=0),this.loop===!1||this.pendingSegment?(this.currentRawFrame=0,this.gotoFrame(),this.pause(),void this.trigger("complete")):(this.trigger("loopComplete"),this.currentRawFrame=(this.totalFrames+this.currentRawFrame)%this.totalFrames,void this.gotoFrame());this.gotoFrame()},Kt.prototype.setSpeed=function(t){this.playSpeed=t,this.updaFrameModifier()},Kt.prototype.setDirection=function(t){this.playDirection=t<0?-1:1,this.updaFrameModifier()},Kt.prototype.updaFrameModifier=function(){this.frameModifier=this.frameMult*this.playSpeed*this.playDirection},Kt.prototype.getPath=function(){return this.path},Kt.prototype.getAssetsPath=function(t){var e="";if(this.assetsPath){var s=t.p;s.indexOf("images/")!==-1&&(s=s.split("/")[1]),e=this.assetsPath+s}else e=this.path,e+=t.u?t.u:"",e+=t.p;return e},Kt.prototype.getAssetData=function(t){for(var e=0,s=this.assets.length;e<s;){if(t==this.assets[e].id)return this.assets[e];e+=1}},Kt.prototype.hide=function(){this.renderer.hide()},Kt.prototype.show=function(){this.renderer.show()},Kt.prototype.getAssets=function(){return this.assets},Kt.prototype.trigger=function(t){if(this._cbs&&this._cbs[t])switch(t){case"enterFrame":this.triggerEvent(t,new r(t,this.currentFrame,this.totalFrames,this.frameMult));break;case"loopComplete":this.triggerEvent(t,new h(t,this.loop,this.playCount,this.frameMult));break;case"complete":this.triggerEvent(t,new n(t,this.frameMult));break;case"segmentStart":this.triggerEvent(t,new o(t,this.firstFrame,this.totalFrames));break;case"destroy":this.triggerEvent(t,new l(t,this));break;default:this.triggerEvent(t)}"enterFrame"===t&&this.onEnterFrame&&this.onEnterFrame.call(this,new r(t,this.currentFrame,this.totalFrames,this.frameMult)),"loopComplete"===t&&this.onLoopComplete&&this.onLoopComplete.call(this,new h(t,this.loop,this.playCount,this.frameMult)),"complete"===t&&this.onComplete&&this.onComplete.call(this,new n(t,this.frameMult)),"segmentStart"===t&&this.onSegmentStart&&this.onSegmentStart.call(this,new o(t,this.firstFrame,this.totalFrames)),"destroy"===t&&this.onDestroy&&this.onDestroy.call(this,new l(t,this))},Kt.prototype.addEventListener=p,Kt.prototype.removeEventListener=f,Kt.prototype.triggerEvent=m;var Qt={};Qt.play=tt,Qt.pause=et,Qt.setLocationHref=$,Qt.togglePause=st,Qt.setSpeed=it,Qt.setDirection=at,Qt.stop=rt,Qt.moveFrame=nt,Qt.searchAnimations=ht,Qt.registerAnimation=ot,Qt.loadAnimation=dt,Qt.setSubframeRendering=mt,Qt.resize=lt,Qt.start=pt,Qt.goToAndStop=ft,Qt.destroy=ct,Qt.setQuality=ut,Qt.inBrowser=gt,Qt.installPlugin=yt,Qt.__getFactory=vt,Qt.version="4.13.0";var $t="__[STANDALONE]__",te="__[ANIMATIONDATA]__",ee="";if($t){var se=document.getElementsByTagName("script"),ie=se.length-1,ae=se[ie]||{src:""},re=ae.src.replace(/^[^\?]+\??/,"");ee=kt("renderer")}var ne=setInterval(bt,100);return Qt})}});
webpackJsonp([6], {
    0: function (t, e, n) {
        t.exports = n(302);
    },
    302: function (t, e, n) {
        "use strict";
        n(2), n(303), n(304), n(305), n(307), n(308), n(309);
    },
    303: function (t, e, n) {
        "use strict";
        function i(t) {
            return t && t.__esModule ? t : { default: t };
        }
        function a(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        var o = (function () {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    (i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
                }
            }
            return function (e, n, i) {
                return n && t(e.prototype, n), i && t(e, i), e;
            };
        })(),
            s = n(3),
            r = i(s),
            u = n(65),
            l = i(u),
            c = (function () {
                function t(e, n) {
                    a(this, t);
                    var i = ((this.options = r["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, r["default"])(e))),
                        o = i.find(".js-notification-toggle"),
                        s = i.find(".js-notification-close");
                    o.on("tap", this.toggle.bind(this)), s.on("tap", this.close.bind(this));
                }
                return (
                    o(t, null, [
                        {
                            key: "Defaults",
                            get: function () {
                                return {};
                            },
                        },
                    ]),
                    o(t, [
                        {
                            key: "toggle",
                            value: function (t) {
                                var e = this.$container;
                                e.toggleClass("notification--collapsed").toggleClass("notification--expanded"), t.preventDefault();
                            },
                        },
                        {
                            key: "close",
                            value: function (t) {
                                var e = (0, r["default"])("html"),
                                    n = this.$container;
                                e.removeClass("with-notification"), n.trigger("resize").remove(), t.preventDefault();
                            },
                        },
                    ]),
                    t
                );
            })();
        r["default"].fn.notification = (0, l["default"])(c);
    },
    304: function (t, e, n) {
        "use strict";
        function i(t) {
            return t && t.__esModule ? t : { default: t };
        }
        function a(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        Object.defineProperty(e, "__esModule", { value: !0 });
        var o = (function () {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    (i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
                }
            }
            return function (e, n, i) {
                return n && t(e.prototype, n), i && t(e, i), e;
            };
        })(),
            s = n(3),
            r = i(s),
            u = n(65),
            l = i(u),
            c = 1,
            f = -1,
            d = (function () {
                function t(e, n) {
                    a(this, t);
                    var i = ((this.options = r["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, r["default"])(e))),
                        o = (this.$items = i.find(".js-process-item"));
                    this.$backgrounds = i.find(".js-process-background");
                    (this.active = null), (this.animations = []), (this.animationQueue = []), i.appear({ callback: this.animateIn.bind(this, 0), margin: "-50vh", effects: "" }), o.on("tap", this.handleItemClick.bind(this));
                }
                return (
                    o(t, null, [
                        {
                            key: "Defaults",
                            get: function () {
                                return { index: 0 };
                            },
                        },
                    ]),
                    o(t, [
                        {
                            key: "handleItemClick",
                            value: function (t) {
                                var e = this.$items,
                                    n = e.index((0, r["default"])(t.target).closest(e));
                                this.animateIn(n);
                            },
                        },
                        {
                            key: "animateIn",
                            value: function (t) {
                                if ("undefined" == typeof bodymovin) return setTimeout(this.animateIn.bind(this, t), 60);
                                var e = this.animations[t];
                                if (this.active !== t) {
                                    var n = this.$items,
                                        i = n.eq(t);
                                    var b = this.$backgrounds;
                                    var a = this.$backgrounds.eq(t);
                                    if ((n.removeClass("is-active"), i.addClass("is-active"), a.appear({ effects: "fade" }), b.addClass("invisible"), a.removeClass("is-hidden"), i.find("p").appear({ effects: "text" }), i.find("h2").appear({ effects: "text" }), e || (e = this.createAnimation(t)), null !== this.active)) return this.animateOut(t);


                                    // (this.active = t), i.find("p").appear({ effects: "text" }), a.addClass("is-hidden"), e.setDirection(c), e.play();

                                }
                            },
                        },
                        {
                            key: "animateOut",
                            value: function (t) {
                                var e = this,
                                    n = this.$backgrounds.eq(this.active),
                                    i = (this.$items.eq(this.active), this.animations[this.active]);
                                this.animationQueue.push(function () {
                                    n.addClass("is-hidden"), e.animateIn(t);
                                }),
                                    (this.active = null),
                                    i.setDirection(f),
                                    i.play();
                            },
                        },
                        {
                            key: "createAnimation",
                            value: function (t) {
                                var e = (this.$container, this.$backgrounds.eq(t)),
                                    n = e.data(),
                                    i = (0, r["default"])('<span style="display: inline-block; position: absolute; left: 0; top: 0; width: 100%; height: 100%"></span>').appendTo(e);
                                (n.bmWidth || n.bmHeight) && e.css({ width: n.bmWidth || "100%", height: n.bmHeight || "100%" });
                                var a = bodymovin.loadAnimation({ wrapper: i.get(0), path: n.bmPath || null, loop: n.bmLoop || !1, animType: n.bmRenderer || "svg", autoplay: !1, assetsPath: n.bmAssetsPath || null });
                                return a.addEventListener("complete", this.animationQueueNext.bind(this)), (this.animations[t] = a), a;
                            },
                        },
                        {
                            key: "animationQueueNext",
                            value: function () {
                                var t = this.animationQueue.shift();
                                t && t();
                            },
                        },
                    ]),
                    t
                );
            })();
        (e["default"] = d), (r["default"].fn.process = (0, l["default"])(d));
    },
    305: function (t, e, n) {
        "use strict";
        function i(t) {
            return t && t.__esModule ? t : { default: t };
        }
        function a(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function o(t, e) {
            if (!t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !e || ("object" != typeof e && "function" != typeof e) ? t : e;
        }
        function s(t, e) {
            if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + typeof e);
            (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, enumerable: !1, writable: !0, configurable: !0 } })), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : (t.__proto__ = e));
        }
        Object.defineProperty(e, "__esModule", { value: !0 });
        var r = (function () {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    (i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
                }
            }
            return function (e, n, i) {
                return n && t(e.prototype, n), i && t(e, i), e;
            };
        })(),
            u = n(3),
            l = i(u),
            c = n(65),
            f = i(c);
        n(306);
        var d = n(304),
            h = i(d);
        l["default"].fn.andSelf = l["default"].fn.addBack;
        var p = (function (t) {
            function e(t, n) {
                a(this, e);
                var i = o(this, (e.__proto__ || Object.getPrototypeOf(e)).call(this, t, n)),
                    s = ((i.options = l["default"].extend({}, i.constructor.Defaults, n)), (i.$container = (0, l["default"])(t)));
                i.$slideshow = s.find(".js-slideshow");
                return (i.$items = (0, l["default"])()), i.createCarousel(), s.appear({ callback: i.animateIn.bind(i, 0), margin: "-50vh", effects: "" }), i;
            }
            return (
                s(e, t),
                r(e, null, [
                    {
                        key: "Defaults",
                        get: function () {
                            return {};
                        },
                    },
                ]),
                r(e, [
                    {
                        key: "createCarousel",
                        value: function () {
                            this.$slideshow.owlCarousel({ loop: !1, margin: 0, nav: !1, dots: !0, lazyLoad: !0, responsive: { 0: { items: 1 } } }), this.$slideshow.on("changed.owl.carousel", this.animateItemIn.bind(this));
                        },
                    },
                    {
                        key: "getCarousel",
                        value: function () {
                            return this.$slideshow.data("owl.carousel");
                        },
                    },
                    {
                        key: "animateItemIn",
                        value: function (t) {
                            var e = this.getCarousel().current();
                            this.animateIn(e);
                        },
                    },
                ]),
                e
            );
        })(h["default"]);
        (e["default"] = p), (l["default"].fn.processSlideshow = (0, f["default"])(p));
    },
    307: function (t, e, n) {
        "use strict";
        function i(t) {
            return t && t.__esModule ? t : { default: t };
        }
        function a(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        var o = (function () {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    (i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
                }
            }
            return function (e, n, i) {
                return n && t(e.prototype, n), i && t(e, i), e;
            };
        })(),
            s = n(3),
            r = i(s),
            u = n(65),
            l = i(u),
            c = (function () {
                function t(e, n) {
                    a(this, t);
                    var i = ((this.options = r["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, r["default"])(e)));
                    i.on("tap", this.handleClick.bind(this));
                }
                return (
                    o(t, null, [
                        {
                            key: "Defaults",
                            get: function () {
                                return { index: 0 };
                            },
                        },
                    ]),
                    o(t, [
                        {
                            key: "handleClick",
                            value: function (t) {
                                var e = (0, r["default"])(".js-landing-questionnaire-tabs"),
                                    n = this.options.index;
                                e.accordion("expand", n, !0), t.preventDefault();
                            },
                        },
                    ]),
                    t
                );
            })();
        r["default"].fn.introQuestionnaire = (0, l["default"])(c);
    },
    308: function (t, e, n) {
        "use strict";
        function i(t) {
            return t && t.__esModule ? t : { default: t };
        }
        function a(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        var o = (function () {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    (i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
                }
            }
            return function (e, n, i) {
                return n && t(e.prototype, n), i && t(e, i), e;
            };
        })(),
            s = n(3),
            r = i(s),
            u = n(65),
            l = i(u),
            c = (function () {
                function t(e, n) {
                    a(this, t);
                    (this.options = r["default"].extend({}, this.constructor.Defaults, n)), (this.$container = (0, r["default"])(e));
                    this.createHoverIcon();
                }
                return (
                    o(t, null, [
                        {
                            key: "Defaults",
                            get: function () {
                                return { index: 0 };
                            },
                        },
                    ]),
                    o(t, [
                        {
                            key: "createHoverIcon",
                            value: function () {
                                if ("undefined" == typeof bodymovin) return setTimeout(this.createHoverIcon.bind(this), 60);
                                var t = this.$container,
                                    e = t.data();
                                this.$inner = (0, r["default"])('<span style="display: inline-block; position: absolute; left: 0; top: 0; width: 100%; height: 100%"></span>').appendTo(t);
                                t.css({ width: e.bmWidth || "100%", height: e.bmHeight || "100%" });
                                var n = t.closest("a, div, p");
                                n.hover(this.playOverAnimation.bind(this), this.playOutAnimation.bind(this)), this.createHoverAnimation();
                            },
                        },
                        {
                            key: "createHoverAnimation",
                            value: function () {
                                var t = this.$inner,
                                    e = this.$container,
                                    n = e.data(),
                                    i = bodymovin.loadAnimation({ wrapper: t.get(0), path: n.bmPath || null, loop: n.bmLoop || !1, animType: n.bmRenderer || "svg", autoplay: !1, assetsPath: n.bmAssetsPath || null });
                                (this.segments = n.bmHoverSegments), (this.anim = i);
                            },
                        },
                        {
                            key: "playOverAnimation",
                            value: function () {
                                var t = this.anim;
                                t.isLoaded && t.playSegments(this.segments[0], !0);
                            },
                        },
                        {
                            key: "playOutAnimation",
                            value: function () {
                                var t = this.anim;
                                if (t.isLoaded) {
                                    var e = [t.currentFrame, this.segments[1][1]];
                                    t.playSegments(e, !0);
                                }
                            },
                        },
                    ]),
                    t
                );
            })();
        r["default"].fn.iconhover = (0, l["default"])(c);
    },
    309: function (t, e, n) {
        (function (t) {
            "use strict";
            t(function () {
                var e = t(".js-clone-source"),
                    n = t(".js-clone-target");
                e.each(function (e, i) {
                    var a = t(i),
                        o = n.filter('[data-clone-id="' + a.data("cloneId") + '"]');
                    o.append(a.html());
                });
            });
        }.call(e, n(3)));
    },
});
// Get the container element
var btnContainer = document.getElementById("first");

// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("btn");


$('.btn--switch').on('click', function(){
   
    if($(this).hasClass('active')){
        $(this).removeClass('active');

    } else{
        $(this).addClass('active');
       
    }
})
webpackJsonp([12],{0:function(t,e,n){t.exports=n(318)},318:function(t,e,n){(function(t){"use strict";function e(t){return t&&t.__esModule?t:{"default":t}}n(2);var i=n(182),r=e(i);n(319),n(320),n(321);var a=n(322),o=e(a);t.extend(r["default"].effects,{pie:o["default"]})}).call(e,n(3))},319:function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}function r(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var a=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),o=n(3),s=i(o),u=n(65),l=i(u),f=function(){function t(e,n){r(this,t),this.options=s["default"].extend({},this.constructor.Defaults,n),this.uid=Math.round(Number.MAX_SAFE_INTEGER*Math.random()),this.$container=(0,s["default"])(e),this.$constraint=this.$container.parent(),this.updateConstraints(),s["default"].scroller.on("resize",this.updateConstraints.bind(this)),s["default"].scroller.onpassive("scroll",this.update.bind(this))}return a(t,null,[{key:"Defaults",get:function(){return{margin:0,viewportMargin:0,constrained:!0}}}]),a(t,[{key:"getConstraints",value:function(){var t=this.options.margin,e=this.$container,n=this.$constraint,i=n.outerHeight(),r=s["default"].scroller.getViewOffset(n).top-t,a=e.outerHeight();return{min:r,max:r+i-a-t,height:a,container:i}}},{key:"updateConstraints",value:function(){this.constraints=this.getConstraints(),this.update()}},{key:"update",value:function(){var t=this.options.margin,e=this.options.viewportMargin,n=this.constraints,i=s["default"].scroller.scrollTop()+e,r=this.$container,a=i;this.options.constrained&&(a=Math.min(Math.max(i,n.min),n.max)-n.min+t),r.css("transform","translateY("+a+"px)")}}]),t}();e["default"]=f,s["default"].fn.inview=(0,l["default"])(f)},320:function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}function r(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var a=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),o=n(3),s=i(o),u=function(){function t(e,n){r(this,t);var i=this.options=s["default"].extend({},this.constructor.Defaults,n);this.$element=i.element?(0,s["default"])(i.element):null,this.$fixed=(0,s["default"])(".fixed-position-detection-fix"),this.$container=(0,s["default"])(e),this.constraints=[],this.updateConstraints(),s["default"].scroller.on("resize",this.updateConstraints.bind(this)),s["default"].scroller.onpassive("scroll",this.update.bind(this))}return a(t,null,[{key:"Defaults",get:function(){return{element:null,align:"center",compare:"element",offset:0,onactive:null,oninactive:null,onprogress:null}}}]),a(t,[{key:"getConstraints",value:function(){var t=this.options,e=this.$container,n=this.$element,i=this.constraints,r=window.innerHeight,a="top"===t.align?0:"bottom"===t.align?1:.5,o=t.offset;return o+=n?this.getElementOffset()+n.outerHeight()*a:r*a,s["default"].map(e,function(t,e){var n=(0,s["default"])(t);return{top:s["default"].scroller.getViewOffset(n).top-o,height:n.outerHeight(),active:!!i[e]&&i[e].active,element:n}})}},{key:"getElementOffset",value:function(){return s["default"].scroller.getViewOffset(this.$element)}},{key:"updateConstraints",value:function(){this.constraints=this.getConstraints(),this.update()}},{key:"update",value:function(){for(var t=s["default"].scroller.scrollTop(),e=this.constraints,n=this.options,i=n.compare,r=0,a=e.length;r<a;r++){var o=e[r];("before"===i||t>=o.top)&&("after"===i||t<o.top+o.height)?o.active||(o.active=!0,n.onactive&&n.onactive(o.element)):o.active&&(o.active=!1,n.oninactive&&n.oninactive(o.element)),n.onprogress&&n.onprogress(o,t)}}}]),t}();e["default"]=u,s["default"].fn.incenter=function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};return new u(this,t),this}},321:function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}var r=n(3),a=i(r);a["default"].fn.sidenav=function(){return this.find("a[href]").each(function(t,e){var n=(0,a["default"])(e),i=n.attr("href").match(/(#.*)$/);(0,a["default"])(i[0]).incenter({align:"top",offset:250,compare:0===t?"before":"element",onactive:function(){return n.addClass("is-active")},oninactive:function(){return n.removeClass("is-active")}})}),this}},322:function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}Object.defineProperty(e,"__esModule",{value:!0}),e["default"]=function(t){return{animate:function(){var e=a["default"].Deferred();return t.$container.addClass("pie--play"),setTimeout(e.resolve.bind(e),1500),e.promise()}}};var r=n(3),a=i(r)}});
webpackJsonp([2],{0:function(n,t,c){n.exports=c(258)},258:function(n,t,c){"use strict";c(2)}});


webpackJsonp([0],{0:function(t,e,n){t.exports=n(1)},1:function(t,e,n){"use strict";n(2),n(251),n(252)},251:function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}function r(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var a=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),s=n(3),o=i(s),u=n(65),l=i(u),h=function(){function t(e,n){r(this,t);var i=(this.options=o["default"].extend({},this.constructor.Defaults,n),this.$container=(0,o["default"])(e)),a=(this.$charts=i.find(".js-chart-charts"),this.$navigation=i.find(".js-chart-navigation"));this.index=-1,this.charts=[],i.appear({margin:"-50vh",effects:"none",callback:this.open.bind(this,0)}),a.on("tap",this.handleItemClick.bind(this))}return a(t,null,[{key:"Defaults",get:function(){return{paths:[]}}}]),a(t,[{key:"handleItemClick",value:function(t){var e=this.$navigation,n=e.index((0,o["default"])(t.target).closest(e));this.index!==n&&this.open(n)}},{key:"open",value:function(t){var e=this;return"undefined"==typeof bodymovin?setTimeout(this.open.bind(this,t),60):(this.load(t).then(function(){e.show(t),e.index!==-1&&e.hide(e.index),e.index=t}),void this.$navigation.removeClass("is-active").eq(t).addClass("is-active"))}},{key:"load",value:function(t){var e=o["default"].Deferred();return this.charts[t]?e.resolve():this.createChart(t).then(function(){return e.resolve()}),e.promise()}},{key:"createChart",value:function(t){var e=o["default"].Deferred(),n=this.charts[t]=bodymovin.loadAnimation({wrapper:(0,o["default"])('<div class="chart__charts__item" />').appendTo(this.$charts).get(0),path:this.options.paths[t],loop:!1,animType:"svg",autoplay:!1});return n.addEventListener("DOMLoaded",e.resolve.bind(e)),e.promise()}},{key:"show",value:function(t){var e=this.charts[t],n=(0,o["default"])(e.wrapper);n.removeClass("is-hidden"),e.goToAndPlay(0,!0)}},{key:"hide",value:function(t){var e=this.charts[t],n=(0,o["default"])(e.wrapper);n.transition("fade-out")}}]),t}();e["default"]=h,o["default"].fn.chart=(0,l["default"])(h)},252:function(t,e,n){"use strict";function i(t){return t&&t.__esModule?t:{"default":t}}function r(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}Object.defineProperty(e,"__esModule",{value:!0});var a=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}(),s=n(3),o=i(s),u=n(65),l=i(u),h=n(7),c=i(h),f=n(170),p=i(f),v=n(253),d=i(v);n(254),n(255);var g={easing:"easingCubicOut",duration:o["default"].durationNormal},m=function(){function t(e,n){var i=this;r(this,t);var a=(this.options=o["default"].extend({},this.constructor.Defaults,n),this.$container=(0,o["default"])(e)),s=this.$items=a.find(".js-viz-item"),u=this.$itemlist=a.find(".js-viz-item-list");this.index=-1,this.$backgrounds=a.find(".js-viz-background"),this.$texts=a.find(".js-viz-text"),this.$titles=a.find(".js-viz-title"),this.$input=a.find("input"),this.$accordion=a.find(".js-viz-accordion"),c["default"].hasHoverSupport()?c["default"].isOldIE()?(s.on("mouseenter",this.handleItemHover.bind(this)),u.on("mouseleave",this.handleItemLeave.bind(this))):s.hover(this.handleItemHover.bind(this),this.handleItemLeave.bind(this)):(s.on("tap",this.handleItemClick.bind(this)),p["default"].enter("sm-down",function(){i.unhighlightAll(i.index+1)}),p["default"].enter("md-up",function(){i.highlightAll(1)})),this.$input.on("change",this.handleToggleChange.bind(this))}return a(t,null,[{key:"Defaults",get:function(){return{}}}]),a(t,[{key:"handleItemClick",value:function(t){var e=(0,o["default"])(t.target).closest(this.$items).data("name");this.highlightAll(e)}},{key:"handleItemHover",value:function(t){var e=(0,o["default"])(t.target).closest(this.$items).data("name");this.highlightAll(e)}},{key:"handleItemLeave",value:function(t){var e=(0,o["default"])(t.target).closest(this.$items).data("name");this.unhighlightAll(e)}},{key:"highlightAll",value:function(t){var e=(t-1)%4+1,n=e+4,i=e-1;this.index!==i&&i!==-1&&(this.index!==-1&&this.unhighlightAll(this.index+1),this.highlight(e),this.highlight(n))}},{key:"unhighlightAll",value:function(t){var e=(t-1)%4+1,n=e+4,i=e-1;i!==-1&&(this.unhighlight(e),this.unhighlight(n))}},{key:"highlight",value:function(t){var e=this,n=t-1,i=this.$container,r=this.$backgrounds.eq(n),a=(0,o["default"])(".viz__overlay--"+t).get(0),s=(0,o["default"])(".viz__outer__item--"+t).get(0);if(d["default"].to(a,{attr:{fillOpacity:1}},g).start(),d["default"].to(s,{attr:{fillOpacity:1}},g).start(),this.$titles.eq(n).addClass("is-active"),this.$texts.eq(n).addClass("is-active"),this.$texts.slice(8).removeClass("is-active"),r.length){var u=this.index;this.index=n;var l="#clip-inner-"+t+"c";d["default"].to(l,{attr:{r:240}},g).start(),r.parent().append(r),r.transition("fade-in",{before:function(){r.appear({effects:""})},after:function(){u!==-1&&u!==e.index&&e.unhighlightIntermediate(u)}}),i.removeClass("ui-primary").addClass("ui-dark")}}},{key:"unhighlight",value:function(t){var e=this,n=t-1,i=(this.$container,this.$backgrounds.eq(n)),r=(0,o["default"])(".viz__overlay--"+t).get(0),a=(0,o["default"])(".viz__outer__item--"+t).get(0);if(d["default"].to(r,{attr:{fillOpacity:0}},g).start(),d["default"].to(a,{attr:{fillOpacity:0}},g).start(),this.$titles.eq(n).removeClass("is-active"),this.$texts.eq(n).removeClass("is-active"),this.$texts.slice(8).addClass("is-active"),i.length){var s="#clip-inner-"+t+"c";d["default"].to(s,{attr:{r:220}},g).start(),setTimeout(function(){e.unhighlightFinal(n)},o["default"].durationNormal)}}},{key:"unhighlightIntermediate",value:function(t){this.$backgrounds.eq(t).transition("fade-out")}},{key:"unhighlightFinal",value:function(t){this.index===t&&(this.index=-1,this.$backgrounds.eq(t).transition("fade-out"),this.$container.removeClass("ui-dark").addClass("ui-primary"))}},{key:"handleToggleChange",value:function(){var t=this.$input,e=this.$accordion;e.accordion("expand",t.prop("checked")?0:1)}}]),t}();e["default"]=m,o["default"].fn.vizualization=(0,l["default"])(m)},253:function(t,e,n){var i,r,a;(function(n){!function(n,s){r=[],i=s,a="function"==typeof i?i.apply(e,r):i,!(void 0!==a&&(t.exports=a))}(this,function(){"use strict";for(var t,e="undefined"!=typeof n?n:window,i=e.performance,r=document.body,a=[],s=null,o="length",u="split",l="indexOf",h="replace",c="offsetWidth",f="offsetHeight",p="options",v="valuesStart",g="valuesEnd",m="valuesRepeat",y="element",b="playing",w="duration",x="delay",k="offset",M="repeat",I="repeatDelay",A="yoyo",O="easing",T="chain",C="keepHex",$="style",_="data-tweening",P="getElementsByTagName",F="addEventListener",E=["color","backgroundColor"],S=["top","left","width","height"],z=["translate3d","translateX","translateY","translateZ","rotate","translate","rotateX","rotateY","rotateZ","skewX","skewY","scale"],Y=["opacity"],q=E.concat(Y,S,z),X={},j=0,N=q[o];j<N;j++)t=q[j],E[l](t)!==-1?X[t]="rgba(0,0,0,0)":S[l](t)!==-1?X[t]=0:"translate3d"===t?X[t]=[0,0,0]:"translate"===t?X[t]=[0,0]:"rotate"===t||/X|Y|Z/.test(t)?X[t]=0:"scale"!==t&&"opacity"!==t||(X[t]=1);var L={duration:700,delay:0,offset:0,repeat:0,repeatDelay:0,yoyo:!1,easing:"linear",keepHex:!1},D=function(){for(var t,e=["Moz","moz","Webkit","webkit","O","o","Ms","ms"],n=0,i=e[o];n<i;n++)if(e[n]+"Transform"in r[$]){t=e[n];break}return t},Z=function(t){var e=!(t in r[$]),n=D();return e?n+(t.charAt(0).toUpperCase()+t.slice(1)):t},B=function(t,e){var n;if(n=e?t instanceof Object||"object"==typeof t?t:document.querySelectorAll(t):"object"==typeof t?t:document.querySelector(t),null===n&&"window"!==t)throw new TypeError("Element not found or incorrect selector: "+t);return n},Q=function(t){return 180*t/Math.PI},H=function(t,e){for(var n,i=parseInt(t)||0,r=["px","%","deg","rad","em","rem","vh","vw"],a=0;a<r[o];a++)if("string"==typeof t&&t[l](r[a])!==-1){n=r[a];break}return n=void 0!==n?n:e?"deg":"px",{v:i,u:n}},R=function(t){if(/rgb|rgba/.test(t)){var n=t[h](/\s|\)/,"")[u]("(")[1][u](","),i=n[3]?n[3]:null;return i?{r:parseInt(n[0]),g:parseInt(n[1]),b:parseInt(n[2]),a:parseFloat(i)}:{r:parseInt(n[0]),g:parseInt(n[1]),b:parseInt(n[2])}}if(/^#/.test(t)){var r=V(t);return{r:r.r,g:r.g,b:r.b}}if(/transparent|none|initial|inherit/.test(t))return{r:0,g:0,b:0,a:0};if(!/^#|^rgb/.test(t)){var a=document[P]("head")[0];a[$].color=t;var s=e.getComputedStyle(a,null).color;return s=/rgb/.test(s)?s[h](/[^\d,]/g,"")[u](","):[0,0,0],a[$].color="",{r:parseInt(s[0]),g:parseInt(s[1]),b:parseInt(s[2])}}},G=function(t,e,n){return"#"+((1<<24)+(t<<16)+(e<<8)+n).toString(16).slice(1)},V=function(t){var e=/^#?([a-f\d])([a-f\d])([a-f\d])$/i;t=t[h](e,function(t,e,n,i){return e+e+n+n+i+i});var n=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(t);return n?{r:parseInt(n[1],16),g:parseInt(n[2],16),b:parseInt(n[3],16)}:null},W=function(t){if(t){for(var e=t[$].cssText[h](/\s/g,"")[u](";"),n={},i=0,r=e[o];i<r;i++)if(/transform/i.test(e[i]))for(var a=e[i][u](":")[1][u](")"),s=0,c=a[o]-1;s<c;s++){var f=a[s][u]("("),p=f[0],v=f[1];z[l](p)!==-1&&(n[p]=/translate3d/.test(p)?v[u](","):v)}return n}},J=function(t,n){var i=t[$],r=e.getComputedStyle(t,null)||t.currentStyle,a=Z(n),s=i[n]&&!/auto|initial|none|unset/.test(i[n])?i[n]:r[a];if("transform"!==n&&(a in r||a in i)){if(s){if("filter"===a){var o=parseInt(s[u]("=")[1][h](")",""));return parseFloat(o/100)}return s}return X[n]}},U=function(t){a.push(t)},K=function(t){var e=a[l](t);e!==-1&&a.splice(e,1)},tt=function(){setTimeout(function(){!a[o]&&s&&(at(s),s=null)},64)},et="ontouchstart"in e||navigator&&navigator.msMaxTouchPoints||!1,nt=et?"touchstart":"mousewheel",it="mouseenter",rt=e.requestAnimationFrame||e.webkitRequestAnimationFrame||function(t){return setTimeout(t,16)},at=e.cancelAnimationFrame||e.webkitCancelRequestAnimationFrame||function(t){return clearTimeout(t)},st=Z("transform"),ot=document[P]("HTML")[0],ut=navigator&&/(EDGE|Mac)/i.test(navigator.userAgent)?r:ot,lt=!(!navigator||null===new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})").exec(navigator.userAgent))&&parseFloat(RegExp.$1),ht=8===lt,ct=e.Interpolate={},ft=ct.number=function(t,e,n){return t=+t,e-=t,t+e*n},pt=(ct.unit=function(t,e,n,i){return t=+t,e-=t,t+e*i+n},ct.color=function(t,e,n,i){var r,a={},s=")",o=",",u="rgb(",l="rgba(";for(r in e)a[r]="a"!==r?ft(t[r],e[r],n)>>0||0:t[r]&&e[r]?(100*ft(t[r],e[r],n)>>0)/100:null;return i?G(a.r,a.g,a.b):a.a?l+a.r+o+a.g+o+a.b+o+a.a+s:u+a.r+o+a.g+o+a.b+s}),vt=ct.translate=function(t,e,n,i){var r={};for(var a in e)r[a]=(t[a]===e[a]?e[a]:(1e3*(t[a]+(e[a]-t[a])*i)>>0)/1e3)+n;return r.x||r.y?"translate("+r.x+","+r.y+")":"translate3d("+r.translateX+","+r.translateY+","+r.translateZ+")"},dt=ct.rotate=function(t,e,n,i){var r={};for(var a in e)r[a]="z"===a?"rotate("+(1e3*(t[a]+(e[a]-t[a])*i)>>0)/1e3+n+")":a+"("+(1e3*(t[a]+(e[a]-t[a])*i)>>0)/1e3+n+")";return r.z?r.z:(r.rotateX||"")+(r.rotateY||"")+(r.rotateZ||"")},gt=ct.skew=function(t,e,n,i){var r={};for(var a in e)r[a]=a+"("+(1e3*(t[a]+(e[a]-t[a])*i)>>0)/1e3+n+")";return(r.skewX||"")+(r.skewY||"")},mt=ct.scale=function(t,e,n){return"scale("+(1e3*(t+(e-t)*n)>>0)/1e3+")"},yt={},bt=function(t){for(var e=0;e<a[o];)wt.call(a[e],t)?e++:a.splice(e,1);s=rt(bt)},wt=function(t){if(t=t||i.now(),t<this._startTime&&this[b])return!0;var e=Math.min((t-this._startTime)/this[p][w],1),n=this[p][O](e);for(var r in this[g])yt[r](this[y],r,this[v][r],this[g][r],n,this[p]);if(this[p].update&&this[p].update.call(this),1===e){if(this[p][M]>0)return isFinite(this[p][M])&&this[p][M]--,this[p][A]&&(this.reversed=!this.reversed,Ot.call(this)),this._startTime=this[p][A]&&!this.reversed?t+this[p][I]:t,!0;this[p].complete&&this[p].complete.call(this),$t.call(this);for(var a=0,s=this[p][T][o];a<s;a++)this[p][T][a].start();return Tt.call(this),!1}return!0},xt=function(){var t=this[y],e=this[p];void 0!==e.perspective&&st in this[g]&&(this[v][st].perspective=this[g][st].perspective),void 0===e.transformOrigin||"svgTransform"in this[g]||(t[$][Z("transformOrigin")]=e.transformOrigin),void 0!==e.perspectiveOrigin&&(t[$][Z("perspectiveOrigin")]=e.perspectiveOrigin),void 0!==e.parentPerspective&&(t.parentNode[$][Z("perspective")]=e.parentPerspective+"px"),void 0!==e.parentPerspectiveOrigin&&(t.parentNode[$][Z("perspectiveOrigin")]=e.parentPerspectiveOrigin)},kt={},Mt={},It={boxModel:function(t,e){t in yt||(yt[t]=function(t,e,n,i,r){t[$][e]=(r>.99||r<.01?(10*ft(n,i,r)>>0)/10:ft(n,i,r)>>0)+"px"});var n=H(e),i="height"===t?f:c;return"%"===n.u?n.v*this[y][i]/100:n.v},transform:function(t,e){if(st in yt||(yt[st]=function(t,e,n,i,r,a){t[$][e]=(n.perspective||"")+("translate"in n?vt(n.translate,i.translate,"px",r):"")+("rotate"in n?dt(n.rotate,i.rotate,"deg",r):"")+("skew"in n?gt(n.skew,i.skew,"deg",r):"")+("scale"in n?mt(n.scale,i.scale,r):"")}),/translate/.test(t)){if("translate3d"===t){var n=e[u](","),i=H(n[0]),r=H(n[1],t3d2=H(n[2]));return{translateX:"%"===i.u?i.v*this[y][c]/100:i.v,translateY:"%"===r.u?r.v*this[y][f]/100:r.v,translateZ:"%"===t3d2.u?t3d2.v*(this[y][f]+this[y][c])/200:t3d2.v}}if(/^translate(?:[XYZ])$/.test(t)){var a=H(e),s=/X/.test(t)?this[y][c]/100:/Y/.test(t)?this[y][f]/100:(this[y][c]+this[y][f])/200;return"%"===a.u?a.v*s:a.v}if("translate"===t){var l,h="string"==typeof e?e[u](","):e,p={},v=H(h[0]),d=h[o]?H(h[1]):{v:0,u:"px"};return h instanceof Array?(p.x="%"===v.u?v.v*this[y][c]/100:v.v,p.y="%"===d.u?d.v*this[y][f]/100:d.v):(l=H(h),p.x="%"===l.u?l.v*this[y][c]/100:l.v,p.y=0),p}}else if(/rotate|skew/.test(t)){if(/^rotate(?:[XYZ])$|skew(?:[XY])$/.test(t)){var g=H(e,!0);return"rad"===g.u?Q(g.v):g.v}if("rotate"===t){var m={},b=H(e,!0);return m.z="rad"===b.u?Q(b.v):b.v,m}}else if("scale"===t)return parseFloat(e)},unitless:function(t,e){return!/scroll/.test(t)||t in yt?"opacity"===t&&(t in yt||(ht?yt[t]=function(t,e,n,i,r){var a="alpha(opacity=",s=")";t[$].filter=a+(100*ft(n,i,r)>>0)+s}:yt[t]=function(t,e,n,i,r){t[$].opacity=(100*ft(n,i,r)>>0)/100})):yt[t]=function(t,e,n,i,r){t.scrollTop=ft(n,i,r)>>0},parseFloat(e)},colors:function(t,e){return t in yt||(yt[t]=function(t,e,n,i,r,a){t[$][e]=pt(n,i,r,a[C])}),R(e)}},At=function(t,e){var n="start"===e?this[v]:this[g],i={},r={},a={},s={};for(var o in t)if(z[l](o)!==-1){var u=["X","Y","Z"];if(/^translate(?:[XYZ]|3d)$/.test(o)){for(var h=0;h<3;h++){var c=u[h];/3d/.test(o)?a["translate"+c]=It.transform.call(this,"translate"+c,t[o][h]):a["translate"+c]="translate"+c in t?It.transform.call(this,"translate"+c,t["translate"+c]):0}s.translate=a}else if(/^rotate(?:[XYZ])$|^skew(?:[XY])$/.test(o)){for(var f=/rotate/.test(o)?"rotate":"skew",p="rotate"===f?r:i,d=0;d<3;d++){var m=u[d];void 0!==t[f+m]&&"skewZ"!==o&&(p[f+m]=It.transform.call(this,f+m,t[f+m]))}s[f]=p}else/(rotate|translate|scale)$/.test(o)&&(s[o]=It.transform.call(this,o,t[o]));n[st]=s}else S[l](o)!==-1?n[o]=It.boxModel.call(this,o,t[o]):Y[l](o)!==-1||"scroll"===o?n[o]=It.unitless.call(this,o,t[o]):E[l](o)!==-1?n[o]=It.colors.call(this,o,t[o]):o in It&&(n[o]=It[o].call(this,o,t[o]))},Ot=function(){if(this[p][A])for(var t in this[g]){var e=this[m][t];this[m][t]=this[g][t],this[g][t]=e,this[v][t]=this[m][t]}},Tt=function(){this[M]>0&&(this[p][M]=this[M]),this[p][A]&&this.reversed===!0&&(Ot.call(this),this.reversed=!1),this[b]=!1,tt()},Ct=function(t){var e=r.getAttribute(_);e&&"scroll"===e&&t.preventDefault()},$t=function(){"scroll"in this[g]&&r.getAttribute(_)&&r.removeAttribute(_)},_t=function(){"scroll"in this[g]&&!r.getAttribute(_)&&r.setAttribute(_,"scroll")},Pt=function(t){return"function"==typeof t?t:"string"==typeof t?Et[t]:void 0},Ft=function(){var t={},n=W(this[y]),i=["rotate","skew"],r=["X","Y","Z"];for(var a in this[v])if(z[l](a)!==-1){var s=/(rotate|translate|scale)$/.test(a);if(/translate/.test(a)&&"translate"!==a)t.translate3d=n.translate3d||X[a];else if(s)t[a]=n[a]||X[a];else if(!s&&/rotate|skew/.test(a))for(var o=0;o<2;o++)for(var u=0;u<3;u++){var h=i[o]+r[u];z[l](h)!==-1&&h in this[v]&&(t[h]=n[h]||X[h])}}else if("scroll"!==a)if("opacity"===a&&ht){var c=J(this[y],"filter");t.opacity="number"==typeof c?c:X.opacity}else q[l](a)!==-1?t[a]=J(this[y],a)||d[a]:t[a]=a in kt?kt[a].call(this,a,this[v][a]):0;else t[a]=this[y]===ut?e.pageYOffset||ut.scrollTop:this[y].scrollTop;for(var f in n)z[l](f)===-1||f in this[v]||(t[f]=n[f]||X[f]);if(this[v]={},At.call(this,t,"start"),st in this[g])for(var p in this[v][st])if("perspective"!==p)if("object"==typeof this[v][st][p])for(var m in this[v][st][p])"undefined"==typeof this[g][st][p]&&(this[g][st][p]={}),"number"==typeof this[v][st][p][m]&&"undefined"==typeof this[g][st][p][m]&&(this[g][st][p][m]=this[v][st][p][m]);else"number"==typeof this[v][st][p]&&"undefined"==typeof this[g][st][p]&&(this[g][st][p]=this[v][st][p])},Et=e.Easing={};Et.linear=function(t){return t},Et.easingSinusoidalIn=function(t){return-Math.cos(t*Math.PI/2)+1},Et.easingSinusoidalOut=function(t){return Math.sin(t*Math.PI/2)},Et.easingSinusoidalInOut=function(t){return-.5*(Math.cos(Math.PI*t)-1)},Et.easingQuadraticIn=function(t){return t*t},Et.easingQuadraticOut=function(t){return t*(2-t)},Et.easingQuadraticInOut=function(t){return t<.5?2*t*t:-1+(4-2*t)*t},Et.easingCubicIn=function(t){return t*t*t},Et.easingCubicOut=function(t){return--t*t*t+1},Et.easingCubicInOut=function(t){return t<.5?4*t*t*t:(t-1)*(2*t-2)*(2*t-2)+1},Et.easingQuarticIn=function(t){return t*t*t*t},Et.easingQuarticOut=function(t){return 1- --t*t*t*t},Et.easingQuarticInOut=function(t){return t<.5?8*t*t*t*t:1-8*--t*t*t*t},Et.easingQuinticIn=function(t){return t*t*t*t*t},Et.easingQuinticOut=function(t){return 1+--t*t*t*t*t},Et.easingQuinticInOut=function(t){return t<.5?16*t*t*t*t*t:1+16*--t*t*t*t*t},Et.easingCircularIn=function(t){return-(Math.sqrt(1-t*t)-1)},Et.easingCircularOut=function(t){return Math.sqrt(1-(t-=1)*t)},Et.easingCircularInOut=function(t){return(t*=2)<1?-.5*(Math.sqrt(1-t*t)-1):.5*(Math.sqrt(1-(t-=2)*t)+1)},Et.easingExponentialIn=function(t){return Math.pow(2,10*(t-1))-.001},Et.easingExponentialOut=function(t){return 1-Math.pow(2,-10*t)},Et.easingExponentialInOut=function(t){return(t*=2)<1?.5*Math.pow(2,10*(t-1)):.5*(2-Math.pow(2,-10*(t-1)))},Et.easingBackIn=function(t){var e=1.70158;return t*t*((e+1)*t-e)},Et.easingBackOut=function(t){var e=1.70158;return--t*t*((e+1)*t+e)+1},Et.easingBackInOut=function(t){var e=2.5949095;return(t*=2)<1?.5*(t*t*((e+1)*t-e)):.5*((t-=2)*t*((e+1)*t+e)+2)},Et.easingElasticIn=function(t){var e,n=.1,i=.4;return 0===t?0:1===t?1:(!n||n<1?(n=1,e=i/4):e=i*Math.asin(1/n)/Math.PI*2,-(n*Math.pow(2,10*(t-=1))*Math.sin((t-e)*Math.PI*2/i)))},Et.easingElasticOut=function(t){var e,n=.1,i=.4;return 0===t?0:1===t?1:(!n||n<1?(n=1,e=i/4):e=i*Math.asin(1/n)/Math.PI*2,n*Math.pow(2,-10*t)*Math.sin((t-e)*Math.PI*2/i)+1)},Et.easingElasticInOut=function(t){var e,n=.1,i=.4;return 0===t?0:1===t?1:(!n||n<1?(n=1,e=i/4):e=i*Math.asin(1/n)/Math.PI*2,(t*=2)<1?-.5*(n*Math.pow(2,10*(t-=1))*Math.sin((t-e)*Math.PI*2/i)):n*Math.pow(2,-10*(t-=1))*Math.sin((t-e)*Math.PI*2/i)*.5+1)},Et.easingBounceIn=function(t){return 1-Et.easingBounceOut(1-t)},Et.easingBounceOut=function(t){return t<1/2.75?7.5625*t*t:t<2/2.75?7.5625*(t-=1.5/2.75)*t+.75:t<2.5/2.75?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375},Et.easingBounceInOut=function(t){return t<.5?.5*Et.easingBounceIn(2*t):.5*Et.easingBounceOut(2*t-1)+.5};var St=function(t,e,n,i){this[y]="scroll"in n&&(void 0===t||null===t)?ut:t,this[b]=!1,this.reversed=!1,this.paused=!1,this._startTime=null,this._pauseTime=null,this._startFired=!1,this[p]={};for(var r in i)this[p][r]=i[r];if(this[p].rpr=i.rpr||!1,this[m]={},this[g]={},this[v]={},At.call(this,n,"end"),this[p].rpr?this[v]=e:At.call(this,e,"start"),void 0!==this[p].perspective&&st in this[g]){var a="perspective("+parseInt(this[p].perspective)+"px)";this[g][st].perspective=a}for(var s in this[g])s in Mt&&!this[p].rpr&&Mt[s].call(this);this[p][T]=[],this[p][O]=Pt(i[O])||Et[L[O]]||Et.linear,this[p][M]=i[M]||L[M],this[p][I]=i[I]||L[I],this[p][A]=i[A]||L[A],this[p][w]=i[w]||L[w],this[p][x]=i[x]||L[x],this[M]=this[p][M]},zt=(St.prototype={start:function(t){_t.call(this),this[p].rpr&&Ft.apply(this),xt.apply(this);for(var e in this[g])e in Mt&&this[p].rpr&&Mt[e].call(this),this[m][e]=this[v][e];return a.push(this),this[b]=!0,this.paused=!1,this._startFired=!1,this._startTime=t||i.now(),this._startTime+=this[p][x],this._startFired||(this[p].start&&this[p].start.call(this),this._startFired=!0),!s&&bt(),this},play:function(){return this.paused&&this[b]&&(this.paused=!1,this[p].resume&&this[p].resume.call(this),this._startTime+=i.now()-this._pauseTime,U(this),!s&&bt()),this},resume:function(){return this.play()},pause:function(){return!this.paused&&this[b]&&(K(this),this.paused=!0,this._pauseTime=i.now(),this[p].pause&&this[p].pause.call(this)),this},stop:function(){return!this.paused&&this[b]&&(K(this),this[b]=!1,this.paused=!1,$t.call(this),this[p].stop&&this[p].stop.call(this),this.stopChainedTweens(),Tt.call(this)),this},chain:function(){return this[p][T]=arguments,this},stopChainedTweens:function(){for(var t=0,e=this[p][T][o];t<e;t++)this[p][T][t].stop()}},function(t,e,n){this.tweens=[];for(var i=[],r=0,a=t[o];r<a;r++)i[r]=n||{},n[x]=n[x]||L[x],i[r][x]=r>0?n[x]+(n[k]||L[k]):n[x],this.tweens.push(qt(t[r],e,i[r]))}),Yt=function(t,e,n,i){this.tweens=[];for(var r=[],a=0,s=t[o];a<s;a++)r[a]=i||{},i[x]=i[x]||L[x],r[a][x]=a>0?i[x]+(i[k]||L[k]):i[x],this.tweens.push(Xt(t[a],e,n,r[a]))},qt=(zt.prototype=Yt.prototype={start:function(t){t=t||i.now();for(var e=0,n=this.tweens[o];e<n;e++)this.tweens[e].start(t);return this},stop:function(){for(var t=0,e=this.tweens[o];t<e;t++)this.tweens[t].stop();return this},pause:function(){for(var t=0,e=this.tweens[o];t<e;t++)this.tweens[t].pause();return this},chain:function(){return this.tweens[this.tweens[o]-1][p][T]=arguments,this},play:function(){for(var t=0,e=this.tweens[o];t<e;t++)this.tweens[t].play();return this},resume:function(){return this.play()}},function(t,e,n){return n=n||{},n.rpr=!0,new St(B(t),e,e,n)}),Xt=function(t,e,n,i){return i=i||{},new St(B(t),e,n,i)},jt=function(t,e,n){return new zt(B(t,!0),e,n)},Nt=function(t,e,n,i){return new Yt(B(t,!0),e,n,i)};return document[F](nt,Ct,!1),document[F](it,Ct,!1),{property:Z,getPrefix:D,selector:B,processEasing:Pt,defaultOptions:L,to:qt,fromTo:Xt,allTo:jt,allFromTo:Nt,ticker:bt,tick:s,tweens:a,update:wt,dom:yt,parseProperty:It,prepareStart:kt,crossCheck:Mt,Tween:St,truD:H,truC:R,rth:G,htr:V,getCurrentStyle:J}})}).call(e,function(){return this}())},254:function(t,e,n){var i,r,a;(function(s){!function(s,o){r=[n(253)],i=o,a="function"==typeof i?i.apply(e,r):i,!(void 0!==a&&(t.exports=a))}(this,function(t){"use strict";var e,n="undefined"!=typeof s?s:window,i=t,r=i.dom,a=i.prepareStart,o=i.parseProperty,u=i.truC,l=i.truD,h=(i.crossCheck,n.Interpolate.unit,n.Interpolate.number),c=n.Interpolate.color,f=function(t,e){return t.getAttribute(e)},p=["fill","stroke","stop-color"],v=function(t){return t.replace(/[A-Z]/g,"-$&").toLowerCase()};return a.attr=function(t,e){var n={};for(var i in e){var r=v(i).replace(/_+[a-z]+/,""),a=f(this.element,r);n[r]=p.indexOf(r)!==-1?a||"rgba(0,0,0,0)":a||(/opacity/i.test(i)?1:0)}return n},o.attr=function(t,n){"attr"in r||(r.attr=function(t,e,n,i,a){for(var s in i)r.attributes[s](t,s,n[s],i[s],a)},e=r.attributes={});var i={};for(var a in n){var s=v(a),o=/(%|[a-z]+)$/,d=f(this.element,s.replace(/_+[a-z]+/,""));if(p.indexOf(s)===-1)if(null!==d&&o.test(d)){var g=l(d).u||l(n[a]).u,m=/%/.test(g)?"_percent":"_"+g;s+m in e||(/%/.test(g)?e[s+m]=function(t,e,n,i,r){var a=a||e.replace(m,"");t.setAttribute(a,(1e3*h(n.v,i.v,r)>>0)/1e3+i.u)}:e[s+m]=function(t,e,n,i,r){var a=a||e.replace(m,"");t.setAttribute(a,(1e3*h(n.v,i.v,r)>>0)/1e3+i.u)}),i[s+m]=l(n[a])}else o.test(n[a])&&null!==d&&(null===d||o.test(d))||(s in e||(/opacity/i.test(a)?e[s]=function(t,e,n,i,r){t.setAttribute(e,(1e3*h(n,i,r)>>0)/1e3)}:e[s]=function(t,e,n,i,r){t.setAttribute(e,(1e3*h(n,i,r)>>0)/1e3)}),i[s]=parseFloat(n[a]));else s in e||(e[s]=function(t,e,i,r,a){t.setAttribute(e,c(i,r,a,n.keepHex))}),i[s]=u(n[a])}return i},this})}).call(e,function(){return this}())},255:function(t,e,n){var i,r,a;(function(s){!function(s,o){r=[n(253)],i=o,a="function"==typeof i?i.apply(e,r):i,!(void 0!==a&&(t.exports=a))}(this,function(t){"use strict";var e="undefined"!=typeof s?s:window,n=t,i=n.dom,r=n.parseProperty,a=n.prepareStart,o=n.getCurrentStyle,u=(n.truC,n.truD,n.crossCheck),l=e.Interpolate.number,h=(e.Interpolate.unit,e.Interpolate.color,n.defaultOptions),c=null!==new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})").exec(navigator.userAgent)&&parseFloat(RegExp.$1);if(!(c&&c<9)){var f=/(m[^(h|v|l)]*|[vhl][^(v|h|l|z)]*)/gim,p="http://www.w3.org/2000/svg",v=e.Interpolate.coords=function(t,e,n,i){for(var r=[],a=0;a<n;a++){r[a]=[];for(var s=0;s<2;s++)r[a].push((1e3*(t[a][s]+(e[a][s]-t[a][s])*i)>>0)/1e3)}return r},d=function(t,e,n){for(var i=[],r=[],a=t.getTotalLength(),s=e.getTotalLength(),o=Math.max(a,s),u=o/n,l=0,h=u*n;(l+=n)<h;)i.push([t.getPointAtLength(l).x,t.getPointAtLength(l).y]),r.push([e.getPointAtLength(l).x,e.getPointAtLength(l).y]);return[i,r]},g=function(t,e,n){for(var i,r,a,s,o,u=[],l=n.length,h=0;h<l;h++)i=Math.abs(n[h][0]-e.x),r=Math.abs(n[h][1]-e.y),u.push(Math.sqrt(i*i+r*r));return a=u.indexOf(Math.min.apply(null,u)),o=n[a-1]?a-1:l-1,s=n[a+1]?a+1:0,Math.abs(n[o][0]-e.x)<t&&Math.abs(n[o][1]-e.y)<t?n[o]:Math.abs(n[s][0]-e.x)<t&&Math.abs(n[s][1]-e.y)<t?n[s]:Math.abs(n[a][0]-e.x)<t&&Math.abs(n[a][1]-e.y)<t?n[a]:[e.x,e.y]},m=function(t){for(var e,n,i=t.match(f),r=i.length,a=0,s=0,o=0;o<r;o++)i[o]=i[o],e=i[o][0],n=new RegExp(e+"[^\\d|\\-]*","i"),i[o]=i[o].replace(/(^|[^,])\s*-/g,"$1,-").replace(/(\s+\,|\s|\,)/g,",").replace(n,"").split(","),i[o][0]=parseFloat(i[o][0]),i[o][1]=parseFloat(i[o][1]),0===o?(a+=i[o][0],s+=i[o][1]):(a=i[o-1][0],s=i[o-1][1],/l/i.test(e)?(i[o][0]="l"===e?i[o][0]+a:i[o][0],i[o][1]="l"===e?i[o][1]+s:i[o][1]):/h/i.test(e)?(i[o][0]="h"===e?i[o][0]+a:i[o][0],i[o][1]=s):/v/i.test(e)&&(i[o][0]=a,i[o][1]="v"===e?i[o][0]+s:i[o][0]));return i},y=function(t){return t.split(/z/i).shift()+"z"},b=function(t){var e=document.createElementNS(p,"path"),n="object"==typeof t?t.getAttribute("d"):t;return e.setAttribute("d",n),e},w=function(t){if("glyph"===t.tagName){var e=b(t);return t.parentNode.appendChild(e),e}return t},x=function(t){var e={},n="object"==typeof t?t:/^\.|^\#/.test(t)?document.querySelector(t):null;return n&&/path|glyph/.test(n.tagName)?(e.e=w(n),e.o=n.getAttribute("d")):!n&&/[a-z][^a-z]*/gi.test(t)&&(e.e=b(t.trim()),e.o=t),e},k=function(t,e){var n,i,r,a,s,o,u,l,h,c,f,p=[],v=this.options.morphIndex;if(this._isPolygon)if(t=m(t),e=m(e),t.length!==e.length){a=Math.max(t.length,e.length),a===e.length?(s=t,o=e):(s=e,o=t),l=s.length,u=b("M"+s.join("L")+"z"),h=u.getTotalLength()/a;for(var y=0;y<a;y++)c=u.getPointAtLength(h*y),f=g(h,c,s),p.push([f[0],f[1]]);a===e.length?(i=o,n=p):(n=o,i=p)}else n=t,i=e;else t=b(t),e=b(e),r=d(t,e,this.options.morphPrecision),n=r[0],i=r[1],a=i.length;if(this.options.reverseFirstPath&&n.reverse(),this.options.reverseSecondPath&&i.reverse(),v){var w=i.splice(v,a-v);i=w.concat(i)}return t=e=null,[n,i]};h.morphPrecision=15,r.path=function(t,e){return"path"in i||(i.path=function(t,e,n,i,r){t.setAttribute("d",1===r?i.o:"M"+v(n.d,i.d,i.d.length,r)+"Z")}),x(e)},a.path=function(t){return this.element.getAttribute("d")},u.path=function(){var t,e=y(this.valuesStart.path.o),n=y(this.valuesEnd.path.o);this.options.morphPrecision=this.options&&"morphPrecision"in this.options?parseInt(this.options.morphPrecision):h.morphPrecision,this._isPolygon=!/[CSQTA]/i.test(e)&&!/[CSQTA]/i.test(n),t=k.apply(this,[e,n]),this.valuesStart.path.d=t[0],this.valuesEnd.path.d=t[1]};var M=function(t,e){return parseFloat(t)/100*e},I=function(t){var e=t.getAttribute("width"),n=t.getAttribute("height");return 2*e+2*n},A=function(t){var e=t.getAttribute("points").split(" "),n=0;if(e.length>1){var i=function(t){var e=t.split(",");if(2==e.length&&!isNaN(e[0])&&!isNaN(e[1]))return[parseFloat(e[0]),parseFloat(e[1])]},r=function(t,e){return void 0!=t&&void 0!=e?Math.sqrt(Math.pow(e[0]-t[0],2)+Math.pow(e[1]-t[1],2)):0};if(e.length>2)for(var a=0;a<e.length-1;a++)n+=r(i(e[a]),i(e[a+1]));n+="polygon"===t.tagName?r(i(e[0]),i(e[e.length-1])):0}return n},O=function(t){var e=t.getAttribute("x1"),n=t.getAttribute("x2"),i=t.getAttribute("y1"),r=t.getAttribute("y2");return Math.sqrt(Math.pow(n-e,2)+Math.pow(r-i,2))},T=function(t){var e=t.getAttribute("r");return 2*Math.PI*e},C=function(t){var e=t.getAttribute("rx"),n=t.getAttribute("ry"),i=2*e,r=2*n;return Math.sqrt(.5*(i*i+r*r))*(2*Math.PI)/2},$=function(t){return/rect/.test(t.tagName)?I(t):/circle/.test(t.tagName)?T(t):/ellipse/.test(t.tagName)?C(t):/polygon|polyline/.test(t.tagName)?A(t):/line/.test(t.tagName)?O(t):void 0},_=function(t,e){var n,i,r,a,s=/path|glyph/.test(t.tagName)?t.getTotalLength():$(t);return e instanceof Object?e:("string"==typeof e?(e=e.split(/\,|\s/),n=/%/.test(e[0])?M(e[0].trim(),s):parseFloat(e[0]),i=/%/.test(e[1])?M(e[1].trim(),s):parseFloat(e[1])):"undefined"==typeof e&&(a=parseFloat(o(t,"stroke-dashoffset")),r=o(t,"stroke-dasharray").split(/\,/),n=0-a,i=parseFloat(r[0])+n||s),{s:n,e:i,l:s})};r.draw=function(t,e){return"draw"in i||(i.draw=function(t,e,n,i,r){var a=(100*n.l>>0)/100,s=(100*l(n.s,i.s,r)>>0)/100,o=(100*l(n.e,i.e,r)>>0)/100,u=0-s,h=o+u;t.style.strokeDashoffset=u+"px",t.style.strokeDasharray=(100*(h<1?0:h)>>0)/100+"px, "+a+"px"}),_(this.element,e)},a.draw=function(){return _(this.element)};var P=function(t,e){return/[a-zA-Z]/.test(t)&&!/px/.test(t)?t.replace(/top|left/,0).replace(/right|bottom/,100).replace(/center|middle/,50):/%/.test(t)?e.x+parseFloat(t)*e.width/100:parseFloat(t)},F=function(t){var e=t&&/\)/.test(t)?t.substring(0,t.length-1).split(/\)\s|\)/):"none",n={};if(e instanceof Array)for(var i=0,r=e.length;i<r;i++){var a=e[i].trim().split("(");n[a[0]]=a[1]}return n},E=function(t){var e,n={},i=this.element.getBBox(),r=i.x+i.width/2,a=i.y+i.height/2,s=this.options.transformOrigin;s=s?s instanceof Array?s:s.split(/\s/):[r,a],s[0]="number"==typeof s[0]?s[0]:P(s[0],i),s[1]="number"==typeof s[1]?s[1]:P(s[1],i),n.origin=s;for(var o in t)"rotate"===o?n[o]="number"==typeof t[o]?t[o]:t[o]instanceof Array?t[o][0]:1*t[o].split(/\s/)[0]:"translate"===o?(e=t[o]instanceof Array?t[o]:/\,|\s/.test(t[o])?t[o].split(","):[t[o],0],n[o]=[1*e[0]||0,1*e[1]||0]):/skew/.test(o)?n[o]=1*t[o]||0:"scale"===o&&(n[o]=parseFloat(t[o])||1);return n};return r.svgTransform=function(t,e){return"svgTransform"in i||(i.svgTransform=function(t,e,n,i,r){var a,s=0,o=0,u=Math.PI/180,h="scale"in i?l(n.scale,i.scale,r):1,c="rotate"in i?l(n.rotate,i.rotate,r):0,f=Math.sin(c*u),p=Math.cos(c*u),v="skewX"in i?l(n.skewX,i.skewX,r):0,d="skewY"in i?l(n.skewY,i.skewY,r):0,g=c||v||d||1!==h||0;s-=g?i.origin[0]:0,o-=g?i.origin[1]:0,s*=h,o*=h,o+=d?s*Math.tan(d*u):0,s+=v?o*Math.tan(v*u):0,a=p*s-f*o,o=c?f*s+p*o:o,s=c?a:s,s+="translate"in i?l(n.translate[0],i.translate[0],r):0,o+="translate"in i?l(n.translate[1],i.translate[1],r):0,s+=g?i.origin[0]:0,o+=g?i.origin[1]:0,t.setAttribute("transform",(s||o?"translate("+(1e3*s>>0)/1e3+(o?","+(1e3*o>>0)/1e3:"")+")":"")+(c?"rotate("+(1e3*c>>0)/1e3+")":"")+(v?"skewX("+(1e3*v>>0)/1e3+")":"")+(d?"skewY("+(1e3*d>>0)/1e3+")":"")+(1!==h?"scale("+(1e3*h>>0)/1e3+")":""))}),E.call(this,e)},a.svgTransform=function(t,e){var n={},i=F(this.element.getAttribute("transform"));for(var r in e)n[r]=r in i?i[r]:"scale"===r?1:0;return n},u.svgTransform=function(){if(this.options.rpr){var t=this.valuesStart.svgTransform,e=this.valuesEnd.svgTransform,n=E.call(this,F(this.element.getAttribute("transform")));for(var i in n)t[i]=n[i];var r=this.element.ownerSVGElement,a=r.createSVGTransformFromMatrix(r.createSVGMatrix().translate(-t.origin[0],-t.origin[1]).translate("translate"in t?t.translate[0]:0,"translate"in t?t.translate[1]:0).rotate(t.rotate||0).skewX(t.skewX||0).skewY(t.skewY||0).scale(t.scale||1).translate(+t.origin[0],+t.origin[1]));
t.translate=[a.matrix.e,a.matrix.f];for(var i in t)i in e||(e[i]=t[i])}},this}})}).call(e,function(){return this}())}});